#include "tk_stepfacecolorrandom.h"

#include <time.h>

// Utilise le depot
#include "ct_global/ct_context.h"

#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_meshmodel.h"

#include "ct_itemdrawable/ct_faceattributescolor.h"
#include "ct_colorcloud/ct_colorcloudstdvector.h"

// Alias for indexing in models
#define DEF_resultIn_inputResult "ir"
#define DEF_groupIn_inputMesh "ig"
#define DEF_itemIn_mesh "im"

// Alias for indexing out models
#define DEF_itemOut_color "ci"
#define DEF_groupOut_color "cg"
#define DEF_resultOut_color "cr"

TK_StepFaceColorRandom::TK_StepFaceColorRandom(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString TK_StepFaceColorRandom::getStepDescription() const
{
    return tr("Coloriser les faces aléatoirement");
}

// Step copy method
CT_VirtualAbstractStep* TK_StepFaceColorRandom::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new TK_StepFaceColorRandom(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void TK_StepFaceColorRandom::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_resultIn_inputResult, tr("Result"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_groupIn_inputMesh, CT_AbstractItemGroup::staticGetType(), tr("Group"), "",  CT_InAbstractGroupModel::CG_ChooseOneIfMultiple);
    resultModel->addItemModel(DEF_groupIn_inputMesh, DEF_itemIn_mesh, CT_MeshModel::staticGetType(), tr("Mesh model"));
}

// Creation and affiliation of OUT models
void TK_StepFaceColorRandom::createOutResultModelListProtected()
{
    // ****************************************************
    // On sort un resultats correspondant a l'attribut de hauteur que l'on va pouvoir colorier :
    //  - un groupe d'item (groupe de hauteur)
    //      - un item (attribut hauteurs)
    // ****************************************************


    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_resultIn_inputResult);

    if(resultModel != NULL)
        resultModel->addItemModel(DEF_groupIn_inputMesh, m_itemOutColorModelName,
                                  new CT_FaceAttributesColor(),
                                  tr("Random Color"));
}

// Semi-automatic creation of step parameters DialogBox
void TK_StepFaceColorRandom::createPostConfigurationDialog()
{
    // No dialog box
}

void TK_StepFaceColorRandom::compute()
{
    CT_ResultGroup* resultOut_color = getOutResultList().first();

    srand( time(NULL) );

    CT_ResultGroupIterator it(resultOut_color, this, DEF_groupIn_inputMesh);

    while(it.hasNext())
    {
        CT_AbstractItemGroup* groupOut_color = (CT_AbstractItemGroup*)it.next();

        const CT_MeshModel* itemIn_mesh = (const CT_MeshModel*)groupOut_color->firstItemByINModelName(this, DEF_itemIn_mesh);

        if(itemIn_mesh != NULL)
        {
            /******************************************************************************
            *      Compute Core
            ******************************************************************************/
            // On recupere le nuage de face du mesh en entree et son nombre de faces
            if(itemIn_mesh->mesh()->abstractFace() != NULL)
            {
                size_t nbFaces = itemIn_mesh->mesh()->abstractFace()->size();

                // On declare un nuage de couleur que l'on va remplir aleatoirement
                CT_ColorCloudStdVector *colorCloud = new CT_ColorCloudStdVector(false);
                CT_Color currentColor;

                for ( size_t i = 0 ; i < nbFaces && !isStopped() ; i++ )
                {
                    currentColor.r() = rand() % 256;
                    currentColor.g() = rand() % 256;
                    currentColor.b() = rand() % 256;
                    currentColor.a() = 255;

                    colorCloud->addColor( currentColor );

                    setProgress( 100.0*i /nbFaces );
                    waitForAckIfInDebugMode();
                }

                // On cree les attributs que l'on met dans un groupe
                CT_FaceAttributesColor* colorAttribute = new CT_FaceAttributesColor(m_itemOutColorModelName.completeName(), resultOut_color, itemIn_mesh->mesh()->registeredFace(), colorCloud);
                groupOut_color->addItemDrawable( colorAttribute );
            }
        }
    }
}
