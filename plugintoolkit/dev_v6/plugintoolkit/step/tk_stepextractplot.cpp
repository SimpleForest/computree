#include "tk_stepextractplot.h"

#include "ct_view/ct_buttongroup.h"
#include "ct_log/ct_logmanager.h"

#include <math.h>
#include <iostream>
#include <QList>

TK_StepExtractPlot::TK_StepExtractPlot() : SuperClass()
{
    _x   = 0.00;
    _y = 0.00;
    _radiusmin = 0.00;
    _radius = 17.00;
    _azbegin = 0;
    _azend = 400;
    _zmin = -10000;
    _zmax = 10000;
}

QString TK_StepExtractPlot::description() const
{
    return tr("Extraire une Placette circulaire");
}

QString TK_StepExtractPlot::detailledDescription() const
{
    return tr("Cette étape permet d'extraire les points de la scène d'entrée contenus dans une placette circulaire.<br>"
              "On définit dans les paramètres son <b>centre (X,Y)</b>, son <b>rayon</b> (maximal), le <b>niveau Z minimum</b> et le <b>niveau Z maximum</b>.<br>"
              "Si on définit un <b>rayon de début de placette</b>, cela permet d'obtenir une placette annulaire.<br>"
              "On peut également définir un <b>azimut de début</b> et un <b>azimut de fin</b>, pour obtenir un secteur.");
}

CT_VirtualAbstractStep* TK_StepExtractPlot::createNewInstance() const
{
    // cree une copie de cette etape
    return new TK_StepExtractPlot();
}

//////////////////// PROTECTED //////////////////

void TK_StepExtractPlot::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène"));
}

// Création et affiliation des modèles OUT
void TK_StepExtractPlot::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("Scène extraite"));
}

void TK_StepExtractPlot::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Coordonnée X du centre de la placette :"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _x);
    postInputConfigDialog->addDouble(tr("Coordonnée Y du centre de la placette :"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _y);
    postInputConfigDialog->addDouble(tr("Rayon de début de la placette :"), "m", 0, std::numeric_limits<double>::max(), 4, _radiusmin);
    postInputConfigDialog->addDouble(tr("Rayon de la placette (maximum) :"), "m", 0.01, std::numeric_limits<double>::max(), 4, _radius);
    postInputConfigDialog->addDouble(tr("Azimut début (Nord = axe Y) :"), tr("Grades"), 0, 400, 4, _azbegin);
    postInputConfigDialog->addDouble(tr("Azimut fin (Nord = axe Y) :"), tr("Grades"), 0, 400, 4, _azend);
    postInputConfigDialog->addDouble(tr("Niveau Z minimum :"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _zmin);
    postInputConfigDialog->addDouble(tr("Niveau Z maximum :"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _zmax);
}

void TK_StepExtractPlot::compute()
{

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_AbstractPointCloudIndex *pointCloudIndex = inScene->pointCloudIndex();
            size_t n_points = pointCloudIndex->size();

            PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("La scène d'entrée comporte %1 points.")).arg(n_points));

            CT_PointCloudIndexVector *resPointCloudIndex = new CT_PointCloudIndexVector();
            resPointCloudIndex->setSortType(CT_PointCloudIndexVector::NotSorted);

            std::cout << " le nombre de points dans ma scene : " << n_points;

            // Extraction des points de la placette
            size_t i = 0;
            double distance = 0;
            double azimut = 0;
            double asinx = 0;
            double acosy = 0;
            double xx = 0;
            double yy = 0;

            double xmin = std::numeric_limits<double>::max();
            double ymin = std::numeric_limits<double>::max();
            double zmin = std::numeric_limits<double>::max();

            double xmax = -std::numeric_limits<double>::max();
            double ymax = -std::numeric_limits<double>::max();
            double zmax = -std::numeric_limits<double>::max();

            CT_PointIterator itP(pointCloudIndex);
            while(itP.hasNext() && (!isStopped()))
            {
                const CT_Point &point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                xx = point(0) - _x;
                yy = point(1) - _y;

                // Calcul de l'azimut du point par rapport au centre de la placette extraite
                // Le nord est place dans la direction de l'axe Y
                distance = sqrt(xx*xx + yy*yy);

                if(!qFuzzyCompare(distance, 0.0)) {
                    asinx = asin(xx/distance);
                    acosy = acos(yy/distance);
                } else {
                    asinx = 0;
                    acosy = 0;
                }

                if (asinx>=0) {
                    azimut = acosy;
                } else {
                    azimut = 2*M_PI-acosy;
                }

                // Conversion en grades 0-400
                azimut = azimut/(2*M_PI)*400;

                if (distance <= _radius && distance >= _radiusmin) {
                    if (point(2) >= _zmin && point(2) <= _zmax) {

                        if (_azbegin <= _azend) {
                            if (azimut >= _azbegin && azimut <= _azend)
                            {
                                resPointCloudIndex->addIndex(index);

                                if (point(0)<xmin) {xmin = point(0);}
                                if (point(0)>xmax) {xmax = point(0);}
                                if (point(1)<ymin) {ymin = point(1);}
                                if (point(1)>ymax) {ymax = point(1);}
                                if (point(2)<zmin) {zmin = point(2);}
                                if (point(2)>zmax) {zmax = point(2);}
                            }
                        } else {
                            if ((azimut >= _azbegin && azimut <= 400) || (azimut >= 0 && azimut <= _azend))
                            {

                                resPointCloudIndex->addIndex(index);

                                if (point(0)<xmin) {xmin = point(0);}
                                if (point(0)>xmax) {xmax = point(0);}
                                if (point(1)<ymin) {ymin = point(1);}
                                if (point(1)>ymax) {ymax = point(1);}
                                if (point(2)<zmin) {zmin = point(2);}
                                if (point(2)>zmax) {zmax = point(2);}
                            }

                        }
                    }
                }

                // progres de 0 à 100
                setProgress(float(100.0*i/n_points));
                ++i;
            }

            if (resPointCloudIndex->size() > 0)
            {
                resPointCloudIndex->setSortType(CT_PointCloudIndexVector::SortedInAscendingOrder);

                // creation et ajout de la scene
                CT_Scene *outScene = new CT_Scene(PS_REPOSITORY->registerPointCloudIndex(resPointCloudIndex));

                outScene->setBoundingBox(xmin,ymin,zmin, xmax,ymax,zmax);
                group->addSingularItem(_outScene, outScene);

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("La scène extraite comporte %1 points.")).arg(outScene->pointCloudIndex()->size()));
            } else {
                delete resPointCloudIndex;
                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Aucun point n'est dans l'emprise choisie"));
            }
        }
    }

}
