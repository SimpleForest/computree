#include "tk_stepextractbox.h"

#include <assert.h>

TK_StepExtractBox::TK_StepExtractBox() : SuperClass()
{
    _botX = -10;
    _botY = -10;
    _botZ = -10;
    _topX = 10;
    _topY = 10;
    _topZ = 10;
}


QString TK_StepExtractBox::description() const
{
    return tr("Extraire les points dans une boite englobante");
}


CT_VirtualAbstractStep* TK_StepExtractBox::createNewInstance() const
{
    return new TK_StepExtractBox();
}

//////////////////// PROTECTED METHODS //////////////////


void TK_StepExtractBox::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène source"));
}


void TK_StepExtractBox::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("Scène extraite"));
}


void TK_StepExtractBox::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addText(tr("Coin en bas à gauche"), "", "");
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botX);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botY);
    postInputConfigDialog->addDouble("Z", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botZ);
    postInputConfigDialog->addText(tr("Coin en haut à droite"), "", "");
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topX);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topY);
    postInputConfigDialog->addDouble("Z", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topZ);
}

void TK_StepExtractBox::compute()
{
    assert( _botX <= _topX );
    assert( _botY <= _topY );
    assert( _botZ <= _topZ );

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            // On Cree un nouveau nuage
            CT_PointCloudIndexVector *extractedCloud = new CT_PointCloudIndexVector();

            const CT_AbstractPointCloudIndex *pointCloudIndex = inScene->pointCloudIndex();
            size_t nbPoints = pointCloudIndex->size();

            CT_PointIterator itP(pointCloudIndex);

            size_t i = 0;
            while (itP.hasNext())
            {
                if (isStopped()) {delete extractedCloud; return;}

                const CT_Point &point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                if ( point(0) <= _topX &&
                     point(1) <= _topY &&
                     point(2) <= _topZ &&
                     point(0) >= _botX &&
                     point(1) >= _botY &&
                     point(2) >= _botZ )
                {
                    extractedCloud->addIndex(index);
                }

                setProgress(float(100.0*i++ /nbPoints));

                waitForAckIfInDebugMode();
            }

            if (extractedCloud->size() > 0)
            {

                CT_Scene* outScene = new CT_Scene(PS_REPOSITORY->registerPointCloudIndex(extractedCloud));
                outScene->updateBoundingBox();

                group->addSingularItem(_outScene, outScene);
            } else {
                delete extractedCloud;
            }
        }
    }
}
