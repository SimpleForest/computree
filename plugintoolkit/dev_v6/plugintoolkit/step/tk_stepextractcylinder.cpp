#include "step/tk_stepextractcylinder.h"

#include "ct_math/ct_mathpoint.h"

TK_StepExtractCylinder::TK_StepExtractCylinder() : SuperClass()
{
    _radius = 1;
    _centerX = 0;
    _centerY = 0;
    _minHeight = -10;
    _maxHeight = 10;
}


QString TK_StepExtractCylinder::description() const
{
    return tr("Extraire les points dans un cylindre");
}


CT_VirtualAbstractStep* TK_StepExtractCylinder::createNewInstance() const
{
    return new TK_StepExtractCylinder();
}

//////////////////// PROTECTED METHODS //////////////////


void TK_StepExtractCylinder::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène source"));
}


void TK_StepExtractCylinder::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("Scène extraite"));
}


void TK_StepExtractCylinder::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Rayon du cylindre"), "", 0, 100, 2, _radius);
    postInputConfigDialog->addText(tr("Centre du cylindre"), "", "");
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _centerX);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _centerY);
    postInputConfigDialog->addText(tr("Hauteur (Z)"), "", "");
    postInputConfigDialog->addDouble(tr("Z Minimum"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _minHeight);
    postInputConfigDialog->addDouble(tr("Z Maximum"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _maxHeight);
}

void TK_StepExtractCylinder::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_AbstractPointCloudIndex *pointCloudIndex = inScene->pointCloudIndex();
            size_t nbPoints = pointCloudIndex->size();

            // On Cree un nouveau nuage qui sera le translate
            CT_PointCloudIndexVector *extractedCloud = new CT_PointCloudIndexVector();
            Eigen::Vector3d cylinderCenter( _centerX, _centerY, 0.0 );

            CT_PointIterator itP(pointCloudIndex);

            size_t i = 0;
            while (itP.hasNext())
            {
                if (isStopped()) {delete extractedCloud; return;}

                const CT_Point &point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                if ( CT_MathPoint::distance2D( point, cylinderCenter ) <= _radius &&
                     point(2) >= _minHeight &&
                     point(2) <= _maxHeight )
                {
                    extractedCloud->addIndex(index);

                }

                setProgress(float(100.0*i++ /nbPoints));

                waitForAckIfInDebugMode();
            }

            if (extractedCloud->size() > 0)
            {
                CT_Scene* outScene = new CT_Scene(PS_REPOSITORY->registerPointCloudIndex(extractedCloud));
                outScene->updateBoundingBox();

                group->addSingularItem(_outScene, outScene);
            } else {
                delete extractedCloud;
            }

        }
    }
}
