#include "tk_stepextractzslicefromattributes.h"

#include "ct_log/ct_logmanager.h"

TK_StepExtractZSliceFromAttributes::TK_StepExtractZSliceFromAttributes() : SuperClass()
{
    _botZ = 0.5;
    _topZ = 5.0;

    _useFixedBot = false;
    _useFixedTop = false;
}


QString TK_StepExtractZSliceFromAttributes::description() const
{
    return tr("Extraire les points dans une tranche horizontale (attributs)");
}


CT_VirtualAbstractStep* TK_StepExtractZSliceFromAttributes::createNewInstance() const
{
    return new TK_StepExtractZSliceFromAttributes();
}

//////////////////// PROTECTED METHODS //////////////////


void TK_StepExtractZSliceFromAttributes::fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog)
{
    preInputConfigDialog->addBool(tr("Fixer Z Minimum"), "", "", _useFixedBot);
    preInputConfigDialog->addBool(tr("Fixer Z Maximum"), "", "", _useFixedTop);
    preInputConfigDialog->addEmpty();
    preInputConfigDialog->addTitle(tr("N.B. : Les limites non fixées seront déterminées à partir d'un attribut d'item."));
}

void TK_StepExtractZSliceFromAttributes::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    if (_useFixedBot || _useFixedTop)
    {
        if (_useFixedBot)
        {
            postInputConfigDialog->addDouble(tr("Z Minimum"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botZ);
        } else {
            postInputConfigDialog->addTitle(tr("N.B. : Z Minimum déterminé à partir d'un attribut d'item"));
        }

        if (_useFixedTop)
        {
            postInputConfigDialog->addDouble(tr("Z Maximum"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topZ);
        } else {
            postInputConfigDialog->addTitle(tr("N.B. : Z Maximum déterminé à partir d'un attribut d'item"));
        }
    }
}


void TK_StepExtractZSliceFromAttributes::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène à découper"));

    if (!_useFixedBot || !_useFixedTop)
    {
        manager.addItem(_inGroup, _inItem, tr("Item avec attribut"));
        if (!_useFixedBot)
        {
            manager.addItemAttribute(_inItem, _inAttributeBot, CT_AbstractCategory::DATA_VALUE, tr("Zmin"));
        }
        if (!_useFixedTop)
        {
            manager.addItemAttribute(_inItem, _inAttributeTop, CT_AbstractCategory::DATA_VALUE, tr("Zmax"));
        }
    }
}


void TK_StepExtractZSliceFromAttributes::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("Scène translatée"));
}

void TK_StepExtractZSliceFromAttributes::compute()
{

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_AbstractSingularItemDrawable* item = group->singularItem(_inItem);
            const CT_AbstractPointCloudIndex *pointCloudIndex = inScene->pointCloudIndex();
            size_t nbPoints = pointCloudIndex->size();

            // On Cree un nouveau nuage qui sera le translate
            CT_PointCloudIndexVector *extractedCloud = new CT_PointCloudIndexVector();

            // Cloud bounding box
            double min = -std::numeric_limits<double>::max();
            double max = std::numeric_limits<double>::max();

            if (_useFixedTop || item == nullptr)
            {
                max =  _topZ;
            } else {
                const CT_AbstractItemAttribute* att = item->itemAttribute(_inAttributeTop);
                if (att != nullptr)
                {
                    max = att->toDouble(inScene, nullptr);
                } else {
                    PS_LOG->addMessage(LogInterface::error, LogInterface::step, QString(tr("Attribut Zmax non trouvé !!!")));
                }
            }

            if (_useFixedBot || item == nullptr)
            {
                min = _botZ;
            } else {
                // TODOV6 - Plantage à l'exécution de la ligne suivante
                const CT_AbstractItemAttribute* att = item->itemAttribute(_inAttributeBot);
                if (att != nullptr)
                {
                    min = att->toDouble(inScene, nullptr);
                } else {
                    PS_LOG->addMessage(LogInterface::error, LogInterface::step, QString(tr("Attribut Zmin non trouvé !!!")));
                }
            }

            CT_PointIterator itP(pointCloudIndex);

            size_t i = 0;
            while (itP.hasNext())
            {
                if (isStopped()) {delete extractedCloud; return;}

                const CT_Point &point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                if ( point(2) <= max &&
                     point(2) >= min)
                {
                    extractedCloud->addIndex(index);
                }

                setProgress(float(100.0*i++ /nbPoints));
            }

            if (extractedCloud->size() > 0)
            {
                CT_Scene* outScene = new CT_Scene(PS_REPOSITORY->registerPointCloudIndex(extractedCloud));
                outScene->updateBoundingBox();

                group->addSingularItem(_outScene, outScene);
            }
        }
    }
}
