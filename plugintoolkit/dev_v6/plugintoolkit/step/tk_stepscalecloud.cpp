#include "tk_stepscalecloud.h"

TK_StepScaleCloud::TK_StepScaleCloud() : SuperClass()
{
    _x = 0;
    _y = 0;
    _z = 0;

    // Cette etape est CT_debugable
    setDebuggable( true );
}


QString TK_StepScaleCloud::description() const
{
    return tr("Homothétie des points");
}


CT_VirtualAbstractStep* TK_StepScaleCloud::createNewInstance() const
{
    return new TK_StepScaleCloud();
}

//////////////////// PROTECTED METHODS //////////////////


void TK_StepScaleCloud::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène à scaler"));
}


void TK_StepScaleCloud::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("Scène scalée"));
}


void TK_StepScaleCloud::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Facteur multiplicatif X"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 7, _x);
    postInputConfigDialog->addDouble(tr("Facteur multiplicatif Y"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 7, _y);
    postInputConfigDialog->addDouble(tr("Facteur multiplicatif Z"), "m", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 7, _z);
}

void TK_StepScaleCloud::compute()
{

    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_AbstractPointCloudIndex *cloudIndex = inScene->pointCloudIndex();

            CT_PointIterator itP(cloudIndex);

            size_t nbPoints = cloudIndex->size();

            // On Cree un nouveau nuage qui sera le translate
            CT_NMPCIR scaledCloud = PS_REPOSITORY->createNewPointCloud(nbPoints);
            CT_MutablePointIterator itPM(scaledCloud);

            // On cree le vecteur d'echelle
            Eigen::Affine3d scaleVector(Eigen::Scaling(_x, _y, _z));

            size_t i = 0;
            // On applique la translation a tous les points du nuage
            while (itP.hasNext() && itPM.hasNext())
            {
                itP.next();

                itPM.next().replaceCurrentPoint(scaleVector * itP.currentPoint());

                // Barre de progression
                setProgress(float(100.0*i++ /nbPoints));

                // On regarde si on est en debug mode
                waitForAckIfInDebugMode();
            }

            CT_Scene* outScene = new CT_Scene(scaledCloud);
            outScene->setBoundingBox( inScene->minX() * _x, inScene->minY() * _y, inScene->minZ() * _z, inScene->maxX() * _x, inScene->maxY() * _y, inScene->maxZ() * _z);

            group->addSingularItem(_outScene, outScene);
        }
    }
}
