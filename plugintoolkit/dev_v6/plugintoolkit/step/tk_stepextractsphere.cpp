#include "tk_stepextractsphere.h"

#include "ct_math/ct_mathpoint.h"

TK_StepExtractSphere::TK_StepExtractSphere() : SuperClass()
{
    _radius = 1;
    _centerX = 0;
    _centerY = 0;
    _centerZ = 0;
}


QString TK_StepExtractSphere::description() const
{
    return tr("Extraire les points dans une sphère");
}


CT_VirtualAbstractStep* TK_StepExtractSphere::createNewInstance() const
{
    return new TK_StepExtractSphere();
}

//////////////////// PROTECTED METHODS //////////////////


void TK_StepExtractSphere::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Scène(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scène source"));
}


void TK_StepExtractSphere::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("Scène extraite"));
}


void TK_StepExtractSphere::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Rayon de la sphère"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _radius);
    postInputConfigDialog->addText(tr("Centre de la sphère"), "", "");
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _centerX);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _centerY);
    postInputConfigDialog->addDouble("Z", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _centerZ);
}

void TK_StepExtractSphere::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractItemDrawableWithPointCloud* inScene : group->singularItems(_inScene))
        {
            if (isStopped()) {return;}

            const CT_AbstractPointCloudIndex *pointCloudIndex = inScene->pointCloudIndex();
            size_t nbPoints = pointCloudIndex->size();

            // On Cree un nouveau nuage qui sera le translate
            CT_PointCloudIndexVector *extractedCloud = new CT_PointCloudIndexVector();
            Eigen::Vector3d sphereCenter( _centerX, _centerY, _centerZ );

            CT_PointIterator itP(pointCloudIndex);

            size_t i = 0;
            while (itP.hasNext())
            {
                if (isStopped()) {delete extractedCloud; return;}

                const CT_Point &point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                if (CT_MathPoint::distance3D( point, sphereCenter ) <= _radius)
                {
                    extractedCloud->addIndex(index);
                }

                setProgress(float(100.0*i++ /nbPoints));

                waitForAckIfInDebugMode();
            }

            if (extractedCloud->size() > 0)
            {
                CT_Scene* outScene = new CT_Scene(PS_REPOSITORY->registerPointCloudIndex(extractedCloud));
                outScene->updateBoundingBox();

                group->addSingularItem(_outScene, outScene);
            } else {
                delete extractedCloud;
            }
        }
    }
}
