#ifndef TK_STEPFILTERPOINTSBYATTRIBUTE_H
#define TK_STEPFILTERPOINTSBYATTRIBUTE_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/abstract/ct_abstractpointattributesscalar.h"

class TK_StepFilterPointsByAttribute : public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    TK_StepFilterPointsByAttribute();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    double    _threshMin;
    double    _threshMax;

    CT_HandleInResultGroupCopy<>                                    _inResult;
    CT_HandleInStdZeroOrMoreGroup                                   _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                           _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>  _inScene;
    CT_HandleInSingularItem<CT_AbstractPointAttributesScalar>       _inAttribute;
    CT_HandleOutSingularItem<CT_Scene>                              _outScene;

};

#endif // TK_STEPFILTERPOINTSBYATTRIBUTE_H
