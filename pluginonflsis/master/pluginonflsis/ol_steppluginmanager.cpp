/****************************************************************************

 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                     and the Laboratoire des Sciences de l'Information et des Systèmes (LSIS), Marseille, France.
                     All rights reserved.

 Contact : alexandre.piboule@onf.fr
                 alexandra.bac@esil.univmed.fr

 Developers : Joris Ravaglia (ONF/LSIS)
 With adaptations by : Alexandre PIBOULE (ONF)

 This file is part of PluginONFLSIS library 1.0.

 PluginONFLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginShared is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginShared.  If not, see <http://www.gnu.org/licenses/lgpl.html>.

*****************************************************************************/


#include "ol_steppluginmanager.h"

#include "ct_stepseparator.h"
#include "ct_steploadfileseparator.h"
#include "step/ol_stepthrowparticules05.h"
#include "step/ol_stepcreatepolylines02.h"
#include "step/ol_stepfilterarcpolylines02.h"

OL_StepPluginManager::OL_StepPluginManager() : CT_AbstractStepPlugin()
{
}

OL_StepPluginManager::~OL_StepPluginManager()
{
}

QString OL_StepPluginManager::getPluginRISCitation() const
{
    return "TY  - COMP\n"
           "TI  - Plugin ONF-LSIS for Computree\n"
           "AU  - Ravaglia, Joris\n"
           "AU  - Piboule, Alexandre\n"
           "PB  - Office National des Forêts, RDI Department\n"
           "PY  - 2017\n"
           "UR  - http://rdinnovation.onf.fr/projects/plugin-onf-lsis/wiki\n"
           "ER  - \n";
}


bool OL_StepPluginManager::loadGenericsStep()
{
    addNewGeometricalShapesStep<OL_StepThrowParticules05>(CT_StepsMenu::LP_Stems);
    addNewGeometricalShapesStep<OL_StepCreatePolylines02>(CT_StepsMenu::LP_Stems);
    addNewGeometricalShapesStep<OL_StepFilterArcPolylines02>(CT_StepsMenu::LP_Stems);

    return true;
}

bool OL_StepPluginManager::loadOpenFileStep()
{
    return true;
}

bool OL_StepPluginManager::loadCanBeAddedFirstStep()
{
    return true;
}

bool OL_StepPluginManager::loadActions()
{
    return true;
}

bool OL_StepPluginManager::loadExporters()
{
    return true;
}

bool OL_StepPluginManager::loadReaders()
{
    return true;
}

