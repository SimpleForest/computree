#include "rsc_pluginentry.h"
#include "rsc_pluginmanager.h"

RSC_PluginEntry::RSC_PluginEntry()
{
    _pluginManager = new RSC_PluginManager();
}

RSC_PluginEntry::~RSC_PluginEntry()
{
    delete _pluginManager;
}

QString RSC_PluginEntry::getVersion() const
{
    return "1.0";
}

CT_AbstractStepPlugin* RSC_PluginEntry::getPlugin() const
{
    return _pluginManager;
}

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    Q_EXPORT_PLUGIN2(plug_rscript, RSC_PluginEntry)
#endif
