#ifndef INTABLE_H
#define INTABLE_H

#include <QObject>
#include "ct_step/abstract/ct_abstractstep.h"
#include "infield.h"

class InTable : public QObject
{
    Q_OBJECT

public:

	InTable ()
	{

	}

    void declareInputModels(CT_StepInModelStructureManager& manager)
    {
        if (_cpy)
        {
            manager.addResult(_inResultCpy, _desc);
            manager.setZeroOrMoreRootGroup(_inResultCpy, _inZeroOrMoreRootGroup);
        } else {
            manager.addResult(_inResult, _desc);
            manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
        }

        manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
        manager.addItem(_inGroup, _inItem, _desc);

        for (int i = 0 ; i < _inFields.size() ; i++)
        {
            manager.addItemAttribute(_inItem, _inFields[i]._inAttLogical, CT_AbstractCategory::DATA_VALUE,_desc);
        }

    }

    QString                 _name;
    QString                 _desc;
    bool                    _cpy;

    CT_HandleInResultGroupCopy<>                                    _inResultCpy;
    CT_HandleInResultGroup<>                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                   _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                           _inGroup;
    CT_HandleInSingularItem<CT_AbstractSingularItemDrawable,1,-1>   _inItem;

    QList<InField>  _inFields;

};

#endif // INTABLE_H
