#ifndef OUTFIELD_H
#define OUTFIELD_H

#include <QObject>
#include "ct_step/abstract/ct_abstractstep.h"

class OutField : public QObject
{
    Q_OBJECT

public:

	OutField ()
	{

	}

    QString _name;
    QString _type;
    QString _desc;
    bool    _multi;
    bool    _opt;

    CT_HandleOutStdItemAttribute<bool>      _outAttLogical;
    CT_HandleOutStdItemAttribute<int>       _outAttInteger;
    CT_HandleOutStdItemAttribute<double>    _outAttNumeric;
    CT_HandleOutStdItemAttribute<QString>   _outAttCharacterOrFactor;

};

#endif // OUTFIELD_H
