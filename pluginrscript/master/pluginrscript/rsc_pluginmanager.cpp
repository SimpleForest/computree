#include "rsc_pluginmanager.h"
#include "ct_stepseparator.h"
#include "ct_steploadfileseparator.h"
#include "ct_stepcanbeaddedfirstseparator.h"
#include "ct_actions/ct_actionsseparator.h"
#include "ct_exporter/ct_standardexporterseparator.h"
#include "ct_reader/ct_standardreaderseparator.h"
#include "ct_actions/abstract/ct_abstractaction.h"

#include "step/rsc_stepuserscript.h"

// Inclure ici les entetes des classes definissant des Ã©tapes/actions/exporters ou readers

RSC_PluginManager::RSC_PluginManager() : CT_AbstractStepPlugin()
{
}

RSC_PluginManager::~RSC_PluginManager()
{
}

QString RSC_PluginManager::getPluginRISCitation() const
{
    return "TY  - COMP\n"
           "TI  - Plugin R-Script for Computree\n"
           "AU  - Piboule, Alexandre\n"
           "PB  - Office National des Forêts, RDI Department\n"
           "PY  - 2018\n"
           "UR  - http://rdinnovation.onf.fr/projects/plugin-r-script/wiki\n"
           "ER  - \n";
}
bool RSC_PluginManager::loadGenericsStep()
{
    addNewBetaStep<RSC_StepUseRScript>("R");

    return true;
}

bool RSC_PluginManager::loadOpenFileStep()
{

    return true;
}

bool RSC_PluginManager::loadCanBeAddedFirstStep()
{

    return true;
}

bool RSC_PluginManager::loadActions()
{

    return true;
}

bool RSC_PluginManager::loadExporters()
{

    return true;
}

bool RSC_PluginManager::loadReaders()
{

    return true;
}

