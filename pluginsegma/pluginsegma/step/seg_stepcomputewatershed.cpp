#include "seg_stepcomputewatershed.h"

#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

// Alias for indexing models
#define DEFin_res "res"
#define DEFin_grp "grp"
#define DEFin_image "image"
#define DEFin_maxima "maxima"

#define DEFin_resDTM "resdtm"
#define DEFin_DTMGrp "dtmgrp"
#define DEFin_DTM "dtm"


// Constructor : initialization of parameters
SEG_StepComputeWatershed::SEG_StepComputeWatershed(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _mergeLimits = true;
    _minHeight = 2.0;
}

// Step description (tooltip of contextual menu)
QString SEG_StepComputeWatershed::getStepDescription() const
{
    return tr("5- Watershed (flooding)");
}

// Step detailled description
QString SEG_StepComputeWatershed::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString SEG_StepComputeWatershed::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* SEG_StepComputeWatershed::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new SEG_StepComputeWatershed(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void SEG_StepComputeWatershed::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_res = createNewInResultModelForCopy(DEFin_res, tr("Image 2D"));
    resIn_res->setZeroOrMoreRootGroup();
    resIn_res->addGroupModel("", DEFin_grp, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    resIn_res->addItemModel(DEFin_grp, DEFin_image, CT_Image2D<float>::staticGetType(), tr("Image (hauteurs)"));
    resIn_res->addItemModel(DEFin_grp, DEFin_maxima, CT_Image2D<qint32>::staticGetType(), tr("Maxima"));

    CT_InResultModelGroup *resultDTM = createNewInResultModel(DEFin_resDTM, tr("MNT"), "", true);
    resultDTM->setZeroOrMoreRootGroup();
    resultDTM->addGroupModel("", DEFin_DTMGrp, CT_AbstractItemGroup::staticGetType(), tr("Group"));
    resultDTM->addItemModel(DEFin_DTMGrp, DEFin_DTM, CT_Image2D<float>::staticGetType(), tr("MNT"));
    resultDTM->setMinimumNumberOfPossibilityThatMustBeSelectedForOneTurn(0);

}

// Creation and affiliation of OUT models
void SEG_StepComputeWatershed::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy_res = createNewOutResultModelToCopy(DEFin_res);

    if (resCpy_res != NULL)
    {
        resCpy_res->addItemModel(DEFin_grp, _watershedImage_ModelName, new CT_Image2D<qint32>(), tr("Watershed"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void SEG_StepComputeWatershed::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
    configDialog->addDouble(tr("Ne pas affecter les pixels d'une valeur inférieure à"), "m",-99999, 99999, 2, _minHeight);
    configDialog->addBool(tr("Affecter les limites à un cluster"), "", "", _mergeLimits);
}

void SEG_StepComputeWatershed::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res = outResultList.at(0);

    CT_Image2D<float>* mnt = NULL;
    if (getInputResults().size() > 1)
    {
        CT_ResultGroup* resin_DTM = getInputResults().at(1);
        CT_ResultItemIterator it(resin_DTM, this, DEFin_DTM);
        if (it.hasNext())
        {
            mnt = (CT_Image2D<float>*) it.next();
        }
    }


    // COPIED results browsing
    CT_ResultGroupIterator itCpy_grp(res, this, DEFin_grp);
    while (itCpy_grp.hasNext() && !isStopped())
    {

        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itCpy_grp.next();
        
        CT_Image2D<float>* imageIn = (CT_Image2D<float>*)grp->firstItemByINModelName(this, DEFin_image);
        CT_Image2D<qint32>* maximaIn = (CT_Image2D<qint32>*)grp->firstItemByINModelName(this, DEFin_maxima);

        if (imageIn != NULL && maximaIn != NULL)
        {

            Eigen::Vector2d min;
            imageIn->getMinCoordinates(min);
            CT_Image2D<qint32>* watershedImage = new CT_Image2D<qint32>(_watershedImage_ModelName.completeName(), res, min(0), min(1), imageIn->colDim(), imageIn->linDim(), imageIn->resolution(), imageIn->level(), -1, 0);
            grp->addItemDrawable(watershedImage);

            watershedImage->getMat() = maximaIn->getMat().clone();

            setProgress(20);

            computeWatershed(imageIn, watershedImage, mnt);

            setProgress(70);

            if (_mergeLimits)
            {
                mergeLimitsWithClusters(imageIn, watershedImage);
            }

            setProgress(90);

            watershedImage->computeMinMax();

            setProgress(99);
        }
    }
    setProgress(100);
}

void SEG_StepComputeWatershed::computeWatershed(const CT_Image2D<float>* imageIn, CT_Image2D<qint32>* watershedImage, CT_Image2D<float>* mnt)
{
    QMultiMap<float, size_t> queue;
    size_t index, col, lin, col2, lin2;

    // Add all neighbours of maxima to the queue
    for (size_t lin = 0; lin < imageIn->linDim() ; lin++)
    {
        for (size_t col = 0; col < imageIn->colDim() ; col++)
        {            
            // Add to the queue if at least one neighbour pixel is already labelled
            if (watershedImage->value(col, lin) == 0)
            {

                float zval = imageIn->value(col, lin);
                if (mnt != NULL)
                {
                    float zmnt = mnt->valueAtCoords(imageIn->getCellCenterColCoord(col), imageIn->getCellCenterLinCoord(lin));
                    if (zmnt != mnt->NA())
                    {
                        zval -= zmnt;
                    }
                }

                if (zval <= _minHeight) {watershedImage->setValue(col, lin, -1);} // flag as checked all pixels <= _minHeight

                qint32 firstVal;
                if (hasExactlyOneNeighbourCluster(col, lin, watershedImage, firstVal))
                {
                    watershedImage->index(col, lin, index);
                    queue.insert(imageIn->value(col, lin), index);
                    watershedImage->setValue(col, lin, -1); // flag as inserted in queue index
                } else if (firstVal > 0) {
                    watershedImage->setValue(col, lin, -1); // flag as checked
                }
            }
        }
    }

    qint32 firstVal;
    qint32 val;

    while (queue.size() > 0)
    {
        index = queue.last();
        imageIn->indexToGrid(index, col, lin);
        queue.remove(queue.lastKey(), index);

        // Etape 2 : si le pixel n'a q'un cluster voisin : ajout au cluster, et récupération de ses voisins
        if (hasExactlyOneNeighbourCluster(col, lin, watershedImage, firstVal))
        {
            watershedImage->setValue(col, lin, firstVal);

            col2 = col - 1; lin2 = lin - 1;
            val = watershedImage->value(col2, lin2);
            if (watershedImage->index(col2, lin2, index) && val == 0)
            {
                queue.insert(imageIn->value(col2, lin2), index);
                watershedImage->setValue(col2, lin2, -1);
            }

            col2 = col - 1; lin2 = lin;
            val = watershedImage->value(col2, lin2);
            if (watershedImage->index(col2, lin2, index) && val == 0)
            {
                queue.insert(imageIn->value(col2, lin2), index);
                watershedImage->setValue(col2, lin2, -1);
            }

            col2 = col - 1; lin2 = lin + 1;
            val = watershedImage->value(col2, lin2);
            if (watershedImage->index(col2, lin2, index) && val == 0)
            {
                queue.insert(imageIn->value(col2, lin2), index);
                watershedImage->setValue(col2, lin2, -1);
            }

            col2 = col; lin2 = lin + 1;
            val = watershedImage->value(col2, lin2);
            if (watershedImage->index(col2, lin2, index) && val == 0)
            {
                queue.insert(imageIn->value(col2, lin2), index);
                watershedImage->setValue(col2, lin2, -1);
            }

            col2 = col + 1; lin2 = lin + 1;
            val = watershedImage->value(col2, lin2);
            if (watershedImage->index(col2, lin2, index) && val == 0)
            {
                queue.insert(imageIn->value(col2, lin2), index);
                watershedImage->setValue(col2, lin2, -1);
            }

            col2 = col + 1; lin2 = lin;
            val = watershedImage->value(col2, lin2);
            if (watershedImage->index(col2, lin2, index) && val == 0)
            {
                queue.insert(imageIn->value(col2, lin2), index);
                watershedImage->setValue(col2, lin2, -1);
            }

            col2 = col + 1; lin2 = lin - 1;
            val = watershedImage->value(col2, lin2);
            if (watershedImage->index(col2, lin2, index) && val == 0)
            {
                queue.insert(imageIn->value(col2, lin2), index);
                watershedImage->setValue(col2, lin2, -1);
            }

            col2 = col; lin2 = lin - 1;
            val = watershedImage->value(col2, lin2);
            if (watershedImage->index(col2, lin2, index) && val == 0)
            {
                queue.insert(imageIn->value(col2, lin2), index);
                watershedImage->setValue(col2, lin2, -1);
            }
        } else if (firstVal > 0) {
            watershedImage->setValue(col, lin, -1); // flag as checked
        }

    }
}

bool SEG_StepComputeWatershed::hasExactlyOneNeighbourCluster(size_t &col, size_t &lin, CT_Image2D<qint32>* watershedImage, qint32 &firstVal)
{
    bool ok = true;

    firstVal = watershedImage->value(col-1, lin-1);

    qint32 val = watershedImage->value(col-1, lin);
    if (val > 0) {
        if (firstVal <= 0) {firstVal = val;}
        else if (val != firstVal) {ok = false;}
    }

    val = watershedImage->value(col-1, lin+1);
    if (ok && val > 0) {
        if (firstVal <= 0) {firstVal = val;}
        else if (val != firstVal) {ok = false;}
    }

    val = watershedImage->value(col, lin+1);
    if (ok && val > 0){
        if (firstVal <= 0) {firstVal = val;}
        else if (val != firstVal) {ok = false;}
    }

    val = watershedImage->value(col+1, lin+1);
    if (ok && val > 0) {
        if (firstVal <= 0) {firstVal = val;}
        else if (val != firstVal) {ok = false;}
    }

    val = watershedImage->value(col+1, lin);
    if (ok && val > 0) {
        if (firstVal <= 0) {firstVal = val;}
        else if (val != firstVal) {ok = false;}
    }

    val = watershedImage->value(col+1, lin-1);
    if (ok && val > 0) {
        if (firstVal <= 0) {firstVal = val;}
        else if (val != firstVal) {ok = false;}
    }

    val = watershedImage->value(col, lin-1);
    if (ok && val > 0) {
        if (firstVal <= 0) {firstVal = val;}
        else if (val != firstVal) {ok = false;}
    }

    if (firstVal <= 0) {ok = false;}

    return ok;
}

void SEG_StepComputeWatershed::mergeLimitsWithClusters(const CT_Image2D<float>* imageIn, CT_Image2D<qint32>* watershedImage)
{
    cv::Mat_<float> result = watershedImage->getMat().clone();

    QMap<qint32, float> sumMap;
    QMap<qint32, int> countMap;

    // Add all neighbours of maxima to the queue
    for (size_t lin = 0; lin < watershedImage->linDim() ; lin++)
    {
        for (size_t col = 0; col < watershedImage->colDim() ; col++)
        {
            if (watershedImage->value(col, lin) <= 0 && (imageIn->value(col, lin) > _minHeight))
            {
                float centerVal = imageIn->value(col, lin);

                SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, col - 1, lin    );
                SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, col    , lin - 1);
                SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, col    , lin - 1);
                SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, col + 1, lin    );

                if (sumMap.size() <= 0)
                {
                    SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, col - 1, lin - 1);
                    SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, col - 1, lin + 1);
                    SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, col + 1, lin - 1);
                    SEG_StepComputeWatershed::computeSumAndCount(imageIn, watershedImage, centerVal, sumMap, countMap, col + 1, lin + 1);
                }

                QMapIterator<qint32, float> itSum(sumMap);
                QMapIterator<qint32, int> itCount(countMap);
                float minVal = std::numeric_limits<float>::max();
                int minCluster = 0;

                while (itSum.hasNext() && itCount.hasNext())
                {
                    itSum.next();
                    itCount.next();

                    float val = itSum.value() / (float)itCount.value();

                    if (val < minVal)
                    {
                        minVal = val;
                        minCluster = itSum.key();
                    }
                }

                result(lin, col) = minCluster;

                sumMap.clear();
                countMap.clear();
            }
        }
    }

    watershedImage->getMat() = result;
}


void SEG_StepComputeWatershed::computeSumAndCount(const CT_Image2D<float>* imageIn, const CT_Image2D<qint32>* watershedImage, float centerVal, QMap<qint32, float> &sumMap, QMap<qint32, int> &countMap, size_t col, size_t lin)
{
    qint32 cluster = watershedImage->value(col, lin);

    if (cluster > 0)
    {
        float sum = sumMap.value(cluster, 0);
        int count = countMap.value(cluster, 0);

        sum += fabs(imageIn->value(col, lin) - centerVal);
        count++;

        sumMap.insert(cluster, sum);
        countMap.insert(cluster, count);
    }

}
