#include "seg_stepextractpointsbycluster02.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

SEG_StepExtractPointsByCluster02::SEG_StepExtractPointsByCluster02() : SuperClass()
{

}

QString SEG_StepExtractPointsByCluster02::description() const
{
    return tr("8- Extraire les points par couronne (v2)");
}

QString SEG_StepExtractPointsByCluster02::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString SEG_StepExtractPointsByCluster02::URL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* SEG_StepExtractPointsByCluster02::createNewInstance() const
{
    return new SEG_StepExtractPointsByCluster02();
}

//////////////////// PROTECTED METHODS //////////////////

void SEG_StepExtractPointsByCluster02::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResultSc, tr("Scene(s)"));//, "", true); //TODO
    manager.setZeroOrMoreRootGroup(_inResultSc, _inZeroOrMoreRootGroupSc);
    manager.addGroup(_inZeroOrMoreRootGroupSc, _inGroupSc);
    manager.addItem(_inGroupSc, _inScene, tr("Scène"));

    manager.addResult(_inResult, tr("Clusters"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inMainGroup);
    manager.addItem(_inMainGroup, _inClusters, tr("Cluster"));
    manager.addGroup(_inMainGroup, _inGroup);
    manager.addItem(_inGroup, _inItemWithID, tr("Item"));
    manager.addItemAttribute(_inItemWithID, _inAttID, CT_AbstractCategory::DATA_ID, tr("ID Cluster"));
}

void SEG_StepExtractPointsByCluster02::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outScene, tr("Scène extraite"));
}


void SEG_StepExtractPointsByCluster02::compute()
{
    for (CT_StandardItemGroup* mainGrp : _inMainGroup.iterateOutputs(_inResult))
    {
        for (const CT_Image2D<qint32>* clusters : mainGrp->singularItems(_inClusters))
        {
            if (isStopped()) {return;}

            qint32 maxIDCluster = clusters->dataMax();
            QVector<CT_StandardItemGroup*> groups(maxIDCluster + 1);
            QVector<CT_PointCloudIndexVector*> clouds(maxIDCluster + 1);
            groups.fill(nullptr);

            for (int i = 0 ; i <= maxIDCluster ; i++)
            {
                clouds[i] = new CT_PointCloudIndexVector();
            }

            for (const CT_StandardItemGroup* grpConst : mainGrp->groups(_inGroup))
            {
                if (isStopped()) {return;}

                CT_StandardItemGroup* group = const_cast<CT_StandardItemGroup*>(grpConst);

                const CT_AbstractSingularItemDrawable* item = grpConst->singularItem(_inItemWithID);
                if (item != nullptr)
                {
                    qint32 clusterID = -1;

                    // TODOV6 - Plantage à l'exécution de la ligne suivante
                    const CT_AbstractItemAttribute* att = item->itemAttribute(_inAttID);
                    if (att != nullptr) {clusterID = att->toInt(item, nullptr);}

                    if (clusterID >= 0)
                    {
                        groups[clusterID] = group;
                    }
                }
            }

            setProgress(5);

            for (const CT_AbstractItemDrawableWithPointCloud* scene : _inScene.iterateInputs(_inResultSc))
            {
                if (isStopped()) {return;}

                const CT_AbstractPointCloudIndex *inCloudIndex = scene->pointCloudIndex();
                if (scene != nullptr && inCloudIndex != nullptr)
                {
                    size_t cpt = 0;
                    double sceneSize = inCloudIndex->size();

                    CT_PointIterator itP(inCloudIndex);
                    while(itP.hasNext() && (!isStopped()))
                    {
                        const CT_Point &point = itP.next().currentPoint();
                        size_t index = itP.currentGlobalIndex();
                        qint32 clusterForPoint = clusters->valueAtCoords(point(0), point(1));
                        if (clusterForPoint >= 0 && clusterForPoint != clusters->NA())
                        {
                            clouds[clusterForPoint]->addIndex(index);
                        }
                        if (++cpt % 100 == 0) {setProgress(float(90.0*cpt/sceneSize + 5.0));}
                    }
                }
            }

            for (int i = 0 ; i <= maxIDCluster ; i++)
            {
                CT_PointCloudIndexVector* cloud = clouds[i];
                if (cloud->size() > 0)
                {
                    CT_Scene* scene = new CT_Scene(PS_REPOSITORY->registerPointCloudIndex(cloud));
                    scene->updateBoundingBox();

                    if (groups[i] != nullptr)
                    {
                        groups[i]->addSingularItem(_outScene, scene);
                    } else {
                        //qDebug() << "Problème cluster i = " << i;
                    }
                } else {
                    delete cloud;
                }
            }
        }
    }
}

