#include "seg_stepcavityfill.h"

#include "tools/cavityfillmain.h"

#include <QFileInfo>
#include <QDir>

SEG_StepCavityFill::SEG_StepCavityFill() : SuperClass()
{
    _filterRadius = 3;
    _laplacianThreshold = 1;
    _medianFilterSize = 3;
    _dilation = 1;
    _heightThreshold = 70;

}

QString SEG_StepCavityFill::description() const
{
    return tr("1- Remplir les trous");
}

QString SEG_StepCavityFill::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString SEG_StepCavityFill::URL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* SEG_StepCavityFill::createNewInstance() const
{
    return new SEG_StepCavityFill();
}

//////////////////// PROTECTED METHODS //////////////////

void SEG_StepCavityFill::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Raster(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inRaster, tr("Raster"));
}

void SEG_StepCavityFill::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outRaster, tr("Raster out"));
}

void SEG_StepCavityFill::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addInt(tr("Rayon du filtre Laplacien"), "px", 3, 999, _filterRadius);
    postInputConfigDialog->addDouble(tr("Seuil sur le Laplacien"), "", -1e+09, 1e+09, 2, _laplacianThreshold);
    postInputConfigDialog->addInt(tr("Taille du filtre médian"), "px", 3, 999, _medianFilterSize);
    postInputConfigDialog->addInt(tr("Dilatation des cavités"), "px", 0, 999, _dilation);
    postInputConfigDialog->addDouble(tr("Hauteur maximale"), "m", 0, 1e+09, 2, _heightThreshold);
}

void SEG_StepCavityFill::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_Image2D<float>* inGridConst : group->singularItems(_inRaster))
        {
            if (isStopped()) {return;}

            CT_Image2D<float>* inGrid = const_cast<CT_Image2D<float>*>(inGridConst);

            CT_Image2D<float> *outGrid = new CT_Image2D<float>(inGrid->minX(), inGrid->minY(), inGrid->xdim(), inGrid->ydim(), inGrid->resolution(), inGrid->level(), inGrid->NA(), inGrid->NA());

            CavityFillMain::cavityFillFilter(inGrid->getPointerToData(), inGrid->ydim(), inGrid->xdim(), _filterRadius, 5.0, float(_laplacianThreshold), _medianFilterSize, _dilation, float(_heightThreshold), outGrid->getPointerToData());

            outGrid->computeMinMax();

            group->addSingularItem(_outRaster, outGrid);

        }
    }
}
