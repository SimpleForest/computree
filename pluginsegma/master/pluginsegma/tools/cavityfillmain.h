#ifndef CAVITYFILLMAIN_H
#define CAVITYFILLMAIN_H

#include <string>
#include <vector>
#include <iostream>
#include <sstream>  // stringstream ss;

#include "tools/list_croissance.h"
#include "tools/commontools.h"

using namespace std;

typedef char FileName[255];
typedef char Param[30];

#define DOT "."
#define HDR "hdr"
#define EITHER_SLASH "/\\"
#define STAR "*"
#define NOT_FOUND -1
#define EMPTY 0

#define FICHIER_PARAMETRES "param_CavityFill.txt"
#define PREFIX "f_"
#define BUFF_SIZE 2
#define MAXIMUM_PIXELS 8
#define RANGE 4
#define MIN_WEIGHT 0.1  // 10% par rapport au poids déjà accumulé
#define MAX_LOOP 2


class CavityFillMain
{

public:

struct Params{
    FileName input_mask;
    int filtre_largeur;
    float filtre_pente;
    float seuil_laplacian;
    int filtre_mediane;
    int dilatation;
    float seuil_hauteur;
    FileName output_dir;
};


/**
  * Function to fill cavities of a canopy height model
  *
  *  source : vector of float of size nrows*ncols
  *  nrows : n lines of the source
  *  ncols : n columns of the source
  *  filter_len : Params.filtre_largeur
  *  filter_slope : Params.filtre_pente
  *  laplacian_thres : Params.seuil_laplacian
  *  median_filter_len : Params.filtre_mediane
  *  dilatation : Params.dilatation
  *  height_thres : Params.seuil_hauteur
  *  target : vector of float of size nrows*ncols
  */
static void cavityFillFilter(float* source, int nrows, int ncols, int filter_len, float filter_slope, float laplacian_thres, int median_filter_len, int dilatation, float height_thres, float* target);

private:

/* --STRUCTURES PRÉDEFINIES-- */

// Chemin d'accès et masque de fichiers
struct Path {
string path;
string mask;
string ext;
};

// Structure du fichier d'en-tête pour les rasters de type FLT
struct FltHdr {
    int ncols;
    int nrows;
    double xllcorner;
    double yllcorner;
    float cellsize;
    int NODATA_value;
    FileName byteorder;
};

template <class dataType>
static dataType *setMem(unsigned long rows, unsigned long cols);

template <class N>
static N toNum(string str);

static Path splitPath(string str);
static float *prepare_filtre_elements(int size, float slope);
static bool *find_holes(const float *scene, int size, float filtre_pente, float laplacian_thres, int dilatation, float height_thres, int snlin, int sncol);
static float *copy_holes(const float *scene, bool *holes, int snlin, int sncol);
static float get_interpolate_value(float *g, List_Croissance list, unsigned long pxl, unsigned long sncol);
static float *interpolate(float *g, int snlin, int sncol);
static float get_median(float *mfe, int array_size);
static void median_filter(float *gi, bool *hole_map2, int msize, int snlin, int sncol, float *out_scene);
static void toLowerCase(string &str);
static void saveHdrParams(string strFileName, FltHdr hdr);


};

#endif // CAVITYFILLMAIN_H
