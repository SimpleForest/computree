<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>QObject</name>
    <message>
        <location filename="../seg_pluginmanager.cpp" line="48"/>
        <location filename="../seg_pluginmanager.cpp" line="49"/>
        <location filename="../seg_pluginmanager.cpp" line="50"/>
        <location filename="../seg_pluginmanager.cpp" line="51"/>
        <location filename="../seg_pluginmanager.cpp" line="53"/>
        <location filename="../seg_pluginmanager.cpp" line="54"/>
        <location filename="../seg_pluginmanager.cpp" line="55"/>
        <location filename="../seg_pluginmanager.cpp" line="56"/>
        <location filename="../seg_pluginmanager.cpp" line="58"/>
        <location filename="../seg_pluginmanager.cpp" line="59"/>
        <source>SEGMA</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SEG_MetricRasterSegma</name>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="49"/>
        <source>Métriques SEGMA</source>
        <translation>SEGMA Metrics</translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="54"/>
        <source>Les valeurs suivantes sont calculées :&lt;br&gt;- MinHeight&lt;br&gt;- MaxHeight&lt;br&gt;- MaxHeight_X&lt;br&gt;- MaxHeight_Y&lt;br&gt;- CrownArea&lt;br&gt;- CentroidX&lt;br&gt;- CentroidY&lt;br&gt;- Eccentricity&lt;br&gt;- Solidity&lt;br&gt;- HtoAratio&lt;br&gt;- CVmax&lt;br&gt;- CentroidShift&lt;br&gt;- Vextent&lt;br&gt;- CrRatio&lt;br&gt;- Diameter&lt;br&gt;- Circularity&lt;br&gt;</source>
        <translation>Following values are computed:&lt;br&gt;- MinHeight&lt;br&gt;- MaxHeight&lt;br&gt;- MaxHeight_X&lt;br&gt;- MaxHeight_Y&lt;br&gt;- CrownArea&lt;br&gt;- CentroidX&lt;br&gt;- CentroidY&lt;br&gt;- Eccentricity&lt;br&gt;- Solidity&lt;br&gt;- HtoAratio&lt;br&gt;- CVmax&lt;br&gt;- CentroidShift&lt;br&gt;- Vextent&lt;br&gt;- CrRatio&lt;br&gt;- Diameter&lt;br&gt;- Circularity&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="270"/>
        <source>MinHeight</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="271"/>
        <source>MaxHeight</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="272"/>
        <source>MaxHeight_X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="273"/>
        <source>MaxHeight_Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="274"/>
        <source>CrownArea</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="275"/>
        <source>CentroidX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="276"/>
        <source>CentroidY</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="277"/>
        <source>Eccentricity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="278"/>
        <source>Solidity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="279"/>
        <source>HtoAratio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="280"/>
        <source>CVmax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="281"/>
        <source>CentroidShift</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="282"/>
        <source>Vextent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="283"/>
        <source>CrRatio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="284"/>
        <source>Diameter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../metric/seg_metricrastersegma.cpp" line="285"/>
        <source>Circularity</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SEG_StepAnalyzeAndFitCrowns</name>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="37"/>
        <source>7- Analyser / Rogner les couronnes</source>
        <translation>7- Analyze/Crop crowns</translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="43"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="64"/>
        <source>Couronnes</source>
        <translation>Crowns</translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="66"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="67"/>
        <source>Clusters</source>
        <translation>Clusters</translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="68"/>
        <source>Couronne</source>
        <translation>Crown</translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="69"/>
        <source>Image (hauteurs)</source>
        <translation>Image (heights)</translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="70"/>
        <source>ID Cluster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="71"/>
        <source>Masque</source>
        <translation>Mask</translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="81"/>
        <source>Clusters modifiés</source>
        <translation>Modified clusters</translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="82"/>
        <source>Image Modifiée</source>
        <translation>Modified image</translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="83"/>
        <source>Masque Modifié</source>
        <translation>Modified mask</translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="85"/>
        <source>Attributs des couronnes</source>
        <translation>Crowns attributes</translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="86"/>
        <source>IDCluster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="87"/>
        <source>MaxHeight</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="88"/>
        <source>XMaxHeight</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="89"/>
        <source>YMaxHeight</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="92"/>
        <source>XCentroid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="93"/>
        <source>YCentroid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="104"/>
        <source>ScoreCentroidShift</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="105"/>
        <source>ScoreEccentricity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="106"/>
        <source>ScoreSolidity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="107"/>
        <source>ScoreCVmax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="108"/>
        <source>Score</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="90"/>
        <source>MinHeight</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="91"/>
        <source>CrownArea</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="94"/>
        <source>Eccentricity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="95"/>
        <source>Solidity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="96"/>
        <source>HtoAratio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="97"/>
        <source>CVmax</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="98"/>
        <source>CentroidShift</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="99"/>
        <source>Vextent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="100"/>
        <source>CrRatio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="101"/>
        <source>Diameter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="102"/>
        <source>Circularity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="109"/>
        <source>OtsuThreshold</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="118"/>
        <source>Seuil en-dessous duquel la couronne est amputée</source>
        <translation>Threshold under which crown is cropped</translation>
    </message>
    <message>
        <location filename="../step/seg_stepanalyzeandfitcrowns.cpp" line="119"/>
        <source>Résolution du profil pour le seuillage d&apos;OTSU</source>
        <translation>Resolution of profile for OTSU thresholding</translation>
    </message>
</context>
<context>
    <name>SEG_StepCavityFill</name>
    <message>
        <location filename="../step/seg_stepcavityfill.cpp" line="39"/>
        <source>1- Remplir les trous</source>
        <translation>1- Fill cavities</translation>
    </message>
    <message>
        <location filename="../step/seg_stepcavityfill.cpp" line="45"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepcavityfill.cpp" line="68"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/seg_stepcavityfill.cpp" line="69"/>
        <source>Raster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepcavityfill.cpp" line="78"/>
        <source>Raster filled</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepcavityfill.cpp" line="87"/>
        <source>Rayon du filtre Laplacien</source>
        <translation>Radius for Laplacian filter</translation>
    </message>
    <message>
        <location filename="../step/seg_stepcavityfill.cpp" line="88"/>
        <source>Seuil sur le Laplacien</source>
        <translation>Threshold for Laplacian filter</translation>
    </message>
    <message>
        <location filename="../step/seg_stepcavityfill.cpp" line="89"/>
        <source>Taille du filtre médian</source>
        <translation>Median filter size</translation>
    </message>
    <message>
        <location filename="../step/seg_stepcavityfill.cpp" line="90"/>
        <source>Dilatation des cavités</source>
        <translation>Cavities dilatation</translation>
    </message>
    <message>
        <location filename="../step/seg_stepcavityfill.cpp" line="91"/>
        <source>Hauteur maximale</source>
        <translation>Maximum height</translation>
    </message>
</context>
<context>
    <name>SEG_StepComputeWatershed</name>
    <message>
        <location filename="../step/seg_stepcomputewatershed.cpp" line="33"/>
        <source>5- Watershed (flooding)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepcomputewatershed.cpp" line="39"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepcomputewatershed.cpp" line="60"/>
        <source>Image 2D</source>
        <translation>2D image</translation>
    </message>
    <message>
        <location filename="../step/seg_stepcomputewatershed.cpp" line="62"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/seg_stepcomputewatershed.cpp" line="63"/>
        <source>Image (hauteurs)</source>
        <translation>Image (heights)</translation>
    </message>
    <message>
        <location filename="../step/seg_stepcomputewatershed.cpp" line="64"/>
        <source>Maxima</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../step/seg_stepcomputewatershed.cpp" line="66"/>
        <location filename="../step/seg_stepcomputewatershed.cpp" line="69"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/seg_stepcomputewatershed.cpp" line="68"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepcomputewatershed.cpp" line="81"/>
        <source>Watershed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepcomputewatershed.cpp" line="89"/>
        <source>Ne pas affecter les pixels d&apos;une valeur inférieure à</source>
        <translation>Don&apos;t accept pixels with value inferior to</translation>
    </message>
    <message>
        <location filename="../step/seg_stepcomputewatershed.cpp" line="90"/>
        <source>Affecter les limites à un cluster</source>
        <translation>Assign limits to clusters</translation>
    </message>
</context>
<context>
    <name>SEG_StepDetectMaxima</name>
    <message>
        <location filename="../step/seg_stepdetectmaxima.cpp" line="35"/>
        <source>3- Détecter les maxima</source>
        <translation>3- Detect maximum</translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima.cpp" line="41"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima.cpp" line="62"/>
        <source>Image 2D</source>
        <translation>2D image</translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima.cpp" line="64"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima.cpp" line="65"/>
        <source>Image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima.cpp" line="67"/>
        <location filename="../step/seg_stepdetectmaxima.cpp" line="70"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima.cpp" line="69"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima.cpp" line="82"/>
        <source>Maxima</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima.cpp" line="90"/>
        <source>Ne pas détécter de maxima en dessous de</source>
        <translation>Don&apos;t detect maximum under</translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima.cpp" line="90"/>
        <source>m</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SEG_StepDetectMaxima02</name>
    <message>
        <location filename="../step/seg_stepdetectmaxima02.cpp" line="36"/>
        <source>3- Détecter les maxima (v2)</source>
        <translation>3- Detect maxima (v2)</translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima02.cpp" line="42"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima02.cpp" line="63"/>
        <source>Image 2D</source>
        <translation>2D image</translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima02.cpp" line="65"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima02.cpp" line="66"/>
        <source>Image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima02.cpp" line="68"/>
        <location filename="../step/seg_stepdetectmaxima02.cpp" line="71"/>
        <source>MNT</source>
        <translation>DTM</translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima02.cpp" line="70"/>
        <source>Group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima02.cpp" line="83"/>
        <source>Maxima</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima02.cpp" line="91"/>
        <source>Ne pas détécter de maxima en dessous de</source>
        <translation>Don&apos;t detect maximum under</translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima02.cpp" line="91"/>
        <location filename="../step/seg_stepdetectmaxima02.cpp" line="92"/>
        <source>m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima02.cpp" line="92"/>
        <source>Distance de recherche</source>
        <translation>Search distance</translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima02.cpp" line="95"/>
        <source>Cercle</source>
        <translation>Circle</translation>
    </message>
    <message>
        <location filename="../step/seg_stepdetectmaxima02.cpp" line="96"/>
        <source>Rectangle</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SEG_StepExtractPointsByCluster</name>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster.cpp" line="34"/>
        <source>8- Extraire les points par couronne</source>
        <translation>8- Extract points for each crown</translation>
    </message>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster.cpp" line="40"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster.cpp" line="61"/>
        <location filename="../step/seg_stepextractpointsbycluster.cpp" line="64"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster.cpp" line="63"/>
        <location filename="../step/seg_stepextractpointsbycluster.cpp" line="69"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster.cpp" line="67"/>
        <source>Image 2D</source>
        <translation>2D image</translation>
    </message>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster.cpp" line="70"/>
        <source>Masque</source>
        <translation>Mask</translation>
    </message>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster.cpp" line="79"/>
        <source>Scène extraite</source>
        <translation>Extracted scene</translation>
    </message>
</context>
<context>
    <name>SEG_StepExtractPointsByCluster02</name>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster02.cpp" line="37"/>
        <source>8- Extraire les points par couronne (v2)</source>
        <translation>8- Extract points for each crown (v2)</translation>
    </message>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster02.cpp" line="43"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster02.cpp" line="64"/>
        <location filename="../step/seg_stepextractpointsbycluster02.cpp" line="67"/>
        <source>Scène(s)</source>
        <translation>Scene(s)</translation>
    </message>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster02.cpp" line="66"/>
        <location filename="../step/seg_stepextractpointsbycluster02.cpp" line="72"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster02.cpp" line="70"/>
        <source>Couronnes</source>
        <translation>Crowns</translation>
    </message>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster02.cpp" line="73"/>
        <source>Couronnes segmentées</source>
        <translation>Segmented crowns</translation>
    </message>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster02.cpp" line="74"/>
        <source>Couronne</source>
        <translation>Crown</translation>
    </message>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster02.cpp" line="75"/>
        <source>Item</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster02.cpp" line="76"/>
        <source>ID Cluster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepextractpointsbycluster02.cpp" line="85"/>
        <source>Scène extraite</source>
        <translation>Extracted scene</translation>
    </message>
</context>
<context>
    <name>SEG_StepFilterMaximaByClusterArea</name>
    <message>
        <location filename="../step/seg_stepfiltermaximabyclusterarea.cpp" line="32"/>
        <source>Filtrer les maxima en fonction de l&apos;aire des clusters</source>
        <translation>Filter maxima in clusters areas</translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyclusterarea.cpp" line="38"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyclusterarea.cpp" line="59"/>
        <location filename="../step/seg_stepfiltermaximabyclusterarea.cpp" line="63"/>
        <source>Maxima</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyclusterarea.cpp" line="61"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyclusterarea.cpp" line="62"/>
        <source>Image (hauteurs)</source>
        <translation>Image (heights)</translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyclusterarea.cpp" line="64"/>
        <source>Clusters</source>
        <translation>Clusters</translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyclusterarea.cpp" line="74"/>
        <source>Maxima filtrés</source>
        <translation>Filtered maximum</translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyclusterarea.cpp" line="77"/>
        <source>Maxima filtrés (Pts)</source>
        <translation>Filtered maxima (Pts)</translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyclusterarea.cpp" line="78"/>
        <source>Maximum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyclusterarea.cpp" line="87"/>
        <source>Aire minimale pour garder un cluster</source>
        <translation>Minimal area to keep a cluster</translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyclusterarea.cpp" line="88"/>
        <source>Créer des points pour les maxima</source>
        <translation>Create points for maxima</translation>
    </message>
</context>
<context>
    <name>SEG_StepFilterMaximaByExclusionRadius</name>
    <message>
        <location filename="../step/seg_stepfiltermaximabyexclusionradius.cpp" line="30"/>
        <source>4- Filtrer les maxima par des rayons d&apos;exclusion</source>
        <translation>4- Filter maximum by exclusion radii</translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyexclusionradius.cpp" line="36"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyexclusionradius.cpp" line="57"/>
        <location filename="../step/seg_stepfiltermaximabyexclusionradius.cpp" line="61"/>
        <source>Maxima</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyexclusionradius.cpp" line="59"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyexclusionradius.cpp" line="60"/>
        <source>Image (hauteurs)</source>
        <translation>Image (heights)</translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyexclusionradius.cpp" line="71"/>
        <source>Maxima filtrés</source>
        <translation>Filtered maximum</translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyexclusionradius.cpp" line="74"/>
        <source>Maxima filtrés (Pts)</source>
        <translation>Filtered maxima (Pts)</translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyexclusionradius.cpp" line="75"/>
        <source>Maximum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyexclusionradius.cpp" line="84"/>
        <source>Fichier de paramètres</source>
        <translation>Parameters file</translation>
    </message>
    <message>
        <location filename="../step/seg_stepfiltermaximabyexclusionradius.cpp" line="85"/>
        <source>Créer des points pour les maxima</source>
        <translation>Create points for maxima</translation>
    </message>
</context>
<context>
    <name>SEG_StepGaussianFilter</name>
    <message>
        <location filename="../step/seg_stepgaussianfilter.cpp" line="28"/>
        <source>2- Filtre Gaussien</source>
        <translation>2- Gaussian filter</translation>
    </message>
    <message>
        <location filename="../step/seg_stepgaussianfilter.cpp" line="34"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepgaussianfilter.cpp" line="55"/>
        <source>Image 2D</source>
        <translation>2D image</translation>
    </message>
    <message>
        <location filename="../step/seg_stepgaussianfilter.cpp" line="57"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/seg_stepgaussianfilter.cpp" line="58"/>
        <source>Image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepgaussianfilter.cpp" line="68"/>
        <source>Image filtrée</source>
        <translation>Filtered image</translation>
    </message>
    <message>
        <location filename="../step/seg_stepgaussianfilter.cpp" line="77"/>
        <source>Sigma (Ecart-type)</source>
        <translation>Sigma (Standard error)</translation>
    </message>
    <message>
        <location filename="../step/seg_stepgaussianfilter.cpp" line="77"/>
        <source>en mètres</source>
        <translation>in meters</translation>
    </message>
    <message>
        <location filename="../step/seg_stepgaussianfilter.cpp" line="79"/>
        <source>N.B. : Portée du filtre = 7.7 x Sigma (en mètres)</source>
        <translation>N.B.: Range of the filter = 7.7 x Sigma (in meters)</translation>
    </message>
</context>
<context>
    <name>SEG_StepReplaceNAByZero</name>
    <message>
        <location filename="../step/seg_stepreplacenabyzero.cpp" line="33"/>
        <source>0- Remplacer les valeurs NA par Zéro</source>
        <translation>0- Replace NA values by Zero</translation>
    </message>
    <message>
        <location filename="../step/seg_stepreplacenabyzero.cpp" line="39"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepreplacenabyzero.cpp" line="62"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/seg_stepreplacenabyzero.cpp" line="63"/>
        <source>Raster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepreplacenabyzero.cpp" line="72"/>
        <source>Modified Raster</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SEG_StepSeparateClusters</name>
    <message>
        <location filename="../step/seg_stepseparateclusters.cpp" line="30"/>
        <source>6-  Créer des rasters par couronne</source>
        <translation>6- Create rasters for each crown</translation>
    </message>
    <message>
        <location filename="../step/seg_stepseparateclusters.cpp" line="36"/>
        <source>No detailled description for this step</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepseparateclusters.cpp" line="57"/>
        <source>Image 2D</source>
        <translation>2D image</translation>
    </message>
    <message>
        <location filename="../step/seg_stepseparateclusters.cpp" line="59"/>
        <source>Groupe</source>
        <translation>Group</translation>
    </message>
    <message>
        <location filename="../step/seg_stepseparateclusters.cpp" line="60"/>
        <source>Clusters</source>
        <translation>Clusters</translation>
    </message>
    <message>
        <location filename="../step/seg_stepseparateclusters.cpp" line="61"/>
        <source>Raster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepseparateclusters.cpp" line="70"/>
        <source>Clusters isolés (grp)</source>
        <translation>Isolated clusters (grp)</translation>
    </message>
    <message>
        <location filename="../step/seg_stepseparateclusters.cpp" line="71"/>
        <source>Cluster isolé</source>
        <translation>Isolated cluster</translation>
    </message>
    <message>
        <location filename="../step/seg_stepseparateclusters.cpp" line="72"/>
        <source>IDcluster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../step/seg_stepseparateclusters.cpp" line="73"/>
        <source>Masque</source>
        <translation>Mask</translation>
    </message>
</context>
</TS>
