#ifndef SEGMENTATION_H
#define SEGMENTATION_H

#include "point.h"
#include "param.h"
#include "forest.h"


#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_grid3d_points.h"
#include "ct_itemdrawable/ct_polygon2d.h"
#include "ct_shape2ddata/ct_polygon2ddata.h"

#include <QList>

class Segmentation
{
public:
    Segmentation(CT_AbstractItemDrawableWithPointCloud *scene, Param &param);


    Forest *segmPPL(int k);
    Forest* segmMultiScale();
    Forest *segmFinale(const QList<Tree *> &keptTrees);

    void createAllDualCombinations(const QList<Tree *> &in, QList<QList<Tree *> > &out, QList<Tree *> &toDelete);
    void getSegmentedScenes(QList<CT_Scene *> &segmentedScenes,
                            QList<CT_Polygon2D *> &segmentedHulls,
                            CT_AbstractResult* result,
                            QString sceneModelName,
                            QString chModelName,
                            QString score_size_ModelName,
                            QString score_regularity_ModelName,
                            QString score_solidity_ModelName,
                            QString score_orientation_ModelName,
                            QString score_ModelName,
                            QString k_ModelName);

    static double addNewPointToHullAndComputeArea(CT_Polygon2DData *hull, double x, double y, size_t nPts);

    void mergeIncludedHulls(Forest* forest);
    
    void createAllNCombinations(int n, const QList<Tree *> &in, QList<QList<Tree *> > &out, QList<Tree *> &toDelete);
    bool incrementCounter(QVector<int> &counter, int maxIndex);
    bool updateCounter(QVector<int> &counter, int maxIndex);    
    bool isCombinationValid(const QList<Tree *> &combination);
    bool createCombination(QList<Tree *> &parentCombination, QList<QList<Tree *> > &combinations, QList<Tree *> &toDelete);
private:
    QList<Point*>       _points;
    CT_Grid3D_Points*   _grid;
    Param*              _param;


};

#endif // SEGMENTATION_H
