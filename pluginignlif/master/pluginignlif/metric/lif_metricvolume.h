/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef LIF_METRICVOLUME_H
#define LIF_METRICVOLUME_H

#include "ctlibmetrics/ct_metric/abstract/ct_abstractmetric_raster.h"
#include "ctlibmetrics/tools/ct_valueandbool.h"

class LIF_MetricVolume : public CT_AbstractMetric_Raster
{
    Q_OBJECT
public:

    struct Config {
        VaB<double>      Vi;
        VaB<double>      Vi_mean;
        VaB<double>      Vo;
        VaB<double>      Vo_mean;
        VaB<double>      VCi;
        VaB<double>      VCi_mean;
        VaB<double>      VCo;
        VaB<double>      VCo_mean;
        VaB<double>      VGi;
        VaB<double>      VGi_mean;
        VaB<double>      VGo;
        VaB<double>      VGo_mean;
        VaB<double>      NACellNB;
        VaB<double>      NotGapCellNB;
        VaB<double>      GapCellNB;
        VaB<double>      GapArea;
        VaB<double>      Hmean;
        VaB<double>      Hvar;
        VaB<double>      Hstd;
        VaB<double>      Hmad;
        VaB<double>      Hmin;
        VaB<double>      Hq00;
        VaB<double>      Hq10;
        VaB<double>      Hq20;
        VaB<double>      Hq30;
        VaB<double>      Hq40;
        VaB<double>      Hmed;
        VaB<double>      Hq60;
        VaB<double>      Hq70;
        VaB<double>      Hq80;
        VaB<double>      Hq90;
        VaB<double>      Hmax;
        VaB<double>      Hq95;
        VaB<double>      Hq99;
        VaB<double>      RumpleTh;
        VaB<double>      Rumple0;
        VaB<double>      RumpleTh_sp;
        VaB<double>      Rumple0_sp;
        VaB<double>      RumpleTh_sp_NA;
        VaB<double>      Rumple0_sp_NA;
    };        

    LIF_MetricVolume();
    LIF_MetricVolume(const LIF_MetricVolume &other);

    QString getShortDescription() const;
    QString getDetailledDescription() const;

    SettingsNodeGroup* getAllSettings() const;
    bool setAllSettings(const SettingsNodeGroup *settings);
    CT_AbstractConfigurableWidget* createConfigurationWidget();


    /**
     * @brief Returns the metric configuration
     */
    LIF_MetricVolume::Config metricConfiguration() const;

    /**
     * @brief Change the configuration of this metric
     */
    void setMetricConfiguration(const LIF_MetricVolume::Config &conf);

    CT_AbstractConfigurableElement* copy() const;


protected:
    void computeMetric();
    void declareAttributes();

private:
    Config  m_configAndResults;
    double _gap_threshold;

    double computeSlope(double val, size_t yy, size_t xx, double inNA);
    double computeArea(size_t yy, size_t xx, double inNA, bool useGap);
    double aire_triang(double a, double b, double c);

};


#endif // LIF_METRICVOLUME_H
