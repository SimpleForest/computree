/****************************************************************************
 Copyright (C) 2010-2017 IGN
                         All rights reserved.

 Contact : cedric.vega@ign.fr

 Developers : Cédric Véga
              with some help of Maryem Fadidi and Alexandre Piboule

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "metric/lif_metricvolume.h"

#include "ct_math/ct_mathstatistics.h"

#include <QDebug>

#define checkAndSetValue(ATT, NAME, TYPE) if((value = group->firstValueByTagName(NAME)) == NULL) { return false; } else { ATT = value->value().value<TYPE>(); }


LIF_MetricVolume::LIF_MetricVolume() : CT_AbstractMetric_Raster()
{
    declareAttributes();
    _gap_threshold = 2.0;
}

LIF_MetricVolume::LIF_MetricVolume(const LIF_MetricVolume &other) : CT_AbstractMetric_Raster(other)
{
    declareAttributes();
    m_configAndResults = other.m_configAndResults;
    _gap_threshold = other._gap_threshold;
}

QString LIF_MetricVolume::getShortDescription() const
{
    return tr("Métriques volume");
}

QString LIF_MetricVolume::getDetailledDescription() const
{

    return tr("Les valeurs suivantes sont calculées :<br>"
              "- Vi<br>"
              "- Vi_mean<br>"
              "- Vo<br>"
              "- Vo_mean<br>"
              "- VCi<br>"
              "- VCi_mean<br>"
              "- VCo<br>"
              "- VCo_mean<br>"
              "- VGi<br>"
              "- VGi_mean<br>"
              "- VGo<br>"
              "- VGo_mean<br>"
              "- NACellNB<br>"
              "- NotGapCellNB<br>"
              "- GapCellNB<br>"
              "- GapArea<br>"
              "- Hmean<br>"
              "- Hvar<br>"
              "- Hstd<br>"
              "- Hmad<br>"
              "- Hmin<br>"
              "- Hq00<br>"
              "- Hq10<br>"
              "- Hq20<br>"
              "- Hq30<br>"
              "- Hq40<br>"
              "- Hmed<br>"
              "- Hq60<br>"
              "- Hq70<br>"
              "- Hq80<br>"
              "- Hq90<br>"
              "- Hq95<br>"
              "- Hq99<br>"
              "- Hmax<br>"
              "- RumpleTh<br>"
              "- Rumple0<br>"
              "- RumpleTh_triangle<br>"
              "- Rumple0_triangle<br>"
              "- RumpleTh_sp<br>"
              "- Rumple0_sp<br>"
              );
}

CT_AbstractConfigurableWidget* LIF_MetricVolume::createConfigurationWidget()
{
    CT_GenericConfigurableWidget* configDialog = new CT_GenericConfigurableWidget();

    configDialog->addDouble(tr("Seuil de hauteur max pour les trouées"), "m", 0, 99999, 2, _gap_threshold);
    configDialog->addEmpty();

    addAllVaBToWidget(configDialog);

    return configDialog;
}

SettingsNodeGroup *LIF_MetricVolume::getAllSettings() const
{
    SettingsNodeGroup *root = CT_AbstractMetric_Raster::getAllSettings();
    SettingsNodeGroup *group = new SettingsNodeGroup("LIF_MetricVolume");

    group->addValue(new SettingsNodeValue("hMin", _gap_threshold));

    return root;
}

bool LIF_MetricVolume::setAllSettings(const SettingsNodeGroup *settings)
{
    if(!CT_AbstractMetric_Raster::setAllSettings(settings))
        return false;

    SettingsNodeGroup *group = settings->firstGroupByTagName("LIF_MetricVolume");

    if(group == NULL)
        return false;

    SettingsNodeValue *value = NULL;
    checkAndSetValue(_gap_threshold, "hMin", double)

            return true;
}



LIF_MetricVolume::Config LIF_MetricVolume::metricConfiguration() const
{
    return m_configAndResults;
}

void LIF_MetricVolume::setMetricConfiguration(const LIF_MetricVolume::Config &conf)
{
    m_configAndResults = conf;
}

CT_AbstractConfigurableElement *LIF_MetricVolume::copy() const
{
    return new LIF_MetricVolume(*this);
}


void LIF_MetricVolume::computeMetric()
{   
    m_configAndResults.Vi.value = 0;
    m_configAndResults.Vi_mean.value = 0;
    m_configAndResults.Vo.value = -std::numeric_limits<double>::max();
    m_configAndResults.Vo_mean.value = 0;
    m_configAndResults.VCi.value = 0;
    m_configAndResults.VCi_mean.value = 0;
    m_configAndResults.VCo.value = -std::numeric_limits<double>::max();
    m_configAndResults.VCo_mean.value = 0;
    m_configAndResults.VGi.value = 0;
    m_configAndResults.VGi_mean.value = 0;
    m_configAndResults.VGo.value = -std::numeric_limits<double>::max();
    m_configAndResults.VGo_mean.value = 0;

    m_configAndResults.NACellNB.value = 0;
    m_configAndResults.NotGapCellNB.value = 0;
    m_configAndResults.GapCellNB.value = 0;
    m_configAndResults.GapArea.value = 0;

    m_configAndResults.RumpleTh.value = 0;
    m_configAndResults.Rumple0.value = 0;
    m_configAndResults.RumpleTh_sp.value = 0;
    m_configAndResults.Rumple0_sp.value = 0;
    m_configAndResults.RumpleTh_sp_NA.value = 0;
    m_configAndResults.Rumple0_sp_NA.value = 0;

    m_configAndResults.Hmean.value = 0;
    m_configAndResults.Hvar.value = 0;
    m_configAndResults.Hstd.value = 0;
    m_configAndResults.Hmad.value = 0;


    m_configAndResults.Hmin.value = std::numeric_limits<double>::max();
    m_configAndResults.Hq00.value = 0;
    m_configAndResults.Hq10.value = 0;
    m_configAndResults.Hq20.value = 0;
    m_configAndResults.Hq30.value = 0;
    m_configAndResults.Hq40.value = 0;
    m_configAndResults.Hmed.value = 0;
    m_configAndResults.Hq60.value = 0;
    m_configAndResults.Hq70.value = 0;
    m_configAndResults.Hq80.value = 0;
    m_configAndResults.Hq90.value = 0;
    m_configAndResults.Hq95.value = 0;
    m_configAndResults.Hq99.value = 0;
    m_configAndResults.Hmax.value = -std::numeric_limits<double>::max();

    double n_na = 0.0;
    double n_nogap = 0.0;
    double n_gap = 0.0;
    double sdsum = 0.0;
    double inNA = _inRaster->NAAsDouble();
    double pixelArea = _inRaster->resolution()*_inRaster->resolution();
    double nbpix_Rumple0 = 0.0;
    double nbpix_RumpleTh = 0.0;

    QList<double> values;

    for (size_t index = 0 ; index < _inRaster->nCells() ; index++)
    {
        double val = _inRaster->valueAtIndexAsDouble(index);
        size_t xx, yy;



        if (qFuzzyCompare(val, inNA))
        {
            // Count NA
            n_na += 1.0;
        } else
        {
            if (_inRaster->indexToGrid(index, xx, yy))
            {
                //Volumes (without considering gap threshold)
                m_configAndResults.Vi.value += val;
                if (val > m_configAndResults.Vo.value) {m_configAndResults.Vo.value = val;}

                // Slope0
                double slope0 = computeSlope(val, yy, xx, inNA);
                // Rumple0
                if(!isnan(slope0)){
                    m_configAndResults.Rumple0.value += std::sqrt(pow(_inRaster->resolution()*slope0, 2) + pixelArea) * _inRaster->resolution();
                    nbpix_Rumple0 += 1.0;
                }

                double s = computeArea(yy, xx, inNA, false);
                m_configAndResults.Rumple0_sp.value += s;

                double sp = computeArea(yy, xx, inNA, true);
                m_configAndResults.Rumple0_sp_NA.value += sp;

                if (val >= _gap_threshold)
                {
                    //Volumes above gap threshold
                    m_configAndResults.VCi.value += val;
                    if (val > m_configAndResults.VCo.value) {m_configAndResults.VCo.value = val;}

                    // Count not NA and not Gap pixels
                    n_nogap += 1.0;

                    // For quantile computing
                    values.append(val);

                    // For Hmean, Hstd and Hvar computing
                    m_configAndResults.Hmean.value += val;
                    sdsum += val*val;

                    // RumpleTh

                    if(!isnan(slope0)){
                        m_configAndResults.RumpleTh.value += std::sqrt(pow(_inRaster->resolution()*slope0, 2) + pixelArea) * _inRaster->resolution();
                        nbpix_RumpleTh += 1.0;
                    }
                    m_configAndResults.RumpleTh_sp.value += s;
                    m_configAndResults.RumpleTh_sp_NA.value += sp;



                } else {
                    // Count not NA but Gap pixels
                    n_gap += 1.0;

                    //Volumes below gap threshold
                    m_configAndResults.VGi.value += val;
                }

                // Min and Max heights
                if (val > m_configAndResults.Hmax.value) {m_configAndResults.Hmax.value = val;}
                if (val < m_configAndResults.Hmin.value && val > 0.0) {m_configAndResults.Hmin.value = val;}
                if (val > m_configAndResults.VGo.value) {m_configAndResults.VGo.value = val;}

            }
        }
    }


    if ((n_nogap + n_gap) > 0)
    {
        // Rumple0 finalization
        m_configAndResults.Rumple0.value /= (nbpix_Rumple0)*pixelArea;
        m_configAndResults.Rumple0_sp.value /= (n_nogap + n_gap)*pixelArea;
        m_configAndResults.Rumple0_sp_NA.value /= (n_nogap + n_gap)*pixelArea;

        // Complete volumes finalization
        m_configAndResults.Vi.value = m_configAndResults.Vi.value * pixelArea;
        m_configAndResults.Vi_mean.value = m_configAndResults.Vi.value / ((n_nogap + n_gap) *1/pixelArea);

        m_configAndResults.Vo.value = m_configAndResults.Vo.value * (n_nogap + n_gap) * pixelArea - m_configAndResults.Vi.value;
        m_configAndResults.Vo_mean.value = m_configAndResults.Vo.value / ((n_nogap + n_gap)*1/pixelArea);
    } else {
        m_configAndResults.Vo.value = 0;
    }

    if (n_nogap > 0)
    {
        // RumpleTh finalization
        m_configAndResults.RumpleTh.value /= nbpix_RumpleTh*pixelArea;
        m_configAndResults.RumpleTh_sp.value /= n_nogap*pixelArea;
        m_configAndResults.RumpleTh_sp_NA.value /= n_nogap*pixelArea;

        // Above gap threshold volumes finalization
        m_configAndResults.VCi.value = m_configAndResults.VCi.value * pixelArea;
        m_configAndResults.VCi_mean.value = m_configAndResults.VCi.value / (n_nogap*1/pixelArea);

        m_configAndResults.VCo.value = m_configAndResults.VCo.value * n_nogap * pixelArea - m_configAndResults.VCi.value;
        m_configAndResults.VCo_mean.value = m_configAndResults.VCo.value / (n_nogap*1/pixelArea);
    } else {
        m_configAndResults.VCo.value = 0;
    }

    if (n_gap > 0)
    {
        // Below gap threshold volumes finalization
        m_configAndResults.VGi.value = m_configAndResults.VGi.value * pixelArea;
        m_configAndResults.VGi_mean.value = m_configAndResults.VGi.value / (n_gap/pixelArea);

        m_configAndResults.VGo.value = m_configAndResults.VGo.value * n_gap * pixelArea - m_configAndResults.VGi.value;
        m_configAndResults.VGo_mean.value = m_configAndResults.VGo.value / (n_gap/pixelArea);
    } else {
        m_configAndResults.VGo.value = 0;
    }


    // Counts and Gaps
    m_configAndResults.NACellNB.value = n_na;
    m_configAndResults.NotGapCellNB.value = n_nogap;
    m_configAndResults.GapCellNB.value = n_gap;
    m_configAndResults.GapArea.value = n_gap * _inRaster->resolution() * _inRaster->resolution();


    // Quantiles and finalization of Hmax and Hmin
    m_configAndResults.Hq00.value = CT_MathStatistics::computeQuantile(values, 0.0, true);
    m_configAndResults.Hq10.value = CT_MathStatistics::computeQuantile(values, 0.10, false);
    m_configAndResults.Hq20.value = CT_MathStatistics::computeQuantile(values, 0.20, false);
    m_configAndResults.Hq30.value = CT_MathStatistics::computeQuantile(values, 0.30, false);
    m_configAndResults.Hq40.value = CT_MathStatistics::computeQuantile(values, 0.40, false);
    m_configAndResults.Hmed.value = CT_MathStatistics::computeQuantile(values, 0.50, false);
    m_configAndResults.Hq60.value = CT_MathStatistics::computeQuantile(values, 0.60, false);
    m_configAndResults.Hq70.value = CT_MathStatistics::computeQuantile(values, 0.70, false);
    m_configAndResults.Hq80.value = CT_MathStatistics::computeQuantile(values, 0.80, false);
    m_configAndResults.Hq90.value = CT_MathStatistics::computeQuantile(values, 0.90, false);
    m_configAndResults.Hq95.value = CT_MathStatistics::computeQuantile(values, 0.95, false);
    m_configAndResults.Hq99.value = CT_MathStatistics::computeQuantile(values, 0.99, false);
    if (qFuzzyCompare(m_configAndResults.Hmin.value, std::numeric_limits<double>::max())) {m_configAndResults.Hmin.value = std::numeric_limits<double>::quiet_NaN(); }
    if (qFuzzyCompare(m_configAndResults.Hmax.value, -std::numeric_limits<double>::max())) {m_configAndResults.Hmax.value = std::numeric_limits<double>::quiet_NaN();}


    // Other height indicators
    if (n_nogap > 0)
    {
        m_configAndResults.Hmean.value /= n_nogap;
        m_configAndResults.Hstd.value = sqrt(sdsum/n_nogap - pow(m_configAndResults.Hmean.value, 2));
        m_configAndResults.Hvar.value = std::pow(m_configAndResults.Hstd.value, 2);

        QList<double> madValues;
        for (int i = 0 ; i < values.size() ; i++)
        {
            madValues.append(std::abs(values.at(i) - m_configAndResults.Hmed.value));
        }
        m_configAndResults.Hmad.value = CT_MathStatistics::computeQuantile(madValues, 0.50, true);
    } else
    {
        m_configAndResults.Hmean.value = std::numeric_limits<double>::quiet_NaN();
        m_configAndResults.Hstd.value = std::numeric_limits<double>::quiet_NaN();
        m_configAndResults.Hvar.value = std::numeric_limits<double>::quiet_NaN();
        m_configAndResults.Hmad.value = std::numeric_limits<double>::quiet_NaN();
    }


    setAttributeValueVaB(m_configAndResults.Vi);
    setAttributeValueVaB(m_configAndResults.Vi_mean);
    setAttributeValueVaB(m_configAndResults.Vo);
    setAttributeValueVaB(m_configAndResults.Vo_mean);
    setAttributeValueVaB(m_configAndResults.VCi);
    setAttributeValueVaB(m_configAndResults.VCi_mean);
    setAttributeValueVaB(m_configAndResults.VCo);
    setAttributeValueVaB(m_configAndResults.VCo_mean);
    setAttributeValueVaB(m_configAndResults.VGi);
    setAttributeValueVaB(m_configAndResults.VGi_mean);
    setAttributeValueVaB(m_configAndResults.VGo);
    setAttributeValueVaB(m_configAndResults.VGo_mean);
    setAttributeValueVaB(m_configAndResults.NACellNB);
    setAttributeValueVaB(m_configAndResults.NotGapCellNB);
    setAttributeValueVaB(m_configAndResults.GapCellNB);
    setAttributeValueVaB(m_configAndResults.GapArea);
    setAttributeValueVaB(m_configAndResults.Hmean);
    setAttributeValueVaB(m_configAndResults.Hvar);
    setAttributeValueVaB(m_configAndResults.Hstd);
    setAttributeValueVaB(m_configAndResults.Hmad);
    setAttributeValueVaB(m_configAndResults.Hmin);
    setAttributeValueVaB(m_configAndResults.Hq00);
    setAttributeValueVaB(m_configAndResults.Hq10);
    setAttributeValueVaB(m_configAndResults.Hq20);
    setAttributeValueVaB(m_configAndResults.Hq30);
    setAttributeValueVaB(m_configAndResults.Hq40);
    setAttributeValueVaB(m_configAndResults.Hmed);
    setAttributeValueVaB(m_configAndResults.Hq60);
    setAttributeValueVaB(m_configAndResults.Hq70);
    setAttributeValueVaB(m_configAndResults.Hq80);
    setAttributeValueVaB(m_configAndResults.Hq90);
    setAttributeValueVaB(m_configAndResults.Hq95);
    setAttributeValueVaB(m_configAndResults.Hq99);
    setAttributeValueVaB(m_configAndResults.Hmax);
    setAttributeValueVaB(m_configAndResults.RumpleTh);
    setAttributeValueVaB(m_configAndResults.Rumple0);
    setAttributeValueVaB(m_configAndResults.RumpleTh_sp);
    setAttributeValueVaB(m_configAndResults.Rumple0_sp);
    setAttributeValueVaB(m_configAndResults.RumpleTh_sp_NA);
    setAttributeValueVaB(m_configAndResults.Rumple0_sp_NA);
}

void LIF_MetricVolume::declareAttributes()
{    
    registerAttributeVaB(m_configAndResults.Vi, CT_AbstractCategory::DATA_NUMBER, tr("Vi"));
    registerAttributeVaB(m_configAndResults.Vi_mean, CT_AbstractCategory::DATA_NUMBER, tr("Vi_mean"));
    registerAttributeVaB(m_configAndResults.Vo, CT_AbstractCategory::DATA_NUMBER, tr("Vo"));
    registerAttributeVaB(m_configAndResults.Vo_mean, CT_AbstractCategory::DATA_NUMBER, tr("Vo_mean"));
    registerAttributeVaB(m_configAndResults.VCi, CT_AbstractCategory::DATA_NUMBER, tr("VCi"));
    registerAttributeVaB(m_configAndResults.VCi_mean, CT_AbstractCategory::DATA_NUMBER, tr("VCi_mean"));
    registerAttributeVaB(m_configAndResults.VCo, CT_AbstractCategory::DATA_NUMBER, tr("VCo"));
    registerAttributeVaB(m_configAndResults.VCo_mean, CT_AbstractCategory::DATA_NUMBER, tr("VCo_mean"));
    registerAttributeVaB(m_configAndResults.VGi, CT_AbstractCategory::DATA_NUMBER, tr("VGi"));
    registerAttributeVaB(m_configAndResults.VGi_mean, CT_AbstractCategory::DATA_NUMBER, tr("VGi_mean"));
    registerAttributeVaB(m_configAndResults.VGo, CT_AbstractCategory::DATA_NUMBER, tr("VGo"));
    registerAttributeVaB(m_configAndResults.VGo_mean, CT_AbstractCategory::DATA_NUMBER, tr("VGo_mean"));
    registerAttributeVaB(m_configAndResults.NACellNB, CT_AbstractCategory::DATA_NUMBER, tr("NACellNB"));
    registerAttributeVaB(m_configAndResults.NotGapCellNB, CT_AbstractCategory::DATA_NUMBER, tr("NotGapCellNB"));
    registerAttributeVaB(m_configAndResults.GapCellNB, CT_AbstractCategory::DATA_NUMBER, tr("GapCellNB"));
    registerAttributeVaB(m_configAndResults.GapArea, CT_AbstractCategory::DATA_NUMBER, tr("GapArea"));
    registerAttributeVaB(m_configAndResults.Hmean, CT_AbstractCategory::DATA_NUMBER, tr("Hmean"));
    registerAttributeVaB(m_configAndResults.Hvar, CT_AbstractCategory::DATA_NUMBER, tr("Hvar"));
    registerAttributeVaB(m_configAndResults.Hstd, CT_AbstractCategory::DATA_NUMBER, tr("Hstd"));
    registerAttributeVaB(m_configAndResults.Hmad, CT_AbstractCategory::DATA_NUMBER, tr("Hmad"));
    registerAttributeVaB(m_configAndResults.Hmin, CT_AbstractCategory::DATA_NUMBER, tr("Hmin"));
    registerAttributeVaB(m_configAndResults.Hq00, CT_AbstractCategory::DATA_NUMBER, tr("Hq00"));
    registerAttributeVaB(m_configAndResults.Hq10, CT_AbstractCategory::DATA_NUMBER, tr("Hq10"));
    registerAttributeVaB(m_configAndResults.Hq20, CT_AbstractCategory::DATA_NUMBER, tr("Hq20"));
    registerAttributeVaB(m_configAndResults.Hq30, CT_AbstractCategory::DATA_NUMBER, tr("Hq30"));
    registerAttributeVaB(m_configAndResults.Hq40, CT_AbstractCategory::DATA_NUMBER, tr("Hq40"));
    registerAttributeVaB(m_configAndResults.Hmed, CT_AbstractCategory::DATA_NUMBER, tr("Hmed"));
    registerAttributeVaB(m_configAndResults.Hq60, CT_AbstractCategory::DATA_NUMBER, tr("Hq60"));
    registerAttributeVaB(m_configAndResults.Hq70, CT_AbstractCategory::DATA_NUMBER, tr("Hq70"));
    registerAttributeVaB(m_configAndResults.Hq80, CT_AbstractCategory::DATA_NUMBER, tr("Hq80"));
    registerAttributeVaB(m_configAndResults.Hq90, CT_AbstractCategory::DATA_NUMBER, tr("Hq90"));
    registerAttributeVaB(m_configAndResults.Hq95, CT_AbstractCategory::DATA_NUMBER, tr("Hq95"));
    registerAttributeVaB(m_configAndResults.Hq99, CT_AbstractCategory::DATA_NUMBER, tr("Hq99"));
    registerAttributeVaB(m_configAndResults.Hmax, CT_AbstractCategory::DATA_NUMBER, tr("Hmax"));
    registerAttributeVaB(m_configAndResults.RumpleTh, CT_AbstractCategory::DATA_NUMBER, tr("RumpleTh"));
    registerAttributeVaB(m_configAndResults.Rumple0, CT_AbstractCategory::DATA_NUMBER, tr("Rumple0"));
    registerAttributeVaB(m_configAndResults.RumpleTh_sp, CT_AbstractCategory::DATA_NUMBER, tr("RumpleTh_sp"));
    registerAttributeVaB(m_configAndResults.Rumple0_sp, CT_AbstractCategory::DATA_NUMBER, tr("Rumple0_sp"));
    registerAttributeVaB(m_configAndResults.RumpleTh_sp_NA, CT_AbstractCategory::DATA_NUMBER, tr("RumpleTh_sp_NA"));
    registerAttributeVaB(m_configAndResults.Rumple0_sp_NA, CT_AbstractCategory::DATA_NUMBER, tr("Rumple0_sp_NA"));
}

double LIF_MetricVolume::computeSlope(double val, size_t yy, size_t xx, double inNA)
{
    size_t index2;
    double a = inNA;
    double b = inNA;
    double c = inNA;
    double d = inNA;
    double f = inNA;
    double g = inNA;
    double h = inNA;
    double i = inNA;

    if (_inRaster->index(xx - 1, yy - 1, index2)){a = _inRaster->valueAtIndexAsDouble(index2);}
    if (_inRaster->index(xx    , yy - 1, index2)){b = _inRaster->valueAtIndexAsDouble(index2);}
    if (_inRaster->index(xx + 1, yy - 1, index2)){c = _inRaster->valueAtIndexAsDouble(index2);}
    if (_inRaster->index(xx - 1, yy    , index2)){d = _inRaster->valueAtIndexAsDouble(index2);}
    if (_inRaster->index(xx + 1, yy    , index2)){f = _inRaster->valueAtIndexAsDouble(index2);}
    if (_inRaster->index(xx - 1, yy + 1, index2)){g = _inRaster->valueAtIndexAsDouble(index2);}
    if (_inRaster->index(xx    , yy + 1, index2)){h = _inRaster->valueAtIndexAsDouble(index2);}
    if (_inRaster->index(xx + 1, yy + 1, index2)){i = _inRaster->valueAtIndexAsDouble(index2);}

    if (a == inNA) {a = val;}
    if (b == inNA) {b = val;}
    if (c == inNA) {c = val;}
    if (d == inNA) {d = val;}
    if (f == inNA) {f = val;}
    if (g == inNA) {g = val;}
    if (h == inNA) {h = val;}
    if (i == inNA) {i = val;}

    double dzdx = ((c + 2.0*f + i) - (a + 2.0*d + g)) / (8.0 * _inRaster->resolution());
    double dzdy = ((g + 2.0*h + i) - (a + 2.0*b + c)) / (8.0 * _inRaster->resolution());
    double slope = std::sqrt(dzdx*dzdx + dzdy*dzdy);
    return slope;
}

double LIF_MetricVolume::computeArea(size_t yy, size_t xx, double inNA, bool useGap)
{
    /* Calculating  surface area of a pixel base on Jenness's method, Calculating landscape surface area from digital elevation models
     Use 8 surrounding cells of the pixel to calculate his surface */

    size_t index2;
    double za= inNA;
    double zb= inNA;
    double zc= inNA;
    double zd= inNA;
    double zf= inNA;
    double zg= inNA;
    double zh= inNA;
    double zi= inNA;
    double ze= inNA;

    if (_inRaster->index(xx - 1, yy - 1, index2)){za = _inRaster->valueAtIndexAsDouble(index2);}
    if (_inRaster->index(xx    , yy - 1, index2)){zb = _inRaster->valueAtIndexAsDouble(index2);}
    if (_inRaster->index(xx + 1, yy - 1, index2)){zc = _inRaster->valueAtIndexAsDouble(index2);}
    if (_inRaster->index(xx - 1, yy    , index2)){zd = _inRaster->valueAtIndexAsDouble(index2);}
    if (_inRaster->index(xx + 1, yy    , index2)){zf = _inRaster->valueAtIndexAsDouble(index2);}
    if (_inRaster->index(xx - 1, yy + 1, index2)){zg = _inRaster->valueAtIndexAsDouble(index2);}
    if (_inRaster->index(xx    , yy + 1, index2)){zh = _inRaster->valueAtIndexAsDouble(index2);}
    if (_inRaster->index(xx + 1, yy + 1, index2)){zi = _inRaster->valueAtIndexAsDouble(index2);}
    if (_inRaster->index(xx    , yy    , index2)){ze = _inRaster->valueAtIndexAsDouble(index2);}


    if(useGap)
    {
        if (za == inNA || za< _gap_threshold) {za = ze;}
        if (zb == inNA || zb< _gap_threshold) {zb = ze;}
        if (zc == inNA || zc< _gap_threshold) {zc = ze;}
        if (zd == inNA || zd< _gap_threshold) {zd = ze;}
        if (zf == inNA || zf< _gap_threshold) {zf = ze;}
        if (zg == inNA || zg< _gap_threshold) {zg = ze;}
        if (zh == inNA || zh< _gap_threshold) {zh = ze;}
        if (zi == inNA || zi< _gap_threshold) {zi = ze;}
    }
    else
    {
        if (za == inNA) {za = ze;}
        if (zb == inNA) {zb = ze;}
        if (zc == inNA) {zc = ze;}
        if (zd == inNA) {zd = ze;}
        if (zf == inNA) {zf = ze;}
        if (zg == inNA) {zg = ze;}
        if (zh == inNA) {zh = ze;}
        if (zi == inNA) {zi = ze;}
    }


    // Calculate lengths for all triangles
    double AB_2d = _inRaster->resolution(); double AB_dz = abs(za-zb); double AB_3d_demi = sqrt( AB_2d* AB_2d +AB_dz*AB_dz)/2;
    double BC_2d = _inRaster->resolution(); double BC_dz = abs(zc-zb); double BC_3d_demi = sqrt( BC_2d* BC_2d +BC_dz*BC_dz)/2;
    double DE_2d = _inRaster->resolution(); double DE_dz = abs(zd-ze); double DE_3d_demi = sqrt( DE_2d* DE_2d +DE_dz*DE_dz)/2;
    double EF_2d = _inRaster->resolution(); double EF_dz = abs(ze-zf); double EF_3d_demi = sqrt( EF_2d* EF_2d +EF_dz*EF_dz)/2;
    double GH_2d = _inRaster->resolution(); double GH_dz = abs(zg-zh); double GH_3d_demi = sqrt( GH_2d* GH_2d +GH_dz*GH_dz)/2;
    double HI_2d = _inRaster->resolution(); double HI_dz = abs(zh-zi); double HI_3d_demi = sqrt( HI_2d* HI_2d +HI_dz*HI_dz)/2;
    double AD_2d = _inRaster->resolution(); double AD_dz = abs(za-zd); double AD_3d_demi = sqrt( AD_2d* AD_2d +AD_dz*AD_dz)/2;
    double BE_2d = _inRaster->resolution(); double BE_dz = abs(ze-zb); double BE_3d_demi = sqrt( BE_2d* BE_2d +BE_dz*BE_dz)/2;
    double CF_2d = _inRaster->resolution(); double CF_dz = abs(zc-zf); double CF_3d_demi = sqrt( CF_2d* CF_2d +CF_dz*CF_dz)/2;
    double DG_2d = _inRaster->resolution(); double DG_dz = abs(zd-zg); double DG_3d_demi = sqrt( DG_2d* DG_2d +DG_dz*DG_dz)/2;
    double EH_2d = _inRaster->resolution(); double EH_dz = abs(ze-zh); double EH_3d_demi = sqrt( EH_2d* EH_2d +EH_dz*EH_dz)/2;
    double FI_2d = _inRaster->resolution(); double FI_dz = abs(zf-zi); double FI_3d_demi = sqrt( FI_2d* FI_2d +FI_dz*FI_dz)/2;
    double EA_2d = sqrt(2)*_inRaster->resolution(); double EA_dz = abs(ze-za); double EA_3d_demi = sqrt( EA_2d* EA_2d +EA_dz*EA_dz)/2;
    double EC_2d = sqrt(2)*_inRaster->resolution(); double EC_dz = abs(ze-zc); double EC_3d_demi = sqrt( EC_2d* EC_2d +EC_dz*EC_dz)/2;
    double EG_2d = sqrt(2)*_inRaster->resolution(); double EG_dz = abs(ze-zg); double EG_3d_demi = sqrt( EG_2d* EG_2d +EG_dz*EG_dz)/2;
    double EI_2d = sqrt(2)*_inRaster->resolution(); double EI_dz = abs(ze-zi); double EI_3d_demi = sqrt( EI_2d* EI_2d +EI_dz*EI_dz)/2;



    double surf = aire_triang(EA_3d_demi,AB_3d_demi,BE_3d_demi)+
                    aire_triang(BE_3d_demi,BC_3d_demi,EC_3d_demi)+
                    aire_triang(AD_3d_demi,DE_3d_demi,EA_3d_demi)+
                    aire_triang(EC_3d_demi,CF_3d_demi,EF_3d_demi)+
                    aire_triang(DE_3d_demi,DG_3d_demi,EG_3d_demi)+
                    aire_triang(EF_3d_demi,FI_3d_demi,EI_3d_demi)+
                    aire_triang(EG_3d_demi,EH_3d_demi,GH_3d_demi)+
                    aire_triang(EH_3d_demi,EI_3d_demi,HI_3d_demi);

    return surf;
}

double LIF_MetricVolume::aire_triang(double a, double b, double c){
    double s = (a+b+c)/2;
    double aire = sqrt(s*(s-a)*(s-b)*(s-c));

    return aire;
}



