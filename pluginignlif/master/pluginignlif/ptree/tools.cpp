#include <algorithm>
#include <vector>
#include <cfloat>
#include <fstream>

#include "tools.h"

using namespace std;

bool compareApexbyScore(Sommet _S1, Sommet _S2)
{
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // Attention est-ce que la conversion double -> intest souhaitée ?
    int score1=_S1.getscoresommet();
    int score2=_S2.getscoresommet();

    return (score1 > score2);
}

bool compare4fby3(Eigen::Vector4d _c1, Eigen::Vector4d _c2)
{
    double z1 = _c1[2];
    double z2 = _c2[2];
    return (z1 > z2);
}


bool compare2fby2(Eigen::Vector2d _c1, Eigen::Vector2d _c2)
{
    double z1 = _c1[1];
    double z2 = _c2[1];
    return (z1 > z2);
}


bool compare3fby1(Eigen::Vector3d _c1, Eigen::Vector3d _c2)
{
    double z1 = _c1[0];
    double z2 = _c2[0];
    return (z1 > z2);
}
bool compare3fby3(Eigen::Vector3d _c1, Eigen::Vector3d _c2)
{
    double z1 = _c1[2];
    double z2 = _c2[2];
    return (z1 > z2);
}

//double calculDist3D (Eigen::Vector3d &current ,std::vector<vcg::GridStaticPtr<CVertexO, double >::ObjPtr> closestObj){

//    std::vector<vcg::GridStaticPtr<CVertexO, double >::ObjPtr>::iterator it ;
//    double  d = 0.0;
//    int cp=0;
//    double Neighbors[100][100];
//    for (it = closestObj.begin() ; it != closestObj.end() ; it++)
//    {
//        if (((*it)[0].P()[2]!= current[2]) )
//        {

//            Neighbors[cp][0]=(*it)[0].P()[0];
//            Neighbors[cp][1]=(*it)[0].P()[1];
//            Neighbors[cp][2]=(*it)[0].P()[2];
//            cp++;
//        }
//    }    // calcul du seuil 3D

//            for (int i=0;i<cp;i++)
//            {
//                d += 2*sqrt(pow(current[0] - Neighbors[i][0],2) + pow(current[1] - Neighbors[i][1],2) + pow(current[2] - Neighbors[i][2],2));
//            }

//            for (int k=0;k<cp-1;k++)
//            {
//                for (int j=k+1;j<cp;j++)
//                {
//                    d += 2*sqrt(pow(Neighbors[k][0] - Neighbors[j][0],2) + pow(Neighbors[k][1] - Neighbors[j][1],2) + pow(Neighbors[k][2] - Neighbors[j][2],2))
//                }
//            }

//            double th3D = d/(cp*(cp+1));

//            return th3D;
//}

bool selectNeighbor(Eigen::Vector3d &current, Eigen::Vector3d &neighbor, double Th2D, double Th3D)
{
    bool slt = false;
    double dist2D =  sqrt(pow(current[0] - neighbor[0],2) + pow(current[1] - neighbor[1],2));

    // seuillage 2D
    if (dist2D<Th2D)
    {
        double dist3D =  sqrt(pow(current[0] - neighbor[0],2) + pow(current[1] - neighbor[1],2) + pow(current[2] - neighbor[2],2));

        if (dist3D<Th3D)
        {
            slt=true;
        }
    }
    return slt;
}
