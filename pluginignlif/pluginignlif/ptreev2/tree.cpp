#include "tree.h"

#include "ct_itemdrawable/ct_image2d.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/types_c.h"
#include "opencv2/core/core.hpp"

#include <QDebug>


Tree::Tree(int k)
{
    _k = k;
    _summit = NULL;
    _convexHull = NULL;
    _score_size = 0;
    _score_regularity = 0;
    _score_solidity = 0;
    _score_summitOffset = 0;
    _score = 0;
    _minZ = std::numeric_limits<double>::max();
    _validated = false;
}

void Tree::addPoint(Point *pt)
{
    _points.append(pt);

    if (_summit == NULL || pt->_coord(2) > _summit->_coord(2))
    {
        _summit = pt;
    }

    if (pt->_coord(2) < _minZ)
    {
        _minZ = pt->_coord(2) ;
    }

    if (_convexHull == NULL || !_convexHull->contains(pt->_coord(0), pt->_coord(1)))
    {
        computeConvexHull();
    }
}

void Tree::computeScores(Param &p)
{
    if(_points.size() < 4)
    {
        _score_size = 0;
        _score_regularity = 0;
        _score_solidity = 0;
        _score_summitOffset = 0;
        _score = 0;
    } else {
        _score_size  = getScore_size(_k, p);
        _score_regularity = getScore_regularity();
        _score_solidity = getScore_solidity(p);
        _score_summitOffset = getScore_summitOffset();
        _score = (_score_regularity + _score_summitOffset) / 2.0;
    }

}

Tree *Tree::mergeTrees(QList<Tree*> treeList, Param &p)
{
    if (treeList.size() < 2) {return NULL;}

    Tree* outputTree = new Tree(0);

    double minZ = std::numeric_limits<double>::max();
    outputTree->_k = 0;
    for (int i = 0 ; i < treeList.size() ; i++)
    {
        Tree* tr = treeList.at(i);
        outputTree->_points.append(tr->_points);

        outputTree->_children.append(tr->_children);

        if (tr->_minZ < minZ) {minZ = tr->_minZ;}

        if (i > 0 && tr->_k !=  outputTree->_k) {qDebug() << "There is a problem: trees have differents k values";}
        outputTree->_k = tr->_k;
    }

    std::sort(outputTree->_points.begin(), outputTree->_points.end(), Point::orderByDecreasingZ);

    outputTree->_minZ = minZ;

    outputTree->computeConvexHull();

    if (outputTree->_points.size() > 0)
    {
        outputTree->_summit = outputTree->_points.first();
        outputTree->computeScores(p);
    }


    return outputTree;
}

void Tree::computeConvexHull()
{
    if (_convexHull != NULL) {delete _convexHull; _convexHull = NULL;}

    QList<Eigen::Vector3d*> pts;
    for (int  i = 0 ; i < _points.size() ; i++)
    {
        pts.append(&(_points.at(i)->_coord));
    }

    CT_Polygon2DData::orderPointsByXY(pts);
    _convexHull = CT_Polygon2DData::createConvexHull(pts);
}


double Tree::getScore_size(int k, Param &p)
{
//    double height = _summit->_coord(2);
//    double length = _points.size();

//    double coef = p._density * log(height);
//    if (coef < 1) {coef = 1;}
//    double nbPts = (coef * k);

//    double score_size = length / nbPts;

//    if (score_size < 0) {score_size = 0;}
//    if (score_size > 1) {score_size = 1;}

    return 0.5;
}

double Tree::getScore_regularity()
{
    if (_convexHull == NULL) {return 0.0;}

    double score_regularity = 2.0*(1.5 - _convexHull->getPerimeter() / (2.0 * M_PI * sqrt(_convexHull->getArea() / M_PI)));
    if (score_regularity > 1) {score_regularity = 1;}
    if (score_regularity < 0) {score_regularity = 0;}
    return score_regularity;
}

double Tree::getScore_solidity(Param &p)
{
    Eigen::Vector3d min, max;
    _convexHull->getBoundingBox(min, max);

    CT_Image2D<quint8>* mask = CT_Image2D<quint8>::createImage2DFromXYCoords(NULL, NULL, min(0) - p._resSolidityGrid, min(1) - p._resSolidityGrid, max(0) + p._resSolidityGrid, max(1) + p._resSolidityGrid,  p._resSolidityGrid, 0, 0, 0);

    for (int i = 0 ; i < _points.size() ; i++)
    {
        Point* pt = _points.at(i);
        mask->setValueAtCoords(pt->_coord(0), pt->_coord(1), 1);
    }

    double nbPix = 0;
    for (size_t cell = 0 ; cell < mask->nCells() ; cell++)
    {
        if (mask->valueAtIndex(cell) == 1)
        {
            nbPix++;
        }
    }

    std::vector<cv::Point> pts;
    const QVector<Eigen::Vector2d*>& hullVertices = _convexHull->getVertices();
    for (int i = 0 ; i < hullVertices.size() ; i++)
    {
        Eigen::Vector2d* vert = hullVertices.at(i);
        size_t index, col, lin;
        mask->indexAtCoords((*vert)(0), (*vert)(1), index);
        mask->indexToGrid(index, col, lin);
        pts.push_back(cv::Point(col, lin));
    }

    double solidity = 1;
    if (pts.size() > 5)
    {
        std::vector<cv::Point> convexHullPoints;
        cv::convexHull(pts, convexHullPoints);
        double nbPixHull = cv::contourArea(convexHullPoints);

        if (nbPixHull > 0) {solidity = (nbPix / nbPixHull);}
    }

    solidity = 2.0 * (solidity - 0.5);
    if (solidity < 0) {solidity = 0;}
    if (solidity > 1) {solidity = 1;}

    return solidity;
}

double Tree::getScore_summitOffset()
{    
    if (_convexHull == NULL) {return 0;}

    double xS = _summit->_coord(0);
    double yS = _summit->_coord(1);

    const Eigen::Vector2d& centroid = _convexHull->getCenter();

    double dist = sqrt(pow(xS - centroid(0), 2) + pow(yS - centroid(1), 2));

    double area = _convexHull->getArea();
    double equivRadius = sqrt(area / M_PI);

    double score_orientation = 1 - dist / equivRadius;

    if (score_orientation < 0) {score_orientation = 0;}
    if (score_orientation > 1) {score_orientation = 1;}

    return score_orientation;
}

