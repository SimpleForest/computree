/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepfiltergridbyvalueandneighborhood.h"

#include "tools/onf_computehitsthread.h"

#include <QFileInfo>

ONF_StepFilterGridByValueAndNeighborhood::ONF_StepFilterGridByValueAndNeighborhood() : SuperClass()
{
    _threshold = -9999;
    _nbCells = 6;
    _neighborhood = 1;
}

QString ONF_StepFilterGridByValueAndNeighborhood::description() const
{
    // Gives the descrption to print in the GUI
    return tr("Seuiller une grille 3D par valeur et voisinnage");
}

QString ONF_StepFilterGridByValueAndNeighborhood::detailledDescription() const
{
    return tr("");
}

CT_VirtualAbstractStep* ONF_StepFilterGridByValueAndNeighborhood::createNewInstance() const
{
    // Creates an instance of this step
    return new ONF_StepFilterGridByValueAndNeighborhood();
}

void ONF_StepFilterGridByValueAndNeighborhood::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Grille"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inGrid, tr("Grille"));
}

void ONF_StepFilterGridByValueAndNeighborhood::declareOutputModels(CT_StepOutModelStructureManager& manager)
{

    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outGrid, tr("Grille filtrée"));
}

void ONF_StepFilterGridByValueAndNeighborhood::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addInt(tr("Seuil (minimum inclus)"),"", 0, std::numeric_limits<int>::max(), _threshold);
    postInputConfigDialog->addInt(tr("Nombre minimal de cellules voisines >= seuil"),"", 0, std::numeric_limits<int>::max(), _nbCells);
    postInputConfigDialog->addInt(tr("Voisinage"),tr("cellules"), 1, std::numeric_limits<int>::max(), _neighborhood);
}

void ONF_StepFilterGridByValueAndNeighborhood::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        if (isStopped()) {return;}

        const CT_Grid3D_Sparse<int>* gridIn = group->singularItem(_inGrid);

        if (gridIn!=nullptr)
        {
            // Declaring the output grids
            CT_Grid3D_Sparse<bool>* outGrid = new CT_Grid3D_Sparse<bool>(gridIn->minX(), gridIn->minY(), gridIn->minZ(), gridIn->xdim(), gridIn->ydim(), gridIn->zdim(), gridIn->resolution(), false, false);

            QList<size_t> list;
            gridIn->getIndicesWithData(list);

            for (int i = 0 ; i < list.size() ; i++)
            {
                size_t index = list.at(i);
                int colX, linY, levZ;

                if (gridIn->valueAtIndex(index) >= _threshold)
                {
                    if (gridIn->indexToGrid(index, colX, linY, levZ))
                    {
                        QList<int> neighbors = gridIn->neighboursValues(colX, linY, levZ, _neighborhood);
                        int nbValidCell = 0;
                        for (int j = 0 ; j < neighbors.size() ; j++)
                        {
                            if (neighbors.at(j) >= _threshold)
                            {
                               ++nbValidCell;
                            }
                        }

                        if (nbValidCell >= _nbCells)
                        {
                            outGrid->setValueAtIndex(index, true);
                        }
                    }
                }
            }

            outGrid->computeMinMax();
            group->addSingularItem(_outGrid, outGrid);
        }
    }

    setProgress(99);
}

