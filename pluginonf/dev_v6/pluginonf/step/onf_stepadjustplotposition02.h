/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPADJUSTPLOTPOSITION02_H
#define ONF_STEPADJUSTPLOTPOSITION02_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_circle2d.h"
#include "ctliblas/itemdrawable/las/ct_stdlaspointsattributescontainer.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_image2d.h"

#include "actions/onf_actionadjustplotposition02.h"

class ONF_StepAdjustPlotPosition02: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    ONF_StepAdjustPlotPosition02();

    ~ONF_StepAdjustPlotPosition02();

    QString description() const;

    QString detailledDescription() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

    void initManualMode();
    void useManualMode(bool quit = false);

private:

    // Step parameters
    QStringList             _exportFile;

    ONF_ActionAdjustPlotPosition02_dataContainer*     _dataContainer;

    DocumentInterface*      _m_doc;


    CT_HandleInResultGroupCopy<>                                                        _inResPlot;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupRef;
    CT_HandleInStdGroup<>                                                               _inGrp;
    CT_HandleInSingularItem<CT_Circle2D>                                                _inRef;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::NUMBER>     _inRefDbh;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::NUMBER>     _inRefHeight;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::STRING>     _inRefID;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::STRING>     _inRefIDplot;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::STRING,0,1> _inSpecies;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::STRING,0,1> _inComment;

    CT_HandleInResultGroup<>                                                            _inResScene;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupSc;
    CT_HandleInStdGroup<>                                                               _inGrpSc;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInSingularItem<CT_StdLASPointsAttributesContainer, 0, 1>                   _inLasAtt;
    CT_HandleInSingularItem<CT_Image2D<qint32> >                                        _inMaxima;
    CT_HandleInSingularItem<CT_Image2D<float> >                                         _inMns;
    CT_HandleInSingularItem<CT_Circle2D>                                                _inArea;
    CT_HandleInSingularItem<CT_PointsAttributesScalarTemplated<float>, 0, 1>            _inHeightAtt;

    CT_HandleInResultGroup<0,1>                                                         _inResultDTM;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupDTM;
    CT_HandleInStdGroup<>                                                            _inGroupDTM;
    CT_HandleInSingularItem<CT_Image2D<float>>                                      _inDTM;

    CT_HandleOutSingularItem<CT_Circle2D>                                               _outCircle;
    CT_HandleOutStdItemAttribute<float>                                                 _outDBHAtt;
    CT_HandleOutStdItemAttribute<float>                                                 _outHeightAtt;
    CT_HandleOutStdItemAttribute<QString>                                               _outPlotIDAtt;
    CT_HandleOutStdItemAttribute<QString>                                               _outTreeIDAtt;
    CT_HandleOutStdItemAttribute<QString>                                               _outSpeciesAtt;
    CT_HandleOutStdItemAttribute<float>                                                 _outTransXAtt;
    CT_HandleOutStdItemAttribute<float>                                                 _outTransYAtt;
    CT_HandleOutStdItemAttribute<bool>                                                  _outMovedAtt;
    CT_HandleOutStdItemAttribute<float>                                                 _outZPointAtt;

};

#endif // ONF_STEPADJUSTPLOTPOSITION02_H
