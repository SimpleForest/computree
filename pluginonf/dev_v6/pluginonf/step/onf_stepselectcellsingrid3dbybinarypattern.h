/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPSELECTCELLSINGRID3DBYBINARYPATTERN_H
#define ONF_STEPSELECTCELLSINGRID3DBYBINARYPATTERN_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_grid3d_sparse.h"

class ONF_StepSelectCellsInGrid3DByBinaryPattern: public CT_AbstractStep
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

    struct PatternCell
    {
        PatternCell(int r, int c, int lev, double v)
        {
            _rowRel = r;
            _colRel = c;
            _levRel = lev;
            _val = v;
        }

        int     _rowRel;
        int     _colRel;
        int     _levRel;
        double  _val;
    };

public:

    ONF_StepSelectCellsInGrid3DByBinaryPattern();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    double      _inThreshold;
    QString     _pattern;
    int         _outThresholdAbsolute;
    double      _outThresholdRelative;
    int         _selectMode;

    bool parsePattern(QList<PatternCell> &parsedPattern);
};

#endif // ONF_STEPSELECTCELLSINGRID3DBYBINARYPATTERN_H
