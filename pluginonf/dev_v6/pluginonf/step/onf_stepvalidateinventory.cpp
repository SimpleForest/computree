/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepvalidateinventory.h"

#include "actions/onf_actionvalidateinventory.h"

#include <QMessageBox>

ONF_StepValidateInventory::ONF_StepValidateInventory() : SuperClass()
{
    m_doc = nullptr;
    setManual(true);

    _speciesList << "Erable" << "Hêtre" << "Chêne" << "Sapin Baumier" << "Epinette Noire";
    _hRef = 1.3;
}

QString ONF_StepValidateInventory::description() const
{
    return tr("Validation d'inventaire");
}

CT_VirtualAbstractStep* ONF_StepValidateInventory::createNewInstance() const
{
    return new ONF_StepValidateInventory();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepValidateInventory::declareInputModels(CT_StepInModelStructureManager& manager)
{
    CT_HandleInResultGroupCopy<>                                                        _inResult;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroup;
    CT_HandleInStdGroup<>                                                               _inGroup;
    CT_HandleInSingularItem<CT_AbstractItemDrawableWithPointCloud>                      _inScene;
    CT_HandleInItemAttribute<CT_AbstractItemAttribute, CT_AbstractCategory::ANY>        _inAtt;

    CT_HandleOutStdGroup                                                                _outGroup;
    CT_HandleOutSingularItem<CT_Scene>                                                  _outScene;
    CT_HandleOutStdItemAttribute<qint32>                                                _outAtt;

    manager.addResult(_inResult, tr("Scene(s)"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inScene, tr("Scene(s)"));
    manager.addItemAttribute(_inScene, _inAtt, CT_AbstractCategory::DATA_VALUE, tr("value"));

    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Groupe"));
    manager.addItem(_outGroup, _outScene, tr("Scene"));
    manager.addItemAttribute(_outScene, _outAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("value"));


    CT_InResultModelGroupToCopy *res_inScres = createNewInResultModelForCopy(_inScres, tr("Items"));
    res_inScres->setZeroOrMoreRootGroup();
    res_inScres->addGroupModel("", _inScBase, CT_AbstractItemGroup::staticGetType(), tr("Groupe de base"));
    res_inScres->addItemModel(_inScBase, _inDtmValue, CT_ReferencePoint::staticGetType(), tr("Z MNT"));
    res_inScres->addGroupModel(_inScBase, _inGroup, CT_AbstractItemGroup::staticGetType(), tr("Groupe"));
    res_inScres->addItemModel(_inGroup, _inItem, CT_AbstractSingularItemDrawable::staticGetType(), tr("Item"));
}

void ONF_StepValidateInventory::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy_scres = createNewOutResultModelToCopy(_inScres);

    if(resCpy_scres != nullptr) {
        resCpy_scres->addItemModel(_inScBase, _attributes, new CT_ReferencePoint(), tr("Position de référence"));

        resCpy_scres->addItemAttributeModel(_attributes,_attribute_sp,
                                            new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_VALUE),
                                            tr("Espèce"));
        resCpy_scres->addItemAttributeModel(_attributes,_attribute_id,
                                            new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_ID),
                                            tr("IDterrain"));
        resCpy_scres->addItemAttributeModel(_attributes,_attribute_idItem,
                                            new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_ID),
                                            tr("IDitem"));
    }

}

void ONF_StepValidateInventory::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{

    postInputConfigDialog->addFileChoice(tr("Fichier d'espèces"), CT_FileChoiceButton::OneExistingFile, "Fichier ascii (*.txt)", _speciesFileName);
    postInputConfigDialog->addDouble(tr("Hauteur de référence"), "m", 0, std::numeric_limits<double>::max(), 2, _hRef);
}

void ONF_StepValidateInventory::compute()
{
    m_doc = nullptr;
    m_status = 0;

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resCpy_scres = outResultList.at(0);

        _selectedItem = new QMap<const CT_StandardItemGroup*, const CT_AbstractSingularItemDrawable*>();
        _availableItem = new QMultiMap<const CT_StandardItemGroup*, const CT_AbstractSingularItemDrawable*>();
        _species = new QMap<const CT_StandardItemGroup*, QString>();
        _ids = new QMap<const CT_StandardItemGroup*, QString>();

        // Boucle sur les groupes racine
        CT_ResultGroupIterator itCpy_scBase(resCpy_scres, this, _inScBase);
        while (itCpy_scBase.hasNext() && !isStopped())
        {
            CT_StandardItemGroup* grpCpy_scBase = (CT_StandardItemGroup*) itCpy_scBase.next();
            const CT_ReferencePoint* itemCpy_zDTM = (const CT_ReferencePoint*) grpCpy_scBase->firstItemByINModelName(this, _inDtmValue);

            if (itemCpy_zDTM != nullptr)
            {
                _dtmZvalues.insert(grpCpy_scBase, itemCpy_zDTM->centerZ());
            } else {
                _dtmZvalues.insert(grpCpy_scBase, 0);
            }

            CT_GroupIterator itCpy_cluster(grpCpy_scBase, this, _inGroup);
            while (itCpy_cluster.hasNext() && !isStopped())
            {
                CT_StandardItemGroup* grpCpy_cluster = (CT_StandardItemGroup*) itCpy_cluster.next();

                const CT_AbstractSingularItemDrawable* itemCpy_item = grpCpy_cluster->firstItemByINModelName(this, _inItem);
                if (itemCpy_item != nullptr)
                {
                    _availableItem->insertMulti(grpCpy_scBase, itemCpy_item);
                }
            }
        }

        // remplit _selectedDbh
        findBestItemForEachGroup();

        // request the manual mode
        requestManualMode();

        CT_ResultGroupIterator itCpy_scBaseOut(resCpy_scres, this, _inScBase);
        while (itCpy_scBaseOut.hasNext() && !isStopped())
        {
            CT_StandardItemGroup* grpCpy_scBase = (CT_StandardItemGroup*) itCpy_scBaseOut.next();
            CT_AbstractSingularItemDrawable* selectedItem = (CT_AbstractSingularItemDrawable*) _selectedItem->value(grpCpy_scBase, nullptr);

            if (selectedItem != nullptr)
            {

                float x = selectedItem->centerX();
                float y = selectedItem->centerY();
                float z = selectedItem->centerZ();

                QString species = _species->value(grpCpy_scBase, "");
                QString id = _ids->value(grpCpy_scBase, "");

                CT_ReferencePoint* itemCpy_refPosition = new CT_ReferencePoint(_attributes.completeName(), resCpy_scres, x, y, z, 0);
                grpCpy_scBase->addSingularItem(itemCpy_refPosition);

                itemCpy_refPosition->addItemAttribute(new CT_StdItemAttributeT<QString>(_attribute_sp.completeName(),
                                                                                       CT_AbstractCategory::DATA_VALUE,
                                                                                       resCpy_scres, species));
                itemCpy_refPosition->addItemAttribute(new CT_StdItemAttributeT<QString>(_attribute_id.completeName(),
                                                                                       CT_AbstractCategory::DATA_ID,
                                                                                       resCpy_scres, id));
                itemCpy_refPosition->addItemAttribute(new CT_StdItemAttributeT<QString>(_attribute_idItem.completeName(),
                                                                                       CT_AbstractCategory::DATA_ID,
                                                                                       resCpy_scres, QString("%1").arg(selectedItem->id())));

            }
        }

        m_status = 1;
        requestManualMode();

        delete _selectedItem;
        delete _availableItem;
        delete _species;
        delete _ids;

}

void ONF_StepValidateInventory::initManualMode()
{
    // create a new 3D document
    if(m_doc == nullptr)
        m_doc = getGuiContext()->documentManager()->new3DDocument();

    m_doc->removeAllItemDrawable();

    m_doc->setCurrentAction(new ONF_ActionValidateInventory(_selectedItem, _availableItem, _species, _ids, _speciesList));

    QMessageBox::information(nullptr, tr("Mode manuel"), tr("Bienvenue dans le mode manuel de cette étape !"), QMessageBox::Ok);
}

void ONF_StepValidateInventory::useManualMode(bool quit)
{
    if(m_status == 0)
    {
        if(quit)
        {
        }
    }
    else if(m_status == 1)
    {
        if(!quit)
        {
            m_doc = nullptr;
            quitManualMode();
        }
    }
}

void ONF_StepValidateInventory::findBestItemForEachGroup()
{
    QList<const CT_StandardItemGroup*> groups = _availableItem->keys();

    QListIterator<const CT_StandardItemGroup*> itGroups(groups);
    while (itGroups.hasNext())
    {
        const CT_StandardItemGroup* group = itGroups.next();
        QList<const CT_AbstractSingularItemDrawable*> items = _availableItem->values(group);

        float mindelta = std::numeric_limits<float>::max();
        const CT_AbstractSingularItemDrawable* bestItem = nullptr;

        QListIterator<const CT_AbstractSingularItemDrawable*> itItems(items);
        while (itItems.hasNext())
        {
            const CT_AbstractSingularItemDrawable* currentItem = itItems.next();
            float dist = std::fabs(currentItem->centerZ() - (_dtmZvalues.value(group, 0) + _hRef));

            if (dist < mindelta)
            {
                mindelta = dist;
                bestItem = currentItem;
            }
        }
        if (bestItem != nullptr)
        {
            _selectedItem->insert(group, bestItem);
        }
    }
}

