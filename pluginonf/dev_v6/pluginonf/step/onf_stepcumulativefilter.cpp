#include "onf_stepcumulativefilter.h"

#include "opencv2/imgproc/imgproc.hpp"

ONF_StepCumulativeFilter::ONF_StepCumulativeFilter() : SuperClass()
{
    _radius = 2.0;
}

QString ONF_StepCumulativeFilter::description() const
{
    return tr("Filtre cumulatif");
}

QString ONF_StepCumulativeFilter::detailledDescription() const
{
    return tr("No detailled description for this step");
}

QString ONF_StepCumulativeFilter::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepCumulativeFilter::createNewInstance() const
{
    return new ONF_StepCumulativeFilter();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepCumulativeFilter::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Image"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inImage, tr("Image"));
}

void ONF_StepCumulativeFilter::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outcumulativeImage, tr("Image cumulée"));
}

void ONF_StepCumulativeFilter::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Rayon de sommation"), tr("en mètres"), 0, 999999, 2, _radius, 1);
}

void ONF_StepCumulativeFilter::compute()
{
    for (CT_StandardItemGroup* grp : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_AbstractImage2D* imageIn : grp->singularItems(_inImage))
        {
            if (isStopped()) {return;}

            int ncells = int(std::ceil((_radius - (imageIn->resolution() / 2.0)) / imageIn->resolution()));
            qDebug() << ncells;

            Eigen::Vector2d min;
            imageIn->getMinCoordinates(min);
            CT_Image2D<float>* cumulativeImage = new CT_Image2D<float>(min(0), min(1), imageIn->xdim(), imageIn->ydim(), imageIn->resolution(), imageIn->level(), -1, 0);
            grp->addSingularItem(_outcumulativeImage, cumulativeImage);

            for (int x = 0 ; x < cumulativeImage->xdim() ; x++)
            {
                for (int y = 0 ; y < cumulativeImage->ydim() ; y++)
                {
                    int xxMin = x - ncells; if (xxMin < 0) {xxMin = 0;}
                    int yyMin = y - ncells; if (yyMin < 0) {yyMin = 0;}

                    int xxMax = x + ncells; if (xxMax >= cumulativeImage->xdim()) {xxMax = cumulativeImage->xdim() - 1;}
                    int yyMax = y + ncells; if (yyMax >= cumulativeImage->ydim()) {yyMax = cumulativeImage->ydim() - 1;}

                    Eigen::Vector3d centerCoord;
                    cumulativeImage->getCellCenterCoordinates(x, y, centerCoord);

                    float sum = 0;
                    for (int xx = xxMin ; xx <= xxMax ; xx++)
                    {
                        for (int yy = yyMin ; yy <= yyMax ; yy++)
                        {
                            size_t index;
                            imageIn->index(xx, yy, index);

                            Eigen::Vector3d cellCoord;
                            cumulativeImage->getCellCenterCoordinates(xx, yy, cellCoord);

                            double dist = std::sqrt(pow(centerCoord(0) - cellCoord(0), 2) + pow(centerCoord(1) - cellCoord(1), 2));
                            if (dist <= _radius)
                            {
                                sum += float(imageIn->valueAtIndexAsDouble(index));
                            }
                        }
                    }

                    cumulativeImage->setValue(x, y, sum);
                }

                setProgress(100*int(double(x) / double(cumulativeImage->xdim())));
            }

            cumulativeImage->computeMinMax();
        }
    }
}
