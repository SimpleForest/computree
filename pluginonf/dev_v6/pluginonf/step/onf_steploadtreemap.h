/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_STEPLOADTREEMAP_H
#define ONF_STEPLOADTREEMAP_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

#include "ct_itemdrawable/ct_circle2d.h"
#include "ct_itemdrawable/ct_loopcounter.h"

#include "ct_view/tools/ct_textfileconfigurationdialog.h"

class ONF_StepLoadTreeMap: public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    using SuperClass = CT_AbstractStepCanBeAddedFirst;

public:

    ONF_StepLoadTreeMap();

    QString description() const;

    QString detailledDescription() const;

    QString URL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

public slots:
    void fileChanged();

signals:
    void updateComboBox(QStringList valuesList, QString value);

protected:

    void fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog) final;

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    virtual void finalizePostSettings() final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters

    int     _mode;
    QList<CT_TextFileConfigurationFields> _neededFields;

    QString _refFileName;
    QString _plotID;

    bool _refHeader;

    QString _refSeparator;

    QString _refDecimal;

    QLocale _refLocale;

    int _refSkip;

    QMap<QString, int> _refColumns;

    QStringList _plotsIds;

    CT_ComboBox *_cbox;



    CT_HandleInResultGroup<>                                                            _inResultCounter;
    CT_HandleInStdZeroOrMoreGroup                                                       _inZeroOrMoreRootGroupCounter;
    CT_HandleInSingularItem<CT_LoopCounter>                                             _inCounter;


    CT_HandleOutResultGroup                                                             _outResultRef;
    CT_HandleOutStdGroup                                                                _outGrpPlot;
    CT_HandleOutStdGroup                                                                _outGrpRef;
    CT_HandleOutSingularItem<CT_Circle2D>                                               _outRef;
    CT_HandleOutStdItemAttribute<float>                                                 _outRefDbh;
    CT_HandleOutStdItemAttribute<float>                                                 _outRefHeight;
    CT_HandleOutStdItemAttribute<QString>                                               _outRefID;
    CT_HandleOutStdItemAttribute<QString>                                               _outRefIDplot;
    CT_HandleOutStdItemAttribute<QString>                                               _outSpecies;
    CT_HandleOutStdItemAttribute<QString>                                               _outComment;


};

#endif // ONF_STEPLOADTREEMAP_H
