/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepsetfootcoordinatesvertically.h"

ONF_StepSetFootCoordinatesVertically::ONF_StepSetFootCoordinatesVertically() : SuperClass()
{
}

QString ONF_StepSetFootCoordinatesVertically::description() const
{
    return tr("5- Récupérer la coordonnée MNT pour chaque Billon");
}

QString ONF_StepSetFootCoordinatesVertically::detailledDescription() const
{
    return tr("No detailled description for this step");
}

CT_VirtualAbstractStep* ONF_StepSetFootCoordinatesVertically::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepSetFootCoordinatesVertically();
}

//////////////////// PROTECTED //////////////////

void ONF_StepSetFootCoordinatesVertically::declareInputModels(CT_StepInModelStructureManager& manager)
{

    manager.addResult(_inResult, tr("Billons"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inBaseGroup, tr("Billon (Grp)"));
    manager.addGroup(_inBaseGroup, _inGroup, tr("Cluster (Grp)"));
    manager.addItem(_inGroup, _inRefPoint, tr("Point de référence"));

    manager.addResult(_inResultDTM, tr("MNT"), "", true);
    manager.setZeroOrMoreRootGroup(_inResultDTM, _inZeroOrMoreRootGroupDTM);
    manager.addGroup(_inZeroOrMoreRootGroupDTM, _inGroupDTM);
    manager.addItem(_inGroupDTM, _inDTM, tr("MNT"));
}

void ONF_StepSetFootCoordinatesVertically::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inBaseGroup, _outRefPoint, tr("Scene"));
}

void ONF_StepSetFootCoordinatesVertically::compute()
{
    for (const CT_Image2D<float>* mnt : _inDTM.iterateInputs(_inResultDTM))
    {
        for (CT_StandardItemGroup* baseGroup : _inBaseGroup.iterateOutputs(_inResult))
        {
            for (const CT_StandardItemGroup* group : baseGroup->groups(_inBaseGroup))
            {
                for (const CT_ReferencePoint* refPoint : group->singularItems(_inRefPoint))
                {
                    if (isStopped()) {return;}

                    float na = mnt->NA();
                    float z_value = mnt->valueAtCoords(refPoint->x(), refPoint->y());
                    CT_ReferencePoint* footCoordinate;
                    if (!qFuzzyCompare(z_value, na))
                    {
                        footCoordinate = new CT_ReferencePoint(refPoint->x(), refPoint->y(), double(z_value), mnt->resolution());
                    } else {
                        footCoordinate = new CT_ReferencePoint(refPoint->x(), refPoint->y(), refPoint->z(), refPoint->xyBuffer());
                    }
                    baseGroup->addSingularItem(_outRefPoint, footCoordinate);
                }
            }
        }

        if (mnt != nullptr) {break;}
    }

}

