/****************************************************************************
 Copyright (C) 2010-2019 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepinterpolatedem.h"

#include "ct_triangulation/ct_delaunayt.h"
#include "ct_triangulation/ct_nodet.h"
#include "ct_triangulation/ct_trianglet.h"

#include "ct_itemdrawable/tools/image2dtools/ct_image2dnaturalneighboursinterpolator.h"
#include "ct_log/ct_logmanager.h"

#include <QtConcurrent>

#define EPSILON 0.000001

ONF_StepInterpolateDEM::ONF_StepInterpolateDEM() : SuperClass()
{
    _nCells = 10;
    _name = tr("MNS");
}

QString ONF_StepInterpolateDEM::description() const
{
    return tr("Interpoler un MNE (MNS, MNT ou autre)");
}

QString ONF_StepInterpolateDEM::detailledDescription() const
{
    return tr("Cette étape permet d'interpoler les valeurs manquantes d'un raster<float>.");
}

CT_VirtualAbstractStep* ONF_StepInterpolateDEM::createNewInstance() const
{
    // cree une copie de cette etape
    return new ONF_StepInterpolateDEM();
}

/////////////////////// PROTECTED ///////////////////////

void ONF_StepInterpolateDEM::declareInputModels(CT_StepInModelStructureManager& manager)
{

    manager.addResult(_inResult, tr("MNE à interpoler"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inDEM, tr("MNE à interpoler"));
}

void ONF_StepInterpolateDEM::fillPreInputConfigurationDialog(CT_StepConfigurableDialog* preInputConfigDialog)
{
    preInputConfigDialog->addString(tr("Type/nom de raster sélectionné (MNS/MNT/MNC...)"), "", _name);
}


void ONF_StepInterpolateDEM::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addInt(tr("Taille de la fenêtre d'interpolation"), tr("Cases"), 1, 10000, _nCells);
}

void ONF_StepInterpolateDEM::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGroup, _outDEM, tr("%1 interpolé").arg(_name));
}

void ONF_StepInterpolateDEM::compute()
{
    for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
    {
        for (const CT_Image2D<float>* inDEM : group->singularItems(_inDEM))
        {
            if (isStopped()) {return;}

            setProgress(20);

            // interpolation du raster
            CT_Image2D<float>* outDEM = new CT_Image2D<float>(inDEM->minX(), inDEM->minY(), inDEM->xdim(), inDEM->ydim(), inDEM->resolution(), inDEM->minZ(), inDEM->NA(), -9999);

            QList<size_t> NAindices;
            size_t index;

            for (int xx = 0 ; xx < outDEM->xdim() ; ++xx)
            {
                for (int yy = 0 ; yy  < outDEM->ydim() ; ++yy)
                {
                    float value = inDEM->value(xx,yy);
                    outDEM->setValue(xx, yy, value);

                    if (qFuzzyCompare(value, inDEM->NA()))
                    {
                        inDEM->index(xx, yy, index);
                        NAindices.append(index);
                    }
                }
            }

            QFuture<void> futur = QtConcurrent::map(NAindices, CT_Image2DNaturalNeighboursInterpolator(inDEM, outDEM, _nCells));

            int progressMin = futur.progressMinimum();
            int progressTotal = futur.progressMaximum() - futur.progressMinimum();
            while (!futur.isFinished())
            {
                setProgress(static_cast<float>(60.0*(futur.progressValue() - progressMin)/progressTotal + 20.0));
            }

            setProgress(80.0f);
            PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Interpolation du DEM terminée"));


            setProgress(90.0f);

            // ajout du raster interpolé
            outDEM->computeMinMax();
            group->addSingularItem(_outDEM, outDEM);
        }
    }
    setProgress(100.0f);
}

