/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepcreateplotmanagerfromfile.h"

#include "ct_log/ct_logmanager.h"

#include "ct_view/ct_asciifilechoicebutton.h"
#include "ct_view/ct_combobox.h"

ONF_StepCreatePlotManagerFromFile::ONF_StepCreatePlotManagerFromFile() : SuperClass()
{
    _neededFields.append(CT_TextFileConfigurationFields("ID_Plot", QRegExp("([pP][lL][oO][tT]|[pP][lL][aA][cC][eE][tT][tT][eE]|[pP][lL][aA][cC]|[iI][dD][_][pP][lL][oO][tT]|[iI][dD][_][pP][lL][aA][cC][eE][tT][tT][eE]|[iI][dD][_][pP][lL][aA][cC])"), false));
    _neededFields.append(CT_TextFileConfigurationFields("X", QRegExp("[xX]"), false));
    _neededFields.append(CT_TextFileConfigurationFields("Y", QRegExp("[yY]"), false));

    _refFileName = "";
    _refHeader = true;
    _refSeparator = "\t";
    _refDecimal = ".";
    _refLocale = QLocale(QLocale::English, QLocale::UnitedKingdom).name();
    _refSkip = 0;

    _plotRadius = 45.0;
}

QString ONF_StepCreatePlotManagerFromFile::description() const
{
    return tr("1- Créée une liste de placettes à partir d'un fichier ASCII");
}

QString ONF_StepCreatePlotManagerFromFile::detailledDescription() const
{
    return tr("Charge des placettes depuis un fichier ASCII. <br>"
              "L'import est configurable, le fichier devant contenir les champs suivants :<br>"
              "- IDplot : Identifiant placette<br>"
              "- X      : Coordonnée X de l'arbre<br>"
              "- Y      : Coordonnée Y de l'arbre<br>");
}

QString ONF_StepCreatePlotManagerFromFile::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

CT_VirtualAbstractStep* ONF_StepCreatePlotManagerFromFile::createNewInstance() const
{
    return new ONF_StepCreatePlotManagerFromFile();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepCreatePlotManagerFromFile::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Emprise(s)"), tr(""), true);
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGroup);
    manager.addItem(_inGroup, _inShape2D, tr("Emprise (sans buffer)"));
}

void ONF_StepCreatePlotManagerFromFile::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addGroup(_inGroup, _outGroup, tr("Placette"));
    manager.addItem(_outGroup, _outPlot, tr("Placette"));
    manager.addItemAttribute(_outPlot, _outPlotAtt, PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_VALUE), tr("PlotID"));
}

void ONF_StepCreatePlotManagerFromFile::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addAsciiFileChoice("Fichier des placettes", "Fichier ASCII (*.txt ; *.asc)", true, _neededFields, _refFileName, _refHeader, _refSeparator, _refDecimal, _refLocale, _refSkip, _refColumns);
    postInputConfigDialog->addDouble(tr("Rayon de placette"), "m", 0, std::numeric_limits<double>::max(), 2, _plotRadius);
}

void ONF_StepCreatePlotManagerFromFile::compute()
{
    // read plot file
    QList<QString> ids;
    QList<double> xcoord;
    QList<double> ycoord;

    int colIDplot_ref  = _refColumns.value("ID_Plot", -1);
    if (colIDplot_ref < 0) {return;}

    QFile fRef(_refFileName);
    if (fRef.exists() && fRef.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&fRef);
        stream.setLocale(_refLocale);

        int colX   = _refColumns.value("X", -1);
        int colY   = _refColumns.value("Y", -1);

        if (colX < 0) {PS_LOG->addMessage(LogInterface::error, LogInterface::step, QString(tr("Champ X non défini")));}
        if (colY < 0) {PS_LOG->addMessage(LogInterface::error, LogInterface::step, QString(tr("Champ Y non défini")));}

        if (colX >= 0 && colY >= 0)
        {

            int colMax = colX;
            if (colY   > colMax) {colMax = colY;}
            if (colIDplot_ref > colMax) {colMax = colIDplot_ref;}

            for (int i = 0 ; i < _refSkip ; i++) {stream.readLine();}
            if (_refHeader) {stream.readLine();}

            size_t cpt = 1;
            while (!stream.atEnd())
            {
                QString line = stream.readLine();
                cpt++;
                if (!line.isEmpty())
                {
                    QStringList values = line.split(_refSeparator);
                    if (values.size() >= colMax)
                    {
                        QString plot = "";
                        if (colIDplot_ref >= 0)
                        {
                            plot =  values.at(colIDplot_ref);
                        }

                        bool okX, okY;
                        double x = _refLocale.toDouble(values.at(colX), &okX);
                        double y = _refLocale.toDouble(values.at(colY), &okY);

                        if (okX && okY)
                        {
                            ids.append(plot);
                            xcoord.append(x);
                            ycoord.append(y);
                        } else {
                            PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("Ligne %1 du fichier REF non valide")).arg(cpt));
                        }
                    }
                }
            }
        }
        fRef.close();
    }

    if (xcoord.size() > 0)
    {
        int cpt = 0;
        for (CT_StandardItemGroup* group : _inGroup.iterateOutputs(_inResult))
        {
            for (const CT_AbstractAreaShape2D* shape2D : group->singularItems(_inShape2D))
            {
                if (isStopped()) {return;}

                for (int i = 0 ; i < xcoord.size() ; i++)
                {
                    double x = xcoord.value(i);
                    double y = ycoord.value(i);
                    QString plot = ids.value(i);

                    if (shape2D->contains(x, y))
                    {
                        ++cpt;
                        CT_StandardItemGroup* plotGroup = new CT_StandardItemGroup();
                        group->addGroup(_outGroup, plotGroup);

                        CT_Circle2D* circle = new CT_Circle2D(new CT_Circle2DData(Eigen::Vector2d(x, y), _plotRadius));
                        plotGroup->addSingularItem(_outPlot, circle);
                        circle->addItemAttribute(_outPlotAtt, new CT_StdItemAttributeT<QString>(PS_CATEGORY_MANAGER->findByUniqueName(CT_AbstractCategory::DATA_ID), plot));

                    }
                }
            }
        }
        PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("%1 placettes sur l'emprise")).arg(cpt));
    }
}
