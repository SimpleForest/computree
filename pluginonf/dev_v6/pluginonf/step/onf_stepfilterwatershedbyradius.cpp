#include "onf_stepfilterwatershedbyradius.h"
#include <QDebug>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"


// Constructor : initialization of parameters
ONF_StepFilterWatershedByRadius::ONF_StepFilterWatershedByRadius() : SuperClass()
{
    _radius = 10.0;
}

// Step description (tooltip of contextual menu)
QString ONF_StepFilterWatershedByRadius::description() const
{
    return tr("Filter un watershed par rayon (d'apex)");
}

// Step detailled description
QString ONF_StepFilterWatershedByRadius::detailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString ONF_StepFilterWatershedByRadius::URL() const
{
    //return tr("STEP URL HERE");
    return SuperClass::URL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ONF_StepFilterWatershedByRadius::createNewInstance() const
{
    return new ONF_StepFilterWatershedByRadius();
}

//////////////////// PROTECTED METHODS //////////////////

void ONF_StepFilterWatershedByRadius::declareInputModels(CT_StepInModelStructureManager& manager)
{
    manager.addResult(_inResult, tr("Maxima"));
    manager.setZeroOrMoreRootGroup(_inResult, _inZeroOrMoreRootGroup);
    manager.addGroup(_inZeroOrMoreRootGroup, _inGrp);
    manager.addItem(_inGrp, _inImage, tr("Image (hauteurs)"));
    manager.addItem(_inGrp, _inClusters, tr("Clusters"));
}

void ONF_StepFilterWatershedByRadius::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    manager.addResultCopy(_inResult);
    manager.addItem(_inGrp, _outClusters, tr("Clusters filtrés"));
}


void ONF_StepFilterWatershedByRadius::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{
    postInputConfigDialog->addDouble(tr("Rayon de filtrage : "), "m", 0, std::numeric_limits<double>::max(), 4, _radius);
}

void ONF_StepFilterWatershedByRadius::compute()
{

    for (CT_StandardItemGroup* grp : _inGrp.iterateOutputs(_inResult))
    {
        if (isStopped()) {return;}

        const CT_Image2D<float>* raster = grp->singularItem(_inImage);
        const CT_Image2D<qint32>* inClusters = grp->singularItem(_inClusters);

        if (inClusters != nullptr && raster != nullptr)
        {
            CT_Image2D<qint32>* outClusters = new CT_Image2D<qint32>(inClusters->minX(), inClusters->minY(), inClusters->xdim(), inClusters->ydim(), inClusters->resolution(), inClusters->level(), inClusters->NA(), inClusters->NA());
            //CT_Image2D<qint32>* outClusters = new CT_Image2D<qint32>(_outClusters_ModelName.completeName(), resOut, inClusters->minX(), inClusters->minY(), inClusters->colDim(), inClusters->linDim(), inClusters->resolution(), inClusters->level(), -1, -1);

            QVector<double> zmax(inClusters->dataMax()+1);
            zmax.fill(-std::numeric_limits<double>::max());

            QVector<Eigen::Vector3d> centers(inClusters->dataMax()+1);

            for (int col = 0 ; col < inClusters->xdim() ; col++)
            {
                for (int lin = 0 ; lin < inClusters->ydim() ; lin++)
                {
                    qint32 cluster = inClusters->value(col, lin);
                    float val = raster->value(col, lin);

                    if (cluster != inClusters->NA() && !qFuzzyCompare(val, raster->NA()))
                    {
                        if (double(val) > zmax[cluster])
                        {
                            zmax[cluster] = double(val);
                            inClusters->getCellCenterCoordinates(col, lin, centers[cluster]);
                        }
                    }
                }
            }

            for (int col = 0 ; col < inClusters->xdim() ; col++)
            {
                for (int lin = 0 ; lin < inClusters->ydim() ; lin++)
                {
                    qint32 cluster = inClusters->value(col, lin);

                    if (cluster != inClusters->NA() && zmax[cluster] > -std::numeric_limits<double>::max())
                    {
                        Eigen::Vector3d cellCenter;
                        inClusters->getCellCenterCoordinates(col, lin, cellCenter);

                        double distance = std::sqrt(pow(cellCenter(0) - centers[cluster](0), 2) + pow(cellCenter(1) - centers[cluster](1), 2));

                        if (distance <= _radius)
                        {
                            outClusters->setValue(col, lin, cluster);
                        }
                    }
                }
            }

            outClusters->computeMinMax();
            grp->addSingularItem(_outClusters, outClusters);
        }
    }
}

