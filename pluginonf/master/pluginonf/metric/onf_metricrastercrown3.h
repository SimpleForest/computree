/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_METRICRASTERCROWN3_H
#define ONF_METRICRASTERCROWN3_H

#include "ctlibmetrics/ct_metric/abstract/ct_abstractmetric_raster.h"
#include "ctlibmetrics/tools/ct_valueandbool.h"

class ONF_MetricRasterCrown3 : public CT_AbstractMetric_Raster
{
    Q_OBJECT
public:

    struct Config {
        VaB<double>      convexArea;
        VaB<double>      maxDistApex;
        VaB<double>      maxDist;
        VaB<double>      minDistApex;
        VaB<double>      minDist;

        VaB<double>      maxExtH_50;
        VaB<double>      hMoyMaxExtH_50;
        VaB<double>      volAboveMaxExtH_50;

        VaB<double>      maxExtH_75;
        VaB<double>      hMoyMaxExtH_75;
        VaB<double>      volAboveMaxExtH_75;

        VaB<double>      maxExtH_90;
        VaB<double>      hMoyMaxExtH_90;
        VaB<double>      volAboveMaxExtH_90;

        VaB<double>      convexArea_50;
        VaB<double>      maxDist_50;

        VaB<double>      convexArea_75;
        VaB<double>      maxDist_75;

        VaB<double>      convexArea_90;
        VaB<double>      maxDist_90;



    };

    ONF_MetricRasterCrown3();
    ONF_MetricRasterCrown3(const ONF_MetricRasterCrown3 &other);

    QString getShortDescription() const;
    QString getDetailledDescription() const;

    /**
     * @brief Returns the metric configuration
     */
    ONF_MetricRasterCrown3::Config metricConfiguration() const;

    /**
     * @brief Change the configuration of this metric
     */
    void setMetricConfiguration(const ONF_MetricRasterCrown3::Config &conf);

    CT_AbstractConfigurableElement* copy() const;

protected:
    void computeMetric();
    void declareAttributes();

private:
    Config  m_configAndResults;


    double computePercentile(const QList<double> &array, const double &p);
};


#endif // ONF_METRICRASTERCROWN3_H
