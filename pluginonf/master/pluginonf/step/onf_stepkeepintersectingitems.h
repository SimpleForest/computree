#ifndef ONF_STEPKEEPINTERSECTINGITEMS_H
#define ONF_STEPKEEPINTERSECTINGITEMS_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_itemdrawable/ct_loopcounter.h"
#include "ct_itemdrawable/ct_standarditemgroup.h"
#include "ct_result/ct_resultgroup.h"


class ONF_StepKeepIntersectingItems : public CT_AbstractStep
{
    Q_OBJECT
public:
    ONF_StepKeepIntersectingItems(CT_StepInitializeData &dataInit);

    ~ONF_StepKeepIntersectingItems();

    QString getStepDescription() const;

    QString getStepDetailledDescription() const;

    virtual CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);


protected:

    virtual void createInResultModelListProtected();

    virtual void createOutResultModelListProtected();

    virtual void createPostConfigurationDialog();

    virtual void compute();

private:
    QList<CT_AbstractSingularItemDrawable*>               _ids;

    void recursiveRemoveGroupIfEmpty(CT_AbstractItemGroup *parent, CT_AbstractItemGroup *group) const;
};

#endif // ONF_STEPKEEPINTERSECTINGITEMS_H
