/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/


#include "onf_stepcomputeclustergrids.h"

#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"

// Inclusion of actions methods
#include "ct_tools/model/ct_outmodelcopyactionaddmodelitemingroup.h"

// Inclusion of standard result class
#include "ct_result/ct_resultgroup.h"

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_grid3d_sparse.h"
#include "ct_iterator/ct_pointiterator.h"

#include "ct_view/ct_stepconfigurabledialog.h"

#include <QFileInfo>
#include <QDebug>
#include <limits>

#define DEF_SearchInResult "r"
#define DEF_SearchInScene   "sc"
#define DEF_SearchInGroup   "gr"
#define DEF_SearchInItemDrawableAtt    "att"
#define DEF_SearchInItemWithName    "iname"


#define DEF_itemOut_grxy "grxy"
#define DEF_itemOut_grxz "grxz"
#define DEF_itemOut_gryz "gryz"

ONF_StepComputeClusterGrids::ONF_StepComputeClusterGrids(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _res = 0.5;
    _dilatation = 1;
}

QString ONF_StepComputeClusterGrids::getStepDescription() const
{
    // Gives the descrption to print in the GUI
    return tr("Créer des grilles booléennes par cluster");
}

// Step description (tooltip of contextual menu)
QString ONF_StepComputeClusterGrids::getStepDetailledDescription() const
{
    return tr("Pour chaque grille d'entrée, cette étape génère un grille voxel booléenne avec true pour les cases contenant un point<br>"
              "Les grilles sont sparse, il est donc possible d'utiliser des résolutions fines."
              "Un attribut stocke le nom du cluster");
}

CT_VirtualAbstractStep* ONF_StepComputeClusterGrids::createNewInstance(CT_StepInitializeData &dataInit)
{
    // Creates an instance of this step
    return new ONF_StepComputeClusterGrids(dataInit);
}

void ONF_StepComputeClusterGrids::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Scène(s)"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInScene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scène"));

    if (_otherItem)
    {
        resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInItemWithName, CT_AbstractSingularItemDrawable::staticGetType(), tr("ItemWithName"));
        resultModel->addItemAttributeModel(DEF_SearchInItemWithName, DEF_SearchInItemDrawableAtt, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::STRING, tr("Name"));
    } else {
        resultModel->addItemAttributeModel(DEF_SearchInScene, DEF_SearchInItemDrawableAtt, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::STRING, tr("Name"));
    }

}

void ONF_StepComputeClusterGrids::createOutResultModelListProtected()
{    
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != NULL)
    {
        res->addItemModel(DEF_SearchInGroup, _boolGrid_ModelName, new CT_Grid3D_Sparse<bool>(), tr("Cluster"));
        res->addItemAttributeModel(_boolGrid_ModelName, _clusterNameAtt_ModelName, new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_VALUE), tr("ClusterName"));
    }
}

void ONF_StepComputeClusterGrids::createPreConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPreConfigurationDialog();
    configDialog->addBool(tr("Nom de fichier dans un autre item"), "", "", _otherItem);

}


void ONF_StepComputeClusterGrids::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Résolution de la grille"),tr("meters"),0.0001,10000,3, _res );
    configDialog->addInt(tr("Dilatation"),tr("Cellules"),0,999, _dilatation);
}

void ONF_StepComputeClusterGrids::compute()
{
    // Gets the out result
    CT_ResultGroup* outResult = getOutResultList().first();

    CT_ResultGroupIterator it0(outResult, this, DEF_SearchInGroup);

    int nclusters = 0;
    while(!isStopped() && it0.hasNext())
    {
        it0.next();
        nclusters++;
    }

    CT_ResultGroupIterator it(outResult, this, DEF_SearchInGroup);
    int cpt = 0;
    // iterate over all groups
    while(!isStopped() && it.hasNext())
    {
        CT_AbstractItemGroup *group = (CT_AbstractItemGroup*)it.next();
        const CT_AbstractItemDrawableWithPointCloud* scene = (CT_AbstractItemDrawableWithPointCloud*)group->firstItemByINModelName(this, DEF_SearchInScene);

        if (scene != NULL)
        {
            CT_AbstractSingularItemDrawable *itemWithName = (CT_AbstractSingularItemDrawable*) scene;
            if (_otherItem)
            {
                itemWithName = (CT_AbstractSingularItemDrawable*) group->firstItemByINModelName(this, DEF_SearchInItemWithName);
            }

            QString clusterName;

            CT_AbstractItemAttribute* att = NULL;
            if (itemWithName != NULL)
            {
                att = itemWithName->firstItemAttributeByINModelName(outResult, this, DEF_SearchInItemDrawableAtt);
            }

            if (att != NULL)
            {
                clusterName = att->toString(itemWithName, NULL);

                QFileInfo fileinfo(clusterName);
                if (fileinfo.exists())
                {
                    clusterName = fileinfo.baseName();
                }
            }

            double minX = (std::floor(scene->minX() - 1) / _res) * _res;
            double minY = (std::floor(scene->minY() - 1) / _res) * _res;
            double minZ = (std::floor(scene->minZ() - 1) / _res) * _res;

            while (minX < scene->minX()) {minX += _res;};
            while (minY < scene->minY()) {minY += _res;};
            while (minZ < scene->minZ()) {minZ += _res;};

            while (minX > scene->minX()) {minX -= _res;};
            while (minY > scene->minY()) {minY -= _res;};
            while (minZ > scene->minZ()) {minZ -= _res;};

            // Declaring the output grids
            CT_Grid3D_Sparse<bool>* boolGrid = CT_Grid3D_Sparse<bool>::createGrid3DFromXYZCoords(_boolGrid_ModelName.completeName(), outResult,
                                                                                minX, minY, minZ,
                                                                                scene->maxX(), scene->maxY(), scene->maxZ(),
                                                                                _res, false, false);

            CT_PointIterator itP(scene->getPointCloudIndex()) ;
            while(itP.hasNext())
            {
                const CT_Point &point = itP.next().currentPoint();
                size_t indice;


                if (boolGrid->indexAtXYZ(point(0), point(1), point(2), indice))
                {
                    // Hits Computing
                    boolGrid->setValueAtIndex(indice, true);

                    if (_dilatation > 0)
                    {
                        size_t col, lin, levz;
                        boolGrid->indexToGrid(indice, col, lin, levz);

                        for (int xx = col - _dilatation; xx <= col +_dilatation ; xx++)
                        {
                            if (xx >= 0 && xx < boolGrid->xdim())
                            {
                                for (int yy = lin - _dilatation; yy <= lin + _dilatation ; yy++)
                                {
                                    if (yy >= 0 && yy < boolGrid->ydim())
                                    {
                                        for (int zz = levz - _dilatation ; zz <= levz + _dilatation ; zz++)
                                        {
                                            if (zz >= 0 && zz < boolGrid->zdim())
                                            {
                                                boolGrid->setValue(xx, yy, zz, true);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                } else {
                    qDebug() << "Le point n'est pas dans la grille";
                }
            }

            group->addItemDrawable(boolGrid);
            boolGrid->addItemAttribute(new CT_StdItemAttributeT<QString>(_clusterNameAtt_ModelName.completeName(), CT_AbstractCategory::DATA_VALUE, outResult, clusterName));

        }

        setProgress(100 * cpt++ / nclusters);
    }


    setProgress(99);
}


