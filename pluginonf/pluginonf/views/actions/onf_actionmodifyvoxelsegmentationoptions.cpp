/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_actionmodifyvoxelsegmentationoptions.h"
#include "ui_onf_actionmodifyvoxelsegmentationoptions.h"
#include "ct_global/ct_context.h"

#include <QColorDialog>

ONF_ActionModifyVoxelSegmentationOptions::ONF_ActionModifyVoxelSegmentationOptions(const ONF_ActionModifyVoxelSegmentation *action) :
    CT_GAbstractActionOptions(action),
    ui(new Ui::ONF_ActionModifyVoxelSegmentationOptions)
{
    ui->setupUi(this);

    ui->pb_SetSceneA->setToolTip(tr("Choisit le groupe A [ALT clic GAUCHE]"));
    ui->pb_SetSceneB->setToolTip(tr("Choisit le groupe Z [ALT clic DROIT]"));
    ui->pb_extend->setToolTip(tr("Etendre la séléction [X]"));
    ui->toolButtonSelectMulti->setToolTip(tr("Maintenir [CTRL] enfoncé pour activer temporairement"));

    ui->pb_toA->setToolTip(tr("Affecte les points au groupe A [A]"));
    ui->pb_toB->setToolTip(tr("Affecte les points au groupe Z [Z]"));
    ui->pb_toNew->setToolTip(tr("Affecte les points à un nouveau groupe [E]"));
    ui->pb_toTrash->setToolTip(tr("Affecte les points à la poubelle [R]"));

    ui->cb_othersVisible->setToolTip(tr("Touche [espace] pour changer"));

    ui->pb_validate->setToolTip(tr("Valider le groupe A [V]"));

    ui->pb_extend->setStyleSheet("QPushButton { background-color: #FFFFFF; }");
    ui->pb_toNew->setStyleSheet("QToolButton { background-color: #FFFFFF; }");
    ui->pb_toTrash->setStyleSheet("QToolButton { background-color: #FFFFFF; }");
    ui->pb_selectSeed->setStyleSheet("QPushButton { background-color: #FFFFFF; }");

    QRegExp rx("^[a-zA-Z0-9_]*$");
    QValidator *validator = new QRegExpValidator(rx, this);
    ui->le_labelA->setValidator(validator);
}

ONF_ActionModifyVoxelSegmentationOptions::~ONF_ActionModifyVoxelSegmentationOptions()
{
    delete ui;
}

GraphicsViewInterface::SelectionMode ONF_ActionModifyVoxelSegmentationOptions::selectionMode() const
{   
    int mode = GraphicsViewInterface::NONE;

    if (ui->toolButtonSelectOne->isChecked())
    {
        if (ui->toolButtonReplaceMode->isChecked()) {
            mode = GraphicsViewInterface::SELECT_ONE_POINT;
        } else if (ui->toolButtonAddMode->isChecked()) {
            mode = GraphicsViewInterface::ADD_ONE_POINT;
        } else {
            mode = GraphicsViewInterface::REMOVE_ONE_POINT;
        }
    } else if (ui->toolButtonSelectMulti->isChecked()) {
        if (ui->toolButtonReplaceMode->isChecked()) {
            mode = GraphicsViewInterface::SELECT_POINTS;
        } else if (ui->toolButtonAddMode->isChecked()) {
            mode = GraphicsViewInterface::ADD_POINTS;
        } else {
            mode = GraphicsViewInterface::REMOVE_POINTS;
        }
    } else {
        return (GraphicsViewInterface::SelectionMode)mode;
    }

    return (GraphicsViewInterface::SelectionMode)mode;
}

bool ONF_ActionModifyVoxelSegmentationOptions::isAVisible() const
{
    return ui->cb_Avisible->isChecked();
}

bool ONF_ActionModifyVoxelSegmentationOptions::isBVisible() const
{
    return ui->cb_Bvisible->isChecked();
}

bool ONF_ActionModifyVoxelSegmentationOptions::isTrashVisible() const
{
    return ui->cb_trashVisible->isChecked();
}

bool ONF_ActionModifyVoxelSegmentationOptions::isOthersVisible() const
{
    return ui->cb_othersVisible->isChecked();
}

bool ONF_ActionModifyVoxelSegmentationOptions::isValidatedVisible() const
{
    return ui->cb_showValidated->isChecked();
}

bool ONF_ActionModifyVoxelSegmentationOptions::isValidatedSelectable() const
{
    return ui->cb_selectValidated->isChecked();
}

void ONF_ActionModifyVoxelSegmentationOptions::on_buttonGroupMode_buttonReleased(int id)
{
    Q_UNUSED(id)

    (dynamic_cast<ONF_ActionModifyVoxelSegmentation*>(action()))->setSelectionMode(selectionMode());
}

void ONF_ActionModifyVoxelSegmentationOptions::on_buttonGroupSelection_buttonReleased(int id)
{
    Q_UNUSED(id)

    (dynamic_cast<ONF_ActionModifyVoxelSegmentation*>(action()))->setSelectionMode(selectionMode());
}

void ONF_ActionModifyVoxelSegmentationOptions::setSelectionMode(GraphicsViewInterface::SelectionMode mode)
{
    if(mode != GraphicsViewInterface::NONE)
    {
        int m = mode;

        while(m > GraphicsViewInterface::REMOVE_ONE_POINT)
            m -= GraphicsViewInterface::REMOVE_ONE;       

        if (mode == GraphicsViewInterface::SELECT_POINTS) {
            ui->toolButtonSelectMulti->setChecked(true);
            ui->toolButtonReplaceMode->setChecked(true);
        } else if (mode == GraphicsViewInterface::ADD_POINTS) {
            ui->toolButtonSelectMulti->setChecked(true);
            ui->toolButtonAddMode->setChecked(true);
        } else if (mode == GraphicsViewInterface::REMOVE_POINTS) {
            ui->toolButtonSelectMulti->setChecked(true);
            ui->toolButtonRemoveMode->setChecked(true);
        } else if (mode == GraphicsViewInterface::SELECT_ONE_POINT) {
            ui->toolButtonSelectOne->setChecked(true);
            ui->toolButtonReplaceMode->setChecked(true);
        } else if (mode == GraphicsViewInterface::ADD_ONE_POINT) {
            ui->toolButtonSelectOne->setChecked(true);
            ui->toolButtonAddMode->setChecked(true);
        } else if (mode == GraphicsViewInterface::REMOVE_ONE_POINT) {
            ui->toolButtonSelectOne->setChecked(true);
            ui->toolButtonRemoveMode->setChecked(true);
        }
    }
}

void ONF_ActionModifyVoxelSegmentationOptions::on_pb_SetSceneA_clicked()
{
    emit selectPositionA();
}

void ONF_ActionModifyVoxelSegmentationOptions::on_pb_SetSceneB_clicked()
{
    emit selectPositionB();
}

void ONF_ActionModifyVoxelSegmentationOptions::on_pb_ColorA_clicked()
{
    QColor tmp = QColorDialog::getColor(_colorA);
    if (!tmp.isValid()) {return;}
    selectColorA(tmp);
    emit setColorA(tmp);
}

void ONF_ActionModifyVoxelSegmentationOptions::selectColorA(QColor color)
{
    _colorA = color;
    ui->pb_SetSceneA->setStyleSheet("QPushButton { background-color: " + _colorA.name() + "; }");
    ui->pb_toA->setStyleSheet("QToolButton { background-color: " + _colorA.name() + "; }");
}

void ONF_ActionModifyVoxelSegmentationOptions::on_pb_ColorB_clicked()
{
    QColor tmp = QColorDialog::getColor(_colorB);
    if (!tmp.isValid()) {return;}
    selectColorB(tmp);
    emit setColorB(tmp);
}

void ONF_ActionModifyVoxelSegmentationOptions::selectColorB(QColor color)
{
    _colorB = color;
    ui->pb_SetSceneB->setStyleSheet("QPushButton { background-color: " + _colorB.name() + "; }");
    ui->pb_toB->setStyleSheet("QToolButton { background-color: " + _colorB.name() + "; }");
}

void ONF_ActionModifyVoxelSegmentationOptions::setMultiSelect(bool multi)
{
    if (multi)
    {
        ui->toolButtonSelectMulti->setChecked(true);
    } else {
        ui->toolButtonSelectOne->setChecked(true);
    }
}

void ONF_ActionModifyVoxelSegmentationOptions::setLabelA(QString label)
{
    ui->le_labelA->setText(label);
}

void ONF_ActionModifyVoxelSegmentationOptions::toggleOthersVisible()
{
    if (ui->cb_othersVisible->isChecked())
    {
        ui->cb_othersVisible->setChecked(false);
    } else {
        ui->cb_othersVisible->setChecked(true);
    }
}

void ONF_ActionModifyVoxelSegmentationOptions::on_pb_toA_clicked()
{
    emit affectClusterToA();
}

void ONF_ActionModifyVoxelSegmentationOptions::on_pb_toB_clicked()
{
    emit affectClusterToB();
}

void ONF_ActionModifyVoxelSegmentationOptions::on_pb_toTrash_clicked()
{
    emit affectClusterToTrash();
}

void ONF_ActionModifyVoxelSegmentationOptions::on_cb_Avisible_toggled(bool checked)
{
    Q_UNUSED(checked);
    emit visibilityChanged();
}

void ONF_ActionModifyVoxelSegmentationOptions::on_cb_Bvisible_toggled(bool checked)
{
    Q_UNUSED(checked);
    emit visibilityChanged();
}

void ONF_ActionModifyVoxelSegmentationOptions::on_cb_othersVisible_toggled(bool checked)
{
    Q_UNUSED(checked);
    emit visibilityChanged();
}

void ONF_ActionModifyVoxelSegmentationOptions::on_cb_trashVisible_toggled(bool checked)
{
    Q_UNUSED(checked);
    emit visibilityChanged();
}


void ONF_ActionModifyVoxelSegmentationOptions::on_pb_extend_clicked()
{
    emit extend();
}

void ONF_ActionModifyVoxelSegmentationOptions::on_pb_validate_clicked()
{
    emit validatePosition();
}

void ONF_ActionModifyVoxelSegmentationOptions::on_cb_showValidated_toggled(bool checked)
{
    Q_UNUSED(checked);
    emit visibilityChanged();
}

void ONF_ActionModifyVoxelSegmentationOptions::on_pb_toNew_clicked()
{
    emit affectClusterToNew();
}

void ONF_ActionModifyVoxelSegmentationOptions::on_pb_selectSeed_clicked()
{
    emit selectSeed();
}

void ONF_ActionModifyVoxelSegmentationOptions::on_le_labelA_textChanged(const QString &arg1)
{
    emit labelAChanged(arg1);
}

