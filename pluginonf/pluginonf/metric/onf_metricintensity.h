/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_METRICINTENSITY_H
#define ONF_METRICINTENSITY_H

#include "ctliblas/metrics/abstract/ct_abstractmetric_las.h"
#include "ctlibmetrics/tools/ct_valueandbool.h"

class ONF_MetricIntensity : public CT_AbstractMetric_LAS
{
    Q_OBJECT
public:

    struct Config {
        VaB<size_t>      n;
        VaB<size_t>      n_first;
        VaB<double>      i_max_first;
        VaB<double>      i_mean_first;
        VaB<double>      i_sd_first;
        VaB<double>      i_cv_first;
        VaB<size_t>      n_only;
        VaB<double>      i_max_only;
        VaB<double>      i_mean_only;
        VaB<double>      i_sd_only;
        VaB<double>      i_cv_only;
        VaB<size_t>      n_5_95_first;
        VaB<double>      i_5_95_mean_first;
        VaB<double>      i_5_95_sd_first;
        VaB<double>      i_5_95_cv_first;
        VaB<size_t>      n_5_95_only;
        VaB<double>      i_5_95_mean_only;
        VaB<double>      i_5_95_sd_only;
        VaB<double>      i_5_95_cv_only;
        VaB<size_t>      n_10_90_first;
        VaB<double>      i_10_90_mean_first;
        VaB<double>      i_10_90_sd_first;
        VaB<double>      i_10_90_cv_first;
        VaB<size_t>      n_10_90_only;
        VaB<double>      i_10_90_mean_only;
        VaB<double>      i_10_90_sd_only;
        VaB<double>      i_10_90_cv_only;
        VaB<double>      i_p05_first;
        VaB<double>      i_p10_first;
        VaB<double>      i_p25_first;
        VaB<double>      i_p50_first;
        VaB<double>      i_p75_first;
        VaB<double>      i_p90_first;
        VaB<double>      i_p95_first;
        VaB<double>      i_p05_only;
        VaB<double>      i_p10_only;
        VaB<double>      i_p25_only;
        VaB<double>      i_p50_only;
        VaB<double>      i_p75_only;
        VaB<double>      i_p90_only;
        VaB<double>      i_p95_only;

        VaB<size_t>      n_top25_first;
        VaB<double>      i_top25_mean_first;
        VaB<double>      i_top25_sd_first;
        VaB<double>      i_top25_cv_first;
        VaB<size_t>      n_top25_only;
        VaB<double>      i_top25_mean_only;
        VaB<double>      i_top25_sd_only;
        VaB<double>      i_top25_cv_only;

    };

    ONF_MetricIntensity();
    ONF_MetricIntensity(const ONF_MetricIntensity &other);

    QString getShortDescription() const;
    QString getDetailledDescription() const;

    /**
     * @brief Returns the metric configuration
     */
    ONF_MetricIntensity::Config metricConfiguration() const;

    /**
     * @brief Change the configuration of this metric
     */
    void setMetricConfiguration(const ONF_MetricIntensity::Config &conf);

    CT_AbstractConfigurableElement* copy() const;

protected:
    void computeMetric();
    void declareAttributes();

private:
    Config  m_conf;
};


#endif // ONF_METRICINTENSITY_H
