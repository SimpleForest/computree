/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef ONF_ACTIONMODIFYDEM_H
#define ONF_ACTIONMODIFYDEM_H

#ifdef USE_OPENCV

#include <QRect>

#include "ct_actions/abstract/ct_abstractactionforgraphicsview.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_image2d.h"
#include "tools/onf_colorlinearinterpolator.h"

/**
 * @brief Action to select items, points, faces, edges, etc...
 *
 * You can use this action in your action if you want. Just call methods :
 *
 * init()
 * mousePressEvent()
 * etc.....
 *
 * in methods of your action.
 *
 * Dont forget to set the selection mode => setSelectionMode method
 */
class ONF_ActionModifyDEM : public CT_AbstractActionForGraphicsView
{
    Q_OBJECT
public:

    ONF_ActionModifyDEM(CT_AbstractItemDrawableWithPointCloud* scene, CT_Image2D<float>* originalDEM, CT_Image2D<float>* modifiedDEM, CT_AbstractImage2D* inred, CT_AbstractImage2D* ingreen, CT_AbstractImage2D* inblue);

    QString uniqueName() const;
    QString title() const;
    QString description() const;
    QIcon icon() const;
    QString type() const;

    void init();

    bool mousePressEvent(QMouseEvent *e);
    bool mouseMoveEvent(QMouseEvent *e);
    bool mouseReleaseEvent(QMouseEvent *e);

    bool keyPressEvent(QKeyEvent *e);
    bool keyReleaseEvent(QKeyEvent *e);

    void drawOverlay(GraphicsViewInterface &view, QPainter &painter);

    CT_AbstractAction* copy() const;

    /**
     * @brief change the selection mode for this action. The mode will be set to view
     *        when the mousePressEvent() method is called
     * @return false if we are between mousePressEvent() and mouseReleaseEvent()
     */
    bool setSelectionMode(GraphicsViewInterface::SelectionMode mode);

    GraphicsViewInterface::SelectionMode selectionMode() const;


public slots:
    void colorizePoints();
    void redraw();
    void levelPoints(bool up, bool maxed);
    void razPoints();
    void smooth();


private:
    QRect                                   m_selectionRectangle;
    int                                     m_status;
    GraphicsViewInterface::SelectionMode    m_selectionMode;
    bool                                    m_mousePressed;
    double _amplitudeRed;
    double _amplitudeGreen;
    double _amplitudeBlue;

    CT_AbstractItemDrawableWithPointCloud*  _scene;
    CT_Image2D<float>*                      _originalDEM;
    CT_Image2D<float>*                      _modifiedDEM;
    CT_AbstractImage2D* _inred;
    CT_AbstractImage2D* _ingreen;
    CT_AbstractImage2D* _inblue;


    ONF_ColorLinearInterpolator                _gradientGrey;

    GraphicsViewInterface::SelectionMode selectionModeToBasic(GraphicsViewInterface::SelectionMode mode) const;

};

#endif

#endif // ONF_ACTIONMODIFYDEM_H
