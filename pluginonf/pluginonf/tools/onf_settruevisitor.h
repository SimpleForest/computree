#ifndef ONF_SETTRUEVISITOR_H
#define ONF_SETTRUEVISITOR_H

#include "ct_itemdrawable/tools/gridtools/ct_abstractgrid3dbeamvisitor.h"
#include "ct_itemdrawable/ct_grid3d_sparse.h"

class ONF_SetTrueVisitor : public CT_AbstractGrid3DBeamVisitor
{
public:

    ONF_SetTrueVisitor(CT_Grid3D_Sparse<bool> *grid);

    virtual void visit(const size_t &index, const CT_Beam *beam);

private:
    CT_Grid3D_Sparse<bool>*     _grid;
};

#endif // ONF_SETTRUEVISITOR_H
