/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepdetectlinesofscan.h"


#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_line.h"
#include "ct_itemdrawable/ct_pointcluster.h"
#include "ctliblas/itemdrawable/las/ct_stdlaspointsattributescontainer.h"
#include "ct_itemdrawable/ct_standarditemgroup.h"

#include "ct_itemdrawable/ct_attributeslist.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_iterator/ct_pointiterator.h"
#include "ct_math/ct_sphericalline3d.h"
#include "ct_math/ct_mathstatistics.h"
#include "ct_math/ct_mathpoint.h"


#include <QtConcurrent>

#include <stdlib.h>

// Alias for indexing models
#define DEFin_res "res"
#define DEFin_grp "grp"
#define DEFin_sceneStem "sceneStem"
#define DEFin_attLAS "attLAS"



// Constructor : initialization of parameters
ONF_StepDetectLinesOfScan::ONF_StepDetectLinesOfScan(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _thresholdGPSTime = 1e-5;
    _maxCurvature = 0.25;
    _maxXYDist = 0.35;
    _thresholdZenithalAngle = 30.0;
    _minPts = 2;
}

// Step description (tooltip of contextual menu)
QString ONF_StepDetectLinesOfScan::getStepDescription() const
{
    return tr("Détecter les lignes de scan");
}

// Step detailled description
QString ONF_StepDetectLinesOfScan::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString ONF_StepDetectLinesOfScan::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ONF_StepDetectLinesOfScan::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ONF_StepDetectLinesOfScan(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ONF_StepDetectLinesOfScan::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_res = createNewInResultModelForCopy(DEFin_res, tr("Scènes"));
    resIn_res->setZeroOrMoreRootGroup();
    resIn_res->addGroupModel("", DEFin_grp, CT_AbstractItemGroup::staticGetType(), tr("Scènes (grp)"));
    resIn_res->addItemModel(DEFin_grp, DEFin_attLAS, CT_StdLASPointsAttributesContainer::staticGetType(), tr("Attributs LAS"), tr("Attribut LAS"));
    resIn_res->addItemModel(DEFin_grp, DEFin_sceneStem, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scène (tiges)"));
}

// Creation and affiliation of OUT models
void ONF_StepDetectLinesOfScan::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy = createNewOutResultModelToCopy(DEFin_res);

    if(resCpy != NULL) {
        resCpy->addGroupModel(DEFin_grp, _grpClusterAll_ModelName, new CT_StandardItemGroup(), tr("Lignes de scan complètes (toutes)"));
        resCpy->addItemModel(_grpClusterAll_ModelName, _clusterAll_ModelName, new CT_PointCluster(), tr("Lignes de scan complètes (toutes)"));
        resCpy->addGroupModel(DEFin_grp, _grpClusterDenoised_ModelName, new CT_StandardItemGroup(), tr("Lignes de scan débruitées (toutes)"));
        resCpy->addItemModel(_grpClusterDenoised_ModelName, _clusterDenoised_ModelName, new CT_PointCluster(), tr("Lignes de scan débruitées (toutes)"));
        resCpy->addGroupModel(DEFin_grp, _grpClusterKept_ModelName, new CT_StandardItemGroup(), tr("Lignes de scan (conservées)"));
        resCpy->addItemModel(_grpClusterKept_ModelName, _clusterKept_ModelName, new CT_PointCluster(), tr("Lignes de scan (conservées)"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void ONF_StepDetectLinesOfScan::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addText(tr("1- Détéction des lignes (Toutes)"));
    configDialog->addDouble(tr("Seuil de temps GPS pour changer de ligne de scan"), "m", 0, 1e+10, 10, _thresholdGPSTime);

    configDialog->addEmpty();
    configDialog->addText(tr("2- Débruitage des lignes (Débruitées)"));
    configDialog->addDouble(tr("Courbure maximale d'une ligne de scan"), "cm", 0, 1e+4, 2, _maxCurvature, 100);
    configDialog->addDouble(tr("Distance XY maximale entre points d'une ligne de scan"), "cm", 0, 1e+4, 2, _maxXYDist, 100);


    configDialog->addEmpty();
    configDialog->addText(tr("3- Filtrage des lignes (Conservées)"));
    configDialog->addDouble(tr("Angle zénithal maximal pour conserver une ligne de scan"), "°", 0, 360, 2, _thresholdZenithalAngle);
    configDialog->addInt(   tr("Nombre de points minimal pour conserver une ligne de scan"), "points", 0, 1000, _minPts);

}

void ONF_StepDetectLinesOfScan::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res = outResultList.at(0);


    QList<CT_StandardItemGroup*> groups;
    // COPIED results browsing
    CT_ResultGroupIterator itCpy_grp(res, this, DEFin_grp);
    while (itCpy_grp.hasNext())
    {
        CT_StandardItemGroup* grp = (CT_StandardItemGroup*) itCpy_grp.next();
        groups.append(grp);
    }


    QFuture<void> futur = QtConcurrent::map(groups, AlignmentsDetectorForScene(this, res));

    int progressMin = futur.progressMinimum();
    int progressTotal = futur.progressMaximum() - futur.progressMinimum();
    while (!futur.isFinished())
    {
        setProgress(100.0*(futur.progressValue() - progressMin)/progressTotal);
    }

}




void ONF_StepDetectLinesOfScan::AlignmentsDetectorForScene::detectAlignmentsForScene(CT_StandardItemGroup* grp)
{

    double thresholdZenithalAngleRadians = M_PI * _step->_thresholdZenithalAngle / 180.0;

    _step->_mutex.lock();

    const CT_AbstractItemDrawableWithPointCloud* sceneStem = (CT_AbstractItemDrawableWithPointCloud*)grp->firstItemByINModelName(_step, DEFin_sceneStem);
    const CT_StdLASPointsAttributesContainer* attributeLAS = (CT_StdLASPointsAttributesContainer*)grp->firstItemByINModelName(_step, DEFin_attLAS);

    _step->_mutex.unlock();

    if (sceneStem != NULL && attributeLAS != NULL)
    {

        if (sceneStem->getPointCloudIndexSize() == 0) {return;}

        // Retrieve attributes
        QHashIterator<CT_LasDefine::LASPointAttributesType, CT_AbstractPointAttributesScalar *> it(attributeLAS->lasPointsAttributes());
        if (!it.hasNext()) {return;}

        CT_AbstractPointAttributesScalar *firstAttribute = it.next().value();
        if (firstAttribute == NULL) {return;}

        const CT_AbstractPointCloudIndex* pointCloudIndexLAS = firstAttribute->getPointCloudIndex();
        if (pointCloudIndexLAS == NULL) {return;}

        CT_AbstractPointAttributesScalar* attributeGPS          = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::GPS_Time);
        CT_AbstractPointAttributesScalar* attributeIntensity     = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Intensity);
        CT_AbstractPointAttributesScalar* attributeLineOfFlight = (CT_AbstractPointAttributesScalar*)attributeLAS->pointsAttributesAt(CT_LasDefine::Point_Source_ID);

        if (attributeIntensity == NULL || attributeGPS == NULL || attributeLineOfFlight == NULL) {return;}

        const CT_AbstractPointCloudIndex* pointCloudIndex = sceneStem->getPointCloudIndex();

        ////////////////////////////////////////////////////
        /// Detection of big stems: lines of scan        ///
        ////////////////////////////////////////////////////


        // Sort indices by GPS time
        QMultiMap<double, size_t> sortedIndices;
        sortIndicesByGPSTime(pointCloudIndexLAS, attributeGPS, pointCloudIndex, sortedIndices);

        // List of not clusterized points
        QList<size_t> isolatedPointIndices;


        // Creation des lignes de scan
        QList<QList<size_t> > linesOfScan;
        createLinesOfScan(sortedIndices, linesOfScan, isolatedPointIndices);
        sortedIndices.clear();

        // store lines of scan (all complete)
        for (int lineN = 0 ; lineN < linesOfScan.size() ; lineN++)
        {
            QList<size_t> completeLine = linesOfScan.at(lineN);

            CT_PointCluster* cluster = new CT_PointCluster(_step->_clusterAll_ModelName.completeName(), _res);
            for (int j = 0 ; j < completeLine.size() ; j++)
            {
                cluster->addPoint(completeLine.at(j));
            }
            CT_StandardItemGroup* grpClKept = new CT_StandardItemGroup(_step->_grpClusterAll_ModelName.completeName(), _res);
            grp->addGroup(grpClKept);
            grpClKept->addItemDrawable(cluster);
        }

        
        // Eliminate noise in lines of scan
        QList<QList<size_t> > simplifiedLinesOfScan;
        denoiseLinesOfScan(linesOfScan, pointCloudIndexLAS, attributeIntensity, simplifiedLinesOfScan, isolatedPointIndices);
        linesOfScan.clear();


        // store lines of scan (all denoised)
        for (int i = 0 ; i < simplifiedLinesOfScan.size() ; i++)
        {
            QList<size_t> line = simplifiedLinesOfScan.at(i);
            CT_PointCluster* cluster = new CT_PointCluster(_step->_clusterDenoised_ModelName.completeName(), _res);
            for (int j = 0 ; j < line.size() ; j++)
            {
                cluster->addPoint(line.at(j));
            }
            CT_StandardItemGroup* grpClKept = new CT_StandardItemGroup(_step->_grpClusterDenoised_ModelName.completeName(), _res);
            grp->addGroup(grpClKept);
            grpClKept->addItemDrawable(cluster);
        }


        // Remove clusters with 1 point or with phi > maxPhi
        QList<ScanLineData*> keptLinesOfScan;
        filterLinesOfScan(simplifiedLinesOfScan, thresholdZenithalAngleRadians, keptLinesOfScan, isolatedPointIndices);
        simplifiedLinesOfScan.clear();



        // store lines of scan (conserved)
        for (int i = 0 ; i < keptLinesOfScan.size() ; i++)
        {
            ScanLineData* line = keptLinesOfScan.at(i);
            CT_PointCluster* cluster = new CT_PointCluster(_step->_clusterKept_ModelName.completeName(), _res);
            for (int j = 0 ; j < line->size() ; j++)
            {
                cluster->addPoint(line->at(j));
            }
            CT_StandardItemGroup* grpClKept = new CT_StandardItemGroup(_step->_grpClusterKept_ModelName.completeName(), _res);
            grp->addGroup(grpClKept);
            grpClKept->addItemDrawable(cluster);
        }
    }


}


void ONF_StepDetectLinesOfScan::AlignmentsDetectorForScene::sortIndicesByGPSTime(const CT_AbstractPointCloudIndex* pointCloudIndexLAS,
                                                                                 const CT_AbstractPointAttributesScalar* attributeGPS,
                                                                                 const CT_AbstractPointCloudIndex* pointCloudIndex,
                                                                                 QMultiMap<double, size_t> &sortedIndices)
{
    CT_PointIterator itP(pointCloudIndex);
    while(itP.hasNext())
    {
        size_t index = itP.next().currentGlobalIndex();
        size_t localIndex = pointCloudIndexLAS->indexOf(index);

        double gpsTime = 0; // Récupération du temps GPS pour le point 1
        if (localIndex < pointCloudIndexLAS->size())
        {
            gpsTime = attributeGPS->dValueAt(localIndex);
            sortedIndices.insert(gpsTime, index);
        }
    }
}

void ONF_StepDetectLinesOfScan::AlignmentsDetectorForScene::createLinesOfScan(const QMultiMap<double, size_t> &sortedIndices,
                                                                              QList<QList<size_t> > &linesOfScan,
                                                                              QList<size_t> &isolatedPointIndices)
{
    QList<size_t> currentIndexList;

    double lastGPSTime = -std::numeric_limits<double>::max();
    QMapIterator<double, size_t> itMapSorted(sortedIndices);
    while (itMapSorted.hasNext())
    {
        itMapSorted.next();
        double gpsTime = itMapSorted.key();
        size_t index = itMapSorted.value();

        double delta = gpsTime - lastGPSTime;
        lastGPSTime = gpsTime;

        if (delta < _step->_thresholdGPSTime)
        {
            // add point to current line of scan
            currentIndexList.append(index);
        } else {
            // init a new line of scan
            if (currentIndexList.size() > 1)
            {
                linesOfScan.append(currentIndexList);
            } else {
                if (!currentIndexList.isEmpty()) {isolatedPointIndices.append(currentIndexList.first());}
            }
            currentIndexList.clear();
            // then add the point
            currentIndexList.append(index);
        }
    }
}

void ONF_StepDetectLinesOfScan::AlignmentsDetectorForScene::denoiseLinesOfScan(const QList<QList<size_t> > &linesOfScan,
                                                                               const CT_AbstractPointCloudIndex* pointCloudIndexLAS,
                                                                               const CT_AbstractPointAttributesScalar* attributeIntensity,
                                                                               QList<QList<size_t> > &simplifiedLinesOfScan,
                                                                               QList<size_t> &isolatedPointIndices)
{
    CT_PointAccessor pointAccessor;

    QList<QList<size_t> > linesOfScanTmp;
    linesOfScanTmp.append(linesOfScan);

    for (int lineN = 0 ; lineN < linesOfScanTmp.size() ; lineN++)
    {
        QList<size_t> completeLine = linesOfScanTmp.at(lineN);
        int sizeCompleteLine = completeLine.size();

        // Find a reference point on the line of scan (max intensity point)
        double maxIntensity = -std::numeric_limits<double>::max();
        int refi = 0;
        for (int i = 0 ; i < sizeCompleteLine ; i++)
        {
            size_t localIndex = pointCloudIndexLAS->indexOf(completeLine.at(i));
            double intensity = maxIntensity;
            if (localIndex < pointCloudIndexLAS->size()) {intensity = attributeIntensity->dValueAt(localIndex);}

            if (intensity > maxIntensity)
            {
                maxIntensity = intensity;
                refi = i;
            }
        }

        int newRefI = 0;
        // Test all lines linking reference point to another point, and keep the best one: max number of points, or if equal min length
        CT_Point p1 = pointAccessor.constPointAt(completeLine.at(refi));
        QList<size_t> bestSimplifiedLine;
        double bestSimplifiedLineLength = std::numeric_limits<double>::max();
        for (int i = 0 ; i < sizeCompleteLine ; i++)
        {
            if (i != refi)
            {
                CT_Point p2 = pointAccessor.constPointAt(completeLine.at(i));
                Eigen::Vector3d direction = p2 - p1;
                direction.normalize();

                QList<size_t> simplifiedLine;

                for (int j = 0 ; j < sizeCompleteLine ; j++)
                {
                    if (j == refi)
                    {
                        simplifiedLine.append(completeLine.at(j));
                        newRefI = simplifiedLine.size() - 1;
                    } else if (j == i) {
                        simplifiedLine.append(completeLine.at(j));
                    } else {
                        size_t index = completeLine.at(j);
                        CT_Point p3 = pointAccessor.constPointAt(index);
                        double curv = CT_MathPoint::distancePointLine(p3, direction, p1);
                        if (curv < _step->_maxCurvature)
                        {
                            simplifiedLine.append(index);
                        }
                    }
                }

                // Compute bestSimplifiedLineLength
                const size_t index01 = simplifiedLine.first();
                const size_t index02 = simplifiedLine.last();

                CT_Point p01 = pointAccessor.constPointAt(index01);
                CT_Point p02 = pointAccessor.constPointAt(index02);

                double length = sqrt(pow(p01(0) - p02(0), 2) + pow(p01(1) - p02(1), 2) + pow(p01(2) - p02(2), 2));

                // Compare with previous best simplified line
                if (simplifiedLine.size() > bestSimplifiedLine.size())
                {
                    bestSimplifiedLine = simplifiedLine;
                    bestSimplifiedLineLength = length;
                } else if (simplifiedLine.size() == bestSimplifiedLine.size() && length < bestSimplifiedLineLength)
                {
                    bestSimplifiedLine = simplifiedLine;
                    bestSimplifiedLineLength = length;
                }
            }
        }

        QList<size_t> finalSimplifiedLine;
        if (bestSimplifiedLine.size() > 1)
        {

            // Delete extremities from reference points, if a distXY between succesive points > threshold
            int firstI = 0;
            bool stop = false;
            for (int i = newRefI - 1 ; !stop && i >= 0 ; i--)
            {
                const size_t index01 = bestSimplifiedLine.at(i);
                const size_t index02 = bestSimplifiedLine.at(i+1);

                CT_Point p01 = pointAccessor.constPointAt(index01);
                CT_Point p02 = pointAccessor.constPointAt(index02);

                double distXY = sqrt(pow(p01(0) - p02(0), 2) + pow(p01(1) - p02(1), 2));
                if (distXY > _step->_maxXYDist)
                {
                    firstI = i + 1;
                    stop = true;
                }
            }

            int lastI = bestSimplifiedLine.size() - 1;
            stop = false;
            for (int i = newRefI + 1 ; !stop && i < bestSimplifiedLine.size() ; i++)
            {
                const size_t index01 = bestSimplifiedLine.at(i);
                const size_t index02 = bestSimplifiedLine.at(i-1);

                CT_Point p01 = pointAccessor.constPointAt(index01);
                CT_Point p02 = pointAccessor.constPointAt(index02);

                double distXY = sqrt(pow(p01(0) - p02(0), 2) + pow(p01(1) - p02(1), 2));
                if (distXY > _step->_maxXYDist)
                {
                    lastI = i - 1;
                    stop = true;
                }
            }

            // If it remains points in the line, create cluster
            if ((lastI - firstI) > 0)
            {
                for (int i = firstI ; i <= lastI ; i++)
                {
                    finalSimplifiedLine.append(bestSimplifiedLine.at(i));
                }
                simplifiedLinesOfScan.append(finalSimplifiedLine);
            }
        }

        // If during the line analysis, enough succesive removed points ara found, there are considered as a separated line, and will be considered again
        // All noise points are added to isolatedPointsIndices or in lines list
        QList<size_t> previousRemovedIndices;
        for (int i = 0 ; i < sizeCompleteLine ; i++)
        {
            size_t index = completeLine.at(i);
            if (finalSimplifiedLine.contains(index))
            {
                if (previousRemovedIndices.size() == 1)
                {
                    isolatedPointIndices.append(previousRemovedIndices.first());
                } else if (previousRemovedIndices.size() > 1) {
                    linesOfScanTmp.append(previousRemovedIndices);
                    previousRemovedIndices.clear();
                }
                previousRemovedIndices.clear();
            } else {
                previousRemovedIndices.append(index);
            }
        }

        if (previousRemovedIndices.size() == 1)
        {
            isolatedPointIndices.append(previousRemovedIndices.first());
        } else if (previousRemovedIndices.size() > 1) {

            if (finalSimplifiedLine.size() > 0)
            {
                linesOfScanTmp.append(previousRemovedIndices);
            } else {
                for (int i = 0 ; i < previousRemovedIndices.size() ; i++)
                {
                    isolatedPointIndices.append(previousRemovedIndices.at(i));
                }
            }
            previousRemovedIndices.clear();
        }


    }
}

void ONF_StepDetectLinesOfScan::AlignmentsDetectorForScene::filterLinesOfScan(QList<QList<size_t> > &simplifiedLinesOfScan,
                                                                              double thresholdZenithalAngleRadians,
                                                                              QList<ScanLineData*> &keptLinesOfScan,
                                                                              QList<size_t> &isolatedPointIndices)
{
    CT_PointAccessor pointAccessor;

    for (int i = 0 ; i < simplifiedLinesOfScan.size() ; i++)
    {
        QList<size_t> simplifiedLine = simplifiedLinesOfScan.at(i);

        if (simplifiedLine.size() < _step->_minPts)
        {
            for (int j = 0 ; j < simplifiedLine.size() ; j++)
            {
                isolatedPointIndices.append(simplifiedLine.at(j));
            }
        } else {

            const size_t index1 = simplifiedLine.first();
            const size_t index2 = simplifiedLine.last();

            CT_Point p1 = pointAccessor.constPointAt(index1);
            CT_Point p2 = pointAccessor.constPointAt(index2);

            float phi, theta, length;
            // revert line if pointing down
            if (p1(2) < p2(2))
            {
                CT_SphericalLine3D::convertToSphericalCoordinates(&p1, &p2, phi, theta, length);
            } else {
                CT_SphericalLine3D::convertToSphericalCoordinates(&p2, &p1, phi, theta, length);

                QList<size_t> simplifiedLineTmp;
                for (int k = simplifiedLine.size() - 1  ; k >= 0 ; k--)
                {
                    simplifiedLineTmp.append(simplifiedLine.at(k));
                }
                simplifiedLine.clear();
                simplifiedLine.append(simplifiedLineTmp);
            }


            if (phi >= thresholdZenithalAngleRadians)
            {
                for (int j = 0 ; j < simplifiedLine.size() ; j++)
                {
                    isolatedPointIndices.append(simplifiedLine.at(j));
                }
            } else {
                double length = sqrt(pow(p1(0) - p2(0), 2) + pow(p1(1) - p2(1), 2) + pow(p1(2) - p2(2), 2));
                CT_Point pLow = pointAccessor.constPointAt(simplifiedLine.first());
                keptLinesOfScan.append(new ScanLineData(simplifiedLine, length, pLow(0),  pLow(1), pLow(2)));
            }
        }
    }
}

