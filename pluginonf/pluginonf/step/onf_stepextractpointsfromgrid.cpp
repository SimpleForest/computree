/****************************************************************************
 Copyright (C) 2010-2012 the Office National des Forêts (ONF), France
                         All rights reserved.

 Contact : alexandre.piboule@onf.fr

 Developers : Alexandre PIBOULE (ONF)

 This file is part of PluginONF library.

 PluginONF is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginONF is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with PluginONF.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "onf_stepextractpointsfromgrid.h"

#ifdef USE_OPENCV

#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/ct_outresultmodelgroupcopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_model/inModel/tools/ct_instdmodelpossibility.h"

#include "ct_view/ct_stepconfigurabledialog.h"

#include "ct_math/ct_mathpoint.h"
#include "ct_iterator/ct_pointiterator.h"
#include "ct_global/ct_context.h"


#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
#include <QtConcurrentMap>
#else
#include <QtConcurrent/QtConcurrentMap>
#endif

#include <QMessageBox>
#include <limits>

// Alias for indexing models
#define DEF_SearchInResult "r"
#define DEF_SearchInGroup   "gr"
#define DEF_SearchInScene  "scene"
#define DEF_SearchInGridSeeds   "gridSeeds"


// Constructor : initialization of parameters
ONF_StepExtractPointsFromGrid::ONF_StepExtractPointsFromGrid(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString ONF_StepExtractPointsFromGrid::getStepDescription() const
{
    return tr("Segmenter une scène à partir d'une grille d'indices");
}

// Step detailled description
QString ONF_StepExtractPointsFromGrid::getStepDetailledDescription() const
{
    return tr("");
}

// Step URL
QString ONF_StepExtractPointsFromGrid::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* ONF_StepExtractPointsFromGrid::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new ONF_StepExtractPointsFromGrid(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void ONF_StepExtractPointsFromGrid::createInResultModelListProtected()
{  
    CT_InResultModelGroupToCopy *resultModel = createNewInResultModelForCopy(DEF_SearchInResult, tr("Grilles"));

    resultModel->setZeroOrMoreRootGroup();
    resultModel->addGroupModel("", DEF_SearchInGroup);
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInScene, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("Scene à segmenter"));
    resultModel->addItemModel(DEF_SearchInGroup, DEF_SearchInGridSeeds, CT_Grid3D_Sparse<int>::staticGetType(), tr("Grille segmentée"));
}


// Creation and affiliation of OUT models
void ONF_StepExtractPointsFromGrid::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res = createNewOutResultModelToCopy(DEF_SearchInResult);

    if(res != NULL) {
        res->addGroupModel(DEF_SearchInGroup, _outSceneGroupModelName, new CT_StandardItemGroup(), tr("Groupe"));
        res->addItemModel(_outSceneGroupModelName, _outSceneModelName, new CT_Scene(), tr("Scènes segmentées"));
        res->addItemAttributeModel(_outSceneModelName, _outSceneIDModelName,new CT_StdItemAttributeT<int>(CT_AbstractCategory::DATA_ID), tr("ID cluster"));
    }
}

// Semi-automatic creation of step parameters DialogBox
void ONF_StepExtractPointsFromGrid::createPostConfigurationDialog()
{
    //CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();
}

void ONF_StepExtractPointsFromGrid::compute()
{
    CT_ResultGroup* res_out = getOutResultList().first();
    CT_StandardItemGroup* grp = NULL;
    // Création de la liste des positions 2D
    CT_ResultGroupIterator grpPosIt(res_out, this, DEF_SearchInGroup);
    if (grpPosIt.hasNext())
    {
        grp = (CT_StandardItemGroup*) grpPosIt.next();

        CT_AbstractItemDrawableWithPointCloud* scene = (CT_AbstractItemDrawableWithPointCloud*)grp->firstItemByINModelName(this, DEF_SearchInScene);
        CT_Grid3D_Sparse<int>* segmentationGrid = (CT_Grid3D_Sparse<int>*)grp->firstItemByINModelName(this, DEF_SearchInGridSeeds);

        if (scene != NULL && segmentationGrid != NULL)
        {
            // create output segmented scenes
            QVector<CT_PointCloudIndexVector*> outClouds(segmentationGrid->dataMax() + 1);
            for (int i = 0 ; i < outClouds.size() ; i++)
            {
                CT_PointCloudIndexVector *resPointCloudIndex = new CT_PointCloudIndexVector();
                resPointCloudIndex->setSortType(CT_PointCloudIndexVector::NotSorted);
                outClouds[i] = resPointCloudIndex;
            }

            const CT_AbstractPointCloudIndex *pointCloudIndex = scene->getPointCloudIndex();
            CT_PointIterator itP(pointCloudIndex);
            while(itP.hasNext())
            {
                const CT_Point &pt = itP.next().currentPoint();
                int clusterNumber = segmentationGrid->valueAtXYZ(pt(0), pt(1), pt(2));
                if (clusterNumber != segmentationGrid->NA())
                {
                    outClouds[clusterNumber]->addIndex(itP.currentGlobalIndex());
                }
            }

            for (int i = 0 ; i < outClouds.size() ; i++)
            {
                CT_PointCloudIndexVector *resPointCloudIndex = outClouds[i];

                if (resPointCloudIndex->size() > 0)
                {
                    resPointCloudIndex->setSortType(CT_PointCloudIndexVector::SortedInAscendingOrder);
                    CT_Scene* scOut = new CT_Scene(_outSceneModelName.completeName(), res_out, PS_REPOSITORY->registerPointCloudIndex(resPointCloudIndex));
                    scOut->updateBoundingBox();

                    scOut->addItemAttribute(new CT_StdItemAttributeT<int>(_outSceneIDModelName.completeName(), CT_AbstractCategory::DATA_ID, res_out, i));

                    CT_StandardItemGroup* scGrp = new CT_StandardItemGroup(_outSceneGroupModelName.completeName(), res_out);
                    scGrp->addItemDrawable(scOut);
                    grp->addGroup(scGrp);
                } else {
                    delete resPointCloudIndex;
                }
            }

        }
    }

}

#endif
