/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sf_correctFirstCylinderInSegment.h"

void
SF_CorrectFirstCylinderInSegment::correctSegment(std::shared_ptr<SF_ModelSegment> segment)
{
  size_t numberBricks = segment->getBuildingBricks().size();
  if (numberBricks <= 1) {
    return;
  }
  std::shared_ptr<Sf_ModelAbstractBuildingbrick> first = segment->getBuildingBricks()[0];
  std::shared_ptr<Sf_ModelAbstractBuildingbrick> second = segment->getBuildingBricks()[1];
  auto axisSecond = second->getAxis().normalized();
  auto length = first->getLength();
  auto axisNew = -(axisSecond)*length;
  auto newStartFirst = first->getEnd() + axisNew;
  first->setStartEndRadius(newStartFirst, first->getEnd(), first->getRadius(), first->getFittingType());
}

void
SF_CorrectFirstCylinderInSegment::compute(std::shared_ptr<SF_ModelQSM> qsm)
{
  if (!qsm) {
    return;
  }
  std::vector<std::shared_ptr<SF_ModelSegment>> segments = qsm->getSegments();
  for (auto segment : segments) {
    correctSegment(segment);
  }
}
