/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_FITRANSACLINEWITHINTERCEPT_H
#define SF_FITRANSACLINEWITHINTERCEPT_H

#include "sf_fitransacline.h"

template<typename T>
class SF_FitRansacLineWithIntercept : public SF_FitRansacLine<T>
{
protected:
  T m_intercept;
  bool m_useWeight = false;
  std::pair<T, T> getEquation(size_t index1);

public:
  SF_FitRansacLineWithIntercept();
  void compute();
  void setIntercept(const T& intercept);
  void setUseWeight(bool useWeight);
};

#include "sf_fitRansacLineWithIntercept.hpp"

#endif // SF_FITRANSACLINEWITHINTERCEPT_H
