/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_FPFH_H
#define SF_FPFH_H

#include "pcl/cloud/feature/sf_abstractFeature.h"

#include <pcl/features/fpfh.h>

class SF_FPFH : public SF_AbstractFeature<SF_PointNormal, pcl::FPFHSignature33>
{
  float m_rangePFH;
  std::uint32_t m_numberOfThreads;

  void createIndices() override;
  void createIndex(SF_PointNormal, float) override;
  void reset() override;

public:
  SF_FPFH(SF_CloudNormal::Ptr cloudIn, pcl::PointCloud<pcl::FPFHSignature33>::Ptr featuresOut);
  void computeFeatures() override;
  void setParameters(float range, std::uint32_t numberOfThreads);
};

#endif // SF_FPFH_H
