/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Charts module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "themewidget.h"

#include <QtCharts/QAbstractBarSeries>
#include <QtCharts/QAreaSeries>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QChartView>
#include <QtCharts/QLegend>
#include <QtCharts/QLineSeries>
#include <QtCharts/QPercentBarSeries>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>
#include <QtCharts/QScatterSeries>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QStackedBarSeries>
#include <QtCore/QTime>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>

ThemeWidget::ThemeWidget(std::shared_ptr<SF_ModelQSM> qsm, QWidget* parent)
  : QWidget(parent)
  , m_qsm(qsm)
  , m_listCount(3)
  , m_valueMax(10)
  , m_valueCount(7)
  , m_dataTable(generateRandomData(m_listCount, m_valueMax, m_valueCount))
{
  connectSignals();
  // create layout
  QGridLayout* baseLayout = new QGridLayout();
  QHBoxLayout* settingsLayout = new QHBoxLayout();
  QLabel* warning = new QLabel;
  warning->setStyleSheet("font-weight: bold; color: black");
  warning->setText("Allometry");
  settingsLayout->addWidget(warning);
  settingsLayout->addWidget(new QLabel(" - by Dr. Jan Hackenberg - www.simpleForest.org:"));
  settingsLayout->addStretch();
  baseLayout->addLayout(settingsLayout, 0, 0, 1, 3);

  // create charts

  QChartView* chartView;

  chartView = new QChartView(createScatterChartVesselVolumeGood());
  baseLayout->addWidget(chartView, 1, 0);
  m_charts << chartView;

  chartView = new QChartView(createScatterChartVesselVolumeAll());
  baseLayout->addWidget(chartView, 1, 1);
  m_charts << chartView;

  chartView = new QChartView(createScatterChartGrowthVolumeGood());
  baseLayout->addWidget(chartView, 2, 0);
  m_charts << chartView;

  chartView = new QChartView(createScatterChartGrowthVolumeAll());
  baseLayout->addWidget(chartView, 2, 1);
  m_charts << chartView;

  chartView = new QChartView(createScatterChartGrowthLengthGood());
  baseLayout->addWidget(chartView, 3, 0);
  m_charts << chartView;

  chartView = new QChartView(createScatterChartGrowthLengthAll());
  baseLayout->addWidget(chartView, 3, 1);
  m_charts << chartView;

  setLayout(baseLayout);

  // Set defaults
  updateUI();
}

ThemeWidget::~ThemeWidget() {}

void
ThemeWidget::connectSignals()
{}

DataTable
ThemeWidget::generateRandomData(int listCount, int valueMax, int valueCount) const
{
  DataTable dataTable;
  std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks = m_qsm->getBuildingBricks();
  {
    DataList dataList;
    for (auto& brick : bricks) {
      if (brick->getGoodQuality()) {
        QPointF value(brick->getVesselVolume(), brick->getRadius());
        QString label = "VesselVolume Good Cylinders";
        dataList << Data(value, label);
      }
    }
    dataTable << dataList;
  }

  {
    DataList dataList;
    for (auto& brick : bricks) {
      if (!brick->getGoodQuality()) {
        QPointF value(brick->getVesselVolume(), brick->getRadius());
        QString label = "VesselVolume Bad Cylinders";
        dataList << Data(value, label);
      }
    }
    dataTable << dataList;
  }
  {
    DataList dataList;
    for (auto& brick : bricks) {
      if (brick->getGoodQuality()) {
        QPointF value(brick->getGrowthVolume(), brick->getRadius());
        QString label = "GrowthVolume Good Cylinders";
        dataList << Data(value, label);
      }
    }
    dataTable << dataList;
  }

  {
    DataList dataList;
    for (auto& brick : bricks) {
      if (!brick->getGoodQuality()) {
        QPointF value(brick->getGrowthVolume(), brick->getRadius());
        QString label = "GrowthVolume Bad Cylinders";
        dataList << Data(value, label);
      }
    }
    dataTable << dataList;
  }
  {
    DataList dataList;
    for (auto& brick : bricks) {
      if (brick->getGoodQuality()) {
        QPointF value(brick->getGrowthLength(), brick->getRadius());
        QString label = "GrowthLength Good Cylinders";
        dataList << Data(value, label);
      }
    }
    dataTable << dataList;
  }

  {
    DataList dataList;
    for (auto& brick : bricks) {
      if (!brick->getGoodQuality()) {
        QPointF value(brick->getGrowthLength(), brick->getRadius());
        QString label = "GrowthLength Bad Cylinders";
        dataList << Data(value, label);
      }
    }
    dataTable << dataList;
  }

  return dataTable;
}

QChart*
ThemeWidget::createAreaChart() const
{
  QChart* chart = new QChart();
  chart->setTitle("Area chart");

  // The lower series initialized to zero values
  QLineSeries* lowerSeries = 0;
  QString name("Series ");
  int nameIndex = 0;
  for (int i(0); i < m_dataTable.count(); i++) {
    QLineSeries* upperSeries = new QLineSeries(chart);
    for (int j(0); j < m_dataTable[i].count(); j++) {
      Data data = m_dataTable[i].at(j);
      if (lowerSeries) {
        const QVector<QPointF>& points = lowerSeries->pointsVector();
        upperSeries->append(QPointF(j, points[i].y() + data.first.y()));
      } else {
        upperSeries->append(QPointF(j, data.first.y()));
      }
    }
    QAreaSeries* area = new QAreaSeries(upperSeries, lowerSeries);
    area->setName(name + QString::number(nameIndex));
    nameIndex++;
    chart->addSeries(area);
    chart->createDefaultAxes();
    lowerSeries = upperSeries;
  }

  return chart;
}

QChart*
ThemeWidget::createBarChart(int valueCount) const
{
  Q_UNUSED(valueCount);
  QChart* chart = new QChart();
  chart->setTitle("Bar chart");

  QStackedBarSeries* series = new QStackedBarSeries(chart);
  for (int i(0); i < m_dataTable.count(); i++) {
    QBarSet* set = new QBarSet("Bar set " + QString::number(i));
    for (const Data& data : m_dataTable[i])
      *set << data.first.y();
    series->append(set);
  }
  chart->addSeries(series);
  chart->createDefaultAxes();

  return chart;
}

QChart*
ThemeWidget::createLineChart() const
{
  QChart* chart = new QChart();
  chart->setTitle("Line chart");

  QString name("Series ");
  int nameIndex = 0;
  for (const DataList& list : m_dataTable) {
    QLineSeries* series = new QLineSeries(chart);
    for (const Data& data : list)
      series->append(data.first);
    series->setName(name + QString::number(nameIndex));
    nameIndex++;
    chart->addSeries(series);
  }
  chart->createDefaultAxes();

  return chart;
}

QChart*
ThemeWidget::createPieChart() const
{
  QChart* chart = new QChart();
  chart->setTitle("Pie chart");

  qreal pieSize = 1.0 / m_dataTable.count();
  for (int i = 0; i < m_dataTable.count(); i++) {
    QPieSeries* series = new QPieSeries(chart);
    for (const Data& data : m_dataTable[i]) {
      QPieSlice* slice = series->append(data.second, data.first.y());
      if (data == m_dataTable[i].first()) {
        slice->setLabelVisible();
        slice->setExploded();
      }
    }
    qreal hPos = (pieSize / 2) + (i / (qreal)m_dataTable.count());
    series->setPieSize(pieSize);
    series->setHorizontalPosition(hPos);
    series->setVerticalPosition(0.5);
    chart->addSeries(series);
  }

  return chart;
}

QChart*
ThemeWidget::createSplineChart() const
{
  // spine chart
  QChart* chart = new QChart();
  chart->setTitle("Spline chart");
  QString name("Series ");
  int nameIndex = 0;
  for (const DataList& list : m_dataTable) {
    QSplineSeries* series = new QSplineSeries(chart);
    for (const Data& data : list)
      series->append(data.first);
    series->setName(name + QString::number(nameIndex));
    nameIndex++;
    chart->addSeries(series);
  }
  chart->createDefaultAxes();
  return chart;
}

QChart*
ThemeWidget::createScatterChartGrowthVolumeGood() const
{
  // scatter chart
  QChart* chart = new QChart();
  chart->setTitle("GrowthVolume");
  const DataList& list = m_dataTable[2];
  QScatterSeries* series = new QScatterSeries(chart);
  for (const Data& data : list)
    series->append(data.first);
  series->setName("Good Cylinders");
  series->setMarkerSize(5);
  chart->addSeries(series);
  chart->createDefaultAxes();
  chart->axisY()->setTitleText("Radius");
  chart->axisX()->setTitleText("GrowthVolume");
  return chart;
}

QChart*
ThemeWidget::createScatterChartGrowthVolumeAll() const
{
  // scatter chart
  QChart* chart = new QChart();
  chart->setTitle("GrowthVolume");
  {
    const DataList& list = m_dataTable[2];
    QScatterSeries* series = new QScatterSeries(chart);
    for (const Data& data : list)
      series->append(data.first);
    series->setName("Good Cylinders");
    series->setMarkerSize(5);
    chart->addSeries(series);
  }
  {
    const DataList& list = m_dataTable[3];
    QScatterSeries* series = new QScatterSeries(chart);
    for (const Data& data : list)
      series->append(data.first);
    series->setName("Bad Cylinders");
    series->setMarkerSize(5);
    chart->addSeries(series);
  }
  chart->createDefaultAxes();
  chart->axisY()->setTitleText("Radius");
  chart->axisX()->setTitleText("GrowthVolume");
  return chart;
}

QChart*
ThemeWidget::createScatterChartGrowthLengthGood() const
{
  // scatter chart
  QChart* chart = new QChart();
  chart->setTitle("GrowthLength");
  const DataList& list = m_dataTable[4];
  QScatterSeries* series = new QScatterSeries(chart);
  for (const Data& data : list)
    series->append(data.first);
  series->setName("Good Cylinders");
  series->setMarkerSize(5);
  chart->addSeries(series);
  chart->createDefaultAxes();
  chart->axisY()->setTitleText("Radius");
  chart->axisX()->setTitleText("GrowthLength");
  return chart;
}

QChart*
ThemeWidget::createScatterChartGrowthLengthAll() const
{
  // scatter chart
  QChart* chart = new QChart();
  chart->setTitle("GrowthLength");
  {
    const DataList& list = m_dataTable[4];
    QScatterSeries* series = new QScatterSeries(chart);
    for (const Data& data : list)
      series->append(data.first);
    series->setName("Good Cylinders");
    series->setMarkerSize(5);
    chart->addSeries(series);
  }
  {
    const DataList& list = m_dataTable[5];
    QScatterSeries* series = new QScatterSeries(chart);
    for (const Data& data : list)
      series->append(data.first);
    series->setName("Bad Cylinders");
    series->setMarkerSize(5);
    chart->addSeries(series);
  }
  chart->createDefaultAxes();
  chart->axisY()->setTitleText("Radius");
  chart->axisX()->setTitleText("GrowthLength");
  return chart;
}

QChart*
ThemeWidget::createScatterChartVesselVolumeGood() const
{
  // scatter chart
  QChart* chart = new QChart();
  chart->setTitle("VesselVolume");
  const DataList& list = m_dataTable[0];
  QScatterSeries* series = new QScatterSeries(chart);
  for (const Data& data : list)
    series->append(data.first);
  series->setName("Good Cylinders");
  series->setMarkerSize(5);
  series->setBorderColor(series->color());
  chart->addSeries(series);
  chart->createDefaultAxes();
  chart->axisY()->setTitleText("Radius");
  chart->axisX()->setTitleText("VesselVolume");
  return chart;
}

QChart*
ThemeWidget::createScatterChartVesselVolumeAll() const
{
  // scatter chart
  QChart* chart = new QChart();
  chart->setTitle("VesselVolume");
  {
    const DataList& list = m_dataTable[0];
    QScatterSeries* series = new QScatterSeries(chart);
    for (const Data& data : list)
      series->append(data.first);
    series->setName("Good Cylinders");
    series->setMarkerSize(5);
    series->setPen(QPen(Qt::PenStyle::NoPen));
    chart->addSeries(series);
  }
  {
    const DataList& list = m_dataTable[1];
    QScatterSeries* series = new QScatterSeries(chart);
    for (const Data& data : list)
      series->append(data.first);
    series->setName("Bad Cylinders");
    series->setMarkerSize(5);
    series->setPen(QPen(Qt::PenStyle::NoPen));
    chart->addSeries(series);
  }
  chart->createDefaultAxes();
  chart->axisY()->setTitleText("Radius");
  chart->axisX()->setTitleText("VesselVolume");
  return chart;
}

void
ThemeWidget::updateUI()
{
  QChart::ChartTheme theme(QChart::ChartThemeBrownSand);

  const auto charts = m_charts;
  if (m_charts.at(0)->chart()->theme() != theme) {
    for (QChartView* chartView : charts) {
      chartView->chart()->setTheme(QChart::ChartThemeBrownSand);
      constexpr bool checked = true;
      chartView->setRenderHint(QPainter::Antialiasing, checked);
      chartView->chart()->setAnimationOptions(QChart::NoAnimation);
      chartView->chart()->legend()->setAlignment(Qt::AlignLeft);
      chartView->chart()->legend()->hide();
      chartView->chart()->setDropShadowEnabled(false);
      QList<QAbstractSeries*> allSeries = chartView->chart()->series();
      for (auto series : allSeries) {
        reinterpret_cast<QScatterSeries*>(series)->setPen(QPen(Qt::PenStyle::NoPen));
      }
    }

    QPalette pal = window()->palette();
    pal.setColor(QPalette::Window, QRgb(0x9e8965));
    pal.setColor(QPalette::WindowText, QRgb(0x404044));
    window()->setPalette(pal);
  }
}
