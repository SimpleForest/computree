/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_ABSTRACTEXPORTPLY_H
#define SF_ABSTRACTEXPORTPLY_H

#include "file/export/sf_abstractExport.h"

#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <random>

enum class SF_ExportPlyPolicy
{
  GROWTH_VOLUME = 0,
  GROWTH_LENGTH = 1,
  STEM = 2
};

class SF_AbstractExportPly : public SF_AbstractExport
{
protected:
  int m_resolution = 8;
  SF_ExportPlyPolicy m_exportPolicy = SF_ExportPlyPolicy::GROWTH_LENGTH;
  virtual int getNumberOfPoints(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks) = 0;
  virtual int getNumberOfFaces(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks) = 0;
  void getMinMax(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks);
  void writeHeader(QTextStream& outStream, std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks);
  void writeCloud(QTextStream& outStream, pcl::PointCloud<pcl::PointXYZI>::Ptr cloud);

  virtual void writeFaces(QTextStream& outStream, std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>> bricks) = 0;
  virtual pcl::PointCloud<pcl::PointXYZI>::Ptr brickToCloud(std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick);
  virtual pcl::PointCloud<pcl::PointXYZI>::Ptr bricksToCloud(std::vector<std::shared_ptr<Sf_ModelAbstractBuildingbrick>>)
  {
    return nullptr;
  }
  float brickToIntensity(std::shared_ptr<Sf_ModelAbstractBuildingbrick> brick);
  QString getFullPath(QString path);

public:
  SF_AbstractExportPly();
  virtual void exportQSM(QString, QString, std::shared_ptr<SF_ModelQSM>, SF_ExportPlyPolicy, int = 8) {}
};

#endif // SF_ABSTRACTEXPORTPLY_H
