/****************************************************************************

Copyright (C) 2017-2021 Dr. Jan Hackenberg, free software developer
All rights reserved.

Contact : https://gitlab.com/simpleForest

Developers : Dr. Jan Hackenberg

This file is part of SimpleForest plugin Version 5 - successor of SimpleTree
Version 4 for Computree.

SimpleForest plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleForest plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleForest plugin.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SF_STEPQSMDETECTWELLFITCYLINDERSADAPTER_H
#define SF_STEPQSMDETECTWELLFITCYLINDERSADAPTER_H

#include <QThreadPool>
#include <converters/CT_To_PCL/sf_converterCTToPCL.h>

#include "qsm/algorithm/distance/sf_qsmFitQuality.h"
#include "qsm/algorithm/postprocessing/sf_qsmDetectWellfitCylinders.h"
#include "steps/param/sf_paramAllSteps.h"

class SF_StepQSMDetectWellfitCylindersAdapter
{
public:
  std::shared_ptr<QMutex> mMutex;

  SF_StepQSMDetectWellfitCylindersAdapter(const SF_StepQSMDetectWellfitCylindersAdapter& obj) { mMutex = obj.mMutex; }

  SF_StepQSMDetectWellfitCylindersAdapter() { mMutex.reset(new QMutex); }

  ~SF_StepQSMDetectWellfitCylindersAdapter() {}

  void operator()(SF_ParamQSMDetectWellfitCylinders<SF_Point>& params)
  {
    Sf_ConverterCTToPCL<SF_PointNormal> converter;
    {
      QMutexLocker m1(&*mMutex);
      converter.setItemCpyCloudInDeprecated(params._itemCpyCloudIn);
    }
    SF_CloudNormal::Ptr cloud;
    converter.compute();
    {
      QMutexLocker m1(&*mMutex);
      params._translation = converter.translation();
      params._qsm->translate(-params._translation);
      cloud = converter.cloudTranslated();
    }
    {
      QMutexLocker m1(&*mMutex);
      if (!params._qsm)
        return;
      SF_CloudToModelDistanceParameters paramsDist;
      paramsDist.m_useAngle = false;
      paramsDist._method = SF_CLoudToModelDistanceMethod::SECONDMOMENTUMORDER;
      paramsDist._k = 9;
      SF_QsmFitQuality<SF_PointNormal> fit(params._qsm, paramsDist, cloud);
    }
    SF_QSMDetectWellfitCylinders detectFit;
    {
      QMutexLocker m1(&*mMutex);
      detectFit.setParams(params);
    }
    detectFit.compute();
    {
      QMutexLocker m1(&*mMutex);
      params._qsm->translate(params._translation);
    }
  }
};
#endif // SF_STEPQSMDETECTWELLFITCYLINDERSADAPTER_H
