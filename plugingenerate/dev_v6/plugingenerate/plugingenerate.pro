CT_PREFIX = ../../computreev6

include($${CT_PREFIX}/plugin_shared.pri)

TARGET = plug_generate

HEADERS += $${CT_LIB_PREFIX}/ctlibplugin/pluginentryinterface.h \
    gen_pluginentry.h \
#    step/gen_stepgenerateplanecloud.h \
#    step/gen_stepgeneratecubecloud.h \
#    step/gen_stepgeneratecylindercloud.h \
#    step/gen_stepgeneratespherecloud.h \
    step/gen_stepgeneraterandomcloud.h \
#    step/gen_stepgenerateraster2d.h \
#    step/gen_stepgenerateraster3d.h \
#    step/gen_stepgenerateray.h \
#    step/gen_stepgeneratescanner.h \
#    step/gen_stepgeneratetorus.h \
#    step/gen_stepgeneratequadraticsurface.h \
#    step/gen_stepgenerateraster4d.h \
#    step/gen_stepgeneratecone.h \
#    step/gen_stepgeneratecylindermesh.h \
#    step/gen_stepgenerateshape2d.h \
#    step/gen_stepgenerateshape3d.h \
    gen_steppluginmanager.h
SOURCES += \
    gen_pluginentry.cpp \
#    step/gen_stepgenerateplanecloud.cpp \
#    step/gen_stepgeneratecubecloud.cpp \
#    step/gen_stepgeneratecylindercloud.cpp \
#    step/gen_stepgeneratespherecloud.cpp \
    step/gen_stepgeneraterandomcloud.cpp \
#    step/gen_stepgenerateraster2d.cpp \
#    step/gen_stepgenerateraster3d.cpp \
#    step/gen_stepgenerateray.cpp \
#    step/gen_stepgeneratescanner.cpp \
#    step/gen_stepgeneratetorus.cpp \
#    step/gen_stepgeneratequadraticsurface.cpp \
#    step/gen_stepgenerateraster4d.cpp \
#    step/gen_stepgeneratecone.cpp \
#    step/gen_stepgeneratecylindermesh.cpp \
#    step/gen_stepgenerateshape2d.cpp \
#    step/gen_stepgenerateshape3d.cpp \
    gen_steppluginmanager.cpp

TRANSLATIONS += languages/plugingenerate_en.ts \
                languages/plugingenerate_fr.ts
