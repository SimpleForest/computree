#include "step/gen_stepgeneratequadraticsurface.h"

// Using the point cloud deposit

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_point.h"
#include "ct_coordinates/ct_defaultcoordinatesystem.h"

// Alias for indexing out models

#include <assert.h>

#include "ct_math/ct_mathpoint.h"

#define RAD_TO_DEG 57.2957795131
#define DEG_TO_RAD 0.01745329251

GEN_StepGenerateQuadraticSurface::GEN_StepGenerateQuadraticSurface() : CT_AbstractStepCanBeAddedFirst()
{
    _a = 1;
    _b = 1;
    _c = 1;
    _d = 1;
    _e = 1;
    _f = 0;

    _xMin = -10;
    _xMax = 10;
    _yMin = -10;
    _yMax = 10;

    _resX = 0.1;
    _resY = 0.1;
}

QString GEN_StepGenerateQuadraticSurface::description() const
{
    return tr("Créer une Surface Quadrique de points");
}

CT_VirtualAbstractStep* GEN_StepGenerateQuadraticSurface::createNewInstance()
{
    return new GEN_StepGenerateQuadraticSurface();
}

//////////////////// PROTECTED METHODS //////////////////

void GEN_StepGenerateQuadraticSurface::declareInputModels(CT_StepInModelStructureManager& manager)
{
    // No in result is needed
    setNotNeedInputResult();
}

void GEN_StepGenerateQuadraticSurface::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultScene, tr("Generated Point Cloud"));

    resultModel->setRootGroup(DEF_groupOut_groupScene, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupScene, DEF_itemOut_scene, new CT_Scene(), tr("Generated Quadratic surface"));
}

void GEN_StepGenerateQuadraticSurface::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{


    postInputConfigDialog->addText("ax2 + bxy + cy2 + dx + ey + f", "", "");
    postInputConfigDialog->addEmpty();

    postInputConfigDialog->addText(tr("Parametres"), "", "");
    postInputConfigDialog->addDouble("a", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _a, 0);
    postInputConfigDialog->addDouble("b", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _b, 0);
    postInputConfigDialog->addDouble("c", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _c, 0);
    postInputConfigDialog->addDouble("d", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _d, 0);
    postInputConfigDialog->addDouble("e", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _e, 0);
    postInputConfigDialog->addDouble("f", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _f, 0);

    postInputConfigDialog->addText(tr("Limites"), "", "");
    postInputConfigDialog->addDouble("xMin", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _xMin, 0);
    postInputConfigDialog->addDouble("xMax", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _xMax, 0);
    postInputConfigDialog->addDouble("yMin", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _yMin, 0);
    postInputConfigDialog->addDouble("yMax", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _yMax, 0);

    postInputConfigDialog->addText(tr("Resolution"), "", "");
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _resX, 0);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _resY, 0);
}

void GEN_StepGenerateQuadraticSurface::compute()
{
    CT_ResultGroup* resultOut_resultScene = getOutResultList().first();

    /******************************************************************************
     *      User's Compute
     ******************************************************************************/
    int nbPts = 0;
    int nbPtsTotal = ( (_xMax - _xMin) / _resX ) * ( ( _yMax - _yMin) / _resY );

    CT_AbstractUndefinedSizePointCloud *undepositPointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();

    // Construction de la surface
    for ( float i = _xMin ; (i < _xMax) && !isStopped() ; i += _resX )
    {
        for ( float j = _yMin ; (j <= _yMax) && !isStopped() ; j += _resY )
        {
            undepositPointCloud->addPoint( Eigen::Vector3d( i, j, _a*i*i + _b*i*j + _c*j*j + _d*i + _e*j + _f ));

            nbPts++;

            // Barre de progression (multiplie par 100/6 parce qu'on a huit face et qu'on est a la premiere
            setProgress(  nbPts * 100 / (float)nbPtsTotal  );

            // On regarde si on est en debug mode
            waitForAckIfInDebugMode();
        }
    }

    // On enregistre le nuage de points cree dans le depot
    CT_NMPCIR depositPointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(undepositPointCloud);

    if(!isStopped()) {
        // ------------------------------
        // Create OUT groups and items

        // ----------------------------------------------------------------------------
        // Works on the result corresponding to DEF_resultOut_resultScene
        CT_StandardItemGroup* groupOut_groupScene = new CT_StandardItemGroup(DEF_groupOut_groupScene, resultOut_resultScene);

        // UNCOMMENT Following lines and complete parameters of the item's contructor
        CT_Scene* itemOut_scene = new CT_Scene(DEF_itemOut_scene, resultOut_resultScene, depositPointCloud);
        itemOut_scene->updateBoundingBox();

        groupOut_groupScene->addItemDrawable(itemOut_scene);
        resultOut_resultScene->addGroup(groupOut_groupScene);
    }
}
