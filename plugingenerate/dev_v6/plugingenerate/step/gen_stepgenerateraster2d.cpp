#include "step/gen_stepgenerateraster2d.h"

#include <ctime>

// Alias for indexing out models

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_grid2dxy.h"
typedef CT_Grid2DXY<float> CT_Grid2DFloat;

GEN_StepGenerateRaster2DFloat::GEN_StepGenerateRaster2DFloat() : CT_AbstractStepCanBeAddedFirst()
{
    _height = 0;
    _botX = -10;
    _botY = -10;
    _topX = 10;
    _topY = 10;
    _res = 1;
    _valMin = 0;
    _valMax = 100;
}

QString GEN_StepGenerateRaster2DFloat::description() const
{
    return tr("Créer un Raster (2D)");
}

CT_VirtualAbstractStep* GEN_StepGenerateRaster2DFloat::createNewInstance()
{
    return new GEN_StepGenerateRaster2DFloat();
}

//////////////////// PROTECTED METHODS //////////////////

void GEN_StepGenerateRaster2DFloat::declareInputModels(CT_StepInModelStructureManager& manager)
{
    // No in result is needed
    setNotNeedInputResult();
}

void GEN_StepGenerateRaster2DFloat::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultRaster2dFloat, tr("Generated Item"));

    resultModel->setRootGroup(DEF_groupOut_groupRaster2DFloat, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupRaster2DFloat, DEF_itemOut_itemRaster2dFloat, new CT_Grid2DFloat(), tr("Generated 2D Raster"));
}

void GEN_StepGenerateRaster2DFloat::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{


    postInputConfigDialog->addDouble(tr("Raster height"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _height, 0);
    postInputConfigDialog->addDouble(tr("Resolution"), "", 0.0001, std::numeric_limits<double>::max(), 4, _res, 0);
    postInputConfigDialog->addText(tr("Bottom left point"), "", "");
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botX, 0);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botY, 0);
    postInputConfigDialog->addText(tr("Top right point"), "", "");
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topX, 0);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _topY, 0);
    postInputConfigDialog->addText(tr("Value range"), "", "");
    postInputConfigDialog->addDouble(tr("Min"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _valMin, 0);
    postInputConfigDialog->addDouble(tr("Max"), "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _valMax, 0);
}

void GEN_StepGenerateRaster2DFloat::compute()
{
    CT_ResultGroup* resultOut_resultRaster2dFloat = getOutResultList().first();

    CT_Grid2DFloat* itemOut_itemRaster2dFloat = CT_Grid2DFloat::createGrid2DXYFromXYCoords(DEF_itemOut_itemRaster2dFloat, resultOut_resultRaster2dFloat, _botX, _botY, _topX, _topY, _res, _height, -999, 0);

    // On initialise l'aleatoire
    srand( time(0) );

    size_t nbCellsX = itemOut_itemRaster2dFloat->colDim();
    size_t nbCellsY = itemOut_itemRaster2dFloat->linDim();

    for ( size_t i = 0 ; (i < nbCellsX) && !isStopped() ; i++ )
    {
        for ( size_t j = 0 ; (j < nbCellsY) && !isStopped() ; j++ )
        {
            itemOut_itemRaster2dFloat->setValue(i,j, _valMin + ( ((double)rand()/RAND_MAX) * (_valMax - _valMin) ) );
        }
    }

    if(!isStopped()) {
        itemOut_itemRaster2dFloat->computeMinMax();
        CT_StandardItemGroup* groupOut_groupRaster2DFloat = new CT_StandardItemGroup(DEF_groupOut_groupRaster2DFloat, resultOut_resultRaster2dFloat);

        groupOut_groupRaster2DFloat->addItemDrawable(itemOut_itemRaster2dFloat);
        resultOut_resultRaster2dFloat->addGroup(groupOut_groupRaster2DFloat);
    } else {
        delete itemOut_itemRaster2dFloat;
    }
}
