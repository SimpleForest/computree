#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateCone : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    using SuperClass = CT_AbstractStepCanBeAddedFirst;

public:

    GEN_StepGenerateCone();

    QString description() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

    // Step parameters
    double    _botX;    /*!< Coordonnees du point inferieur gauche du cube */
    double    _botY;    /*!< Coordonnees du point inferieur gauche du cube */
    double    _botZ;    /*!< Coordonnees du point inferieur gauche du cube */
    double    _height;
    double    _alpha;
    double    _resAlpha;    /*!< Espace entre deux points selon Ox */
    double    _resH;    /*!< Espace entre deux points selon Oy */
};
