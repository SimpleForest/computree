#ifndef GEN_STEPGENERATESHAPE3D_H
#define GEN_STEPGENERATESHAPE3D_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

/*!
 * \class GEN_StepGenerateShape3D
 * \ingroup Steps_GEN
 * \brief <b>No short description for this step.</b>
 *
 * No detailled description for this step
 *
 * \param _nbCircle
 * \param _nbCylinder
 * \param _nbElllipse
 * \param _nbLine
 * \param _nbSphere
 *
 */

class GEN_StepGenerateShape3D: public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    using SuperClass = CT_AbstractStep;

public:

    GEN_StepGenerateShape3D();

    QString description() const;

    QString detailledDescription() const;

    QString getStepURL() const;

    CT_VirtualAbstractStep* createNewInstance() const final;

protected:

    void declareInputModels(CT_StepInModelStructureManager& manager) final;

    void fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog) final;

    void declareOutputModels(CT_StepOutModelStructureManager& manager) final;

    void compute() final;

private:

    // Step parameters
    int    _nbCircle;    /*!<  */
    int    _nbCylinder;    /*!<  */
    int    _nbEllipse;    /*!<  */
    int    _nbLine;    /*!<  */
    int    _nbSphere;    /*!<  */

    double  _minx;
    double  _maxx;
    double  _miny;
    double  _maxy;
    double  _minz;
    double  _maxz;
};

#endif // GEN_STEPGENERATESHAPE3D_H
