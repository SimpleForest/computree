#include "gen_stepgeneratecone.h"

#include <QErrorMessage>

// Initialising the randomiser
#include <ctime>

// Using the point cloud deposit

// Inclusion of used ItemDrawable classes
#include "ct_itemdrawable/ct_scene.h"
#include "ct_point.h"
#include "ct_coordinates/ct_defaultcoordinatesystem.h"

// Alias for indexing out models

#include <assert.h>

#include "ct_math/ct_mathpoint.h"

#define RAD_TO_DEG 57.2957795131
#define DEG_TO_RAD 0.01745329251

GEN_StepGenerateCone::GEN_StepGenerateCone() : SuperClass()
{
    _botX = 0;
    _botY = 0;
    _botZ = 0;
    _height = 10;
    _alpha = 45;
    _resAlpha = 1;
    _resH = 0.5;
}

QString GEN_StepGenerateCone::description() const
{
    return tr("Créer un Cône de points");
}

CT_VirtualAbstractStep* GEN_StepGenerateCone::createNewInstance() const
{
    return new GEN_StepGenerateCone();
}

//////////////////// PROTECTED METHODS //////////////////

void GEN_StepGenerateCone::declareInputModels(CT_StepInModelStructureManager& manager)
{
    // No in result is needed
    setNotNeedInputResult();
}

void GEN_StepGenerateCone::declareOutputModels(CT_StepOutModelStructureManager& manager)
{
    CT_OutResultModelGroup *resultModel = createNewOutResultModel(DEF_resultOut_resultScene, tr("Generated Point Cloud"));

    resultModel->setRootGroup(DEF_groupOut_groupScene, new CT_StandardItemGroup(),tr("Group"));
    resultModel->addItemModel(DEF_groupOut_groupScene, DEF_itemOut_scene, new CT_Scene(), tr("Generated Cone"));
}

void GEN_StepGenerateCone::fillPostInputConfigurationDialog(CT_StepConfigurableDialog* postInputConfigDialog)
{


    postInputConfigDialog->addText(tr("Sommet"), "", "");
    postInputConfigDialog->addDouble("X", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botX, 0);
    postInputConfigDialog->addDouble("Y", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botY, 0);
    postInputConfigDialog->addDouble("Z", "", -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 4, _botZ, 0);
    postInputConfigDialog->addText(tr("Resolutions"), "", "");
    postInputConfigDialog->addDouble(tr("Hauteur"), "", 0.0001, std::numeric_limits<double>::max(), 4, _resH, 0);
    postInputConfigDialog->addDouble(tr("Rayon"), "", 0.0001, std::numeric_limits<double>::max(), 4, _resAlpha, 0);
    postInputConfigDialog->addText(tr("Angle du cone"), "", "");
    postInputConfigDialog->addDouble(tr("Rayon"), "", 0.0001, std::numeric_limits<double>::max(), 4, _alpha, 0);
    postInputConfigDialog->addText(tr("Hauteur du cone"), "", "");
    postInputConfigDialog->addDouble(tr("Hauteur"), "", 0.0001, std::numeric_limits<double>::max(), 4, _height, 0);
}

void GEN_StepGenerateCone::compute()
{
    CT_ResultGroup* resultOut_resultScene = getOutResultList().first();

    // On initialise l'aleatoire pour le bruit par la suite
    srand( time(0) );

    // Conversion des angles en radians
    _resAlpha *= DEG_TO_RAD;

    size_t nbPts = 0;
    size_t nbPtsAlpha = ceil( 2*M_PI / _resAlpha );
    size_t nbPtsHeight = ceil( _height / _resH );
    size_t nbPtsTotal = nbPtsAlpha*nbPtsHeight;

    CT_AbstractUndefinedSizePointCloud *undepositPointCloud = PS_REPOSITORY->createNewUndefinedSizePointCloud();

    // Construction du cote du cylindre vertical qui a pour base 0,0,0
    for ( float i = 0 ; (i <= 2*M_PI) && !isStopped(); i += _resAlpha )
    {
        for ( float j = 0 ; j <= _height ; j += _resH )
        {
            undepositPointCloud->addPoint( Eigen::Vector3d( cos( i ) * tan(_alpha) * j + _botX, sin( i ) * tan(_alpha) * j + _botY, -j + _botZ));
            nbPts++;

            setProgress(  (double)nbPts * 100.0 / (double)nbPtsTotal  );

            // On regarde si on est en debug mode
            waitForAckIfInDebugMode();
        }
    }

    // On enregistre le nuage de points cree dans le depot
    CT_NMPCIR depositPointCloud = PS_REPOSITORY->registerUndefinedSizePointCloud(undepositPointCloud);

    if(!isStopped()) {
        CT_StandardItemGroup* groupOut_groupScene = new CT_StandardItemGroup(DEF_groupOut_groupScene, resultOut_resultScene);

        CT_Scene* itemOut_scene = new CT_Scene(DEF_itemOut_scene, resultOut_resultScene, depositPointCloud);
        itemOut_scene->updateBoundingBox();

        groupOut_groupScene->addItemDrawable(itemOut_scene);
        resultOut_resultScene->addGroup(groupOut_groupScene);
    }

    // Conversion des angles en degres
    _resAlpha *= RAD_TO_DEG;
}
