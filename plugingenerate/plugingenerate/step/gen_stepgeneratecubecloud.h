#ifndef GEN_STEPGENERATECUBECLOUD_H
#define GEN_STEPGENERATECUBECLOUD_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateCubeCloud : public CT_AbstractStepCanBeAddedFirst
{
    Q_OBJECT
    
public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    GEN_StepGenerateCubeCloud(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    // Step parameters
    double    _botX;    /*!< Coordonnees du point inferieur gauche du cube */
    double    _botY;    /*!< Coordonnees du point inferieur gauche du cube */
    double    _botZ;    /*!< Coordonnees du point inferieur gauche du cube */
    double    _topX;    /*!< Coordonnees du point superieur droit du cube */
    double    _topY;    /*!< Coordonnees du point superieur droit du cube */
    double    _topZ;    /*!< Coordonnees du point superieur droit du cube */
    double    _resX;    /*!< Espace entre deux points selon Ox */
    double    _resY;    /*!< Espace entre deux points selon Oy */
    double    _resZ;    /*!< Espace entre deux points selon Oz */
    double    _noiseX;
    double    _noiseY;
    double    _noiseZ;
    double    _axisX;
    double    _axisY;
    double    _axisZ;

//    // Useful parameters for debug visualisation, also use to be the result of the step
//    DocumentInterface*          _m_doc;
//    CT_PointCloudStdVector*     _resultPointCloud;
//    CT_PointCloudIndexVector*   _resultIndexCloud;
//    CT_Scene*                   _resultScene;
//    CT_AxisAlignedBoundingBox*  _resultBoundingBox;
};

#endif // GEN_STEPGENERATECUBECLOUD_H
