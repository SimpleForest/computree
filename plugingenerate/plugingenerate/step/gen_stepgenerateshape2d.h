#ifndef GEN_STEPGENERATESHAPE2D_H
#define GEN_STEPGENERATESHAPE2D_H

#include "ct_step/abstract/ct_abstractstepcanbeaddedfirst.h"

class GEN_StepGenerateShape2D : public CT_AbstractStepCanBeAddedFirst
{

    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    GEN_StepGenerateShape2D(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    // Step parameters
    int     _boxNb;
    int     _circleNb;
    int     _pointNb;
    int     _lineNb;
    int     _polygonNb;
    int     _polylineNb;

    double  _minx;
    double  _maxx;
    double  _miny;
    double  _maxy;

};

#endif // GEN_STEPGENERATESHAPE2D_H
