/********************************************************************************
* Filename :  NORM_leastsquarefitting.cpp							*
*																				*
* Copyright (C) 2015 Joris RAVAGLIA												*
* Author: Joris Ravaglia														*
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                        *
*																				*
* This file is part of the oc_pluginoctree plugin                               *
* for the CompuTree v2.0 software.                                              *
* The oc_pluginoctree plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the               *
* GNU Lesser General Public License as published by the Free Software Foundation*
* either version 3 of the License, or (at your option) any later version.       *
*																				*
* The oc_pluginoctree plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY*
* or FITNESS FOR A PARTICULAR PURPOSE.                                          *
* See the GNU Lesser General Public License for more details.                   *
*																				*
* You should have received a copy of the GNU Lesser General Public License		*
* along with this program. If not, see <http://www.gnu.org/licenses/>.			*
*																				*
*********************************************************************************/

#include "norm_leastsquarefitting.h"

#include <Eigen/Dense>
#include "ct_cloudindex/registered/abstract/ct_abstractmodifiablecloudindexregisteredt.h"
#include "ct_iterator/ct_pointiterator.h"
#include <QDebug>
#include "ct_accessor/ct_pointaccessor.h"

void NORM_Tools::NORM_LeastSquareFitting::quadraticSurfaceFitting( const CT_AbstractPointCloudIndex* indexCloud,
                                                               float& outA, float& outB, float& outC, float& outD, float& outE, float& outF )
{
    size_t nPts_itemIn_ItemScene = indexCloud->size();

    // 1 - On remplit la matrice A des xx xy yy x y 1 et le vecteur B des z
    Eigen::MatrixXd A( nPts_itemIn_ItemScene, 6 );
    Eigen::VectorXd B( nPts_itemIn_ItemScene );

    CT_PointIterator itPoint( indexCloud );
    size_t i = 0;
    while( itPoint.hasNext() )
    {
        const CT_Point &currPoint = itPoint.next().currentPoint();
        A( i, 0 ) = currPoint(0)*currPoint(0);  // xx
        A( i, 1 ) = currPoint(0)*currPoint(1);  // xy
        A( i, 2 ) = currPoint(1)*currPoint(1);  // yy
        A( i, 3 ) = currPoint(0);               // x
        A( i, 4 ) = currPoint(1);               // y
        A( i, 5 ) = 1;                          // 1

        B( i ) = currPoint(2);                  // z

        i++;
    }

    // 2 - On laisse eigen faire le travail de resolution du systeme aux moindres carres
    // A x - B le plus proche de zero
    Eigen::VectorXd coeffs(6);
    coeffs = A.fullPivHouseholderQr().solve(B);

    outA = coeffs(0);
    outB = coeffs(1);
    outC = coeffs(2);
    outD = coeffs(3);
    outE = coeffs(4);
    outF = coeffs(5);
}

void NORM_Tools::NORM_LeastSquareFitting::quadraticSurfaceFitting(const QVector<int> *indexCloud,
                                                                  float &outA, float &outB, float &outC, float &outD, float &outE, float &outF)
{
    size_t nPts_itemIn_ItemScene = indexCloud->size();

    // 1 - On remplit la matrice A des xx xy yy x y 1 et le vecteur B des z
    Eigen::MatrixXd A( nPts_itemIn_ItemScene, 6 );
    Eigen::VectorXd B( nPts_itemIn_ItemScene );

    CT_PointAccessor pointAccessor;
    for( int i = 0 ; i < nPts_itemIn_ItemScene ; i++ )
    {
        const CT_Point &currPoint = pointAccessor.pointAt( indexCloud->at(i) );
        A( i, 0 ) = currPoint(0)*currPoint(0);  // xx
        A( i, 1 ) = currPoint(0)*currPoint(1);  // xy
        A( i, 2 ) = currPoint(1)*currPoint(1);  // yy
        A( i, 3 ) = currPoint(0);               // x
        A( i, 4 ) = currPoint(1);               // y
        A( i, 5 ) = 1;                          // 1

        B( i ) = currPoint(2);                  // z
    }

    // 2 - On laisse eigen faire le travail de resolution du systeme aux moindres carres
    // A x - B le plus proche de zero
    Eigen::VectorXd coeffs(6);
    coeffs = A.fullPivHouseholderQr().solve(B);

    outA = coeffs(0);
    outB = coeffs(1);
    outC = coeffs(2);
    outD = coeffs(3);
    outE = coeffs(4);
    outF = coeffs(5);
}

void NORM_Tools::NORM_LeastSquareFitting::quadraticSurfaceFitting( const CT_MPCIR indexCloud,
                                                               float& outA, float& outB, float& outC, float& outD, float& outE, float& outF )
{
    if( indexCloud.data() != NULL )
    {
        quadraticSurfaceFitting( indexCloud->abstractCloudIndexT(),
                                 outA, outB, outC, outD, outE, outF );
    }
}


CT_Point NORM_Tools::NORM_ChangeBasis::pointInNewBasis(Eigen::Matrix3d& changeBasisMatrix,
                                                       const CT_Point& pointInOldBasis)
{
    return (changeBasisMatrix * pointInOldBasis);
}


Eigen::Matrix3d NORM_Tools::NORM_ChangeBasis::changeBasisMatrix(CT_Point &ob1, CT_Point &ob2, CT_Point &ob3, CT_Point &nb1, CT_Point &nb2, CT_Point &nb3)
{
    // ob = old basis
    // nb = new basis
    // Les colonnes de la matrice de passage B telle que p*B = p'
    // Avec p dans l'ancienne base et p' dans la nouvelle base
    // Sont formees des coordonnees de l'ancienne base dans la nouvelle
    // Soit :
    // | ob1.nb1 ob2.nb1 ob3.nb1 |
    // | ob1.nb2 ob2.nb2 ob3.nb2 |
    // | ob1.nb3 ob2.nb3 ob3.nb3 |
    Eigen::Matrix3d rslt;

    // Premiere ligne
    rslt(0,0) = NORM_Tools::NORM_ChangeBasis::scalarProduct( ob1, nb1 );
    rslt(0,1) = NORM_Tools::NORM_ChangeBasis::scalarProduct( ob2, nb1 );
    rslt(0,2) = NORM_Tools::NORM_ChangeBasis::scalarProduct( ob3, nb1 );

    // Deuxieme ligne
    rslt(1,0) = NORM_Tools::NORM_ChangeBasis::scalarProduct( ob1, nb2 );
    rslt(1,1) = NORM_Tools::NORM_ChangeBasis::scalarProduct( ob2, nb2 );
    rslt(1,2) = NORM_Tools::NORM_ChangeBasis::scalarProduct( ob3, nb2 );

    // Troisieme ligne
    rslt(2,0) = NORM_Tools::NORM_ChangeBasis::scalarProduct( ob1, nb3 );
    rslt(2,1) = NORM_Tools::NORM_ChangeBasis::scalarProduct( ob2, nb3 );
    rslt(2,2) = NORM_Tools::NORM_ChangeBasis::scalarProduct( ob3, nb3 );

    return rslt;
}


float NORM_Tools::NORM_ChangeBasis::scalarProduct(CT_Point &p1, CT_Point &p2)
{
    return ( (p1.x()*p2.x()) + (p1.y()*p2.y()) + (p1.z()*p2.z()) );
}


void NORM_Tools::NORM_LeastSquareFitting::quadraticSurfaceFitting(const QVector<CT_Point> *points,
                                                                  float &outA, float &outB, float &outC, float &outD, float &outE, float &outF)
{
    size_t nPts_itemIn_ItemScene = points->size();

    // 1 - On remplit la matrice A des xx xy yy x y 1 et le vecteur B des z
    Eigen::MatrixXd A( nPts_itemIn_ItemScene, 6 );
    Eigen::VectorXd B( nPts_itemIn_ItemScene );

    for( size_t i = 0 ; i < nPts_itemIn_ItemScene ; i++ )
    {
        const CT_Point &currPoint = points->at(i);
        A( i, 0 ) = currPoint(0)*currPoint(0);  // xx
        A( i, 1 ) = currPoint(0)*currPoint(1);  // xy
        A( i, 2 ) = currPoint(1)*currPoint(1);  // yy
        A( i, 3 ) = currPoint(0);               // x
        A( i, 4 ) = currPoint(1);               // y
        A( i, 5 ) = 1;                          // 1

        B( i ) = currPoint(2);                  // z
    }

    // 2 - On laisse eigen faire le travail de resolution du systeme aux moindres carres
    // A x - B le plus proche de zero
    Eigen::VectorXd coeffs(6);
    coeffs = A.fullPivHouseholderQr().solve(B);

    outA = coeffs(0);
    outB = coeffs(1);
    outC = coeffs(2);
    outD = coeffs(3);
    outE = coeffs(4);
    outF = coeffs(5);
}

void NORM_Tools::NORM_LeastSquareFitting::quadraticSurfaceFitting(QVector<int>* indices,
                                                                  const CT_PointAccessor& pAccessor,
                                                                  const CT_Point centroid,
                                                                  Eigen::Matrix3d& changeBaseMatrix,
                                                                  float& outA, float& outB, float& outC, float& outD, float& outE, float& outF)
{
    size_t nPts_itemIn_ItemScene = indices->size();

    // 1 - On remplit la matrice A des xx xy yy x y 1 et le vecteur B des z
    Eigen::MatrixXd A( nPts_itemIn_ItemScene, 6 );
    Eigen::VectorXd B( nPts_itemIn_ItemScene );

    for( size_t i = 0 ; i < nPts_itemIn_ItemScene ; i++ )
    {
        // Recupere le point
        const CT_Point currPointOldBase = pAccessor.constPointAt( indices->at(i) );

        // Recentre le point
        CT_Point currPointCentred = currPointOldBase - centroid;

        // Le passe dans la base de l'acp
        CT_Point currPoint = NORM_Tools::NORM_ChangeBasis::pointInNewBasis( changeBaseMatrix,
                                                                            currPointCentred );

        // Remplit la matrice
        A( i, 0 ) = currPoint(0)*currPoint(0);  // xx
        A( i, 1 ) = currPoint(0)*currPoint(1);  // xy
        A( i, 2 ) = currPoint(1)*currPoint(1);  // yy
        A( i, 3 ) = currPoint(0);               // x
        A( i, 4 ) = currPoint(1);               // y
        A( i, 5 ) = 1;                          // 1

        B( i ) = currPoint(2);                  // z
    }

    // 2 - On laisse eigen faire le travail de resolution du systeme aux moindres carres
    // A x - B le plus proche de zero
    Eigen::VectorXd coeffs(6);
    coeffs = A.fullPivHouseholderQr().solve(B);

    outA = coeffs(0);
    outB = coeffs(1);
    outC = coeffs(2);
    outD = coeffs(3);
    outE = coeffs(4);
    outF = coeffs(5);
}

float NORM_Tools::NORM_LeastSquareFitting::quadraticSurfaceFittingWithRMSE(QVector<int>* indices,
                                                                           const CT_PointAccessor& pAccessor,
                                                                           const CT_Point centroid,
                                                                           Eigen::Matrix3d& changeBaseMatrix,
                                                                           float& outA, float& outB, float& outC, float& outD, float& outE, float& outF)
{
    size_t nPts_itemIn_ItemScene = indices->size();

    // 1 - On remplit la matrice A des xx xy yy x y 1 et le vecteur B des z
    Eigen::MatrixXd A( nPts_itemIn_ItemScene, 6 );
    Eigen::VectorXd B( nPts_itemIn_ItemScene );

    for( size_t i = 0 ; i < nPts_itemIn_ItemScene ; i++ )
    {
        // Recupere le point
        const CT_Point currPointOldBase = pAccessor.constPointAt( indices->at(i) );

        // Recentre le point
        CT_Point currPointCentred = currPointOldBase - centroid;

        // Le passe dans la base de l'acp
        CT_Point currPoint = NORM_Tools::NORM_ChangeBasis::pointInNewBasis( changeBaseMatrix,
                                                                            currPointCentred );

        // Remplit la matrice
        A( i, 0 ) = currPoint(0)*currPoint(0);  // xx
        A( i, 1 ) = currPoint(0)*currPoint(1);  // xy
        A( i, 2 ) = currPoint(1)*currPoint(1);  // yy
        A( i, 3 ) = currPoint(0);               // x
        A( i, 4 ) = currPoint(1);               // y
        A( i, 5 ) = 1;                          // 1

        B( i ) = currPoint(2);                  // z
    }

    // 2 - On laisse eigen faire le travail de resolution du systeme aux moindres carres
    // A x - B le plus proche de zero
    Eigen::VectorXd coeffs(6);
    coeffs = A.fullPivHouseholderQr().solve(B);

    outA = coeffs(0);
    outB = coeffs(1);
    outC = coeffs(2);
    outD = coeffs(3);
    outE = coeffs(4);
    outF = coeffs(5);

    Eigen::VectorXd errors( nPts_itemIn_ItemScene );
    errors = A * coeffs;

    // Renvoie la RMSE
    return sqrt( errors.squaredNorm() / (float)nPts_itemIn_ItemScene );
}

Eigen::Vector4f NORM_Tools::NORM_LeastSquareFitting::newton(Eigen::Matrix3f &A, Eigen::Vector3f &B, float C, Eigen::Vector4f x0, float seuil, int nIterMax )
{
    Eigen::Vector4f xi = x0;
    Eigen::Vector4f xiSave = x0;
    Eigen::Vector4f tmp = quadFunc( A, B, C, xi, x0 );
    float distanceEntreTemps = std::numeric_limits<float>::max();

    int i = 0;
    while( distanceEntreTemps > seuil && i < nIterMax )
    {
        xiSave = xi;
        xi = newtonStep(A, B, C, xi, x0 );
        tmp = quadFunc( A, B, C, xi, x0 );
        distanceEntreTemps = sqrt( (xi(0)-xiSave(0))*(xi(0)-xiSave(0)) +
                                   (xi(1)-xiSave(1))*(xi(1)-xiSave(1)) +
                                   (xi(2)-xiSave(2))*(xi(2)-xiSave(2)) );
//        qDebug() << "Distance iteration " << i << distanceEntreTemps;
        i++;
    }

    return xi;
}

Eigen::Vector4f NORM_Tools::NORM_LeastSquareFitting::newtonStep(Eigen::Matrix3f& A, Eigen::Vector3f& B, float C, Eigen::Vector4f &x, Eigen::Vector4f x0)
{
    return (x - ( quadGrad(A, B, x).inverse() * quadFunc(A, B, C, x, x0) ) );
}

Eigen::Vector4f NORM_Tools::NORM_LeastSquareFitting::quadFunc(Eigen::Matrix3f& A, Eigen::Vector3f& B, float C, Eigen::Vector4f &x, Eigen::Vector4f &x0)
{
    Eigen::Vector3f XX = x.block<3,1>(0,0);
    float l = x(3);
    Eigen::Vector3f rslt3 = 2*(XX - x0.block<3,1>(0,0) ) + l * (2*A*XX+B);
    float a = B.transpose() * XX;
    float b = ((XX.transpose() * A) * XX);
    float rslt4 = a+b+C;
    Eigen::Vector4f rslt;

    for( int i = 0 ; i < 3 ; i++ )
    {
        rslt(i) = rslt3(i);
    }
    rslt(3) = rslt4;

    return rslt;
}


Eigen::Matrix4f NORM_Tools::NORM_LeastSquareFitting::quadGrad(Eigen::Matrix3f& A, Eigen::Vector3f& B, Eigen::Vector4f &x)
{
    Eigen::Vector3f XX = x.block<3,1>(0,0);
    float l = x(3);
    Eigen::Vector3f grad = 2*A*XX + B;
    Eigen::Matrix3f M = 2 * Eigen::Matrix<float, 3, 3>::Identity() + l * A;

    Eigen::Matrix4f rslt;
    for( int i = 0 ; i < 3 ; i++ )
    {
        for( int j = 0 ; j < 3 ; j++ )
        {
            rslt(i,j) = M(i,j);
        }

        rslt(i,3) = grad(i);

        rslt(3,i) = grad(i);
    }
    rslt(3,3) = 0;

    return rslt;
}
