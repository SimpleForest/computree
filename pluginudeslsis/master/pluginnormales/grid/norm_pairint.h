#ifndef NORM_PAIRINT_H
#define NORM_PAIRINT_H

#include <QPair>

class NORM_PairInt : public QPair<int, int>
{
public:
    NORM_PairInt();
    NORM_PairInt(int firstInt, int secondInt );
};

#endif // NORM_PAIRINT_H
