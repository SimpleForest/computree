#include "norm_stepcomputesigma.h"

#include <QDebug>
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_iterator/ct_pointiterator.h"

#include "octree/norm_octree.h"
#include "octree/norm_octreenode.h"
#include "../tools/norm_acp.h"

// Alias for indNORMing models
#define DEFin_inResultOctree "inResultOctree"
#define DEFin_inGroupOctree "inGroupOctree"
#define DEFin_inItemOctree "inItemItemWithOctree"

#define DEFout_outResultSigma "outResultSigma"
#define DEFout_outGroupSigma "outGroupSigma"
#define DEFout_outItemSigma "outItemSigma"

typedef CT_PointsAttributesScalarTemplated<float> CT_PointAttributeSigma;

#include "ct_attributes/ct_attributesnormal.h"

// Constructor : initialization of parameters
NORM_StepComputeSigma::NORM_StepComputeSigma(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _neiRadius = 0.01;
    _preciseNeighbourhood = false;
}

// Step description (tooltip of contNORMtual menu)
QString NORM_StepComputeSigma::getStepDescription() const
{
    return tr("Calcule un indice de planeite en chaque point");
}

// Step detailled description
QString NORM_StepComputeSigma::getStepDetailledDescription() const
{
    return tr("L'indice de planeite se calcule par ACP :<br>"
"lambda3 / somme_des_labdas");
}

// Step URL
QString NORM_StepComputeSigma::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepComputeSigma::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepComputeSigma(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepComputeSigma::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_inResultOctree = createNewInResultModel(DEFin_inResultOctree, tr("inResultOctree"));
    resIn_inResultOctree->setRootGroup(DEFin_inGroupOctree, CT_AbstractItemGroup::staticGetType(), tr("inGroupOctree"));
    resIn_inResultOctree->addItemModel(DEFin_inGroupOctree, DEFin_inItemOctree, NORM_Octree::staticGetType(), tr("inItemItemWithOctree"));
}

// Creation and affiliation of OUT models
void NORM_StepComputeSigma::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_outResultSigma = createNewOutResultModel(DEFout_outResultSigma, tr("outResultSigma"));
    res_outResultSigma->setRootGroup(DEFout_outGroupSigma, new CT_StandardItemGroup(), tr("outGroupSigma"));
    res_outResultSigma->addItemModel(DEFout_outGroupSigma, DEFout_outItemSigma, new CT_PointAttributeSigma(), tr("outItemSigma"));
}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepComputeSigma::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Rayon du voisinage spherique", "(m)", 0.0001, 100, 4, _neiRadius, 1);
    configDialog->addBool("Use precise neighbourhood","","", _preciseNeighbourhood );
}

void NORM_StepComputeSigma::compute()
{
    // DONT'T FORGET TO ADD THIS STEP TO THE PLUGINMANAGER !!!!!
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_inResultOctree = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_outResultSigma = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_inGroupOctree(resIn_inResultOctree, this, DEFin_inGroupOctree);
    const NORM_Octree* itemIn_inItemOctree;
    const CT_AbstractItemGroup* grpIn_inGroupOctree;
    while (itIn_inGroupOctree.hasNext() && !isStopped())
    {
        // Access the octree
        grpIn_inGroupOctree = (CT_AbstractItemGroup*) itIn_inGroupOctree.next();
        itemIn_inItemOctree = (NORM_Octree*)grpIn_inGroupOctree->firstItemByINModelName(this, DEFin_inItemOctree);
        if ( itemIn_inItemOctree != NULL)
        {
            // And its point cloud
            CT_PCIR inputIndexCloud = itemIn_inItemOctree->indexCloud();

            // On cree le nuage d'attributs qui va servir a contenir les sigmas
            CT_PointAttributeSigma* outSigmaAttributes = new CT_PointAttributeSigma(DEFout_outItemSigma, res_outResultSigma, inputIndexCloud );
            CT_Point bot = itemIn_inItemOctree->bot();
            CT_Point top = itemIn_inItemOctree->top();
            outSigmaAttributes->setBoundingBox( bot(0), bot(1), bot(2), top(0), top(1), top(2) );

            // Parcours de tous les points du nuage pour
            // 1 - Calculer un voisinage spherique
            // 2 - sur lequel on fait une acp
            // 3 - qui nous donne le sigma attendu
            CT_PointIterator itPoint( inputIndexCloud );// Iterateur pour le parcours des points
            size_t i = 0;                               // Indice de parcours du nuage d'indices
            size_t nbPoints = inputIndexCloud->size();  // Taille du nuage d'indice
            float floati = 0;                           // Utilise pour avoir la progress bar (identique a i mais en float pour les divisions)
            float floatNbPoints = nbPoints;             // Utilise pour avoir la progress bar (identique a nbPoints mais en float pour les divisions)
            float currSigma = 0;                        // Valeur de sigma pour le point courant
            float sigmaMin = std::numeric_limits<float>::max(); // Valeur min des sigmas calcules
            float sigmaMax = -std::numeric_limits<float>::max();// Valeur max des sigmas calcules
            size_t nbErrors = 0;                                // Number of points for which sigma could not be computed (not enough points in the neighbourhood)
            while( itPoint.hasNext() && !isStopped() )
            {
                // 1 - Accede au point courrant
                itPoint.next();
                const CT_Point& currPoint = itPoint.currentPoint();

                // 1 - Accede au voisinage psherique (strictement spherique ou plus osuple selon le choix de l'utilisateur)
                CT_PointCloudIndexVector* nei;
                if ( _preciseNeighbourhood )
                {
                    // Recherche du voisinage spherique de curPoint
                    nei = itemIn_inItemOctree->sphericalNeighbourhood( currPoint, _neiRadius );
                }

                else
                {
                    nei = itemIn_inItemOctree->approximateSphericalNeighbourhood( currPoint, _neiRadius );
                }

                // 2 - Si le voisinage spherique contient assez de points pour faire une ACP
                if( nei->size() >= 3 )
                {
                    // 2 - Calcul de l'ACP sur le voisinage
                    float lambda1, lambda2, lambda3;
                    CT_Point v1, v2, v3;
                    NORM_Tools::NORM_PCA::pca( nei,
                                               lambda1, lambda2, lambda3,
                                               v1, v2, v3 );

                    currSigma = lambda3 / (lambda1 + lambda2 + lambda3 );
                }

                // 2 - Le voisinage contenait trop peu de points, on donne une valeur negative
                else
                {
                    currSigma = -100;
                    nbErrors++;
                }

                // 3 - Affectation de l'attribut
                outSigmaAttributes->setValueAt( i, currSigma );

                // 3 - Update min and max sigma values
                if ( outSigmaAttributes->valueAt(i) < sigmaMin )
                {
                    sigmaMin = outSigmaAttributes->valueAt(i);
                }
                if ( outSigmaAttributes->valueAt(i) > sigmaMax )
                {
                    sigmaMax = outSigmaAttributes->valueAt(i);
                }

                // 3 - Libere la memoire
                delete nei;

                // Barre de progression
                setProgress( floati * 100 / floatNbPoints );
                i++;
                floati++;
            }

            PS_LOG->addErrorMessage( PS_LOG->step , QString::number( nbErrors ) + " points n'ont pas pu avoir de sigma");
            qDebug() << "Nb Error = " << nbErrors;

            // Affectation des attributs min et max
            outSigmaAttributes->setMin( sigmaMin );
            outSigmaAttributes->setMax( sigmaMax );

            // Ajouts du sigma au resultat
            CT_StandardItemGroup* grp_outGroupSigmaValues = new CT_StandardItemGroup(DEFout_outGroupSigma, res_outResultSigma);
            grp_outGroupSigmaValues->addItemDrawable(outSigmaAttributes);
            res_outResultSigma->addGroup( grp_outGroupSigmaValues );
            /* *********************************************************************************************************************************************** */
        }
    }
}
