#ifndef NORM_NEWSTEPCOMPARESETOFNORMALS_H
#define NORM_NEWSTEPCOMPARESETOFNORMALS_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"

class NORM_NewStepCompareSetOfNormals : public CT_AbstractStep
{
    Q_OBJECT

public:
    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    NORM_NewStepCompareSetOfNormals(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    void getFileFormat(QString fileName,
                       int& outnChamps,
                       int& outNHeaderLines);

    void compareTwoFiles(QString file1Name,
                         int nChampsFile1,
                         int nHeaderLinesFile1,
                         QString file2Name,
                         int nChampsFile2,
                         int nHeaderLinesFile2,
                         QString outputFileName );

    float angle3D( float x1, float y1, float z1,
                   float x2, float y2, float z2 );

private :
    CT_AutoRenameModels     _autoRenameModelGroup;
    CT_AutoRenameModels     _autoRenameModelScene;
    CT_AutoRenameModels     _autoRenameModelAngles;

    QStringList             _fileList;
};

#endif // NORM_NEWSTEPCOMPARESETOFNORMALS_H
