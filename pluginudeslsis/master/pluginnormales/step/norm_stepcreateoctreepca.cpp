/************************************************************************************
* Filename :  norm_stepcreateoctreepca.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "norm_stepcreateoctreepca.h"

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_pointsattributesscalartemplated.h"
#include "ct_itemdrawable/ct_pointsattributescolor.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

#include "octree/norm_octreepca.h"

// Alias for indexing models
#define DEFin_Result "Result"
#define DEFin_Group "Group"
#define DEFin_IndexCloud "IndexCloud"

#define DEFout_ResultOctree "ResultOctree"
#define DEFout_GroupOctree "GroupOctree"
#define DEFout_ItemAttributeCloudIdNode "ItemAttributeCloudIdNode"
#define DEFout_ItemAttributeCloudColorNode "ItemAttributeCloudColorNode"
#define DEFout_ItemAttributeCloudSigmaFromNodes "ItemAttributeCloudSigmaFromNodes"
#define DEFout_ItemOctree "Octree"

typedef CT_PointsAttributesScalarTemplated<int> AttributEntier;
typedef CT_PointsAttributesScalarTemplated<float> AttributFloat;
typedef CT_PointsAttributesColor AttributCouleur;

// Constructor : initialization of parameters
NORM_StepCreateOctreePCA::NORM_StepCreateOctreePCA(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _minNodeSize = 0.0001;
    _nbPointsMinInNode = 1;
    _threshPCA = 10;
    _minPcaDepth = 5;
}

// Step description (tooltip of contextual menu)
QString NORM_StepCreateOctreePCA::getStepDescription() const
{
    return tr("Cree un octree PCA");
}

// Step detailled description
QString NORM_StepCreateOctreePCA::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString NORM_StepCreateOctreePCA::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepCreateOctreePCA::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepCreateOctreePCA(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepCreateOctreePCA::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_Result = createNewInResultModel(DEFin_Result, tr("inResult"));
    resIn_Result->setRootGroup(DEFin_Group, CT_AbstractItemGroup::staticGetType(), tr("inGroup"));
    resIn_Result->addItemModel(DEFin_Group, DEFin_IndexCloud, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("IndexCloud"));
}

// Creation and affiliation of OUT models
void NORM_StepCreateOctreePCA::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_ResultOctree = createNewOutResultModel(DEFout_ResultOctree, tr("Resultat creation octree"));
    res_ResultOctree->setRootGroup(DEFout_GroupOctree, new CT_StandardItemGroup(), tr("GroupeOctree"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemAttributeCloudIdNode, new AttributEntier(), tr("Identifiant des noeuds [0,7]"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemAttributeCloudColorNode, new AttributCouleur(), tr("Coloration des points par noeuds"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemAttributeCloudSigmaFromNodes, new AttributFloat(), tr("Coloration des points par sigma des noeuds"));
    res_ResultOctree->addItemModel(DEFout_GroupOctree, DEFout_ItemOctree, new NORM_OctreePCA(), tr("Octree"));
}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepCreateOctreePCA::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Taille de cellule minimale", "", 0.0001, 100, 4, _minNodeSize, 1);
    configDialog->addInt("Nombre de points minimum par cellule", "", 1, 10000, _nbPointsMinInNode);
    configDialog->addDouble("Seuil PCA", "%", 0.0001, 100, 4, _threshPCA, 1);
    configDialog->addInt("Min Pca Depth", "", 0, 10000, _minPcaDepth);
}

void NORM_StepCreateOctreePCA::compute()
{
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_Result = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_ResultOctree = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_Group(resIn_Result, this, DEFin_Group);
    while (itIn_Group.hasNext() && !isStopped())
    {
        const CT_AbstractItemGroup* grpIn_Group = (CT_AbstractItemGroup*) itIn_Group.next();

        const CT_AbstractItemDrawableWithPointCloud* itemIn_IndexCloud = (CT_AbstractItemDrawableWithPointCloud*)grpIn_Group->firstItemByINModelName(this, DEFin_IndexCloud);
        CT_PCIR inputIndexCloud = itemIn_IndexCloud->getPointCloudIndexRegistered();

        if (itemIn_IndexCloud != NULL)
        {
            // OUT results creation (move it to the appropried place in the code)
            CT_StandardItemGroup* grp_GroupOctree= new CT_StandardItemGroup(DEFout_GroupOctree, res_ResultOctree);
            res_ResultOctree->addGroup(grp_GroupOctree);

            // On cree l'octree
            CT_Point bot = createCtPoint( itemIn_IndexCloud->minX(), itemIn_IndexCloud->minY(), itemIn_IndexCloud->minZ() );
            CT_Point top = createCtPoint( itemIn_IndexCloud->maxX(), itemIn_IndexCloud->maxY(), itemIn_IndexCloud->maxZ() );
            NORM_OctreePCA* octree = new NORM_OctreePCA ( _minNodeSize, _nbPointsMinInNode,
                                                          bot, top,
                                                          _threshPCA, inputIndexCloud,
                                                          DEFout_ItemOctree ,res_ResultOctree,
                                                          _minPcaDepth );
            octree->createOctree();

            AttributEntier* item_AttributeCloudIdNode = octree->getIdAttribute( DEFout_ItemAttributeCloudIdNode, res_ResultOctree );
            AttributColor* item_AttributeCloudColorNode = octree->getColorAttribute( DEFout_ItemAttributeCloudColorNode, res_ResultOctree );
            AttributFloat* item_AttributeCloudSigmaFronNodes = octree->getSigmaAttributeFromNodes( DEFout_ItemAttributeCloudSigmaFromNodes, res_ResultOctree );

            grp_GroupOctree->addItemDrawable(item_AttributeCloudIdNode);
            grp_GroupOctree->addItemDrawable(item_AttributeCloudColorNode);
            grp_GroupOctree->addItemDrawable(item_AttributeCloudSigmaFronNodes);
            grp_GroupOctree->addItemDrawable( octree );
        }
    }
}
