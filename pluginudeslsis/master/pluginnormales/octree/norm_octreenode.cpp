#include "norm_octreenode.h"

#include "norm_abstractoctree.h"
#include "norm_octreettools.h"

#include "ct_accessor/ct_pointaccessor.h"

#include <QDebug>
#include "ct_color.h"

#define QCOLOR0 QColor::fromHsv( 0, 255, 255)
#define QCOLOR1 QColor::fromHsv( 45, 255, 255)
#define QCOLOR2 QColor::fromHsv( 90, 255, 255)
#define QCOLOR3 QColor::fromHsv( 135, 255, 255)
#define QCOLOR4 QColor::fromHsv( 180, 255, 255)
#define QCOLOR5 QColor::fromHsv( 225, 255, 255)
#define QCOLOR6 QColor::fromHsv( 270, 255, 255)
#define QCOLOR7 QColor::fromHsv( 315, 255, 255)

#define CTCOLOR0 CT_Color( QColor::fromHsv( 0, 255, 255) )
#define CTCOLOR1 CT_Color( QColor::fromHsv( 45, 255, 255) )
#define CTCOLOR2 CT_Color( QColor::fromHsv( 90, 255, 255) )
#define CTCOLOR3 CT_Color( QColor::fromHsv( 135, 255, 255) )
#define CTCOLOR4 CT_Color( QColor::fromHsv( 180, 255, 255) )
#define CTCOLOR5 CT_Color( QColor::fromHsv( 225, 255, 255) )
#define CTCOLOR6 CT_Color( QColor::fromHsv( 270, 255, 255) )
#define CTCOLOR7 CT_Color( QColor::fromHsv( 315, 255, 255) )

const QVector<QColor> NORM_OctreeNode::_qColorChild = QVector<QColor>() << QCOLOR0 << QCOLOR1 << QCOLOR2 << QCOLOR3 << QCOLOR4 << QCOLOR5 << QCOLOR6 << QCOLOR7;
const QVector<CT_Color> NORM_OctreeNode::_ctColorChild = QVector<CT_Color>() << CTCOLOR0 << CTCOLOR1 << CTCOLOR2 << CTCOLOR3 << CTCOLOR4 << CTCOLOR5 << CTCOLOR6 << CTCOLOR7;

NORM_OctreeNode::NORM_OctreeNode(NORM_AbstractOctree *octree,
                                 NORM_OctreeNode *father,
                                 size_t first,
                                 size_t last,
                                 const CT_Point& center ,
                                 size_t codex,
                                 size_t codey,
                                 size_t codez)
    : NORM_AbstractNode(),
      _octree( octree ),
      _codeX( codex ),
      _codeY( codey ),
      _codeZ( codez ),
      _first( first ),
      _last( last ),
      _father( father ),
      _children( QVector< NORM_OctreeNode* >(8, NULL) )
{
    // Si le noeud a creer est une racine
    if ( father == NULL )
    {
        _depth = 0;
        _width = _octree->top()(0) - _octree->bot()(0);
        _center = ( _octree->top() + _octree->bot() ) / 2.0;
    }

    else
    {
        _depth = father->_depth + 1;
        _width = father->_width /2.0;
        _center = center;
    }
}

bool NORM_OctreeNode::isSubdivisible()
{
    // Critere sur le nombre de points
    if( (_last <= _first) || (_last - _first <= _octree->minNbPoints()) )
    {
        return false;
    }

    // Critere sur la profondeur maximale
    if( _depth >= _octree->maxDepth() )
    {
        return false;
    }

    return true;
}

void NORM_OctreeNode::subdivide()
{
    // Dans un premier temps on trie les points par ordre de morton
    size_t bornes[9];
    NORM_OctreeTools::sortMortonOrder( _first, _last,
                                       _center, bornes,
                                       _octree->modifiablepointCloudIndexRegistered() );

    // Dans un second temps on cree les fils
    CT_Point centerChild;
    size_t codexChild, codeyChild, codezChild;
    for( size_t i = 0 ; i < 8 ; i++ )
    {
        // Calcul du centre du fils
        getChildCenterFromID( i, centerChild );

        // Calcul du code du fils
        getChildCodeFromID(i, codexChild, codeyChild, codezChild );

        // Instanciation du fils
        _children[i] = new NORM_OctreeNode( _octree,
                                            this,
                                            bornes[i],
                                            bornes[i+1],
                                            centerChild,
                                            codexChild,
                                            codeyChild,
                                            codezChild);
    }
}

void NORM_OctreeNode::createOctreeRecursive()
{
    // Si le noeud est divisible
    if ( isSubdivisible() )
    {
        // On le divise effectivement
        subdivide();

        // Et on lance la recursion sur les huit fils
        for ( int i = 0 ; i < 8 ; i++ )
        {
            _children[i]->createOctreeRecursive();
        }
    }
}

void NORM_OctreeNode::createOctreeRecursive(int minPcaDepth)
{
    Q_UNUSED(minPcaDepth);

    // Si le noeud est divisible
    if ( isSubdivisible() )
    {
        // On le divise effectivement
        subdivide();

        // Et on lance la recursion sur les huit fils
        for ( int i = 0 ; i < 8 ; i++ )
        {
            _children[i]->createOctreeRecursive();
        }
    }
}

void NORM_OctreeNode::setIdAttributeRecursive(AttributEntier *attributs, size_t currentID)
{
    // Si on est sur une feuille alors on met a jour les identifiants
    if( isLeaf() )
    {
        for( size_t index = _first ; index < _last ; index++ )
        {
            attributs->setValueAt( index, currentID );
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            _children[i]->setIdAttributeRecursive( attributs, i );
        }
    }
}

void NORM_OctreeNode::setColorAttributeRecursive(CT_AbstractColorCloud *attributs, size_t currentID)
{
    // Si on est sur une feuille alors on met a jour les identifiants
    if( isLeaf() )
    {
        for( size_t index = _first ; index < _last ; index++ )
        {
            (*attributs)[index] = _ctColorChild[currentID];
        }
    }

    // Sinon on continue la recursion
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            _children[i]->setColorAttributeRecursive( attributs, i );
        }
    }
}

void NORM_OctreeNode::getChildCenterFromID(size_t id, CT_Point& outChildCenter )
{
    CT_Point deplacement = createCtPoint(1,1,1);

    for( size_t i = 0 ; i < 3 ; i++ )
    {
        if( (id & ( 1 << i ) ) == 0 )
        {
            deplacement(i) = -1;
        }
    }

    outChildCenter = _center + ( deplacement * ( _width / 4.0 ));
}

void NORM_OctreeNode::getChildCodeFromID(size_t id, size_t& outCodeX, size_t& outCodeY, size_t& outCodeZ )
{
    // Deplacement en X
    size_t deplX = ( ( id & (1 << 0) ) >> 0 );
    outCodeX = _codeX | ( deplX << ( _octree->maxDepth() - (_depth + 1) ) ); // +1 puisq'uon prend la profondeur du fils

    size_t deplY = ( ( id & (1 << 1) ) >> 1 );
    outCodeY = _codeY | ( deplY << ( _octree->maxDepth() - (_depth + 1) ) );

    size_t deplZ = ( ( id & (1 << 2) ) >> 2 );
    outCodeZ = _codeZ | ( deplZ << ( _octree->maxDepth() - (_depth + 1) ) );
}

void NORM_OctreeNode::getCenterFromID(size_t id)
{
    CT_Point deplacement = createCtPoint(1,1,1);

    for( size_t i = 0 ; i < 3 ; i++ )
    {
        if( (id & ( 1 << i ) ) == 0 )
        {
            deplacement(i) = -1;
        }
    }

    _center = _father->center() + ( deplacement * (_width / 2.0) );
}

void NORM_OctreeNode::printInfos()
{
    float halfWidth = _width / 2.0;
    qDebug() << "Depth = " << _depth;
    qDebug() << "Center = " << _center(0) << _center(1) << _center(2);
    qDebug() << "Bot = " << _center(0) - halfWidth << _center(1) - halfWidth << _center(2) - halfWidth;
    qDebug() << "Top = " << _center(0) + halfWidth << _center(1) + halfWidth << _center(2) + halfWidth;
    qDebug() << "Width = " << _width;
    qDebug() << "First et last et nb " << _first << _last << _last - _first;
    printCodes();
}

void NORM_OctreeNode::printInfosRecursive()
{
    qDebug() << "-----";
    printInfos();
    qDebug() << "*****";

    if( isLeaf() )
    {
        return;
    }

    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            _children[i]->printInfosRecursive();
        }
    }
}

// TEMPORAIRE
void NORM_OctreeNode::printNodesInBBoxRecursive(const CT_Point &bot, const CT_Point top, size_t& totalNbPts )
{
    if( isLeaf() )
    {
        CT_Point cellBot, cellTop;
        getBoundingBox( cellBot, cellTop );
        CT_Point p0 = createCtPoint( cellBot(0), cellBot(1), cellBot(2) );
        CT_Point p1 = createCtPoint( cellBot(0), cellBot(1), cellTop(2) );
        CT_Point p2 = createCtPoint( cellBot(0), cellTop(1), cellBot(2) );
        CT_Point p3 = createCtPoint( cellBot(0), cellTop(1), cellTop(2) );
        CT_Point p4 = createCtPoint( cellTop(0), cellBot(1), cellBot(2) );
        CT_Point p5 = createCtPoint( cellTop(0), cellBot(1), cellTop(2) );
        CT_Point p6 = createCtPoint( cellTop(0), cellTop(1), cellBot(2) );
        CT_Point p7 = createCtPoint( cellTop(0), cellTop(1), cellTop(2) );

        bool isIn = false;
        isIn |= pointIsInBBox( bot, top, p0 );
        isIn |= pointIsInBBox( bot, top, p1 );
        isIn |= pointIsInBBox( bot, top, p2 );
        isIn |= pointIsInBBox( bot, top, p3 );
        isIn |= pointIsInBBox( bot, top, p4 );
        isIn |= pointIsInBBox( bot, top, p5 );
        isIn |= pointIsInBBox( bot, top, p6 );
        isIn |= pointIsInBBox( bot, top, p7 );

        if( isIn )
        {
            printInfos();
            totalNbPts += nbPoints();
        }
    }

    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            _children[i]->printNodesInBBoxRecursive(bot, top, totalNbPts );
        }
    }
}
// TEMPORAIRE FIN

void NORM_OctreeNode::printBBox()
{
    CT_Point bot, top;
    getBoundingBox( bot, top );
    qDebug() <<"Bot " << bot(0) << bot(1) << bot(2);
    qDebug() <<"Top " << top(0) << top(1) << top(2);
}

void NORM_OctreeNode::printCodes()
{
    printBinary( _codeX, _octree->maxDepth() + 1, "Code x = " );
    printBinary( _codeY, _octree->maxDepth() + 1, "Code y = " );
    printBinary( _codeZ, _octree->maxDepth() + 1, "Code z = " ); // +1 parce qu'on veut aussi afficher la profondeur 0 (qui est la racine)
}

void NORM_OctreeNode::getBoundingBox(CT_Point &outBot, CT_Point &outTop)
{
    double halfWidth = ((double)_width) / 2.0;

    for( size_t i = 0 ; i < 3 ; i++ )
    {
        outBot(i) = _center(i) - halfWidth;
        outTop(i) = _center(i) + halfWidth;
    }
}

double NORM_OctreeNode::getBot(size_t c)
{
    return ( _center(c) - (_width/2.0) );
}

double NORM_OctreeNode::getTop(size_t c)
{
    return ( _center(c) + (_width/2.0) );
}

void NORM_OctreeNode::printPoints()
{
    CT_PointAccessor pAccessor;
    CT_Point currPoint;
    CT_MPCIR indexCloud = _octree->modifiablepointCloudIndexRegistered();

    // Pour chaque point dans la cellule
    for( size_t i = _first ; i < _last ; i++ )
    {
        // On recupere le point
        pAccessor.pointAt( indexCloud->indexAt(i), currPoint );

        // On l'affiche
        qDebug() << "Indice local = " << i
                 << ", Indice global = "
                 << ", point = [" << indexCloud->indexAt(i) << currPoint(0) << currPoint(1) << currPoint(2) << "]";
    }
}
CT_Point NORM_OctreeNode::center() const
{
    return _center;
}

void NORM_OctreeNode::setCenter(const CT_Point &center)
{
    _center = center;
}

void NORM_OctreeNode::printBinary( size_t n, size_t length, QString bef )
{
    QDebug deb = qDebug();
    deb << bef << " ";
    for( int i = length-1 ; i >= 0 ; i-- )
    {
        deb << ( ( n & (1 << i) ) >> i );
    }
}

NORM_OctreeNode* NORM_OctreeNode::leafNodeAtCodeRecursive(size_t codex,
                                                          size_t codey,
                                                          size_t codez,
                                                          size_t maxDepth )
{
    // If the node is a leaf we reached the deepest octree cell conaining the querry code
    if ( _depth >= maxDepth || isLeaf() )
    {
        return this;
    }

    // Otherwise, continue the recursive querry
    else
    {
        // Compute the child id from location codes
        size_t childID = 0;
        childID = childID | ( ( codex & ( 1 << ( _octree->maxDepth() - _depth - 1 ) ) ) >> ( _octree->maxDepth() - _depth - 1 ) );      // x
        childID = childID | ( ( codey & ( 1 << ( _octree->maxDepth() - _depth - 1 ) ) ) >> ( _octree->maxDepth() - _depth - 1 ) ) << 1; // y
        childID = childID | ( ( codez & ( 1 << ( _octree->maxDepth() - _depth - 1 ) ) ) >> ( _octree->maxDepth() - _depth - 1 ) ) << 2; // z

        return _children[childID]->leafNodeAtCodeRecursive( codex, codey, codez, maxDepth );
    }
}

NORM_OctreeNode* NORM_OctreeNode::parentNodeAtCodeRecursive( size_t maxDepth )
{
    // If the node is a leaf we reached the deepest octree cell conaining the querry code
    if ( _depth == maxDepth || _depth == 0 )
    {
        return this;
    }

    else
    {
        _father->parentNodeAtCodeRecursive( maxDepth );
    }
}

CT_PointCloudIndexVector* NORM_OctreeNode::getIndexCloud()
{
    CT_PointCloudIndexVector* rslt = new CT_PointCloudIndexVector();
    rslt->setSortType( CT_AbstractCloudIndex::NotSorted );
    CT_MPCIR cloudIndex = _octree->modifiablepointCloudIndexRegistered();

    for( size_t i = _first ; i < _last ; i++ )
    {
        rslt->addIndex( cloudIndex->indexAt(i) );
    }

    return rslt;
}

QVector<CT_Point>* NORM_OctreeNode::getPointCloud()
{
    QVector<CT_Point>* rslt = new QVector<CT_Point>();
    CT_MPCIR cloudIndex = _octree->modifiablepointCloudIndexRegistered();
    CT_PointAccessor ptAccessor;

    for( size_t i = _first ; i < _last ; i++ )
    {
        rslt->push_back( ptAccessor.pointAt( cloudIndex->indexAt(i) ) );
    }

    return rslt;
}

void NORM_OctreeNode::getAllLeavesRecursive (QVector<NORM_AbstractNode *> &outLeafNodes )
{
    // If the node is a leaf add it to the result and stop recursion
    if ( isLeaf() )
    {
        outLeafNodes.push_back( this );
    }

    // Otherwise continue recursion on the children
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            _children[i]->getAllLeavesRecursive( outLeafNodes );
        }
    }
}

void NORM_OctreeNode::getAllLeavesRecursiveInSphere(QVector<NORM_AbstractNode *> &outLeafNodes,
                                                    const CT_Point& center,
                                                    float radius )
{
    // If the node is a leaf add it to the result and stop recursion
    if ( isLeaf() )
    {
        outLeafNodes.push_back( this );
    }

    // Otherwise continue recursion on the children
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            if( _children[i]->intersectsSphere(center, radius) )
            {
                _children[i]->getAllLeavesRecursive( outLeafNodes );
            }
        }
    }
}

bool NORM_OctreeNode::intersectsSphere( const CT_Point& center, float radius )
{
    // Calculate the bbox of the node
    CT_Point bot;
    CT_Point top;
    for( size_t i = 0 ; i < 3 ; i++ )
    {
        bot(i) = _center(i) - (_width / 2.0);
        top(i) = _center(i) + (_width / 2.0);
    }

    return sphereBBoxCollision( bot, top, center, radius );
}

void NORM_OctreeNode::getCentersOfNeighboursAtSameLevel(QVector<CT_Point> &outCentersOfNeighbours)
{
    // Clear the vector
    outCentersOfNeighbours.clear();

    // Computes the neighbours center
    for( int i = -1 ; i <= 1 ; i++ )
    {
        for( int j = -1 ; j <= 1 ; j++ )
        {
            for ( int k = -1 ; k <= 1 ;k++ )
            {
                // Do not take into account the node itself
                if( (i != 0) | (j != 0) | (k != 0) )
                {
                    CT_Point currNeiCenter = _center;
                    currNeiCenter(0) += i*_width;
                    currNeiCenter(1) += j*_width;
                    currNeiCenter(2) += k*_width;

                    // Make sure the center is in the bounding cube of the octree
                    if( _octree->contains( currNeiCenter ) )
                    {
                        outCentersOfNeighbours.push_back( currNeiCenter );
                    }

                    // Else insert a fake specific point (it will be used latter to find the traversing direction of the octree to get the neighbour leaves
                    else
                    {
                        CT_Point fakeCenter = createCtPoint( nan(""), nan(""), nan("") );
                        outCentersOfNeighbours.push_back( fakeCenter );
                    }
                }
            }
        }
    }
}

void NORM_OctreeNode::leavesInTraversalDirectionRecursive(const CT_Point &traversalDirection,
                                                          QVector<NORM_AbstractNode *> &outNeiLeafInTraversalDirection)
{
    // If the current traversed node is a leaf add it to the result and end recursion
    if ( isLeaf() )
    {
        outNeiLeafInTraversalDirection.push_back( this );
    }

    else
    {
        // Get the children in the traversal direction
        QVector< NORM_OctreeNode* > traversedChildrenInDirection;
        getTraversedChildInDirection( traversalDirection, traversedChildrenInDirection );

        // Continue recursion with these children
        foreach( NORM_OctreeNode* child, traversedChildrenInDirection )
        {
            child->leavesInTraversalDirectionRecursive( traversalDirection, outNeiLeafInTraversalDirection );
        }
    }
}

void NORM_OctreeNode::getTraversedChildInDirection(const CT_Point& direction,
                                                   QVector<NORM_OctreeNode *> &outTraversedChildrenInDirection)
{
    // For all children
    for( size_t i = 0 ; i < 8 ; i++ )
    {
        // Si le numero du fils correspond avec la direction
        bool isInDirection = true;
        for ( size_t j = 0 ; j < 3 ; j++ )
        {
            // On ne teste que les digits non nuls
            if( direction(j) == -1 )
            {
                // On doit comparer le -1 avec un 0 dans la direction
                if( ( i >> j ) != 0  )
                {
                    isInDirection = false;
                }
            }

            else if ( direction(j) == 1 )
            {
                // Par contre si on a un 1 il faut comparer avec un 1
                if( (i >> j ) != 1 )
                {
                    isInDirection = false;
                }
            }
        }

        if ( isInDirection )
        {
            outTraversedChildrenInDirection.push_back( _children[i] );
        }
    }
}

void NORM_OctreeNode::drawRecursive(GraphicsViewInterface &view,
                                    PainterInterface &painter,
                                    size_t idChild ) const
{
    // Draw each leaf and stop recursion
    if ( isLeaf() )
    {
        if( _last > _first )
        {
            painter.setColor( _qColorChild[idChild] );
            float halfWidth = _width / 2.0;
            painter.drawCube( _center(0) - halfWidth, _center(1) - halfWidth, _center(2) - halfWidth,
                              _center(0) + halfWidth, _center(1) + halfWidth, _center(2) + halfWidth);
        }
    }

    // else continue recursion over children
    else
    {
        for( size_t i = 0 ; i < 8 ; i++ )
        {
            _children[i]->drawRecursive( view, painter, i );
        }
    }
}
size_t NORM_OctreeNode::first() const
{
    return _first;
}

void NORM_OctreeNode::setFirst(const size_t &first)
{
    _first = first;
}
size_t NORM_OctreeNode::last() const
{
    return _last;
}

void NORM_OctreeNode::setLast(const size_t &last)
{
    _last = last;
}



bool sphereBBoxCollision(const CT_Point &bot, const CT_Point &top, const CT_Point& center, float radius )
{
    float distance = 0;

    for( int i = 0; i < 3; i++ )
    {
        if( center(i) < bot(i) )
        {
            distance += ( center(i) - bot(i) ) * ( center(i) - bot(i) );
        }

        else if( center(i) > top(i) )
        {
            distance += ( center(i) - top(i) ) * ( center(i) - top(i) );
        }
    }

    return ( distance <= (radius * radius) );
}

bool pointIsInBBox(const CT_Point &bot, const CT_Point &top, const CT_Point &p)
{
    bool rslt = true;

    for( size_t i = 0 ; i < 3 ; i++ )
    {
        if( p(i) < bot(i) || p(i) > top(i) )
        {
            rslt = false;
        }
    }

    return rslt;
}
