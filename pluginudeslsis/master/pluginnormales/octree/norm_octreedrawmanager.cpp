#include "norm_octreedrawmanager.h"
#include "norm_octree.h"

NORM_OctreeDrawManager::NORM_OctreeDrawManager( QString drawConfigurationName )
    : CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager( drawConfigurationName )
{
}


void NORM_OctreeDrawManager::draw(GraphicsViewInterface& view,
                                         PainterInterface& painter,
                                         const CT_AbstractItemDrawable& itemDrawable) const
{
    // Get the item to display
    NORM_Octree& octree = dynamic_cast< NORM_Octree& > ( const_cast<CT_AbstractItemDrawable&>(itemDrawable) );

    // Draw the octree
    octree.draw( view, painter );
}

CT_ItemDrawableConfiguration NORM_OctreeDrawManager::createDrawConfiguration(QString drawConfigurationName) const
{
    return CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::createDrawConfiguration( drawConfigurationName );
}
