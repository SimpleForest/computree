CT_PREFIX = ../../computreev3

exists(../../computreev5) {
    CT_PREFIX = ../../computreev5
    DEFINES += COMPUTREE_V5
}

include($${CT_PREFIX}/all_check_dependencies.pri) # Just verify if there is problems with dependencies, do nothing else
include($${CT_PREFIX}/shared.pri)
include($${PLUGIN_SHARED_DIR}/include.pri)

TARGET = plug_isolatecrown

HEADERS += $${PLUGIN_SHARED_INTERFACE_DIR}/interfaces.h \
    icro_pluginentry.h \
    icro_pluginmanager.h \
    step/icro_step3dgridbooltomultiple2drasters.h \
    step/icro_step3dgriddoubletomultiple2drasters.h \
    step/icro_step3dgridfloattomultiple2drasters.h \
    step/icro_step3dgridinttomultiple2drasters.h \
    step/icro_stepgetouttersurfaceof3dgridbool.h \
    step/icro_stepgetouttersurfaceof3dgriddouble.h \
    step/icro_stepgetouttersurfaceof3dgridfloat.h \
    step/icro_stepgetouttersurfaceof3dgridint.h \
    step/icro_stepgetconnexcomponentsfrom3dgridint.h \
    step/icro_stepcomputelabeledgridfromstempositions.h \
    step/icro_stepmultiressplitcomponentsfromstempositions.h \
    test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.h \
    tools/multiresolutionpixel.h \
    tools/connectedcomponent.h \
    test_steps/icro_stepgetgradientnormfrom3dgridint.h \
    test_steps/icro_stepgetsimplexconnectedcomponentgrid.h \
    test_steps/icro_stepsplitandmergeconnectedcomponents.h \
    step/icro_stepnormalize3dgridint.h \
    step/icro_stepgetdensitygridfromhitgrid.h \
    tools/icro_customvoronoigrid.h \
    tools/icro_customvoronoigrid.hpp \
    test_steps/icro_stepgetvoronoigraphfromcomponents.h \
    tools/icro_templatedgraph.h \
    tools/icro_templatedgraph.hpp \
    tools/icro_unorientedgraphadjacencylist.h \
    test_steps/icro_steptestconnectedcomponentsfrompositionv1.h
SOURCES += \
    icro_pluginentry.cpp \
    icro_pluginmanager.cpp \
    step/icro_step3dgridbooltomultiple2drasters.cpp \
    step/icro_step3dgriddoubletomultiple2drasters.cpp \
    step/icro_step3dgridfloattomultiple2drasters.cpp \
    step/icro_step3dgridinttomultiple2drasters.cpp \
    step/icro_stepgetouttersurfaceof3dgridbool.cpp \
    step/icro_stepgetouttersurfaceof3dgriddouble.cpp \
    step/icro_stepgetouttersurfaceof3dgridfloat.cpp \
    step/icro_stepgetouttersurfaceof3dgridint.cpp \
    step/icro_stepgetconnexcomponentsfrom3dgridint.cpp \
    step/icro_stepcomputelabeledgridfromstempositions.cpp \
    step/icro_stepmultiressplitcomponentsfromstempositions.cpp \
    test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.cpp \
    tools/multiresolutionpixel.cpp \
    tools/connectedcomponent.cpp \
    test_steps/icro_stepgetgradientnormfrom3dgridint.cpp \
    test_steps/icro_stepgetsimplexconnectedcomponentgrid.cpp \
    test_steps/icro_stepsplitandmergeconnectedcomponents.cpp \
    step/icro_stepnormalize3dgridint.cpp \
    step/icro_stepgetdensitygridfromhitgrid.cpp \
    tools/icro_customvoronoigrid.cpp \
    test_steps/icro_stepgetvoronoigraphfromcomponents.cpp \
    tools/icro_templatedgraph.cpp \
    tools/icro_unorientedgraphadjacencylist.cpp \
    test_steps/icro_steptestconnectedcomponentsfrompositionv1.cpp

TRANSLATIONS += languages/pluginisolatecrown_en.ts \
                languages/pluginisolatecrown_fr.ts

LIBS += -lopencv_core
LIBS += -lopencv_highgui
