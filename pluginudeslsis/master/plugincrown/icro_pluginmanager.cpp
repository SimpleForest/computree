#include "icro_pluginmanager.h"
#include "ct_stepseparator.h"
#include "ct_steploadfileseparator.h"
#include "ct_stepcanbeaddedfirstseparator.h"
#include "ct_actions/ct_actionsseparator.h"
#include "ct_exporter/ct_standardexporterseparator.h"
#include "ct_reader/ct_standardreaderseparator.h"
#include "ct_actions/abstract/ct_abstractaction.h"

#include "step/icro_step3dgridbooltomultiple2drasters.h"
#include "step/icro_step3dgriddoubletomultiple2drasters.h"
#include "step/icro_step3dgridfloattomultiple2drasters.h"
#include "step/icro_step3dgridinttomultiple2drasters.h"

#include "step/icro_stepgetouttersurfaceof3dgridbool.h"
#include "step/icro_stepgetouttersurfaceof3dgriddouble.h"
#include "step/icro_stepgetouttersurfaceof3dgridfloat.h"
#include "step/icro_stepgetouttersurfaceof3dgridint.h"

#include "step/icro_stepgetconnexcomponentsfrom3dgridint.h"

#include "step/icro_stepcomputelabeledgridfromstempositions.h"

#include "step/icro_stepmultiressplitcomponentsfromstempositions.h"

#include "test_steps/icro_stepgetconnectedcomponentonintgridmultiresolution.h"
#include "test_steps/icro_stepgetgradientnormfrom3dgridint.h"

#include "test_steps/icro_stepgetsimplexconnectedcomponentgrid.h"
#include "test_steps/icro_stepsplitandmergeconnectedcomponents.h"
#include "test_steps/icro_stepgetvoronoigraphfromcomponents.h"

#include "step/icro_stepnormalize3dgridint.h"
#include "step/icro_stepgetdensitygridfromhitgrid.h"

// Inclure ici les entetes des classes definissant des Ã©tapes/actions/exporters ou readers
ICRO_PluginManager::ICRO_PluginManager() : CT_AbstractStepPlugin()
{
}

ICRO_PluginManager::~ICRO_PluginManager()
{
}

bool ICRO_PluginManager::loadGenericsStep()
{
    CT_StepSeparator *sep = addNewSeparator(new CT_StepSeparator());

    // Ajouter ici les etapes
    sep->addStep(new ICRO_Step3dGridBoolToMultiple2dRasters(*createNewStepInitializeData(NULL)));
    sep->addStep(new ICRO_Step3dGridIntToMultiple2dRasters(*createNewStepInitializeData(NULL)));
    sep->addStep(new ICRO_Step3dGridFloatToMultiple2dRasters(*createNewStepInitializeData(NULL)));
    sep->addStep(new ICRO_Step3dGridDoubleToMultiple2dRasters(*createNewStepInitializeData(NULL)));

    sep->addStep(new ICRO_StepGetOutterSurfaceFrom3dGridBool(*createNewStepInitializeData(NULL)));
    sep->addStep(new ICRO_StepGetOutterSurfaceFrom3dGridDouble(*createNewStepInitializeData(NULL)));
    sep->addStep(new ICRO_StepGetOutterSurfaceFrom3dGridFloat(*createNewStepInitializeData(NULL)));
    sep->addStep(new ICRO_StepGetOutterSurfaceFrom3dGridInt(*createNewStepInitializeData(NULL)));

    sep->addStep(new ICRO_StepGetConnexComponentsFrom3dGridInt(*createNewStepInitializeData(NULL)));
    sep->addStep(new ICRO_StepGetConnectedComponentOnIntGridMultiResolution(*createNewStepInitializeData(NULL)));

    sep->addStep(new ICRO_StepComputeLabeledGridFromStemPositions(*createNewStepInitializeData(NULL)));

    sep->addStep(new ICRO_StepNormalize3dGridInt(*createNewStepInitializeData(NULL)));
    sep->addStep(new ICRO_StepGetDensityGridFromHitGrid(*createNewStepInitializeData(NULL)));

    sep->addStep(new ICRO_StepMultiResSplitComponentsFromStemPositions(*createNewStepInitializeData(NULL)));
    sep->addStep(new ICRO_StepGetGradientNormFrom3dGridInt(*createNewStepInitializeData(NULL)));
    sep->addStep(new ICRO_StepGetSimplexConnectedComponentGrid(*createNewStepInitializeData(NULL)));
    sep->addStep(new ICRO_StepSplitAndMergeConnectedComponents(*createNewStepInitializeData(NULL)));
    sep->addStep(new ICRO_StepGetVoronoiGraphFromComponents(*createNewStepInitializeData(NULL)));

    return true;
}

bool ICRO_PluginManager::loadOpenFileStep()
{
    clearOpenFileStep();

    CT_StepLoadFileSeparator *sep = addNewSeparator(new CT_StepLoadFileSeparator(("TYPE DE FICHIER")));
    //sep->addStep(new NomDeLEtape(*createNewStepInitializeData(NULL)));

    return true;
}

bool ICRO_PluginManager::loadCanBeAddedFirstStep()
{
    clearCanBeAddedFirstStep();

    CT_StepCanBeAddedFirstSeparator *sep = addNewSeparator(new CT_StepCanBeAddedFirstSeparator());
    //sep->addStep(new NomDeLEtape(*createNewStepInitializeData(NULL)));

    return true;
}

bool ICRO_PluginManager::loadActions()
{
    clearActions();

    CT_ActionsSeparator *sep = addNewSeparator(new CT_ActionsSeparator(CT_AbstractAction::TYPE_SELECTION));
    //sep->addAction(new NomDeLAction());

    return true;
}

bool ICRO_PluginManager::loadExporters()
{
    clearExporters();

    CT_StandardExporterSeparator *sep = addNewSeparator(new CT_StandardExporterSeparator("TYPE DE FICHIER"));
    //sep->addExporter(new NomDeLExporter());

    return true;
}

bool ICRO_PluginManager::loadReaders()
{
    clearReaders();

    CT_StandardReaderSeparator *sep = addNewSeparator(new CT_StandardReaderSeparator("TYPE DE FICHIER"));
    //sep->addReader(new NomDuReader());

    return true;
}

