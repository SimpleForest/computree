#ifndef ICRO_STEPNORMALIZE3DGRIDINT_H
#define ICRO_STEPNORMALIZE3DGRIDINT_H

#include "ct_step/abstract/ct_abstractstep.h"

#include "ct_itemdrawable/ct_grid3d.h"

/*!
 * \class ICRO_StepNormalize3dGridInt
 * \ingroup Steps_ICRO
 * \brief <b>Normalise une grille 3D.</b>
 *
 * No detailled description for this step
 *
 * \param _minValue
 *
 */

class ICRO_StepNormalize3dGridInt: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     *
     * Create a new instance of the step
     *
     * \param dataInit Step parameters object
     */
    ICRO_StepNormalize3dGridInt(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     *
     * Return a description of the step function
     */
    QString getStepDescription() const;

    /*! \brief Step detailled description
     *
     * Return a detailled description of the step function
     */
    QString getStepDetailledDescription() const;

    /*! \brief Step URL
     *
     * Return a URL of a wiki for this step
     */
    QString getStepURL() const;

    /*! \brief Step copy
     *
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     *
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     *
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     *
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     *
     * Step computation, using input results, and creating output results
     */
    void compute();

    // Normalise l'image selon les deux normalisations globales et locales (avec coefficient de ponderation)
    // Puis renormalise le tout entre 0 et 255 pour garder une image de int
    void normalizeImage( CT_Grid3D<int>* inputImage,
                         double globalWeight,
                         CT_Grid3D<int>* outputNormalizedImage );

    // Renvoie la normalisation locale (relation de la valeur du pixel avec le mi/max de son voisinage) entre 0 et 255
    int localNormalisationOfPixel( CT_Grid3D<int>* inputImage,
                                   int x,
                                   int y,
                                   int z );

    // Renvoie la normalisation globale (relation de la valeur du pixel avec le mi/max de l'image) entre 0 et 255
    int globalNormalisationOfPixel( CT_Grid3D<int>* inputImage,
                                    int x,
                                    int y,
                                    int z );

private:

    // Step parameters
    double      _w;         /*!< Ponderation entre la normalisation globale et locale */
};

#endif // ICRO_STEPNORMALIZE3DGRIDINT_H
