#include "lsis_stepsnakesinhoughspace.h"

#include <iostream>

//Qt dependencies
#include <QElapsedTimer>
#include <QFileInfo>

//In/Out dependencies
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_attributeslist.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_view/ct_groupbox.h"
#include "ct_itemdrawable/ct_meshmodel.h"
#include "ct_itemdrawable/ct_grid4d_dense.h"
#include "ct_itemdrawable/ct_grid4d_sparse.h"

//Tools dependencies
#include "../pixel/lsis_pixel4ddecrescentvaluesorter.h"
#include "../openactivecontour/lsis_activecontour4dcontinuous.h"

// Alias for indexing models
#define DEFin_rsltHoughSpace "rsltHoughSpace"
#define DEFin_grpHoughSpace "grpHoughSpace"
#define DEFin_itmHoughSpace "itmHoughSpace"
#define DEFin_itmMnt "itmMnt"
#define DEFin_itmName "itmName"
#define DEFin_itmNameField "itmNameField"

// Constructor : initialization of parameters
LSIS_StepSnakesInHoughSpace::LSIS_StepSnakesInHoughSpace(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _minValueForHoughSpaceMaxima = 10;
    _houghSpaceMaximaNeighbourhoodSize = 1;

    _alpha = 1.5;
    _beta = 1.5;
    _gama = 1.3;

    _minHeightForMaximumSearch = 1.1;
    _maxHeightForMaximumSearch = 2;
    _minValueForMaximumSearch = 5;

    _treeHeightMaximum = 30;
    _growCoeff = 1;
    _angleConeRecherche = 20;
    _tailleConeRecherche = 3;
    _tailleConeRechercheCm = 10;
    _seuilSigmaL1 = 0.75;
    _seuilSigmaL4 = 0.125;

    _nIterMaxOptim = 1000;
    _timeStep = 0.001;
    _longueurMin = 1;
    _threshGradMove = 0.001;

    _forkSearchActive = false;
    _nForkLevels = 1;

    _nSnakesMax = 1;
    _optionalMnt = NULL;

    setDebuggable( true );
}

// Step description (tooltip of contextual menu)
QString LSIS_StepSnakesInHoughSpace::getStepDescription() const
{
    return tr("3 - Créer des courbes caractéristiques dans un espace de Hough");
}

// Step detailled description
QString LSIS_StepSnakesInHoughSpace::getStepDetailledDescription() const
{
    return tr("Utilise les valeurs élevées contenues dans un espace de Hough (une "
              "grille 4D) pour faire croitre des courbes caractéristiques. La quatrième "
              "dimension de la grille représentant les rayons des cercles issus "
              "de la transformée de Hough.");
}

// Step URL
QString LSIS_StepSnakesInHoughSpace::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepSnakesInHoughSpace::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepSnakesInHoughSpace(dataInit);
}

void LSIS_StepSnakesInHoughSpace::preWaitForAckIfInDebugMode()
{
    // Si il est possible d'acceder a la GUI (ce qui est le cas uniquement en mode debug)
    if ( getGuiContext() != NULL )
    {
        // On choisi un document dans lequel afficher
        setDocumentForDebugDisplay( getGuiContext()->documentManager()->documents() );

        // Si un document est disponible pour l'affichage de la grille
        if ( _debugStructure.documentHoughSpace != NULL )
        {
            // Si on a un espace de Hough a afficher en debug mode on l'ajoute au document
            if ( _debugStructure.debugHoughSpace != NULL )
            {
                _debugStructure.documentHoughSpace->addItemDrawable( *(_debugStructure.debugHoughSpace) );
            }

            // On ajoute le squelette au meme document que celui qui contient l'espace de Hough
            if ( _debugStructure.debugSkeleton != NULL )
            {
                int nbLines = _debugStructure.debugSkeleton->size();
                for ( int i = 0 ; i < nbLines ; i++ )
                {
                    _debugStructure.documentHoughSpace->addItemDrawable( *(_debugStructure.debugSkeleton->at(i)) );
                }
            }

            // On actualise l'affichage du document
            _debugStructure.documentHoughSpace->redrawGraphics();
        }

        // Si un document est disponible pour l'affichage des cercles
        if ( _debugStructure.documentCircle != NULL )
        {
            // Si on a un espace de Hough a afficher en debug mode on l'ajoute au document
            if ( _debugStructure.debugCircles != NULL )
            {
                int nbCercles = _debugStructure.debugCircles->size();
                for ( int i = 0 ; i < nbCercles ; i++ )
                {
                    _debugStructure.documentCircle->addItemDrawable( *((*(_debugStructure.debugCircles))[i]) );
                }
            }

            // On ajoute le squelette au meme document que celui qui contient l'espace de Hough
            if ( _debugStructure.debugSkeleton != NULL )
            {
                int nbLines = _debugStructure.debugSkeleton->size();
                for ( int i = 0 ; i < nbLines ; i++ )
                {
                    _debugStructure.documentCircle->addItemDrawable( *(_debugStructure.debugSkeleton->at(i)) );
                }
            }

            // On actualise l'affichage du document
            _debugStructure.documentCircle->redrawGraphics();
        }
    }
}

void LSIS_StepSnakesInHoughSpace::postWaitForAckIfInDebugMode()
{
    // Si un document etait disponible pour affichage
    if(_debugStructure.documentHoughSpace != NULL)
    {
        if ( _debugStructure.debugHoughSpace != NULL )
        {
            // On enleve la grille des elements du document
            _debugStructure.documentHoughSpace->removeItemDrawable(*_debugStructure.debugHoughSpace);
        }

        if ( _debugStructure.debugSkeleton != NULL )
        {
            int nbLines = _debugStructure.debugSkeleton->size();
            for ( int i = 0 ; i < nbLines ; i++ )
            {
                _debugStructure.documentHoughSpace->removeItemDrawable( *((*(_debugStructure.debugSkeleton))[i]) );
            }
        }

        // Et on lui demande de redessinner le tout (sans la grille)
        _debugStructure.documentHoughSpace->redrawGraphics();

        _debugStructure.debugHoughSpace = NULL;
        _debugStructure.debugSkeleton = NULL;
    }

    // Si un document etait disponible pour affichage
    if(_debugStructure.documentCircle != NULL)
    {
        if ( _debugStructure.debugCircles != NULL )
        {
            // On enleve les cercles des elements du document
            int nbCercles = _debugStructure.debugCircles->size();
            for ( int i = 0 ; i < nbCercles ; i++ )
            {
                _debugStructure.documentCircle->removeItemDrawable( *((*(_debugStructure.debugCircles))[i]) );
            }
        }

        if ( _debugStructure.debugSkeleton != NULL )
        {
            // On enleve les cercles des elements du document
            int nbLines = _debugStructure.debugSkeleton->size();
            for ( int i = 0 ; i < nbLines ; i++ )
            {
                _debugStructure.documentCircle->removeItemDrawable( *((*(_debugStructure.debugSkeleton))[i]) );
            }
        }

        // Et on lui demande de redessinner le tout (sans la grille)
        _debugStructure.documentCircle->redrawGraphics();

        _debugStructure.documentCircle = NULL;
    }
}

void LSIS_StepSnakesInHoughSpace::setDocumentForDebugDisplay( QList<DocumentInterface *> docList )
{
    // On choisit le premier document disponible pour l'affichage des infos de debug
    // Si aucun n'est disponible on recupere null
    if(docList.isEmpty())
    {
        _debugStructure.documentHoughSpace = NULL;
        _debugStructure.documentCircle = NULL;
    }

    else
    {
        _debugStructure.documentCircle = docList.first();

        if ( docList.size() > 1 )
        {
            _debugStructure.documentHoughSpace = docList.at(1);
        }

        else
        {
            _debugStructure.documentHoughSpace = NULL;
        }
    }
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepSnakesInHoughSpace::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_rsltrsltHoughSpace = createNewInResultModelForCopy(DEFin_rsltHoughSpace, tr("Hough space"));
    resIn_rsltrsltHoughSpace->setZeroOrMoreRootGroup();
    resIn_rsltrsltHoughSpace->addGroupModel("",DEFin_grpHoughSpace,CT_AbstractItemGroup::staticGetType(),tr("Group"));
    resIn_rsltrsltHoughSpace->addItemModel(DEFin_grpHoughSpace, DEFin_itmHoughSpace, LSIS_HoughSpace4D::staticGetType(), tr("Hough space"));

    // Un MNT optionnel
    resIn_rsltrsltHoughSpace->addItemModel(DEFin_grpHoughSpace, DEFin_itmMnt, CT_Image2D<float>::staticGetType(), tr("Dtm (optional)"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
}

// Creation and affiliation of OUT models
void LSIS_StepSnakesInHoughSpace::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities* resultModel = createNewOutResultModelToCopy(DEFin_rsltHoughSpace);

    if (resultModel != NULL)
    {
        resultModel->addGroupModel(DEFin_grpHoughSpace, _grpSnakes_AutoRenameModel, new CT_StandardItemGroup(), tr("Snake group"));
        resultModel->addItemModel (_grpSnakes_AutoRenameModel, _activeContour_AutoRenameModel, new LSIS_ActiveContour4DContinuous<int>(), tr("Tree Data"));
        resultModel->addItemModel (_grpSnakes_AutoRenameModel, _itmMesh_AutoRenameModel, new CT_MeshModel(), tr("MeshLevel0"));
        resultModel->addGroupModel(_grpSnakes_AutoRenameModel, _grpCircles_AutoRenameModel, new CT_StandardItemGroup(), tr("Circle group"));
        resultModel->addItemModel (_grpCircles_AutoRenameModel, _itmCircle_AutoRenameModel, new CT_Circle(), tr("CircleLevel0"));
        //If the search for forks is active, enables more levels of branches
        if(_forkSearchActive){
            for(int i = 0;i<_nForkLevels;i++){
                QString meshName = QString("MFork level%1").arg(i+1);
                QString circleName = QString("CFork level%1").arg(i+1);
                resultModel->addItemModel (_grpSnakes_AutoRenameModel, _itmMeshForks_AutoRenameModel[i], new CT_MeshModel(), tr(qPrintable(meshName)));
                resultModel->addItemModel (_grpCircles_AutoRenameModel, _itmCircleForks_AutoRenameModel[i], new CT_Circle(), tr(qPrintable(circleName)));
            }
        }
    }
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepSnakesInHoughSpace::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addInt(tr("Nombre d'arbre recherché"), "", 1, 100000, _nSnakesMax, tr("Renseignez le nombre d'arbre qu'il existe dans la scène"));
    configDialog->addDouble(tr("Hauteur maximum de l'arbre"), "[m]", 0, 99999999, 3, _treeHeightMaximum);
    configDialog->addDouble(tr("Longueur minimum de la courbe"), "", 0, 100, 2, _longueurMin, 1, tr("Indiquez la longueur minimum d'une courbe. Si une courbe n'a pas la longueur requise elle n'apparaitra pas dans les résultats."));

    configDialog->addTitle(tr("<html><b>Initialisation de la recherche</b></html>"));
    configDialog->addDouble(tr("Hauteur minimum des graines"), "[m]", 1e-06, 100, 6, _minHeightForMaximumSearch, 1, tr("Définissez la hauteur minimum à partir du sol où la recherche à le droit de débuter"));
    configDialog->addDouble(tr("Hauteur maximum des graines"), "[m]", 1e-06, 100, 6, _maxHeightForMaximumSearch, 1, tr("Définissez la hauteur maximum à partir du sol où la recherche à le droit de débuter"));

    CT_GroupBox* gp = configDialog->addNewGroupBox(tr("Mode avancée"));

    gp->addTitle(tr("<html><b>Energie des contours actifs</b></html>"));
    gp->addDouble(tr("Alpha"), "", 1e-06, 100, 6, _alpha, 1, tr("Joue sur l'élasticité de la courbe. Plus \"Alpha\" est petit et plus la courbe est élastique."));
    gp->addDouble(tr("Beta"), "", 1e-06, 100, 6, _beta, 1, tr("Joue sur la flexion de la courbe. Si \"Beta\" est grand la courbe sera plus lisse et si il est petit il permet à la courbe de former des vagues."));
    gp->addDouble(tr("Gamma"), "", 1e-06, 100, 6, _gama, 1, tr("Joue sur l'attraction des maxima. Plus \"Gamma\" est fort et plus la courbe est attirée par les forts maxima."));

    gp->addTitle(tr("<html><b>Initialisation de la recherche des maxima</b></html>"));
    gp->addInt(tr("Nombre de points minimum d'un cercle"), "", 1, 100000, _minValueForHoughSpaceMaxima, tr("Indiquez le nombre de points minimum qu'un cercle doit avoir pour être considéré comme maxima par l'algorithme de recherche."));
    gp->addInt(tr("Taille de fenêtre d'analyse des maxima locaux"), "", 1, 100000, _houghSpaceMaximaNeighbourhoodSize, tr("Lors de l'initialisation l'algorithme "
                                                                                                                                    "recherche tous les maxima locaux. Si deux "
                                                                                                                                    "maxima sont contenus dans la fenêtre d'analyse "
                                                                                                                                    "le plus grand des deux (ou si ils sont égaux : le premier trouvé) "
                                                                                                                                    "est gardé en mémoire. La valeur indiquée représente le nombre "
                                                                                                                                    "de cellules. Plus cette valeur est grande et plus les maxima seront "
                                                                                                                                    "rassemblés."));

    gp->addTitle(tr("<html><b>Croissance des contours actifs</b></html>"));
    gp->addDouble(tr("Vitesse de croissance"), "", 1e-06, 100, 6, _growCoeff, 1, tr("Choix entre vitesse et précision. Augmenter la valeur si vous souhaitez que le traitement aille plus vite cependant il sera moins précis."));
    gp->addDouble(tr("Angle du cone de recherche"), tr("[°]"), 5, 80, 4, _angleConeRecherche, 1);
//    gp->addInt("Taille Cone Recherche", "", 1, 100, _tailleConeRecherche);
    gp->addDouble(tr("Longueur du cone de recherche"), "[cm]", 0.0001, 99999, 4, _tailleConeRechercheCm, 1);
    gp->addDouble(tr("Seuil de fiabilité"), "", 0, 1, 4, _seuilSigmaL1, 1, tr("Représente la fiabilité de la croissance entre 0 et 1. Si le calcul de la fiabilité est inférieur à cette valeur "
                                                                                        "l'algorithme arrête la croissance de la courbe courante. La fiabilité est par exemple réduite lors d'un départ "
                                                                                        "de branchaison."));
//    gp->addDouble("SeuilSigmaL4", "", 0, 0.33, 4, _seuilSigmaL4, 1);

    gp->addTitle(tr("<html><b>Optimisation</b></html>"));
    gp->addInt(tr("Nombre de pas d'optimisation maximum"), "", 0, 99999999, _nIterMaxOptim, tr("Indiquez une valeur forte pour que l'algorithme est assez de temps pour optimiser "
                                                                                              "la forme de la courbe. Evitez une valeur trop forte afin d'arrêtez l'optimisation "
                                                                                               "dans un temps raisonable."));
    gp->addDouble(tr("Vitesse d'optimisation"), "", 1e-06, 100, 6, _timeStep, 1, tr("Choix entre vitesse et précision. Augmenter la valeur si vous souhaitez que le traitement aille plus vite cependant il sera moins précis."));
    gp->addDouble(tr("Seuil de mouvement"), "", 0.0000001, 1, 7, _threshGradMove, 1, tr("Représente la différence entre deux optimisations de la courbe. Si cette différence est inférieur au seuil indiqué l'algorithme arrête l'optimisation de la courbe courante."));

    gp->addTitle("<html><b>Recherche de fourches</b></html>");
    gp->addBool("", "", tr("Active"), _forkSearchActive, tr("Active ou non la recherche de fourches"));
    gp->addInt(tr("Nombre de niveau de fourches par arbre"), "", 1, 3, _nForkLevels, tr("Représente le nombre de niveau de fourches à rechercher par arbre. "
                                                                                        "Un niveau plus élevé peut rendre le calcul incroyablement long dépendamment du nombre de fourches"
                                                                                        "et des fourches potentielles à différents niveaux."));
}

void LSIS_StepSnakesInHoughSpace::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    _snakesResult = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_grpHoughSpace(_snakesResult, this, DEFin_grpHoughSpace);
    while (itIn_grpHoughSpace.hasNext() && !isStopped())
    {
        CT_StandardItemGroup* grpIn_grpHoughSpace = (CT_StandardItemGroup*) itIn_grpHoughSpace.next();
        
        const LSIS_HoughSpace4D* itemIn_itmHoughSpace = (LSIS_HoughSpace4D*)grpIn_grpHoughSpace->firstItemByINModelName(this, DEFin_itmHoughSpace);

        _optionalMnt = (CT_Image2D<float>*)grpIn_grpHoughSpace->firstItemByINModelName(this, DEFin_itmMnt);

        if (itemIn_itmHoughSpace != NULL)
        {
            compute( itemIn_itmHoughSpace, grpIn_grpHoughSpace);
        }
    }
}

void LSIS_StepSnakesInHoughSpace::compute( const LSIS_HoughSpace4D *houghSpace, CT_StandardItemGroup* rootGrp)
{
    QElapsedTimer timer;

    float energyImageGlobalWeight = 1;
    int currentRecursionLevel = 0;

    // On recupere les maximas de l'espace de Hough
    QVector< LSIS_Pixel4D >* maximas;
    if( _optionalMnt != NULL )
    {
       qDebug() << "Debut de recherche des maximas avec MNT";
        timer.start();
        maximas = houghSpace->getLocalMaximasInHeighRangeOverDTM( _optionalMnt,
                                                                  _minHeightForMaximumSearch,
                                                                  _maxHeightForMaximumSearch,
                                                                  _minValueForHoughSpaceMaxima,
                                                                  _houghSpaceMaximaNeighbourhoodSize );
    }
    else
    {
        timer.start();
        maximas = houghSpace->getLocalMaximasWithinHeightRange(_minHeightForMaximumSearch,
                                                               _maxHeightForMaximumSearch,
                                                               _minValueForHoughSpaceMaxima,
                                                               _houghSpaceMaximaNeighbourhoodSize );
    }

    // WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING
    // Si il n'y a pas de maximas alors on sors de l'etape
    // Un tel cas est quand meme plus que bizarre et ne devrait jamais arriver
    // WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING
    if( maximas->isEmpty() )
    {
        return;
    }

    // On les trie par ordre decroissant
    timer.start();
    maximas = sortPixelsByValueDecrescentOrder( maximas, houghSpace );

    // Pour chaque maxima on initialise un contour que l'on stocke dans un tableau
    QVector< LSIS_ActiveContour4DContinuous<int>* >* contours = new QVector< LSIS_ActiveContour4DContinuous<int>* >();
    QVector< LSIS_ActiveContour4DContinuous<int>* >* contourCurrentSnake = new QVector< LSIS_ActiveContour4DContinuous<int>* >();

    // Cree une image de repulsion
    timer.start();
    CT_Grid4D_Sparse<bool> repulseImage( NULL, NULL,
                                         0, houghSpace->minX(), houghSpace->minY(), houghSpace->minZ(),
                                         0.1, houghSpace->maxX(), houghSpace->maxY(), houghSpace->maxZ(),
                                         1, houghSpace->xres(), houghSpace->yres(), houghSpace->zres(),
                                         false, false);

    const int nIterMaxGrow = qMax(_treeHeightMaximum / houghSpace->zres(), 1.0);

    for( int cpt = 0 ; cpt < maximas->size() && contours->size() < _nSnakesMax && !isStopped() ; cpt++ )
    {
        float average = (cpt / (float)maximas->size())*0.5 + ( contours->size() / (float)_nSnakesMax )*0.5;
        setProgress( average * 100 );

        // On recupere une nouvelle graine de contours actif (un maxima de l'espace de hough)
        // Le contours ne doit croitre que si il n'est pas repousse par un contours deja en place
        LSIS_Pixel4D currMax = maximas->at(cpt);
        LSIS_Pixel4D currMaxRepulse = maximas->at(cpt);
        currMaxRepulse.w() = 0;

        // Croissance du contours actif
        _tailleConeRecherche = ceil( ( _tailleConeRechercheCm / 100.0 ) / houghSpace->xres() );

        if( currMaxRepulse.valueIn( repulseImage ) == false )
        {
            timer.start();
            // Initialise un contours actif a partir du maxima courant
            LSIS_ActiveContour4DContinuous<int>* currentSnake = new LSIS_ActiveContour4DContinuous<int>( _activeContour_AutoRenameModel.completeName(), _snakesResult, houghSpace, &repulseImage, currMax, 20 );

            currentSnake->setOptionalMnt( _optionalMnt );
            currentSnake->setIntensityMin( _minValueForHoughSpaceMaxima );

            // On lui donne l'etape courante pour que le snake puisse la mettre en pause
            currentSnake->setStep( this );

            timer.start();
            currentSnake->growv2( &repulseImage,
                                  nIterMaxGrow,
                                  _growCoeff,
                                  _angleConeRecherche,
                                  _tailleConeRecherche,
                                  _seuilSigmaL1 );

            // Filtrage par longeur du contours
            if( currentSnake->length3D() < _longueurMin )
            {
                delete currentSnake;
            }
            else
            { 
                // Effectue un resampling pour diminuer la quantite de points dans le snake
                timer.start();
                currentSnake->resample( 0.1 );

                currentSnake->sortPointsbyHeight();
                // Ajoute le snake a la liste des snakes resultats
                currentSnake->_id = LSIS_ActiveContour4DContinuous<int>::getUniqueId();
                currentSnake->_parent = NULL;
                currentSnake->_ordre = 0;
                currentSnake->_children = QVector< LSIS_ActiveContour4DContinuous<int>* >();
                contours->push_back( currentSnake );

                //Using this Vector for fork recursion and emptying it after fork recursion on each snake
                contourCurrentSnake->push_back(currentSnake);

                // On marque la repulsion du contour
                timer.start();
                currentSnake->markRepulsion(&repulseImage, 0.5 );

                // On relache le contour actif pour optimiser l'energie
                timer.start();
                currentSnake->relax( _nIterMaxOptim,
                                     _alpha, _beta, _gama,
                                     energyImageGlobalWeight,
                                     _timeStep,
                                     _threshGradMove ); // Equivalent de 1 millimetre par defaut

                if(_forkSearchActive)
                {
                    recursiveForkBuilding(houghSpace,
                                          rootGrp,
                                          currentRecursionLevel,
                                          _nForkLevels,
                                          contourCurrentSnake,
                                          &repulseImage,
                                          _snakesResult);
                }

                //Emptying vector containing the snake to prevent recursion on snakes that already have their forks drawn
                contourCurrentSnake->clear();

                // On transforme le snake en mesh que l'on attache au resultat
                timer.start();
                CT_MeshModel* currSnakeMesh = currentSnake->toMeshModel( _itmMesh_AutoRenameModel.completeName(), _snakesResult, 5, 40 );
                CT_StandardItemGroup* grpMeshSnake = new CT_StandardItemGroup(_grpSnakes_AutoRenameModel.completeName(), _snakesResult);
                rootGrp->addGroup( grpMeshSnake );
                grpMeshSnake->addItemDrawable( currSnakeMesh );
                grpMeshSnake->addItemDrawable(currentSnake);

                // On transforme chaque snake en tuboide qu'on attache au resultat
                timer.start();
                QVector<CT_Circle*> tuboid = currentSnake->toCircles( 5,
                                                                      LSIS_ActiveContour4DContinuous<int>::GLOBAL_SCORE,
                                                                      _itmCircle_AutoRenameModel.completeName(),
                                                                      _snakesResult );
                timer.start();
                foreach( CT_Circle* currCircle, tuboid )
                {
                    CT_StandardItemGroup* grpCircles= new CT_StandardItemGroup(_grpCircles_AutoRenameModel.completeName(), _snakesResult);
                    grpMeshSnake->addGroup(grpCircles);
                    grpCircles->addItemDrawable(currCircle);
                }

                // On sauvegarde le contours actif dans un fichier csv
                timer.start();
            }//If the snake is long enough
        }
    }//End of for each snake

    delete contours;
    delete contourCurrentSnake;
}

//Recursive method that recalls itself until the end of the fork recursion.
void LSIS_StepSnakesInHoughSpace::recursiveForkBuilding(const LSIS_HoughSpace4D *houghSpace,
                                                        CT_StandardItemGroup* rootGrp,
                                                        int currentRecursionLevel,
                                                        int maxRecursionLevel,
                                                        QVector< LSIS_ActiveContour4DContinuous<int>* >* contours,
                                                        CT_Grid4D_Sparse<bool> *repulseImage,
                                                        const CT_AbstractResult *result)
{
    //Used to pass over the Vector to the next level of recursion
    QVector< LSIS_ActiveContour4DContinuous<int>* >* contoursForks = new QVector< LSIS_ActiveContour4DContinuous<int>* >();

    const int nIterMaxGrow = qMax(_treeHeightMaximum / houghSpace->zres(), 1.0);
    float energyImageGlobalWeight = 1;

    //Ends the fork recursion
    if(currentRecursionLevel >= maxRecursionLevel)
    {
        return;
    }

    //For each contour
    for(int j=0;j < contours->size();j++)
    {
        LSIS_ActiveContour4DContinuous<int>* curContour = contours->at(j);
//        curContour->sortPointsbyHeight();

        //Iterate through every point in the snake to determine possible forks
        for(int i = 0 ; i < curContour->npoints() && !isStopped() ; i++ )
        {
            float lengthForkSearch;
            //LSIS_Point4DFloat snakePoint;
            LSIS_Point4DFloat snakePoint = curContour->pointAt(i);
            LSIS_Pixel4D snakePixel = curContour->pixelAt(i);
            lengthForkSearch = snakePoint.w()+houghSpace->maxW();
            float lenghthHoughSpace = ceil(lengthForkSearch/houghSpace->xres());

            // Calcule la bbox correspondant a un cube centre autour de l'extremite du snake et de taille lenghthHoughSpace
            LSIS_Pixel4D forkBBoxBot = snakePixel - LSIS_Pixel4D( 0, lenghthHoughSpace, lenghthHoughSpace, lenghthHoughSpace);
            LSIS_Pixel4D forkBBoxTop = snakePixel + LSIS_Pixel4D( 0, lenghthHoughSpace, lenghthHoughSpace, lenghthHoughSpace);

            QVector< LSIS_Pixel4D >* maximasFork;
            maximasFork = houghSpace->getLocalMaximasInBBox(forkBBoxBot,
                                                            forkBBoxTop,
                                                            _minValueForMaximumSearch,
                                                            _houghSpaceMaximaNeighbourhoodSize);

            if( !maximasFork->isEmpty() )
            {
                maximasFork = sortPixelsByValueDecrescentOrder( maximasFork, houghSpace );
                for( int ii = 0 ; ii < maximasFork->size() && !isStopped() ; ii++ )
                {
                    // On recupere une nouvelle graine de contours actif (un maxima de l'espace de hough)
                    // Le contours ne doit croitre que si il n'est pas repousse par un contours deja en place
                    LSIS_Pixel4D currMaxFork = maximasFork->at(ii);
                    LSIS_Pixel4D currMaxRepulseFork = maximasFork->at(ii);
                    currMaxRepulseFork.w() = 0;

                    if( currMaxRepulseFork.valueIn( repulseImage ) == false )
                    {
                        // Initialise un contours actif a partir du maxima courant
                        LSIS_ActiveContour4DContinuous<int>* currentFork = new LSIS_ActiveContour4DContinuous<int>(houghSpace, repulseImage, currMaxFork, 20 );
                        currentFork->setIsFork(true); //Bool to help determine if this snake is a fork of a snake or not.
                        currentFork->setOptionalMnt( _optionalMnt );
                        currentFork->setIntensityMin( _minValueForHoughSpaceMaxima );

                        // On lui donne l'etape courante pour que le snake puisse la mettre en pause
                        currentFork->setStep( this );

                        currentFork->growv2( repulseImage,
                                              nIterMaxGrow,
                                              _growCoeff,
                                              _angleConeRecherche,
                                              _tailleConeRecherche,
                                              _seuilSigmaL1 );
                        // Filtrage par longeur du contours
                        if( currentFork->length3D() < _longueurMin)
                        {
                            delete currentFork;
                        }
                        else
                        {
                            //Finds the starting point of the fork to the current snake
                            LSIS_Point4DFloat headFork = currentFork->head();
                            LSIS_Point4DFloat backFork = currentFork->back();
                            //Adds that point to the forks of the current snake for exportation purposes.
                            if(headFork.z() >= backFork.z())
                            {
                                curContour->addFork(backFork);
                            }
                            else
                            {
                                curContour->addFork(headFork);
                            }
                            // Effectue un resampling pour diminuer la quantite de points dans le snake
                            currentFork->resample( 0.1 );

                            // Construit les relations de parents/enfants
                            currentFork->_id = LSIS_ActiveContour4DContinuous<int>::getUniqueId();
                            currentFork->_parent = curContour;
                            currentFork->_ordre = curContour->_ordre + 1;
                            currentFork->_children = QVector< LSIS_ActiveContour4DContinuous<int>* >();
                            curContour->_children.push_back( currentFork );

                            // Ajoute le snake a la liste des snakes resultats
                            contoursForks->push_back( currentFork );

                            // On marque la repulsion du contour
                            currentFork->markRepulsion(repulseImage, 0.5 );

                            // On relache le contour actif pour optimiser l'energie
                            currentFork->relax( _nIterMaxOptim,
                                                 _alpha, _beta, _gama,
                                                 energyImageGlobalWeight,
                                                 _timeStep,
                                                 _threshGradMove ); // Equivalent de 1 millimetre par defaut
                            // On transforme le snake fork en mesh que l'on attache au resultat
                            CT_MeshModel* currForkMesh = currentFork->toMeshModel( _itmMeshForks_AutoRenameModel[currentRecursionLevel].completeName(), _snakesResult, 5, 40 );
                            CT_StandardItemGroup* grpMeshFork = new CT_StandardItemGroup(_grpSnakes_AutoRenameModel.completeName(), _snakesResult);
                            rootGrp->addGroup( grpMeshFork );
                            grpMeshFork->addItemDrawable( currForkMesh );

                            // On transforme chaque snake en tuboide qu'on attache au resultat
                            QVector<CT_Circle*> tuboid = currentFork->toCircles( 5,
                                                                                  LSIS_ActiveContour4DContinuous<int>::GLOBAL_SCORE,
                                                                                  _itmCircleForks_AutoRenameModel[currentRecursionLevel].completeName(),
                                                                                  _snakesResult );
                            foreach( CT_Circle* currCircle, tuboid )
                            {
                                CT_StandardItemGroup* grpCircles= new CT_StandardItemGroup(_grpCircles_AutoRenameModel.completeName(), _snakesResult);
                                grpMeshFork->addGroup(grpCircles);
                                grpCircles->addItemDrawable(currCircle);
                            }
                        }
                    }//End loop if maxima is not repulsed
                }//End loop for maxima iteration
            }//End loop for maximas for snake point not empty
        }//End loop for possible forks for the snake
    }
    currentRecursionLevel++;
    recursiveForkBuilding(houghSpace,rootGrp,currentRecursionLevel,maxRecursionLevel,contoursForks,repulseImage,_snakesResult);
}
