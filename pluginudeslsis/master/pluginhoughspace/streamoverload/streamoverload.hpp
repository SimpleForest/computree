/************************************************************************************
* Filename :  streamoverload.hpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef STREAMOVERLOAD_HPP
#define STREAMOVERLOAD_HPP

#include "streamoverload.h"

#include "../pixel/lsis_pixel4d.h"
#include "../histogram/lsis_histogram1d.h"

template <typename DataT>
QDebug operator<< (QDebug debug, Eigen::DenseBase<DataT> const & m )
{
    int cols = m.cols();
    int rows = m.rows();

    for( int i = 0; i < rows; i++ )
    {
        debug << "(";
        for( int j = 0; j < cols; j++ )
        {
            debug << m(i, j);

            if( j != cols-1 )
            {
                debug << ",";
            }
        }
        debug << ")\n";
    }

    return debug;
}

template <typename DataT>
QTextStream operator<< (QTextStream stream, Eigen::DenseBase<DataT> const & m )
{
    int cols = m.cols();
    int rows = m.rows();

    for( int i = 0; i < rows; i++ )
    {
        stream << "(";
        for( int j = 0; j < cols; j++ )
        {
            stream << m(i, j);

            if( j != cols-1 )
            {
                stream << ",";
            }
        }
        stream << ")\n";
    }

    return stream;
}

template< typename DataT >
QDebug operator<< (QDebug stream, const CT_Grid4D<DataT>& grid )
{
    stream << "Bounding box : (" << grid.minW() << grid.minX() << grid.minY() << grid.minZ() << ") (" << grid.maxW() << grid.maxX() << grid.maxY() << grid.maxZ() << ")" <<"\n";
    stream << "Resolution : " << grid.wres() << grid.xres() << grid.yres() << grid.zres() <<"\n";
    stream << "Dimensions : " << grid.wdim() << grid.xdim() << grid.ydim() << grid.zdim() <<"\n";
    stream << "Valeurs min et max dans la grille " << grid.dataMin() << grid.dataMax() <<"\n";
    stream << "NA value " << grid.NA();
    return stream;
}

template< typename DataT >
std::ostream& operator<< (std::ostream& stream, const CT_Grid4D<DataT>& grid )
{
    stream << "Bounding box : (" << grid.minW() << grid.minX() << grid.minY() << grid.minZ() << ") (" << grid.maxW() << grid.maxX() << grid.maxY() << grid.maxZ() << ")" <<"\n";
    stream << "Resolution : " << grid.wres() << " " << grid.xres() << " " << grid.yres() << " " << grid.zres() <<"\n";
    stream << "Dimensions : " << grid.wdim() << " " << grid.xdim() << " " << grid.ydim() << " " << grid.zdim() <<"\n";
    stream << "Valeurs min et max dans la grille " << grid.dataMin() << " " << grid.dataMax() <<"\n";
    stream << "NA value " << grid.NA();
    return stream;
}

template< typename DataImage >
std::ostream& operator<< (std::ostream stream, LSIS_ActiveContour4DContinuous<DataImage> const & c )
{
    int npoints = c.npoints();
    LSIS_Point4DFloat   currPoint;
    LSIS_Pixel4D        currPix;

    stream << "PointCartesien <-> PixelSurImage\n";
    for( int i = 0 ; i < npoints ; i++ )
    {
        currPoint = c.pointAt(i);
        currPix.setFromCartesian( currPoint, c.image() );
        stream << currPoint << " <-> " << currPix << "\n";
    }

    return stream;
}

template< typename DataImage >
QDebug operator<< (QDebug debug, LSIS_ActiveContour4DContinuous<DataImage> const & c )
{
    int npoints = c.npoints();
    LSIS_Point4DFloat   currPoint;
    LSIS_Pixel4D        currPix;

    debug << "PointCartesien <-> PixelSurImage\n";
    for( int i = 0 ; i < npoints ; i++ )
    {
        currPoint = c.pointAt(i);
        currPix.setFromCartesian( currPoint, c.image() );
        debug << currPoint << " <-> " << currPix << "\n";
    }

    return debug;
}

template <typename DataT>
QDebug operator<< (QDebug stream, LSIS_Histogram1D<DataT> const & h )
{
    float startBin;
    float endBin;

    stream << "Min = " << h.getMin() << " Max = " << h.getMax() << " Res = " << h.getRes()
           << " NBins = " << h.getNBins() << " NEntries = " << h.getNEntries() << "\n";

    int nBins = h.getNBins();
    DataT min = h.getMin();
    float res = h.getRes();
    for ( int i = 0 ; i < nBins ; i++ )
    {
        startBin = min + (i*res);
        endBin = startBin + res;
        stream << "Bin " << i << "[" << startBin << "," << endBin << "] a une valeur de " << h.getValueAtBin(i) << "\n";
    }

    return stream;
}

template <typename DataT>
std::ostream& operator<< (std::ostream& stream, LSIS_Histogram1D<DataT> const & h )
{
//    float startBin;
//    float endBin;

//    stream << "Min = " << h.getMin() << " Max = " << h.getMax() << " Res = " << h.getRes()
//           << " NBins = " << h.getNBins() << " NEntries = " << h.getNEntries() << "\n";

    int nBins = h.getNBins();
//    DataT min = h.getMin();
//    float res = h.getRes();
    for ( int i = 0 ; i < nBins ; i++ )
    {
//        startBin = min + (i*res);
//        endBin = startBin + res;
//        stream << "Bin " << i << "[" << startBin << "," << endBin << "] a une valeur de " << h.getValueAtBin(i) << "\n";
        stream << h.getValueAtBin(i) << " ";
    }

    return stream;
}

#endif // STREAMOVERLOAD_HPP
