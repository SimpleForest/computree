#include "norm_octree.h"

//// Templates implementations are to be found in norm_octree.hpp

#include "norm_octreettools.h"

#include "ct_iterator/ct_pointiterator.h"
#include "ct_accessor/ct_pointaccessor.h"
#include "ct_cloudindex/registered/abstract/ct_abstractcloudindexregisteredt.h"

#include "tools/norm_interpolation.h"
#include "ct_colorcloud/ct_colorcloudstdvector.h"

#include <QDebug>


const NORM_OctreeDrawManager NORM_Octree::OCTREE_DRAW_MANAGER = NORM_OctreeDrawManager();


NORM_Octree::NORM_Octree()
    : CT_AbstractItemDrawableWithoutPointCloud( NULL, NULL ),
      _root(NULL),
      _maxDepth(0),
      _powMaxDepth(0),
      _minNodeSize( std::numeric_limits<float>::max() ),
      _minNbPoints( std::numeric_limits<size_t>::max() ),
      _inputIndexCloud(NULL),
      _indexCloud(NULL)
{
}

NORM_Octree::NORM_Octree(float minNodeSize,
                         size_t minNbPoints,
                         CT_Point bot,
                         CT_Point top,
                         CT_PCIR inputIndexCloud)
    : CT_AbstractItemDrawableWithoutPointCloud( NULL, NULL ),
      _root( NULL ),
      _maxDepth(0),
      _minNodeSize( minNodeSize ),
      _minNbPoints( minNbPoints ),
      _bot( bot ),
      _top( top ),
      _inputBot( bot ),
      _inputTop( top ),
      _inputIndexCloud( inputIndexCloud )
{
    // On va rechercher le cube englobant au lieu de la boite englobante pour calculer la profondeur maximum
    NORM_OctreeTools::getBoundingCubeFromBoundingBox( bot, top, _bot, _top );
    float width = _top(0) - _bot(0);

    if( width <= minNodeSize )
    {
        _maxDepth = 0;
    }

    else
    {
        _maxDepth = ceil( log2( width / _minNodeSize ) );
    }

    // Computes th powMaxDepth value used for querry purposes
    _powMaxDepth = pow( 2, _maxDepth );

    // On cree le nuage d'indices sur lesquel on va effectuer les permutations
    CT_PointCloudIndexVector* indexCloudVector = new CT_PointCloudIndexVector();
    size_t nbPts = inputIndexCloud->size();
    for( size_t i = 0 ; i < nbPts ; i++ )
    {
        indexCloudVector->addIndex( inputIndexCloud->indexAt(i) );
    }
    // On enregistre le nuage d'indices que l'on a permute
    // On pourra acceder aux points a partir de celui ci maintenant
    // Ca facilitera le parcours des poitns des cellules
    _indexCloud = PS_REPOSITORY->registerPointCloudIndex( indexCloudVector );

    setBoundingBox( _bot(0), _bot(1), _bot(2), _top(0), _top(1), _top(2) );
}

NORM_Octree::NORM_Octree(float minNodeSize,
                         size_t minNbPoints,
                         CT_Point bot,
                         CT_Point top,
                         CT_PCIR inputIndexCloud,
                         const CT_OutAbstractSingularItemModel *model,
                         const CT_AbstractResult *result)
    : CT_AbstractItemDrawableWithoutPointCloud( model, result ),
      _root( NULL ),
      _maxDepth(0),
      _minNodeSize( minNodeSize ),
      _minNbPoints( minNbPoints ),
      _bot( bot ),
      _top( top ),
      _inputBot( bot ),
      _inputTop( top ),
      _inputIndexCloud( inputIndexCloud )
{
    // On va rechercher le cube englobant au lieu de la boite englobante pour calculer la profondeur maximum
    NORM_OctreeTools::getBoundingCubeFromBoundingBox( bot, top, _bot, _top );
    float width = _top(0) - _bot(0);

    if( width <= minNodeSize )
    {
        _maxDepth = 0;
    }

    else
    {
        _maxDepth = ceil( log2( width / _minNodeSize ) );
    }

    // Computes th powMaxDepth value used for querry purposes
    _powMaxDepth = pow( 2, _maxDepth );

    // On cree le nuage d'indices sur lesquel on va effectuer les permutations
    CT_PointCloudIndexVector* indexCloudVector = new CT_PointCloudIndexVector();
    size_t nbPts = inputIndexCloud->size();
    for( size_t i = 0 ; i < nbPts ; i++ )
    {
        indexCloudVector->addIndex( inputIndexCloud->indexAt(i) );
    }
    // On enregistre le nuage d'indices que l'on a permute
    // On pourra acceder aux points a partir de celui ci maintenant
    // Ca facilitera le parcours des poitns des cellules
    _indexCloud = PS_REPOSITORY->registerPointCloudIndex( indexCloudVector );

    setBoundingBox( _bot(0), _bot(1), _bot(2), _top(0), _top(1), _top(2) );
}

NORM_Octree::NORM_Octree( float minNodeSize,
                          size_t minNbPoints,
                          CT_Point bot,
                          CT_Point top,
                          CT_PCIR inputIndexCloud,
                          const QString& modelName,
                          const CT_AbstractResult *result)
    : CT_AbstractItemDrawableWithoutPointCloud( modelName, result ),
      _root(NULL),
      _maxDepth(0),
      _minNodeSize( minNodeSize ),
      _minNbPoints( minNbPoints ),
      _bot( bot ),
      _top( top ),
      _inputBot( bot ),
      _inputTop( top ),
      _inputIndexCloud( inputIndexCloud )
{
    // On va rechercher le cube englobant au lieu de la boite englobante pour calculer la profondeur maximum
    NORM_OctreeTools::getBoundingCubeFromBoundingBox( bot, top, _bot, _top );
    float width = _top(0) - _bot(0);

    if( width <= minNodeSize )
    {
        _maxDepth = 0;
    }

    else
    {
        _maxDepth = ceil( log2( width / _minNodeSize ) );
    }

    // Computes th powMaxDepth value used for querry purposes
    _powMaxDepth = pow( 2, _maxDepth );

    // On cree le nuage d'indices sur lesquel on va effectuer les permutations
    CT_PointCloudIndexVector* indexCloudVector = new CT_PointCloudIndexVector();
    size_t nbPts = inputIndexCloud->size();
    for( size_t i = 0 ; i < nbPts ; i++ )
    {
        indexCloudVector->addIndex( inputIndexCloud->indexAt(i) );
    }
    // On enregistre le nuage d'indices que l'on a permute
    // On pourra acceder aux points a partir de celui ci maintenant
    // Ca facilitera le parcours des poitns des cellules
    _indexCloud = PS_REPOSITORY->registerPointCloudIndex( indexCloudVector );

    setBoundingBox( _bot(0), _bot(1), _bot(2), _top(0), _top(1), _top(2) );
}

void NORM_Octree::createOctree()
{
    initRoot();

    // Et on lance la creation de l'octree par recursio
    _root->createOctreeRecursive();
}

NORM_AbstractNode* NORM_Octree::nodeContainingPoint(CT_Point &p) const
{
    // Transform the point p into [0,1] accordingly to the bounding cube
    CT_Point interpolatedP;
    NORM_Tools::NORM_Interpolation::linearInterpolation( _bot, _top,
                                                         createCtPoint(0,0,0), createCtPoint(1,1,1),
                                                         p,
                                                         interpolatedP );

    // Calculates the code of the cell containing the querry point
    size_t codex = (size_t) (interpolatedP(0) * _powMaxDepth );
    size_t codey = (size_t) (interpolatedP(1) * _powMaxDepth );
    size_t codez = (size_t) (interpolatedP(2) * _powMaxDepth );

    // Traversing the octree to return the cell corresponding to the given code
    return nodeAtCode( codex, codey, codez );
}

NORM_AbstractNode* NORM_Octree::nodeAtCode(size_t codex, size_t codey, size_t codez) const
{
    return _root->leafNodeAtCodeRecursive( codex, codey, codez );
}

NORM_AbstractNode* NORM_Octree::nodeAtCodeAndMaxDepth(size_t codex, size_t codey, size_t codez, size_t maxDepth) const
{
    return _root->leafNodeAtCodeRecursive( codex, codey, codez, maxDepth );
}

void NORM_Octree::neighboursNodeAtSameLevelOrAbove(NORM_AbstractNode *querryNode, QVector<NORM_AbstractNode *> &outNeighbours, bool insertNullNodes )
{
    // Clear vector
    outNeighbours.clear();

    // Computes the 26 (or less if some are outside the octree bounding cube) centers of the neighbours node of same level in 3D
    QVector<CT_Point> neighboursCenter;
    querryNode->getCentersOfNeighboursAtSameLevel( neighboursCenter );

    // For each center computed, add the corresponding node
    foreach ( CT_Point neiCenter, neighboursCenter )
    {
        // If we want to insert each node (even the one outside the bounding cube)
        // This is used later on to find leaves neighbour
        if ( insertNullNodes )
        {
            if ( std::isnan( neiCenter(0) ) )
            {
                outNeighbours.push_back( NULL );
            }

            else
            {
                outNeighbours.push_back( nodeContainingPoint( neiCenter ) );
            }
        }

        // If it was inside the bounding cube
        else if ( !std::isnan(neiCenter(0)) )
        {
            // Add NULL to the result
            outNeighbours.push_back( nodeContainingPoint( neiCenter ) );
        }
    }
}

void NORM_Octree::neighboursLeafNodes( NORM_AbstractNode* querryNode,
                                              QVector<NORM_AbstractNode*>& outNeighbours )
{
    // Clear vector
    outNeighbours.clear();

    // Computes the 26 (or less if some are outside the octree bounding cube) centers of the neighbours node of same level in 3D
    QVector< NORM_AbstractNode* > neighboursSameLevel;
    neighboursNodeAtSameLevelOrAbove( querryNode, neighboursSameLevel, true );

    // The 26 neighbours have been added in a sorted way : z first then y then x
    // i.e.
    // Neighbour[0] => -1 -1 -1
    // Neighbour[1] => -1 -1 0
    // Neighbour[2] => -1 -1 1
    // Neighbour[3] => -1 0 -1
    // Neighbour[4] => -1 0 0
    // Neighbour[5] => -1 0 1
    // Neighbour[6] => -1 1 -1
    // etc.
    // The traversing direction in such cases is the opposite to that one
    // i.e.
    // Traverse[0] => 1 1 1
    // Traverse[1] => 1 1 0
    // Traverse[2] => 1 1 -1
    // Traverse[3] => 1 0 1
    // Traverse[4] => 1 0 0
    // Traverse[5] => 1 0 -1
    // Traverse[6] => 1 -1 1
    // etc.

    // Same loops than in neighboursNodeAtSameLevelOrAbove in order to compute the neighbours traversal directions
    NORM_AbstractNode* nei;
    size_t nNei = 0;
    for( int i = -1 ; i <= 1 ; i++ )
    {
        for( int j = -1 ; j <= 1 ; j++ )
        {
            for ( int k = -1 ; k <= 1 ;k++ )
            {
                // Do not take into account the node itself
                if( (i != 0) | (j != 0) | (k != 0) )
                {
                    // Get the neighbour node
                    nei = neighboursSameLevel.at( nNei );

                    if ( nei != NULL )
                    {
                        // Compute the traversal direction as the opposite direction
                        CT_Point traversalDirection = createCtPoint( -i, -j, -k );

                        // Get all the leaves in this direction
                        QVector< NORM_AbstractNode* > neiLeafInTraversalDirection;
                        nei->leavesInTraversalDirectionRecursive( traversalDirection, neiLeafInTraversalDirection );

                        // Add them to the output
                        foreach( NORM_AbstractNode* curNeiLeaf, neiLeafInTraversalDirection )
                        {
                            outNeighbours.push_back( curNeiLeaf );
                        }

                    }

                    // We traversed a node
                    nNei++;
                }
            }
        }
    }
}

NORM_AbstractNode* NORM_Octree::deepestNodeContainingBBBox(const CT_Point &bot, const CT_Point &top) const
{
    // Transform the bounding box into [0,1] accordingly to the bounding cube
    CT_Point interpolatedBot;
    NORM_Tools::NORM_Interpolation::linearInterpolation( _bot, _top,
                                                         createCtPoint(0,0,0), createCtPoint(1,1,1),
                                                         bot,
                                                         interpolatedBot );

    CT_Point interpolatedTop;
    NORM_Tools::NORM_Interpolation::linearInterpolation( _bot, _top,
                                                         createCtPoint(0,0,0), createCtPoint(1,1,1),
                                                         top,
                                                         interpolatedTop );

    // Calculates location code for bot an top
    size_t codexbot = (size_t) (interpolatedBot(0) * _powMaxDepth );
    size_t codeybot = (size_t) (interpolatedBot(1) * _powMaxDepth );
    size_t codezbot = (size_t) (interpolatedBot(2) * _powMaxDepth );

    size_t codextop = (size_t) (interpolatedTop(0) * _powMaxDepth );
    size_t codeytop = (size_t) (interpolatedTop(1) * _powMaxDepth );
    size_t codeztop = (size_t) (interpolatedTop(2) * _powMaxDepth );

    // Look the deepest common ancestor by XORing the codes
    size_t xdiff = codexbot ^ codextop;
    size_t ydiff = codeybot ^ codeytop;
    size_t zdiff = codezbot ^ codeztop;

    // Determine the depth of the deepest node where the locations differ
    // This is done by calculating the left most significant bit
    size_t imsbx = _maxDepth - indexOfMostSignificantBit( xdiff, _maxDepth );
    size_t imsby = _maxDepth - indexOfMostSignificantBit( ydiff, _maxDepth );
    size_t imsbz = _maxDepth - indexOfMostSignificantBit( zdiff, _maxDepth );

    size_t depthOfDeepestCommonAncestor = std::min( std::min( imsbx, imsby ), imsbz );

    if( depthOfDeepestCommonAncestor != 0 )
    {
        depthOfDeepestCommonAncestor -= 1; // The xOR gives the index of the fork in the tree between bot and top, so the common ancestor is at depth - 1
    }

    if( depthOfDeepestCommonAncestor == 0 )
    {
        return nodeAtCodeAndMaxDepth( codexbot, codeybot, codezbot, depthOfDeepestCommonAncestor);
    }

    return nodeAtCodeAndMaxDepth( codexbot, codeybot, codezbot, depthOfDeepestCommonAncestor - 1 );
}

CT_PointCloudIndexVector* NORM_Octree::sphericalNeighbourhood(const CT_Point &center, double radius) const
{
    CT_PointCloudIndexVector* rslt = new CT_PointCloudIndexVector();

    CT_PointCloudIndexVector* approximateSphere = approximateSphericalNeighbourhood( center, radius );

    // Now only add to the result the points inside the sphere
    CT_PointAccessor ptAccessor;
    size_t nbPts = approximateSphere->size();
    for( size_t i = 0 ; i < nbPts ; i++ )
    {
        const CT_Point& currPoint = ptAccessor.constPointAt( approximateSphere->indexAt(i) );
        CT_Point diff;
        diff = center - currPoint;

        if( diff.norm() <= radius )
        {
            rslt->addIndex( approximateSphere->indexAt(i) );
        }
    }

    return rslt;
}

CT_PointCloudIndexVector* NORM_Octree::approximateSphericalNeighbourhood(const CT_Point &center, double radius) const
{
    CT_PointCloudIndexVector* rslt = new CT_PointCloudIndexVector();

    // Calculate the bounding box of the sphere
    CT_Point bot;
    CT_Point top;
    for( size_t i = 0 ; i < 3 ; i++ )
    {
        bot(i) = center(i) - radius;
        top(i) = center(i) + radius;

        // If outside the bounding cube put it back in !
        if( bot(i) < _bot(i) )
        {
            bot(i) = _bot(i);
        }

        if( bot(i) > _top(i) )
        {
            bot(i) = _top(i);
        }

        if( top(i) < _bot(i) )
        {
            top(i) = _bot(i);
        }

        if( top(i) > _top(i) )
        {
            top(i) = _top(i);
        }
    }

    if( bot == top )
    {
        return NULL;
    }

    // Get the nodes inside this bounding box
    NORM_AbstractNode* deepestContainingBBox = deepestNodeContainingBBBox( bot, top );
    QVector< NORM_AbstractNode* > nodesInsideBBox;
    deepestContainingBBox->getAllLeavesRecursiveInSphere( nodesInsideBBox, center, radius );

    // For each leaf found make sure it intersect the sphere
    foreach ( NORM_AbstractNode* currNode, nodesInsideBBox )
    {
//        // If they do collide (either the box is inside the sphere or on its surface)
//        // Add the points to the point vector²
//        if( currNode->intersectsSphere( center, radius ) )
//        {
            CT_PointCloudIndexVector* currCloud = currNode->getIndexCloud();

            size_t currNbPts = currCloud->size();
            for( size_t currIndex = 0 ; currIndex < currNbPts ; currIndex++ )
            {
                rslt->addIndex( currCloud->indexAt( currIndex ) );
            }

            delete currCloud;
//        }
    }

    return rslt;
}

AttributEntier* NORM_Octree::getIdAttribute(QString modelName, CT_AbstractResult *result )
{
    // On parcours l'arbre jusqu'a trouver les feuilles
    // Tout en gardant leur id de fils (entre 0 et 7)
    // Et pour chaque point de chaque feuille on affecte cet identifiant
    AttributEntier* rslt = new AttributEntier( modelName, result, _indexCloud );

    _root->setIdAttributeRecursive( rslt, 0 );

    rslt->setBoundingBox( _inputBot(0), _inputBot(1), _inputBot(2),
                          _inputTop(0), _inputTop(1), _inputTop(2) );

    return rslt;
}

AttributCouleur* NORM_Octree::getColorAttribute(QString modelName, CT_AbstractResult *result )
{
    // On parcours l'arbre jusqu'a trouver les feuilles
    // Tout en gardant leur id de fils (entre 0 et 7)
    // Et pour chaque point de chaque feuille on affecte cet identifiant
    CT_ColorCloudStdVector* rsltColorCloud = new CT_ColorCloudStdVector( _indexCloud->size() );
    AttributCouleur* rslt = new AttributCouleur( modelName, result, _indexCloud, rsltColorCloud );

    _root->setColorAttributeRecursive( rsltColorCloud, 0 );

    rslt->setBoundingBox( _inputBot(0), _inputBot(1), _inputBot(2),
                          _inputTop(0), _inputTop(1), _inputTop(2) );

    return rslt;
}

bool NORM_Octree::contains(const CT_Point &p)
{
    bool rslt = true;
    for( size_t i = 0 ; i < 3 ; i++ )
    {
        if( ( p(i) < _bot(i) ) || ( p(i) >= _top(i) ) )
        {
            rslt = false;
        }
    }

    return rslt;
}

CT_Point NORM_Octree::bot() const
{
    return _bot;
}

void NORM_Octree::setBot(const CT_Point &bot)
{
    _bot = bot;
}

CT_Point NORM_Octree::top() const
{
    return _top;
}

void NORM_Octree::setTop(const CT_Point &top)
{
    _top = top;
}

CT_MPCIR NORM_Octree::indexCloud() const
{
    return _indexCloud;
}

void NORM_Octree::setIndexCloud(const CT_MPCIR &indexCloud)
{
    _indexCloud = indexCloud;
}

CT_PCIR NORM_Octree::inputIndexCloud() const
{
    return _inputIndexCloud;
}

void NORM_Octree::setInputIndexCloud(const CT_PCIR &inputIndexCloud)
{
    _inputIndexCloud = inputIndexCloud;
}

size_t NORM_Octree::minNbPoints() const
{
    return _minNbPoints;
}

void NORM_Octree::setMinNbPoints(const size_t &minNbPoints)
{
    _minNbPoints = minNbPoints;
}

float NORM_Octree::minNodeSize() const
{
    return _minNodeSize;
}

void NORM_Octree::setMinNodeSize(float minNodeSize)
{
    _minNodeSize = minNodeSize;
}

size_t NORM_Octree::maxDepth() const
{
    return _maxDepth;
}

void NORM_Octree::setMaxDepth(const size_t &maxDepth)
{
    _maxDepth = maxDepth;
}

inline void NORM_Octree::draw(GraphicsViewInterface& view,
                              PainterInterface& painter )
{
    _root->drawRecursive( view, painter, 0 );
}

void NORM_Octree::printTree() const
{
    _root->printInfosRecursive();
}

NORM_AbstractNode* NORM_Octree::root() const
{
    return _root;
}

void NORM_Octree::setRoot(NORM_AbstractNode* root)
{
    _root = root;
}

size_t indexOfMostSignificantBit(size_t n, size_t nBits )
{
    size_t rslt = nBits;

    while( rslt > 0 && ( ( ( n & (1 << rslt) ) >> rslt ) == 0 ) )
    {
        rslt--;
    }

    return rslt;
}
