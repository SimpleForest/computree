#include "norm_newoctreedrawmanagerv2.h"

#include "norm_newoctreev2.h"

// Initialise static attributes
const QString NORM_NewOctreeDrawManagerV2::INDEX_CONFIG_DISPLAY_BOXES = NORM_NewOctreeDrawManagerV2::staticInitConfigDisplayBoxes();
const QString NORM_NewOctreeDrawManagerV2::INDEX_CONFIG_DISPLAY_CENTROID = NORM_NewOctreeDrawManagerV2::staticInitConfigDisplayCentroid();
const QString NORM_NewOctreeDrawManagerV2::INDEX_CONFIG_DISPLAY_ACP_V3 = NORM_NewOctreeDrawManagerV2::staticInitConfigDisplayV3Pca();
const QString NORM_NewOctreeDrawManagerV2::INDEX_CONFIG_SIZE_ACP_V3 = NORM_NewOctreeDrawManagerV2::staticInitConfigSizeV3Pca();

NORM_NewOctreeDrawManagerV2::NORM_NewOctreeDrawManagerV2(QString drawConfigurationName) :
    CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager(drawConfigurationName.isEmpty() ? QString("NORM_NEW_OCTREE_OPOPOPOPOPOPO") : drawConfigurationName)
{
}

void NORM_NewOctreeDrawManagerV2::draw(GraphicsViewInterface& view,
                                       PainterInterface& painter,
                                       const CT_AbstractItemDrawable& itemDrawable) const
{
    CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::draw(view, painter, itemDrawable);

    // Get the item to display
    const NORM_NewOctreeV2& octree = dynamic_cast<const NORM_NewOctreeV2& >(itemDrawable);

    bool drawBoxes = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_DISPLAY_BOXES).toBool();
    bool drawCentroid = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_DISPLAY_CENTROID).toBool();
    bool drawAcpV3 = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_DISPLAY_ACP_V3).toBool();
    double sizeV3 = getDrawConfiguration()->getVariableValue(INDEX_CONFIG_SIZE_ACP_V3).toDouble();

    octree.draw( view,
                 painter,
                 drawBoxes,
                 drawCentroid,
                 drawAcpV3,
                 sizeV3 );
}

QString NORM_NewOctreeDrawManagerV2::staticInitConfigDisplayBoxes()
{
    return "NORM_NewOctreeDrawManagerV2_DB";
}

QString NORM_NewOctreeDrawManagerV2::staticInitConfigDisplayCentroid()
{
    return "NORM_NewOctreeDrawManagerV2_DC";
}

QString NORM_NewOctreeDrawManagerV2::staticInitConfigDisplayV3Pca()
{
    return "NORM_NewOctreeDrawManagerV2_DV3";
}

QString NORM_NewOctreeDrawManagerV2::staticInitConfigSizeV3Pca()
{
    return "NORM_NewOctreeDrawManagerV2_SV3";
}

CT_ItemDrawableConfiguration NORM_NewOctreeDrawManagerV2::createDrawConfiguration(QString drawConfigurationName) const
{
    CT_ItemDrawableConfiguration item = CT_ItemDrawableConfiguration(drawConfigurationName);
    item.addAllConfigurationOf(CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::createDrawConfiguration(drawConfigurationName));

    item.addNewConfiguration(staticInitConfigDisplayBoxes(), "Display Boxes", CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(staticInitConfigDisplayCentroid(), "Display Centroid", CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(staticInitConfigDisplayV3Pca(), "Display V3", CT_ItemDrawableConfiguration::Bool, false);
    item.addNewConfiguration(staticInitConfigSizeV3Pca(), "Size V3", CT_ItemDrawableConfiguration::Double, 0.1 );

    return item;
}
