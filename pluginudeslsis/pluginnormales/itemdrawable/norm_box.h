#ifndef NORM_BOX_H
#define NORM_BOX_H

// Derive d'un CT_Circlepuisqu'ils ont les memes parametres, seul le drawManager change
#include "norm_standardboxdrawmanager.h"
#include "ct_itemdrawable/abstract/ct_abstractshape.h"
#include "ct_shapedata/ct_boxdata.h"

/*!
 * \class NORM_Box
 * \ingroup PluginShared_Items
 * \brief <b>ItemDrawable managing a NORM_BoxData</b>
 *
 * It represents a box in 3D, defined by a center, a direction and a radius.
 *
 */
class NORM_Box : public CT_AbstractShape
{
    // IMPORTANT pour avoir le nom de l'ItemDrawable
    Q_OBJECT

public:
    /*!
     * \brief NORM_Box
     * Default constructor
     */
    NORM_Box();

    /**
      * \brief Contructeur avec une instance des donnes (NORM_BoxData*), ne peut etre NULL !
      * (Supprime dans le destructeur de la classe).
      */
    NORM_Box(const CT_OutAbstractSingularItemModel *model,
             const CT_AbstractResult *result,
             CT_BoxData* data);

    /**
      * \brief Contructeur avec une instance des donnes (NORM_BoxData*), ne peut etre NULL !
      * (Supprime dans le destructeur de la classe).
      */
    NORM_Box(const QString &modelName,
             const CT_AbstractResult *result,
             CT_BoxData* data);


    /**
      * ATTENTION : ne pas oublier de redfinir ces deux méthodes si vous héritez de cette classe.
      */
    virtual QString getType() const;
    static QString staticGetType();

    inline double getBotX() const { return ( getCenterX() - (getLengthX() / 2.0) ); }
    inline double getBotY() const { return ( getCenterY() - (getLengthY() / 2.0) ); }
    inline double getBotZ() const { return ( getCenterZ() - (getLengthZ() / 2.0) ); }
    inline double getTopX() const { return ( getCenterX() + (getLengthX() / 2.0) ); }
    inline double getTopY() const { return ( getCenterY() + (getLengthY() / 2.0) ); }
    inline double getTopZ() const { return ( getCenterZ() + (getLengthZ() / 2.0) ); }
    inline double getLengthX() const { return ((CT_BoxData&)getData()).getWidthDirection()(0); }
    inline double getLengthY() const { return ((CT_BoxData&)getData()).getWidthDirection()(1); }
    inline double getLengthZ() const { return ((CT_BoxData&)getData()).getWidthDirection()(2); }
    inline double getCenterX() const { return ((CT_BoxData&)getData()).getCenter()(0); }
    inline double getCenterY() const { return ((CT_BoxData&)getData()).getCenter()(1); }
    inline double getCenterZ() const { return ((CT_BoxData&)getData()).getCenter()(2); }

    virtual CT_AbstractItemDrawable* copy(const CT_OutAbstractItemModel *model,
                                          const CT_AbstractResult *result,
                                          CT_ResultCopyModeList copyModeList);

    virtual CT_AbstractItemDrawable *copy(const QString &modelName,
                                          const CT_AbstractResult *result,
                                          CT_ResultCopyModeList copyModeList);

private:

    CT_DEFAULT_IA_BEGIN(NORM_Box)
    CT_DEFAULT_IA_V2(NORM_Box, CT_AbstractCategory::staticInitDataX(), &NORM_Box::getCenterX, QObject::tr("CenterX"))
    CT_DEFAULT_IA_V2(NORM_Box, CT_AbstractCategory::staticInitDataY(), &NORM_Box::getCenterY, QObject::tr("CenterY"))
    CT_DEFAULT_IA_V2(NORM_Box, CT_AbstractCategory::staticInitDataZ(), &NORM_Box::getCenterZ, QObject::tr("CenterZ"))
    CT_DEFAULT_IA_V2(NORM_Box, CT_AbstractCategory::staticInitDataX(), &NORM_Box::getBotX, QObject::tr("BotX"))
    CT_DEFAULT_IA_V2(NORM_Box, CT_AbstractCategory::staticInitDataY(), &NORM_Box::getBotY, QObject::tr("BotY"))
    CT_DEFAULT_IA_V2(NORM_Box, CT_AbstractCategory::staticInitDataZ(), &NORM_Box::getBotZ, QObject::tr("BotZ"))
    CT_DEFAULT_IA_V2(NORM_Box, CT_AbstractCategory::staticInitDataX(), &NORM_Box::getTopX, QObject::tr("TopX"))
    CT_DEFAULT_IA_V2(NORM_Box, CT_AbstractCategory::staticInitDataY(), &NORM_Box::getTopY, QObject::tr("TopY"))
    CT_DEFAULT_IA_V2(NORM_Box, CT_AbstractCategory::staticInitDataZ(), &NORM_Box::getTopZ, QObject::tr("TopZ"))
    CT_DEFAULT_IA_V2(NORM_Box, CT_AbstractCategory::staticInitDataXDimension(), &NORM_Box::getLengthX, QObject::tr("Longueur X"))
    CT_DEFAULT_IA_V2(NORM_Box, CT_AbstractCategory::staticInitDataYDimension(), &NORM_Box::getLengthY, QObject::tr("Longueur Y"))
    CT_DEFAULT_IA_V2(NORM_Box, CT_AbstractCategory::staticInitDataZDimension(), &NORM_Box::getLengthZ, QObject::tr("Longueur Z"))
    CT_DEFAULT_IA_END(NORM_Box)

    const static NORM_StandardBoxDrawManager BOX_DRAW_MANAGER;
};

#endif // NORM_BOX_H
