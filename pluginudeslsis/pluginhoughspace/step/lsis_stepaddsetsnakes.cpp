#include "lsis_stepaddsetsnakes.h"

//In/Out dependencies
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_circle.h"

//Tools dependencies
#include "ct_point.h"

//Qt dependencies
#include <QFile>
#include <QTextStream>

// Alias for indexing models
#define DEFout_RsltSetSnakes "RsltSetSnakes"
#define DEFout_GrpSnakes "GrpSnakes"
#define DEFout_GrpGrpCircles "GrpGrpCircles"
#define DEFout_GrpCircles "GrpCircles"
#define DEFout_ItmCircle "ItmCircle"

// Constructor : initialization of parameters
LSIS_StepAddSetSnakes::LSIS_StepAddSetSnakes(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

// Step description (tooltip of contextual menu)
QString LSIS_StepAddSetSnakes::getStepDescription() const
{
    return tr("Ajoute plusieurs snakes sous formede cercles");
}

// Step detailled description
QString LSIS_StepAddSetSnakes::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString LSIS_StepAddSetSnakes::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepAddSetSnakes::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepAddSetSnakes(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepAddSetSnakes::createInResultModelListProtected()
{
    // No in result is needed
    setNotNeedInputResult();
}

// Creation and affiliation of OUT models
void LSIS_StepAddSetSnakes::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_RsltSetSnakes = createNewOutResultModel(DEFout_RsltSetSnakes, tr("Result"));
    res_RsltSetSnakes->setRootGroup(DEFout_GrpSnakes, new CT_StandardItemGroup(), tr("Group"));
    res_RsltSetSnakes->addGroupModel(DEFout_GrpSnakes, DEFout_GrpGrpCircles, new CT_StandardItemGroup(), tr("Group"));
    res_RsltSetSnakes->addGroupModel(DEFout_GrpGrpCircles, DEFout_GrpCircles, new CT_StandardItemGroup(), tr("Group"));
    res_RsltSetSnakes->addItemModel(DEFout_GrpCircles, DEFout_ItmCircle, new CT_Circle(), tr("Circle"));

}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepAddSetSnakes::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addFileChoice("Snake files", CT_FileChoiceButton::OneOrMoreExistingFiles, "*", _fileNames);
}

void LSIS_StepAddSetSnakes::compute()
{
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_RsltSetSnakes = outResultList.at(0);

    CT_StandardItemGroup* grp_GrpSnakes= new CT_StandardItemGroup(DEFout_GrpSnakes, res_RsltSetSnakes);
    res_RsltSetSnakes->addGroup(grp_GrpSnakes);

    foreach ( QString curFileName, _fileNames )
    {
        CT_StandardItemGroup* grp_GrpGrpCircle= new CT_StandardItemGroup(DEFout_GrpGrpCircles, res_RsltSetSnakes);
        QFile file( curFileName );

        if( !file.open( QIODevice::ReadOnly ) )
        {
            PS_LOG->addErrorMessage( PS_LOG->error, "Can not access file : \""+file.fileName()+"\"" );
            return;
        }

        // Outils de lecture
        QTextStream fileStream(&file);

        // Skip header
        fileStream.readLine();

        float x,y,z,r;
        CT_Point curCenter;
        CT_Point curOrientation;
        curOrientation.setValues(0,0,1);

        int nCircles = 0;
        while( !fileStream.atEnd() )
        {
            fileStream >> r >> x >> y >> z;

            curCenter.setValues( x, y, z );

            CT_Circle* curCircle = new CT_Circle( DEFout_ItmCircle,
                                                  res_RsltSetSnakes,
                                                  new CT_CircleData( curCenter, curOrientation, r ) );

            CT_StandardItemGroup* grp_GrpCircle= new CT_StandardItemGroup(DEFout_GrpCircles, res_RsltSetSnakes);
            grp_GrpCircle->addItemDrawable(curCircle);
            grp_GrpGrpCircle->addGroup( grp_GrpCircle );
            nCircles++;
        }

        grp_GrpSnakes->addGroup( grp_GrpGrpCircle );
        file.close();
    }
}
