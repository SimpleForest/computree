#include "lsis_stepcreatehoughspace.h"

//In/Out dependencies
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_stepconfigurabledialog.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_pointsattributesnormal.h"

//Tools dependencies
#include "../houghspace/lsis_houghspace4d.h"
#include "streamoverload/streamoverload.h"

//Qt dependencies
#include <QElapsedTimer>

// Alias for indexing models
#define DEFin_rsltNormals "rsltNormals"
#define DEFin_grpNormals "grpNormals"
#define DEFin_itmNormals "itmNormals"

#define DEFout_rsltHoughSpace "rsltHoughSpace"
#define DEFout_grpHoughSpace "grpHoughSpace"
#define DEFout_itmHoughSpace "itmHoughSpace"

// Constructor : initialization of parameters
LSIS_StepCreateHoughSpace::LSIS_StepCreateHoughSpace(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _minr = 0.05;
    _maxr = 0.15;
    _resx = 0.02;
    _resy = 0.02;
    _resz = 0.02;
    _resr = 0.01;
}

// Step description (tooltip of contextual menu)
QString LSIS_StepCreateHoughSpace::getStepDescription() const
{
    return tr("1 - Créer un espace de Hough à partir d'un nuage de points");
}

// Step detailled description
QString LSIS_StepCreateHoughSpace::getStepDetailledDescription() const
{
    return tr("Créer et remplir une grille 4D contenant les valeurs de la transformée de Hough "
              "pour chaque point du nuage. Cette étape à besoin d'avoir un nuage de point avec "
              "des normales. Si votre nuage de points ne possède pas de normales vous pouvez les "
              "obtenir à l'aide de l'étape d'estimation des normales contenue dans le "
              "plugin \"Toolkit\" par exemple. Chaque cellule de la grille représente un cercle "
              "potentiel en 3D et la transformée de Hough associe à chacun de ces cercles le nombre "
              "de points qui lui appartient. Des valeurs élevées vont se formées dans les cellules "
              "où le rayon du cercle s'ajuste bien par rapport à un amat de points. "
              "L'algorithme considère que la direction de la normale du point n'est pas "
              "correctement calculée c'est pourquoi le parcours de la grille est effectué dans la "
              "direction de la normale et dans le sens opposé.");
}

// Step URL
QString LSIS_StepCreateHoughSpace::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepCreateHoughSpace::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepCreateHoughSpace(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepCreateHoughSpace::createInResultModelListProtected()
{
//    CT_InResultModelGroup *resIn_rsltNormals = createNewInResultModel(DEFin_rsltNormals, tr("Normals"));
//    resIn_rsltNormals->setZeroOrMoreRootGroup();
//    resIn_rsltNormals->addGroupModel("",DEFin_grpNormals,CT_AbstractItemGroup::staticGetType(),tr("Group"));
//    resIn_rsltNormals->addItemModel(DEFin_grpNormals, DEFin_itmNormals, CT_PointsAttributesNormal::staticGetType(), tr("Normals"));

    CT_InResultModelGroupToCopy *resIn_rsltNormals = createNewInResultModelForCopy(DEFin_rsltNormals,"Normal Cloud");
    resIn_rsltNormals->setZeroOrMoreRootGroup();
    resIn_rsltNormals->addGroupModel("",DEFin_grpNormals,CT_AbstractItemGroup::staticGetType(),tr("Group"));
    resIn_rsltNormals->addItemModel(DEFin_grpNormals, DEFin_itmNormals, CT_PointsAttributesNormal::staticGetType(), tr("Normals"));
}

// Creation and affiliation of OUT models
void LSIS_StepCreateHoughSpace::createOutResultModelListProtected()
{
//    CT_OutResultModelGroup *res_rsltHoughSpace = createNewOutResultModel(DEFout_rsltHoughSpace, tr("Hough Space"));
//    res_rsltHoughSpace->setRootGroup(DEFout_grpHoughSpace, new CT_StandardItemGroup(), tr("Group"));
//    res_rsltHoughSpace->addItemModel(DEFout_grpHoughSpace, DEFout_itmHoughSpace, new LSIS_HoughSpace4D(), tr("Hough Space 4D"));

    CT_OutResultModelGroupToCopyPossibilities *res_rsltHoughSpace = createNewOutResultModelToCopy(DEFin_rsltNormals);
    if(res_rsltHoughSpace != NULL)
        res_rsltHoughSpace->addItemModel(DEFin_grpNormals, _autoRenameModelsHoughSpace, new LSIS_HoughSpace4D(), tr("Hough space"));
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepCreateHoughSpace::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble(tr("Rayon minimum"), "[m]", 0.0001, 100, 4, _minr, 1);
    configDialog->addDouble(tr("Rayon maximum"), "[m]", 0.0002, 100, 4, _maxr, 1);
    configDialog->addDouble(tr("Résolution du rayon"), "[m]", 0.0001, 2, 4, _resr, 1, tr("Indiquez le pas entre les rayons minimum et maximum (4ème dimension de la grille 4D)"));
    configDialog->addDouble(tr("Résolution x/y/z de la grille 4D"), "[m]", 0.0001, 2, 4, _resx, 1, tr("La valeur doit être au minimum 2 fois la résolution du rayon."));
    configDialog->addEmpty();
}

void LSIS_StepCreateHoughSpace::compute()
{
    // DONT'T FORGET TO ADD THIS STEP TO THE PLUGINMANAGER !!!!!
//    QList<CT_ResultGroup*> inResultList = getInputResults();
//    CT_ResultGroup* resIn_rsltNormals = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    _outResultHoughSpace = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_grpNormals(_outResultHoughSpace, this, DEFin_grpNormals);
    while (itIn_grpNormals.hasNext() && !isStopped())
    {
//        const CT_AbstractItemGroup* grpIn_grpNormals = (CT_AbstractItemGroup*) itIn_grpNormals.next();
        CT_StandardItemGroup* grpIn_grpNormals = (CT_StandardItemGroup*) itIn_grpNormals.next();
        
        const CT_PointsAttributesNormal* itemIn_itmNormals = (CT_PointsAttributesNormal*)grpIn_grpNormals->firstItemByINModelName(this, DEFin_itmNormals);
        if (itemIn_itmNormals != NULL)
        {
            //qDebug() << "Start compute ici";

            // BBox and resolution of the HS
            _minx = itemIn_itmNormals->minX();
            _miny = itemIn_itmNormals->minY();
            _minz = itemIn_itmNormals->minZ();
            _maxx = itemIn_itmNormals->maxX();
            _maxy = itemIn_itmNormals->maxY();
            _maxz = itemIn_itmNormals->maxZ();
            _resy = _resx;
            _resz = _resx;

            assert( _resr > 0 );
            assert( _resx > 0 );
            assert( _resy > 0 );
            assert( _resz > 0 );
            assert( _minr < _maxr );
            assert( _minx < _maxx );
            assert( _miny < _maxy );
            assert( _minz < _maxz );
            assert( _minr > 0 );

//            qDebug() << "Hough space min max radius" << _minr << _maxr;
//            qDebug() << "Hough space BBox (" << _minx << _miny << _minz << ") (" << _maxx << _maxy << _maxz << ")";
//            qDebug() << "Hough space resolution(" << _resr << _resx << _resy << _resz << ")";

//            size_t dimw = ceil((_maxr - _minr)/_resr);
//            size_t dimx = ceil((_maxx - _minx)/_resx);
//            size_t dimy = ceil((_maxy - _miny)/_resy);
//            size_t dimz = ceil((_maxz - _minz)/_resz);

//            qDebug() << "Dimensions : " << dimw << dimx << dimy << dimz;
//            qDebug() << "Ncells" << dimw*dimx*dimy*dimz;
//            qDebug() << "Int max = " << std::numeric_limits<int>::max();

            // On cree un espace de Hough a partir de la bounding box du nuage de points
            LSIS_HoughSpace4D* hs = new LSIS_HoughSpace4D( _autoRenameModelsHoughSpace.completeName(), _outResultHoughSpace,
                                                           _minr,
                                                           _minx,
                                                           _miny,
                                                           _minz,
                                                           _maxr,
                                                           _maxx,
                                                           _maxy,
                                                           _maxz,
                                                           _resr,
                                                           _resx,
                                                           _resy,
                                                           _resz,
                                                           true,
                                                           itemIn_itmNormals,
                                                           this );

            QString msg2 = "Nb points" + QString::number( itemIn_itmNormals->getPointCloudIndex()->size() );
            PS_LOG->addMessage(LogInterface::info, LogInterface::step, msg2);

//            qDebug() << "HS dimensions " << hs->dim();
//            qDebug() << "HS min max " << hs->dataMin() << hs->dataMax();
            grpIn_grpNormals->addItemDrawable( hs );

        //    qDebug() << "Debut du filtrage par otsu";
        //    QElapsedTimer timer;
        //    timer.start();
        //    hs->threshWithRatioAndOtsu(0.8,0.8,50);
        //    qDebug() << "Fin du filtrage par otsu, temps :" << timer.elapsed();
        }
    }

    setProgress(100);
}

void LSIS_StepCreateHoughSpace::compute(const CT_PointsAttributesNormal *normals)
{
//    // BBox and resolution of the HS
//    _minx = normals->minX();
//    _miny = normals->minY();
//    _minz = normals->minZ();
//    _maxx = normals->maxX();
//    _maxy = normals->maxY();
//    _maxz = normals->maxZ();
//    _resy = _resx;
//    _resz = _resx;

//    qDebug() << "Hough space BBox (" << _minx << _miny << _minz << ") (" << _maxx << _maxy << _maxz << ")";
//    qDebug() << "Hough space resolution(" << _resr << _resx << _resy << _resz << ")";

//    // On cree un espace de Hough a partir de la bounding box du nuage de points
//    LSIS_HoughSpace4D* hs = new LSIS_HoughSpace4D( _autoRenameModelsHoughSpace.completeName(), _outResultHoughSpace,
//                                                   _minr,
//                                                   _minx,
//                                                   _miny,
//                                                   _minz,
//                                                   _maxr,
//                                                   _maxx,
//                                                   _maxy,
//                                                   _maxz,
//                                                   _resr,
//                                                   _resx,
//                                                   _resy,
//                                                   _resz,
//                                                   true,
//                                                   normals,
//                                                   this );

//    qDebug() << "HS dimensions " << hs->dim();

//    qDebug() << "Debut du filtrage par otsu";
//    QElapsedTimer timer;
//    timer.start();
//    hs->threshWithRatioAndOtsu(0.8,0.8,50);
//    qDebug() << "Fin du filtrage par otsu, temps :" << timer.elapsed();

    // OUT results creation (move it to the appropried place in the code)
//    CT_StandardItemGroup* grp_grpHoughSpace= new CT_StandardItemGroup(DEFout_grpHoughSpace, _outResultHoughSpace);
//    grp_grpHoughSpace->addItemDrawable( hs );
//    _outResultHoughSpace->addGroup(grp_grpHoughSpace);
}
