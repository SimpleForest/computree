#include "lsis_stepslicepointcloud.h"

//In/Out dependencies
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_scene.h"

//Tools dependencies
#include "ct_iterator/ct_pointiterator.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_view/ct_stepconfigurabledialog.h"

//Qt dependencies
#include <QDebug>

// Alias for indexing models
#define DEFin_rsltScene "rsltScene"
#define DEFin_grpScene "grpScene"
#define DEFin_itmScene "itmScene"

#define DEFout_rsltSlices "rsltSlices"
#define DEFout_grpSlice "grpSlice"
#define DEFout_itmSlice "itmSlice"

// Constructor : initialization of parameters
LSIS_StepSlicePointCloud::LSIS_StepSlicePointCloud(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _axis = 2;
}

// Step description (tooltip of contextual menu)
QString LSIS_StepSlicePointCloud::getStepDescription() const
{
    return tr("Tranche une scene en transects");
}

// Step detailled description
QString LSIS_StepSlicePointCloud::getStepDetailledDescription() const
{
    return tr("");
}

// Step URL
QString LSIS_StepSlicePointCloud::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepSlicePointCloud::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepSlicePointCloud(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepSlicePointCloud::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_rsltScene = createNewInResultModelForCopy(DEFin_rsltScene,"Nuage a trancher");
    resIn_rsltScene->setZeroOrMoreRootGroup();
    resIn_rsltScene->addGroupModel("",DEFin_grpScene,CT_AbstractItemGroup::staticGetType(),tr("Group"));
    resIn_rsltScene->addItemModel(DEFin_grpScene, DEFin_itmScene, CT_Scene::staticGetType(), tr("Scene a trancher"));
}

// Creation and affiliation of OUT models
void LSIS_StepSlicePointCloud::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *res_rsltSlices = createNewOutResultModelToCopy(DEFin_rsltScene);
    if(res_rsltSlices != NULL)
    {
        res_rsltSlices->addGroupModel(DEFin_grpScene, _autoRenameModelsGroupSlice );
        res_rsltSlices->addItemModel(_autoRenameModelsGroupSlice, _autoRenameModelsItemSlice, new CT_Scene(), "Tranche" );
    }
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepSlicePointCloud::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Epaisseur de tranche","",0.0001,9999999,4,_sliceWidth);

    configDialog->addText("Axe de decoupe");
    CT_ButtonGroup& bg_axis = configDialog->addButtonGroup(_axis);
    configDialog->addExcludeValue("", "", tr("X"), bg_axis, 0);
    configDialog->addExcludeValue("", "", tr("Y"), bg_axis, 1);
    configDialog->addExcludeValue("", "", tr("Z"), bg_axis, 2);
}

void LSIS_StepSlicePointCloud::compute()
{
    // DONT'T FORGET TO ADD THIS STEP TO THE PLUGINMANAGER !!!!!
    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* outResultSlices = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_grpScene(outResultSlices, this, DEFin_grpScene);
    while (itIn_grpScene.hasNext() && !isStopped())
    {
        CT_StandardItemGroup* grpIn_grpScene = (CT_StandardItemGroup*) itIn_grpScene.next();
        const CT_Scene* itemIn_itmScene = (CT_Scene*)grpIn_grpScene->firstItemByINModelName(this, DEFin_itmScene);

        if (itemIn_itmScene != NULL)
        {
            // On recupere la bbox de la scene d'entree
            CT_Point sceneMin, sceneMax;
            itemIn_itmScene->getBoundingBox( sceneMin, sceneMax );

            // On calcule le nombre de tranches en sortie en fonction de la bbox, de l'axe et de l'épaisseur
            int nSlices = ceil( ( sceneMax(_axis) - sceneMin(_axis) ) / _sliceWidth );

            // On cree autant de nuages que de tranches
            QVector< CT_PointCloudIndexVector* > outPointClouds;
            QVector< CT_Point > outMins;
            QVector< CT_Point > outMaxs;
            CT_Point pmin = createCtPoint( std::numeric_limits<double>::max(),
                                          std::numeric_limits<double>::max(),
                                          std::numeric_limits<double>::max() );
            CT_Point pmax = createCtPoint( -std::numeric_limits<double>::max(),
                                          -std::numeric_limits<double>::max(),
                                          -std::numeric_limits<double>::max() );
            for( int i = 0 ; i < nSlices ; i++ )
            {
                outPointClouds.push_back( new CT_PointCloudIndexVector() );
                outMins.push_back( pmin );
                outMaxs.push_back( pmax );
            }

            // On commence a classer les points de la scene d'entree
            CT_PointIterator itP( itemIn_itmScene->getPointCloudIndex() );
            while(itP.hasNext() && (!isStopped()))
            {
                const CT_Point& point = itP.next().currentPoint();
                size_t index = itP.currentGlobalIndex();

                // Calcule la tranche a laquelle appartient le point
                int slice = floor( ( point(_axis) - sceneMin(_axis) ) / _sliceWidth );

                if( slice >= nSlices ) // Peut arriver lorsque le dernier point est pile a la limite d'un voxel
                {
                    slice = nSlices-1;
                }

                // Ajoute l'indice au nuage d'indice correspondant
                outPointClouds[slice]->addIndex(index);
            }

            // Pour chaque nuage d'indice on cree une scene que l'on ajoute au resultat

            for( int i = 0 ; i < nSlices ; i++ )
            {
                qDebug() << "Coucou ajout de scene " << i << " contenant " << outPointClouds.at(i)->size() << "Points";

                // creation du groupe
                CT_StandardItemGroup *outGroup = new CT_StandardItemGroup(_autoRenameModelsGroupSlice.completeName(),outResultSlices);

                // creation et ajout de la scene
                CT_Scene *outScene = new CT_Scene(_autoRenameModelsItemSlice.completeName(), outResultSlices );

//                outScene->setBoundingBox(xmin,ymin,zmin, xmax,ymax,zmax);
                outScene->setPointCloudIndexRegistered( PS_REPOSITORY->registerPointCloudIndex( outPointClouds.at(i) ) );
                outGroup->addItemDrawable( outScene );

                // ajout au résultat
                grpIn_grpScene->addGroup(outGroup);
            }
        }
    }

    setProgress(100);
}
