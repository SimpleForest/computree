/************************************************************************************
* Filename :  norm_octreestd.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "norm_octreestd.h"

NORM_OctreeStd::NORM_OctreeStd()
    : NORM_Octree()
{
}

NORM_OctreeStd::NORM_OctreeStd(float minNodeSize,
                               size_t minNbPoints,
                               CT_Point bot,
                               CT_Point top,
                               CT_PCIR inputIndexCloud)
    : NORM_Octree( minNodeSize,
                   minNbPoints,
                   bot,
                   top,
                   inputIndexCloud )
{
}

NORM_OctreeStd::NORM_OctreeStd(float minNodeSize,
                               size_t minNbPoints,
                               CT_Point bot,
                               CT_Point top,
                               CT_PCIR inputIndexCloud,
                               const CT_OutAbstractSingularItemModel *model,
                               const CT_AbstractResult *result)
    : NORM_Octree( minNodeSize,
                   minNbPoints,
                   bot,
                   top,
                   inputIndexCloud,
                   model,
                   result )
{
}

NORM_OctreeStd::NORM_OctreeStd(float minNodeSize,
                               size_t minNbPoints,
                               CT_Point bot,
                               CT_Point top,
                               CT_PCIR inputIndexCloud,
                               const QString &modelName,
                               const CT_AbstractResult *result)
    : NORM_Octree( minNodeSize,
                   minNbPoints,
                   bot,
                   top,
                   inputIndexCloud,
                   modelName,
                   result )
{
}

void NORM_OctreeStd::initRoot()
{
    _root = new NORM_OctreeNode( this,
                                 NULL,
                                 0,
                                 _indexCloud->size(),
                                 (_bot + _top) / 2.0,
                                 0, 0, 0 );
}

NORM_OctreeNode* NORM_OctreeStd::nodeContainingPoint(CT_Point &p) const
{
    return ((NORM_OctreeNode *)NORM_Octree::nodeContainingPoint(p));
}

NORM_OctreeNode *NORM_OctreeStd::nodeAtCode(size_t codex, size_t codey, size_t codez) const
{
    return ((NORM_OctreeNode *)NORM_Octree::nodeAtCode(codex, codey, codez ));
}

NORM_OctreeNode *NORM_OctreeStd::nodeAtCodeAndMaxDepth(size_t codex, size_t codey, size_t codez, size_t maxDepth) const
{
    return ((NORM_OctreeNode *)NORM_Octree::nodeAtCodeAndMaxDepth( codex, codey, codez, maxDepth ) );
}

NORM_OctreeNode *NORM_OctreeStd::deepestNodeContainingBBBox(const CT_Point &bot, const CT_Point &top) const
{
    return ((NORM_OctreeNode *)NORM_Octree::deepestNodeContainingBBBox( bot, top ));
}

NORM_OctreeNode *NORM_OctreeStd::root() const
{
    return ((NORM_OctreeNode *)NORM_Octree::root());
}
