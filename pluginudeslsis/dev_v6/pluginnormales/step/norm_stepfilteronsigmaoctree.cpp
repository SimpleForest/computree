/************************************************************************************
* Filename :  norm_stepfilteronsigmaoctree.cpp                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#include "norm_stepfilteronsigmaoctree.h"
#include <QDebug>

// Alias for indNORMing models
#define DEFin_inResultOctree "inResultOctree"
#define DEFin_inGroupOctree "inGroupOctree"
#define DEFin_inItemOctree "inItemItemWithOctree"

#define DEFout_outResultFilteredCloud "outResultFilteredCloud"
#define DEFout_outGroupFilteredCloud "outGroupFilteredCloud"
#define DEFout_outItemFilteredCloud "outItemFilteredCloud"

typedef CT_PointsAttributesScalarTemplated<float> CT_PointAttributeSigma;

#include "ct_attributes/ct_attributesnormal.h"

// Constructor : initialization of parameters
NORM_StepFilterOnSigmaOctree::NORM_StepFilterOnSigmaOctree(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _seuilSigma = 0.009;
}

// Step description (tooltip of contNORMtual menu)
QString NORM_StepFilterOnSigmaOctree::getStepDescription() const
{
    return tr("Filtre un octree PCA en fonction de la valeur du sigma de chaque cellule");
}

// Step detailled description
QString NORM_StepFilterOnSigmaOctree::getStepDetailledDescription() const
{
    return tr("L'indice de planeite se calcule par ACP :<br>"
"lambda3 / somme_des_labdas");
}

// Step URL
QString NORM_StepFilterOnSigmaOctree::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* NORM_StepFilterOnSigmaOctree::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new NORM_StepFilterOnSigmaOctree(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void NORM_StepFilterOnSigmaOctree::createInResultModelListProtected()
{
    CT_InResultModelGroup *resIn_inResultOctree = createNewInResultModel(DEFin_inResultOctree, tr("inResultOctree"));
    resIn_inResultOctree->setRootGroup(DEFin_inGroupOctree, CT_AbstractItemGroup::staticGetType(), tr("inGroupOctree"));
    resIn_inResultOctree->addItemModel(DEFin_inGroupOctree, DEFin_inItemOctree, NORM_OctreePCA::staticGetType(), tr("inItemItemWithOctree"));
}

// Creation and affiliation of OUT models
void NORM_StepFilterOnSigmaOctree::createOutResultModelListProtected()
{
    CT_OutResultModelGroup *res_outResultFilteredCloud = createNewOutResultModel(DEFout_outResultFilteredCloud, tr("outResultScene"));
    res_outResultFilteredCloud->setRootGroup(DEFout_outGroupFilteredCloud, new CT_StandardItemGroup(), tr("outGroupFilteredCloud"));
    res_outResultFilteredCloud->addItemModel(DEFout_outGroupFilteredCloud, DEFout_outItemFilteredCloud, new CT_Scene(), tr("outItemFilteredCloud"));
}

// Semi-automatic creation of step parameters DialogBox
void NORM_StepFilterOnSigmaOctree::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addDouble("Seuil sigma", "(%)", 0.0001, 100, 4, _seuilSigma, 1);
}

void NORM_StepFilterOnSigmaOctree::compute()
{
    // DONT'T FORGET TO ADD THIS STEP TO THE PLUGINMANAGER !!!!!
    QList<CT_ResultGroup*> inResultList = getInputResults();
    CT_ResultGroup* resIn_inResultOctree = inResultList.at(0);

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_outResultFilteredScene = outResultList.at(0);

    // IN results browsing
    CT_ResultGroupIterator itIn_inGroupOctree(resIn_inResultOctree, this, DEFin_inGroupOctree);
    const NORM_OctreePCA* itemIn_inItemOctree;
    const CT_AbstractItemGroup* grpIn_inGroupOctree;
    while (itIn_inGroupOctree.hasNext() && !isStopped())
    {
        // Access the octree
        grpIn_inGroupOctree = (CT_AbstractItemGroup*) itIn_inGroupOctree.next();
        itemIn_inItemOctree = (NORM_OctreePCA*)grpIn_inGroupOctree->firstItemByINModelName(this, DEFin_inItemOctree);
        if ( itemIn_inItemOctree != NULL)
        {
            // Cree un nuage pour la scene de sortie
            CT_AbstractUndefinedSizePointCloud* curOutScene = PS_REPOSITORY->createNewUndefinedSizePointCloud();;

            // Log temporaire
            QFile file ( "/home/joris/Desktop/log-octree.xyz" );
            if( !file.open( QIODevice::WriteOnly ) )
            {
                qDebug() << "Impossible d'ouvrir \"/home/joris/Desktop/log-octree.xyz\"";
                return;
            }
            QTextStream stream( &file );

            // Lance le filtrage
            int nPoints = 0;
            filterOnSigmaRecursive( itemIn_inItemOctree->root(), curOutScene, _seuilSigma, nPoints, stream );
            qDebug() << "Nb points output : " << nPoints;

            // Creation du resultat
            CT_NMPCIR registeredCurSubCloud = PS_REPOSITORY->registerUndefinedSizePointCloud( curOutScene );
            CT_Scene* curScene = new CT_Scene( DEFout_outItemFilteredCloud, res_outResultFilteredScene, registeredCurSubCloud );

            CT_StandardItemGroup* grpCurExtractedCloud = new CT_StandardItemGroup( DEFout_outGroupFilteredCloud, res_outResultFilteredScene );
            grpCurExtractedCloud->addItemDrawable( curScene );
            res_outResultFilteredScene->addGroup(grpCurExtractedCloud);

            file.close();
        }
    }
}

void NORM_StepFilterOnSigmaOctree::filterOnSigmaRecursive(NORM_OctreeNodePCA *node,
                                                          CT_AbstractUndefinedSizePointCloud* outPointCloud,
                                                          double seuilSigmaMax,
                                                          int& nOutputPoints,
                                                          QTextStream &stream)
{
    // Si on est pas une feuille on continue a traverser l'octreee pour atteindre les feuilles
    if( !node->isLeaf() )
    {
        for( int i = 0 ; i < 8 ; i++ )
        {
            filterOnSigmaRecursive( node->getChild(i), outPointCloud, seuilSigmaMax, nOutputPoints, stream );
        }
    }

    // Si on est une feuille on regarde la valeur de sigma
    if( node->sigma() < seuilSigmaMax )
    {
        CT_PointCloudIndexVector* indexCloud = node->getIndexCloud();

        CT_PointAccessor pAccessor;
        CT_Point curPoint;

        size_t nbPoints = indexCloud->size();
//        qDebug() << node->lambda1() << node->lambda2() << node->lambda3();

        for( size_t i = 0 ; i < nbPoints ; i++ )
        {
            pAccessor.pointAt( indexCloud->indexAt(i), curPoint );

            // On l'ajoute au nuage de sortie
            outPointCloud->addPoint( curPoint );

            stream << curPoint.x() << " " << curPoint.y() << " " << curPoint.z() << "\n";

            nOutputPoints++;
        }
    }
}
