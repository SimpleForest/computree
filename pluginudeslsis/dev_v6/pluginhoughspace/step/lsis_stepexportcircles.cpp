#include "lsis_stepexportcircles.h"

//In/Out dependencies
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"

//Itemdrawable dependencies
#include "ct_itemdrawable/ct_referencepoint.h"
#include "ct_shapedata/ct_circledata.h"
#include "ct_itemdrawable/ct_circle.h"
#include "ct_itemdrawable/ct_scene.h"
#include "ct_itemdrawable/ct_cylinder.h"
#include "ct_itemdrawable/ct_image2d.h"

//Tools dependencies
#include "ct_iterator/ct_pointiterator.h"
#include "ct_pointcloudindex/ct_pointcloudindexvector.h"
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_accessor/ct_pointaccessor.h"

//Qt dependencies
#include <QFile>
#include <QTextStream>

// Alias for indexing models
#define DEFin_rsltEstimatedCircles "DEFin_rsltEstimatedCircles"
#define DEFin_grpGrpGrp_PB_SGLF "DEFin_grpGrpGrp_PB_SGLF"
#define DEFin_grpGrpNiveauZ "DEFin_grpGrpNiveauZ"
#define DEFin_grpCluster "DEFin_grpCluster"
#define DEFin_itmCircles "DEFin_itmCircles"
#define DEFin_itmName "DEFin_itmName"
#define DEFin_itmNameField "itmNameField"

// Constructor : initialization of parameters
LSIS_StepExportCircles::LSIS_StepExportCircles(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _saveDir = "";
    _saveDirList.clear();
    _filePrefix = "cercles_";
    _optionalMnt = NULL;
}

// Step description (tooltip of contextual menu)
QString LSIS_StepExportCircles::getStepDescription() const
{
    return tr("Enregistre cercles de clusters");
}

// Step detailled description
QString LSIS_StepExportCircles::getStepDetailledDescription() const
{
    return tr("No detailled description for this step");
}

// Step URL
QString LSIS_StepExportCircles::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LSIS_StepExportCircles::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LSIS_StepExportCircles(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LSIS_StepExportCircles::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_rsltHitGridSparse = createNewInResultModelForCopy(DEFin_rsltEstimatedCircles,"Set of Circles");
    resIn_rsltHitGridSparse->setZeroOrMoreRootGroup();
    resIn_rsltHitGridSparse->addGroupModel("", DEFin_grpGrpGrp_PB_SGLF, CT_AbstractItemGroup::staticGetType(), tr("Input PB_SGLF Group"));
    resIn_rsltHitGridSparse->addGroupModel(DEFin_grpGrpGrp_PB_SGLF, DEFin_grpGrpNiveauZ, CT_AbstractItemGroup::staticGetType(), tr("Input NiveauZ Group"));
    resIn_rsltHitGridSparse->addGroupModel(DEFin_grpGrpNiveauZ, DEFin_grpCluster, CT_AbstractItemGroup::staticGetType(), tr("Input Cluster Group"));
    resIn_rsltHitGridSparse->addItemModel( DEFin_grpCluster, DEFin_itmCircles, CT_Circle::staticGetType(), tr("Cercle"));
    resIn_rsltHitGridSparse->addItemModel( DEFin_grpGrpNiveauZ, DEFin_itmName, CT_AbstractSingularItemDrawable::staticGetType(), tr("Name"), "", CT_InAbstractModel::C_ChooseOneIfMultiple, CT_InAbstractModel::F_IsOptional);
    resIn_rsltHitGridSparse->addItemAttributeModel(DEFin_itmName, DEFin_itmNameField, QList<QString>() << CT_AbstractCategory::DATA_VALUE, CT_AbstractCategory::ANY, tr("FileName"), "", CT_InAbstractModel::C_ChooseMultipleIfMultiple, CT_InAbstractModel::F_IsOptional);
}

// Creation and affiliation of OUT models
void LSIS_StepExportCircles::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities* resultModel = createNewOutResultModelToCopy(DEFin_rsltEstimatedCircles);
}

// Semi-automatic creation of step parameters DialogBox
void LSIS_StepExportCircles::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addFileChoice( "OutputDir", CT_FileChoiceButton::OneExistingFolder, "", _saveDirList );
    configDialog->addString("OutputFilePrefix", "", _filePrefix );
}

void LSIS_StepExportCircles::compute()
{
    if( _saveDirList.empty() )
    {
        qDebug() << "Il faut selectionner un dossier de sortie !";
        return;
    }
    _saveDir = _saveDirList.first();

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* res_rsltSetOfCircles= outResultList.at(0);

    QString outputCirclesFileName = _saveDir + "/" + _filePrefix + "_cercles.csv";
    QFile outputCirclesFile( outputCirclesFileName );
    if( !outputCirclesFile.open( QIODevice::WriteOnly ) )
    {
        qDebug() << "Impossible d'ouvrir le fichier " << outputCirclesFileName << " pour y ecrire";
        return;
    }
    QTextStream outputCirclesFileStream( &outputCirclesFile );

    size_t totalNumberOfSnakes = 0;
    // IN results browsing
    CT_ResultGroupIterator itIn_grpGrpGrpPB_SGLF( res_rsltSetOfCircles, this, DEFin_grpGrpGrp_PB_SGLF );
    while ( itIn_grpGrpGrpPB_SGLF.hasNext() && !isStopped() )
    {
        CT_StandardItemGroup* grpIn_grpGrpGrpPB_SGLFS = (CT_StandardItemGroup*) itIn_grpGrpGrpPB_SGLF.next();
        CT_GroupIterator itIn_grpGrpNiveauZ( grpIn_grpGrpGrpPB_SGLFS, this, DEFin_grpGrpNiveauZ );

        outputCirclesFileStream << "CenterX CenterY CenterZ Radius_m\n";
        while ( itIn_grpGrpNiveauZ.hasNext() && !isStopped() )
        {
            ++totalNumberOfSnakes;
            CT_StandardItemGroup* grpIn_grpGrpNiveauZ = (CT_StandardItemGroup*) itIn_grpGrpNiveauZ.next();

            const CT_AbstractSingularItemDrawable* itemIn_name = (CT_AbstractSingularItemDrawable*)grpIn_grpGrpNiveauZ->firstItemByINModelName(this, DEFin_itmName);
            QString baseFileName = "";
            if (itemIn_name != NULL)
            {
                baseFileName = itemIn_name->firstItemAttributeByINModelName(res_rsltSetOfCircles, this, DEFin_itmNameField)->toString(itemIn_name, NULL);
                baseFileName = QFileInfo(baseFileName).baseName();
            }

            CT_GroupIterator itIn_grpCluster( grpIn_grpGrpNiveauZ, this, DEFin_grpCluster );
            while ( itIn_grpCluster.hasNext() && !isStopped() )
            {
                CT_StandardItemGroup* grpIn_grpCluster = (CT_StandardItemGroup*) itIn_grpCluster.next();
                const CT_Circle* itemIn_itmCircle = (CT_Circle*)grpIn_grpCluster->firstItemByINModelName(this, DEFin_itmCircles);

                if( itemIn_itmCircle != NULL )
                {
                    outputCirclesFileStream << "Snake_"<<totalNumberOfSnakes  << " "
                                            << itemIn_itmCircle->getCenterX()  << " "
                                            << itemIn_itmCircle->getCenterY() << " "
                                            << itemIn_itmCircle->getCenterZ() << " "
                                            << itemIn_itmCircle->getRadius() << "\n";
                }
            }
        }
    }
    outputCirclesFile.close();
}
