/************************************************************************************
* Filename :  lsis_abstractvisitorgrid4d.h                                                            *
*                                                                                   *
* Copyright (C) 2015 Joris RAVAGLIA                                               *
* Author: Joris Ravaglia                                                            *
* Contact :  joris [dot] ravaglia [at] univ-amu [dot] fr                            *
*                                                                                   *
* This file is part of the pluginisolatecrowns plugin                               *
* for the CompuTree v3.0 software.                                                  *
* The pluginisolatecrowns plugin is free software :                                 *
* you can redistribute it and/or modify it under the terms of the                   *
* GNU Lesser General Public License as published by the Free Software Foundation    *
* either version 3 of the License, or (at your option) any later version.           *
*                                                                                   *
* The pluginisolatecrowns plugin is distributed in the hope that it will be useful, *
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY    *
* or FITNESS FOR A PARTICULAR PURPOSE.                                              *
* See the GNU Lesser General Public License for more details.                       *
*                                                                                   *
* You should have received a copy of the GNU Lesser General Public License          *
* along with this program. If not, see <http://www.gnu.org/licenses/>.              *
*                                                                                   *
************************************************************************************/

#ifndef LSIS_ABSTRACTVISITORGRID4D_H
#define LSIS_ABSTRACTVISITORGRID4D_H

#include "lsis_beam4d.h"

template< typename DataT >
class CT_Grid4D;

/*!
 * \brief The LSIS_AbstractVisitorGrid4D class
 *
 * Abstract visitor class.
 * Should be used during 4D raytracing :
 *
 * Each voxel pierced during the traversal algorithm is visited by a visitor that inherits of this abstract class.
 * (For more information, see the visitor design pattern : http://en.wikipedia.org/wiki/Visitor_pattern)
 *
 */
template < typename DataT >
class LSIS_AbstractVisitorGrid4D
{
public:
    /*!
     * \brief LSIS_AbstractVisitorGrid4D
     *
     * Constructor
     *
     * \param grid : Grid to be visited
     */
    LSIS_AbstractVisitorGrid4D( CT_Grid4D<DataT>* grid );

    /*!
     * \brief ~LSIS_AbstractVisitorGrid4D
     * Virtual destructor
     */
    virtual ~LSIS_AbstractVisitorGrid4D();

    /*!
     * \brief visit
     *
     * This method visits a given cell in the 4D
     *
     * \param levw : coordinate of the cell in the grid
     * \param levx : coordinate of the cell in the grid
     * \param levy : coordinate of the cell in the grid
     * \param levz : coordinate of the cell in the grid
     * \param beam : beam traversing the cell
     */
    virtual void visit(size_t levw, size_t levx, size_t levy, size_t levz, LSIS_Beam4D const * beam) = 0;

protected :
    CT_Grid4D<DataT>*   _grid; /*!< Grid to be visited */
};

// Include template implementation
#include "lsis_abstractvisitorgrid4d.hpp"

#endif // LSIS_ABSTRACTVISITORGRID4D_H
