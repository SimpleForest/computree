#ifndef LSIS_EFFECTIVEFILTERVISITOR_H
#define LSIS_EFFECTIVEFILTERVISITOR_H

// Inherits from abstract visitor
#include "./lsis_abstractvisitorgrid4d.h"

template < typename DataImage >
class CT_Grid4D;

template < typename DataT >
class LSIS_EffectiveFilterVisitor : public LSIS_AbstractVisitorGrid4D<DataT>
{
public:
    /*!
     * \brief LSIS_EffectiveFilterVisitor
     *
     * Constructeur
     *
     * \param grid : grille que le visiteur viste
     */
    LSIS_EffectiveFilterVisitor(CT_Grid4D<DataT>* grid);

    /*!
      * Destructeur (rien a faire, il ne doit pas liberer l'image qu'il visite!!)
      */
    virtual ~LSIS_EffectiveFilterVisitor();

    /*!
     * \brief visit
     *
     * \param levw : coordonnee du pixel a visiter
     * \param levx : coordonnee du pixel a visiter
     * \param levy : coordonnee du pixel a visiter
     * \param levz : coordonnee du pixel a visiter
     * \param beam : rayon qui traverse la grille
     */
    virtual void visit(size_t levw, size_t levx, size_t levy, size_t levz, LSIS_Beam4D const * beam);
};

// Inclusion des implementations template
#include "lsis_effectivefiltervisitor.hpp"

#endif // LSIS_EFFECTIVEFILTERVISITOR_H
