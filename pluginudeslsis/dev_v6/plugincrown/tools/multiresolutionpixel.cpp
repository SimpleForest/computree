#include "multiresolutionpixel.h"

SuperPixel::SuperPixel()
{
    _p.x() = -1;
    _p.y() = -1;
    _p.z() = -1;
    _res = -1;
}

SuperPixel::SuperPixel(const SuperPixel &sp)
{
    _p = sp._p;
    _res = sp._res;
    _botx = sp._botx;
    _boty = sp._boty;
    _botz = sp._botz;
    _topx = sp._topx;
    _topy = sp._topy;
    _topz = sp._topz;
}

SuperPixel::SuperPixel(int resolution)
{
    _p.x() = -1;
    _p.y() = -1;
    _p.z() = -1;
    _res = resolution;
}

SuperPixel::SuperPixel(int x,
                       int y,
                       int z,
                       int res,
                       CT_Grid3D<int> *grid)
{
    _p.x() = x;
    _p.y() = y;
    _p.z() = z;
    _res = res;

    getBBoxInGrid( _botx, _boty, _botz,
                   _topx, _topy, _topz,
                   grid );
}

SuperPixel::SuperPixel(int x,
                       int y,
                       int z,
                       int res,
                       ICRO_CustomVoronoiGrid *grid)
{
    _p.x() = x;
    _p.y() = y;
    _p.z() = z;
    _res = res;

    getBBoxInVoronoiGrid( _botx, _boty, _botz,
                          _topx, _topy, _topz,
                          grid );
}

SuperPixel::SuperPixel(Eigen::Vector3i &v, int res, CT_Grid3D<int> *grid)
{
    _p = v;
    _res = res;

    getBBoxInGrid( _botx, _boty, _botz,
                   _topx, _topy, _topz,
                   grid );
}

SuperPixel& SuperPixel::operator=(const SuperPixel& sp)
{
    _p = sp._p;
    _res = sp._res;
    _botx = sp._botx;
    _boty = sp._boty;
    _botz = sp._botz;
    _topx = sp._topx;
    _topy = sp._topy;
    _topz = sp._topz;

    return *this;
}

bool SuperPixel::containsNonLabeledPixelAboveValue(CT_Grid3D<int> *valueGrid,
                                                   CT_Grid3D<int> *labelGrid,
                                                   int valueMin)
{
    int x, y, z;
    BEGIN_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
    {
        if( labelGrid->value(x,y,z) == labelGrid->NA() )
        {
            if( valueGrid->value(x,y,z) >= valueMin )
            {
                return true;
            }
        }
    }
    END_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
    return false;
}

bool SuperPixel::containsNonLabeledPixelAboveValueVoronoi( CT_Grid3D<int> *valueGrid,
                                                           ICRO_CustomVoronoiGrid *labelGrid,
                                                           int valueMin)
{
    int x, y, z;
    BEGIN_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
    {
        if( labelGrid->distance(x,y,z) == labelGrid->NaDistance() )
        {
            if( valueGrid->value(x,y,z) >= valueMin )
            {
                return true;
            }
        }
    }
    END_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
    return false;
}

bool SuperPixel::containsValue(CT_Grid3D<int> *grid,
                               int value)
{
    int x, y, z;
    BEGIN_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
    {
        if( grid->value(x,y,z) == value )
        {
            return true;
        }
    }
    END_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
    return false;
}

bool SuperPixel::containsValueAbove(CT_Grid3D<int> *grid, int value)
{
    int x, y, z;
    BEGIN_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
    {
        if( grid->value(x,y,z) >= value )
        {
            return true;
        }
    }
    END_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
    return false;
}

void SuperPixel::setValueInGrid(CT_Grid3D<int> *grid,
                                int value)
{
    int x, y, z;
    BEGIN_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
    {
        grid->setValue(x,y,z,value);
    }
    END_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
}

void SuperPixel::setDistanceInVoronoiGrid(ICRO_CustomVoronoiGrid *grid,
                                          int value)
{
    int x, y, z;
    BEGIN_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
    {
        grid->setDistance(x,y,z,value);
    }
    END_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
}

void SuperPixel::addComponentInVoronoiGrid(ICRO_CustomVoronoiGrid *grid, ConnectedComponent *component)
{
    int x, y, z;
    BEGIN_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
    {
        grid->addComponent(x,y,z,component);
    }
    END_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
}

void SuperPixel::getPixels(QVector<Eigen::Vector3i> &outPixels)
{
    int x, y, z;
    Eigen::Vector3i v;
    BEGIN_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
    {
        v.x() = x;
        v.y() = y;
        v.z() = z;
        outPixels.push_back(v);
    }
    END_FOR_ALL_PIXELS_IN_MULTI_RESOLUTION_PIXEL( x, y, z )
}

void SuperPixel::getBBoxInVoronoiGrid(int &outBotx, int &outBoty, int &outBotz, int &outTopx, int &outTopy, int &outTopz, ICRO_CustomVoronoiGrid *grid)
{
    // Convertit le super pixel en pixels
    outBotx = _p.x() * _res;
    outBoty = _p.y() * _res;
    outBotz = _p.z() * _res;

    outTopx = std::min( ((_p.x()+1) * _res), (int)(grid->xArraySize()-1 ));
    outTopy = std::min( ((_p.y()+1) * _res), (int)(grid->yArraySize()-1 ));
    outTopz = std::min( ((_p.z()+1) * _res), (int)(grid->zArraySize()-1 ));
}
