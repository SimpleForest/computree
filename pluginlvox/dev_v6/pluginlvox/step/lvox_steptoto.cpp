#include "lvox_steptoto.h"

#include "ct_itemdrawable/abstract/ct_abstractitemdrawablewithpointcloud.h"
#include "ct_itemdrawable/ct_beam.h"
#include "ct_itemdrawable/tools/iterator/ct_groupiterator.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_view/ct_stepconfigurabledialog.h"

// Alias for indexing models
#define DEFin_tyty "tyty"
#define DEFin_a "a"
#define DEFin_b "b"
#define DEFin_c "c"



// Constructor : initialization of parameters
LVOX_Steptoto::LVOX_Steptoto(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
    _tutu = true;
    _zertwf = "val1";
}

// Step description (tooltip of contextual menu)
QString LVOX_Steptoto::getStepDescription() const
{
    return tr("description courte");
}

// Step detailled description
QString LVOX_Steptoto::getStepDetailledDescription() const
{
    return tr("description détaillée");
}

// Step URL
QString LVOX_Steptoto::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStep::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* LVOX_Steptoto::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new LVOX_Steptoto(dataInit);
}

//////////////////// PROTECTED METHODS //////////////////

// Creation and affiliation of IN models
void LVOX_Steptoto::createInResultModelListProtected()
{
    CT_InResultModelGroupToCopy *resIn_tyty = createNewInResultModelForCopy(DEFin_tyty, tr("hgf"), tr(""), true);
    resIn_tyty->setZeroOrMoreRootGroup();
    resIn_tyty->addGroupModel("", DEFin_a, CT_AbstractItemGroup::staticGetType(), tr("nb"));
    resIn_tyty->addItemModel(DEFin_a, DEFin_b, CT_AbstractItemDrawableWithPointCloud::staticGetType(), tr("nb"));
    resIn_tyty->addItemModel(DEFin_a, DEFin_c, CT_Beam::staticGetType(), tr("qwsdxfg"));

}

// Creation and affiliation of OUT models
void LVOX_Steptoto::createOutResultModelListProtected()
{
    CT_OutResultModelGroupToCopyPossibilities *resCpy_tyty = createNewOutResultModelToCopy(DEFin_tyty);

    resCpy_tyty->removeItemModel(DEFin_b);
    resCpy_tyty->addGroupModel(DEFin_a, _ghgh_ModelName, new CT_StandardItemGroup(), tr("ghghg"));

    resCpy_tyty->addItemModel(_ghgh_ModelName, _sfsdf_ModelName, new CT_Beam());
}

// Semi-automatic creation of step parameters DialogBox
void LVOX_Steptoto::createPostConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPostConfigurationDialog();

    configDialog->addBool("zqsedfrgty", "qsdfg", "sqdfgh", _tutu);

    QStringList list_zertwf;
    list_zertwf.append("val1");
    list_zertwf.append("val2");
    list_zertwf.append("val3");

    configDialog->addStringChoice("hnsgf", "fsdf", list_zertwf, _zertwf);
}

void LVOX_Steptoto::compute()
{
    // DONT'T FORGET TO ADD THIS STEP TO THE PLUGINMANAGER !!!!!

    QList<CT_ResultGroup*> outResultList = getOutResultList();
    CT_ResultGroup* resCpy_tyty = outResultList.at(0);

    // IN results browsing


    // COPIED results browsing
    CT_ResultGroupIterator itCpy_a(resCpy_tyty, this, DEFin_a);
    while (itCpy_a.hasNext() && !isStopped())
    {
        CT_StandardItemGroup* grpCpy_a = (CT_StandardItemGroup*) itCpy_a.next();
        
        const CT_Beam* itemCpy_c = (CT_Beam*)grpCpy_a->firstItemByINModelName(this, DEFin_c);
        if (itemCpy_c != NULL)
        {
        }
        CT_StandardItemGroup* grpCpy_ghgh= new CT_StandardItemGroup(_ghgh_ModelName.completeName(), resCpy_tyty);
        grpCpy_a->addGroup(grpCpy_ghgh);
        
        CT_Beam* itemCpy_sfsdf = new CT_Beam(_sfsdf_ModelName.completeName(), resCpy_tyty);
        grpCpy_ghgh->addItemDrawable(itemCpy_sfsdf);

    }
    


    // OUT results creation (move it to the appropried place in the code)


}
