#include "lvox3_stepfiltervoxelatdtm.h"

//In/Out
#include "ct_result/model/inModel/ct_inresultmodelgrouptocopy.h"
#include "ct_result/model/outModel/tools/ct_outresultmodelgrouptocopypossibilities.h"
#include "ct_result/ct_resultgroup.h"

//Tools
#include "ct_view/ct_stepconfigurabledialog.h"
#include "ct_view/tools/ct_configurablewidgettodialog.h"
#include "mk/tools/lvox3_errorcode.h"

//Drawables
#include "mk/tools/lvox3_gridtype.h"

//Models
#define DEF_SearchInSourceResult      "rs"
#define DEF_SearchInSourceGroup       "gs"
#define DEF_SearchInGrid              "grid"
#define DEF_SearchInDTM               "dtm"

//TODO: reimplement to permit to use the zlevel raster filter in the tools
LVOX3_StepFilterVoxelAtDTM::LVOX3_StepFilterVoxelAtDTM(CT_StepInitializeData &dataInit) : CT_AbstractStep(dataInit)
{
}

QString LVOX3_StepFilterVoxelAtDTM::getStepDescription() const
{
    return tr("Filtrage de voxels au niveau du MNT");
}

// Step detailed description
QString LVOX3_StepFilterVoxelAtDTM::getStepDetailledDescription() const
{
    return tr("Cette étape permet de filtrer tous les voxels se situant au niveau du MNT ou sous le MNT.");
}

CT_VirtualAbstractStep* LVOX3_StepFilterVoxelAtDTM::createNewInstance(CT_StepInitializeData &dataInit)
{
    // Creates an instance of this step
    return new LVOX3_StepFilterVoxelAtDTM(dataInit);
}

void LVOX3_StepFilterVoxelAtDTM::createInResultModelListProtected()
{
    // We must have
    // - at least one grid
    CT_InResultModelGroupToCopy *inResultRefCopy = createNewInResultModelForCopy(DEF_SearchInSourceResult, tr("Grille d'entrée"), "", true);
    inResultRefCopy->setZeroOrMoreRootGroup();
    inResultRefCopy->addGroupModel("", DEF_SearchInSourceGroup);
    //Needs a 3DGrid and a DTM, the grid is abstract because we don't want to restrict which grid can be added to the filter
    inResultRefCopy->addItemModel(DEF_SearchInSourceGroup, DEF_SearchInGrid, LVOX3_AbstractGrid3D::staticGetType(), tr("Grille 3D"));
    inResultRefCopy->addItemModel(DEF_SearchInSourceGroup, DEF_SearchInDTM, CT_Image2D<float>::staticGetType(), tr("DTM"));
}

void LVOX3_StepFilterVoxelAtDTM::createOutResultModelListProtected()
{
    // create a new OUT result that is a copy of the IN result selected by the user
    CT_OutResultModelGroupToCopyPossibilities *resultModel = createNewOutResultModelToCopy(DEF_SearchInSourceResult);

    if (!resultModel)
        return;

    resultModel->addItemModel(DEF_SearchInSourceGroup, _grid_ModelName, new lvox::Grid3Df(), tr("Grille 3D(filtrée)"));
}

void LVOX3_StepFilterVoxelAtDTM::compute()
{
    CT_ResultGroup* outResult = getOutResultList().first();

    CT_ResultGroupIterator itR(outResult, this, DEF_SearchInSourceGroup);

    CT_Image2D<float>* safetyDTM;

    //For every grid in the result
    while (itR.hasNext() && !isStopped())
    {
        CT_StandardItemGroup *group = dynamic_cast<CT_StandardItemGroup*>((CT_AbstractItemGroup*)itR.next());
        CT_Image2D<float>* dtm = (CT_Image2D<float>*)group->firstItemByINModelName(this, DEF_SearchInDTM);
        //If the DTM isn't null, to prevent the possibility of having one DTM and several scenes
        if(dtm !=NULL){
            safetyDTM = dtm;
        }else{
            dtm = safetyDTM;
        }

        LVOX3_AbstractGrid3D* inGrid = (LVOX3_AbstractGrid3D*) group->firstItemByINModelName(this, DEF_SearchInGrid);
        size_t counterVoxels = 0;
        size_t n_voxels;
        size_t filteredVoxels = 0;


        //If grid has data
        if (inGrid != NULL)
        {
            //Number of voxels depending on grid resolution
            n_voxels = (inGrid->xdim()*inGrid->ydim()*inGrid->zdim());
            //If grid has dimensions and voxel resolution
            if((inGrid->xdim() > 0)
                    && (inGrid->ydim() > 0)
                    && (inGrid->zdim() > 0)
                    && (inGrid->xresolution() > 0)
                    && (inGrid->yresolution() > 0)
                    && (inGrid->zresolution() > 0)) {
                //Declaring output grid to be able to export personalized grid of profile
                lvox::Grid3Df *outGrid = new lvox::Grid3Df(_grid_ModelName.completeName(), outResult, inGrid->minX(), inGrid->minY(), inGrid->minZ(), inGrid->xdim(), inGrid->ydim(), inGrid->zdim(), inGrid->xresolution(),inGrid->yresolution(),inGrid->zresolution(), lvox::Max_Error_Code, 0);
                group->addItemDrawable(outGrid);

                //Iterates through the inGrid and sends out the filtered grid
                for(size_t col = 0;col <inGrid->xdim() && (!isStopped());col++){
                    for(size_t lin = 0;lin <inGrid->ydim() && (!isStopped());lin++){
                        for(size_t level = 0;level <inGrid->zdim() && (!isStopped());level++){
                            Eigen::Vector3d centerCoordVoxel;
                            size_t index;
                            inGrid->index(col, lin, level, index); //updates param index to give index
                            double value = inGrid->valueAtIndexAsDouble(index);
                            inGrid->getCellCenterCoordinates(index,centerCoordVoxel); //updates param centerCoordVoxel for coordinates at current index

                            //makes sure if the voxel is, at least partly, above the DTM
                            if(evaluateVoxel(centerCoordVoxel, inGrid->zresolution(), dtm->valueAtCoords(centerCoordVoxel(0),centerCoordVoxel(1)))){
                                outGrid->addValueAtIndex(index,value);
                            }else{
                                outGrid->addValueAtIndex(index,0.0);
                                ++filteredVoxels;
                            }

                            ++counterVoxels;
                            // progress of 0 to 100%
                            setProgress((100.0*counterVoxels)/n_voxels);
                        }
                    }
                }
                outGrid->computeMinMax(); //Updates the lower and higher limit for visualization
            }
        }

        //Log messages to give the user feedback as to what was removed from one step to the other
        PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("La grille d'entrée comporte %1 voxels.")).arg(n_voxels));
        PS_LOG->addMessage(LogInterface::info, LogInterface::step, QString(tr("La grille de sortie comporte %1 voxels.")).arg(n_voxels - filteredVoxels));
    }
}

//Test to see if any part of the voxel is above the DTM level (if above return true, else return false)
bool LVOX3_StepFilterVoxelAtDTM::evaluateVoxel(Eigen::Vector3d centerCoords, double gridResolutionZ, float dtmZ){

    //If the upper part of the voxel is inside the dtm, it is removed.
    if(centerCoords(2)+gridResolutionZ <= dtmZ){
        return false;
    }
    //If half of the voxel is inside the dtm or lower, it is removed.
    if(centerCoords(2)<= dtmZ){
        return false;
    }
    //If only part of the lower part of the voxel is in the DTM, we let it slide
    if(centerCoords(2)-gridResolutionZ<= dtmZ){
        return true;
    }

    return true;
}
