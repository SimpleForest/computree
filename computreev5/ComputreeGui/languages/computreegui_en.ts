<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>ColorPickerPopup</name>
    <message>
        <source>Custom</source>
        <translation type="vanished">Custom</translation>
    </message>
</context>
<context>
    <name>DM_AbstractAttributes</name>
    <message>
        <source>Une erreur inconnu est survenu lors du traitement.</source>
        <translation type="vanished">An unknown error occurred during processing.</translation>
    </message>
</context>
<context>
    <name>DM_ContextMenuColouristAdder</name>
    <message>
        <source>Couleur uni</source>
        <translation type="vanished">Plain color</translation>
    </message>
    <message>
        <source>%1</source>
        <translation type="vanished">%1</translation>
    </message>
    <message>
        <source>Couleur automatique</source>
        <translation type="vanished">Automatic color</translation>
    </message>
    <message>
        <source>Couleur automatique (Couleur distincte)</source>
        <translation type="vanished">Automatic color (disctinct colors)</translation>
    </message>
    <message>
        <source>Couleur automatique (Gradient de couleur)</source>
        <translation type="vanished">Automatic color (color gradient)</translation>
    </message>
    <message>
        <source>Colorier par...</source>
        <translation type="vanished">Colorize by...</translation>
    </message>
    <message>
        <source>Colorier les points de chaque éléments par</source>
        <translation type="vanished">Colorize points of each element by</translation>
    </message>
    <message>
        <source>Colorier les points de tous les éléments par</source>
        <translation type="vanished">Colorize points of all elements by</translation>
    </message>
    <message>
        <source>Impossible de convertir l&apos;attribut %1 en valeur double ou booléenne</source>
        <translation type="vanished">Impossible to convert attribute %1 in double or boolean value</translation>
    </message>
</context>
<context>
    <name>DM_GuiManager</name>
    <message>
        <source>Veuillez patienter pendant l&apos;ajout du resultat au document actif.</source>
        <translation type="vanished">Please wait while adding the result to the current document.</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant l&apos;ajout des items au document actif.</source>
        <translation type="vanished">Please wait while adding items to the active document.</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant l&apos;ajout des CT_AbstractItemDrawable au document actif.</source>
        <translation type="vanished">Please wait while adding items to the active document.</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant la suppression du resultat du(des) document(s).</source>
        <translation type="vanished">Please wait while deleting the result from document(s).</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant la suppression des items du(des) document(s).</source>
        <translation type="vanished">Please wait while deleting items from document(s).</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant la suppression des items du document.</source>
        <translation type="vanished">Please wait while deleting items from the document.</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant la suppression des CT_AbstractItemDrawable du document actif.</source>
        <translation type="vanished">Please wait while deleting items from active document.</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant la suppression des CT_AbstractItemDrawable du(des) document(s).</source>
        <translation type="vanished">Please wait while deleting items from document(s).</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant la suppression de l&apos;etape.</source>
        <translation type="vanished">Please wait while deleting the step.</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant la suppression des etapes.</source>
        <translation type="vanished">Please wait while deleting steps.</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant le chargement des resultats.</source>
        <translation type="vanished">Please wait while loading the results.</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant l&apos;exportation.</source>
        <translation type="vanished">Please wait while export.</translation>
    </message>
</context>
<context>
    <name>DM_ItemDrawableTreeViewController</name>
    <message>
        <source>Veuillez patienter pendant la construction de la table</source>
        <translation type="vanished">Please wait while the construction of the table</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant le rafraichissement de la table</source>
        <translation type="vanished">Please wait while the refreshment of the table</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant l&apos;ajout des éléments à la table</source>
        <translation type="vanished">Please wait while adding items to the table</translation>
    </message>
</context>
<context>
    <name>DM_MultipleItemDrawableToOsgWorker</name>
    <message>
        <source>Veuillez patienter pendant la conversion des items.</source>
        <translation type="vanished">Please wait during items conversion</translation>
    </message>
</context>
<context>
    <name>DM_SortFilterMathProxyModel</name>
    <message>
        <source>Erreur dans l&apos;expression mathématique : </source>
        <translation type="vanished">Error in the mathematical expression: </translation>
    </message>
    <message>
        <source>Exception muParser : %1</source>
        <translation type="vanished">MuParser Exception: %1</translation>
    </message>
</context>
<context>
    <name>G3DGraphicsView</name>
    <message>
        <source>Colorer les points par...</source>
        <translation type="vanished">Colorize points by...</translation>
    </message>
    <message>
        <source>Colorer les faces par...</source>
        <translation type="vanished">Colorize faces by...</translation>
    </message>
    <message>
        <source>Colorer les edges par...</source>
        <translation type="vanished">Colorize edges by...</translation>
    </message>
    <message>
        <source>Configurer</source>
        <translation type="vanished">Configure</translation>
    </message>
</context>
<context>
    <name>GAboutDialog</name>
    <message>
        <source>A propos de Computree</source>
        <translation type="vanished">About CompuTree</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Computree 2.0&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Développé par :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Krebs Michaël (Arts et Métiers ParisTech site de Cluny, ARTS, Equipe Bois)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Alexandre Piboule (ONF)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Utilise les librairies :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;QGLViewer : &lt;/span&gt;&lt;a href=&quot;http://www.libqglviewer.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.libqglviewer.com/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;MuParser : &lt;/span&gt;&lt;a href=&quot;http://muparser.beltoforion.de/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://muparser.beltoforion.de/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Computree 3.0&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Developed by:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Krebs Michaël (Arts et Métiers ParisTech of Cluny, ARTS, Wood Team)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Alexandre Piboule (ONF)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Use libraries:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;QGLViewer : &lt;/span&gt;&lt;a href=&quot;http://www.libqglviewer.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.libqglviewer.com/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;MuParser : &lt;/span&gt;&lt;a href=&quot;http://muparser.beltoforion.de/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://muparser.beltoforion.de/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Fermer</source>
        <translation type="vanished">Close</translation>
    </message>
</context>
<context>
    <name>GAboutMemory</name>
    <message>
        <source>A propos de la mémoire...</source>
        <translation type="vanished">About memory...</translation>
    </message>
    <message>
        <source>Mémoire utilisée :</source>
        <translation type="vanished">Used memory:</translation>
    </message>
    <message>
        <source>Taille</source>
        <translation type="vanished">Size</translation>
    </message>
    <message>
        <source>Mémoire utilisée</source>
        <translation type="vanished">Used memory</translation>
    </message>
    <message>
        <source>Nuage global de points</source>
        <translation type="vanished">Global point cloud</translation>
    </message>
    <message>
        <source>Nuage global de faces</source>
        <translation type="vanished">Global face cloud</translation>
    </message>
    <message>
        <source>Nuage global d&apos;arêtes</source>
        <translation type="vanished">Global edge cloud</translation>
    </message>
    <message>
        <source>Index des points</source>
        <translation type="vanished">Points index</translation>
    </message>
    <message>
        <source>Index des faces</source>
        <translation type="vanished">Faces index</translation>
    </message>
    <message>
        <source>Index des arêtes</source>
        <translation type="vanished">Edges index</translation>
    </message>
    <message>
        <source>Nuages synchronisés aux points</source>
        <translation type="vanished">Clouds synchornized with points</translation>
    </message>
    <message>
        <source>Nuages synchronisés aux faces</source>
        <translation type="vanished">Clouds synchornized with faces</translation>
    </message>
    <message>
        <source>Nuages synchronisés aux arêtes</source>
        <translation type="vanished">Clouds synchornized with edges</translation>
    </message>
    <message>
        <source>%1</source>
        <translation type="vanished">%1</translation>
    </message>
    <message>
        <source>%1 (%2 éléments)</source>
        <translation type="vanished">%1 (%2 elements)</translation>
    </message>
</context>
<context>
    <name>GAboutPluginsDialog</name>
    <message>
        <source>A propos des plugins</source>
        <translation type="vanished">About plugins</translation>
    </message>
    <message>
        <source>Configurer</source>
        <translation type="vanished">Configure</translation>
    </message>
    <message>
        <source>Info Etape</source>
        <translation type="vanished">Step info</translation>
    </message>
    <message>
        <source>Exporter liste des étapes</source>
        <translation type="vanished">Export steps list</translation>
    </message>
    <message>
        <source>Export redmine</source>
        <translation type="vanished">Redmine export</translation>
    </message>
    <message>
        <source>Recharger</source>
        <translation type="vanished">Reload</translation>
    </message>
    <message>
        <source>Plugins d&apos;étapes</source>
        <translation type="vanished">Step plugins</translation>
    </message>
    <message>
        <source>Plugins</source>
        <translation type="vanished">Plugins</translation>
    </message>
    <message>
        <source>Normal Step</source>
        <translation type="vanished">Standard Step</translation>
    </message>
    <message>
        <source>Exporter</source>
        <translation type="vanished">Export</translation>
    </message>
    <message>
        <source>Vous ne pouvez pas recharger les plugins tant que vous avez des étapes dans l&apos;arbre des traitements</source>
        <translation type="vanished">You can&apos;t re-load plugins while it remains steps in the Step Manager</translation>
    </message>
    <message>
        <source>Choisir le fichier d&apos;export</source>
        <translation type="vanished">Choose export file</translation>
    </message>
    <message>
        <source>Fichier texte (*.txt)</source>
        <translation type="vanished">Text file (*.txt)</translation>
    </message>
    <message>
        <source>h1. Etapes du plugin </source>
        <translation type="vanished">h1. Plugin&apos;s Steps</translation>
    </message>
    <message>
        <source>h1. Etapes de chargement</source>
        <translation type="vanished">h1. Load file Steps</translation>
    </message>
    <message>
        <source>h1. Etapes pouvant être ajoutées en premier</source>
        <translation type="vanished">h1. Can be added first Steps</translation>
    </message>
    <message>
        <source>h1. Etapes standard</source>
        <translation type="vanished">h1. Standard Steps</translation>
    </message>
    <message>
        <source>*_Description courte_* : *</source>
        <translation type="vanished">*_Short description_* : *</translation>
    </message>
    <message>
        <source>*_Description détaillée_* : </source>
        <translation type="vanished">*_Detailed description_* : </translation>
    </message>
</context>
<context>
    <name>GAboutStepDialog</name>
    <message>
        <source>Documentation de l&apos;étape</source>
        <translation type="vanished">Step documentation</translation>
    </message>
    <message>
        <source>Nom du plugin</source>
        <translation type="vanished">Plugin name</translation>
    </message>
    <message>
        <source>Nom de l&apos;étape</source>
        <translation type="vanished">Step name</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Page wiki : &lt;a href=&quot;http://rdinnovation.onf.fr&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://rdinnovation.onf.fr&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wiki page: &lt;a href=&quot;http://rdinnovation.onf.fr&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://rdinnovation.onf.fr&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Aide en ligne : &lt;a href=&quot;%1&quot;&gt;Page internet du plugin&lt;/a&gt;</source>
        <translation type="vanished">Online help: &lt;a href=&quot;%1&quot;&gt;Internet page of the plugin&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Aide en ligne : &lt;a href=&quot;%1&quot;&gt;Page internet de cette étape&lt;/a&gt;</source>
        <translation type="vanished">Online help: &lt;a href=&quot;%1&quot;&gt;Internet page of this step&lt;/a&gt;</translation>
    </message>
    <message>
        <source>&lt;em&gt;Description détaillée&lt;/em&gt; :&lt;br&gt;&lt;br&gt;</source>
        <translation type="vanished">&lt;em&gt;Detailled description&lt;/em&gt; :&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>Données en entrée :</source>
        <translation type="vanished">Input data:</translation>
    </message>
    <message>
        <source>Données en sortie :</source>
        <translation type="vanished">Output data:</translation>
    </message>
    <message>
        <source>IN Models :</source>
        <translation type="vanished">In Models:</translation>
    </message>
    <message>
        <source>OUT Models :</source>
        <translation type="vanished">OUT Models:</translation>
    </message>
</context>
<context>
    <name>GActionsManager</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Actions Manager</translation>
    </message>
    <message>
        <source>Interne</source>
        <translation type="vanished">Internal</translation>
    </message>
</context>
<context>
    <name>GCameraCoordinatesOptions</name>
    <message>
        <source>Appliquer</source>
        <translation type="vanished">Apply</translation>
    </message>
</context>
<context>
    <name>GCameraGraphicsOptions</name>
    <message>
        <source>Centrer la vue sur l&apos;origine</source>
        <translation type="vanished">Center the view on the origin</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ajuster la caméra ainsi que le point de pivot aux éléments visibles&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ajust camera and pivot point to visible elements&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Déplacer la caméra</source>
        <translation type="vanished">Move the camera</translation>
    </message>
    <message>
        <source>Centrer la vue sur le barycentre des éléments présents dans la vue</source>
        <translation type="vanished">Center the view on the centroid of the elements in the view</translation>
    </message>
    <message>
        <source>Centrer la vue sur le barycentre des éléments sélectionnés</source>
        <translation type="vanished">Center the view on the centroid of the selected items</translation>
    </message>
    <message>
        <source>Vue personnalisée</source>
        <translation type="vanished">Personalized view</translation>
    </message>
    <message>
        <source>Ajuster la caméra aux éléments visibles</source>
        <translation type="vanished">Adjust camera to visible elements</translation>
    </message>
    <message>
        <source>Définir le point de pivot à l&apos;origine (0,0,0)</source>
        <translation type="vanished">Define the pivot point to the origine (0,0,0)</translation>
    </message>
    <message>
        <source>Définir le point de pivot au barycentre des éléments visibles</source>
        <translation type="vanished">Define the pivot point to the barycenter of visible elements</translation>
    </message>
    <message>
        <source>Définir le point de pivot au barycentre des éléments sélectionnés</source>
        <translation type="vanished">Define the pivot point to the barycenter of selected elements</translation>
    </message>
    <message>
        <source>Desynchroniser ce document</source>
        <translation type="vanished">Unsynchronize this document</translation>
    </message>
    <message>
        <source>Synchroniser ce document</source>
        <translation type="vanished">Synchronize this document</translation>
    </message>
</context>
<context>
    <name>GDocumentViewForGraphics</name>
    <message>
        <source>Impossible d&apos;affecter une couleur à un item dont le résultat est NULL</source>
        <translation type="vanished">Impossible to set colorfor item with NULL result</translation>
    </message>
    <message>
        <source>Impossible de récupérer une couleur d&apos;un item dont le résultat est NULL</source>
        <translation type="vanished">Impossible to retrieve color for item with NULL result</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant le traitement...</source>
        <translation type="vanished">Please wait while processing ...</translation>
    </message>
    <message>
        <source>Point de vue</source>
        <translation type="vanished">Point of view</translation>
    </message>
    <message>
        <source>Veuillez entrer un nom pour le point de vue :</source>
        <translation type="vanished">Please enter a name for the point of view:</translation>
    </message>
    <message>
        <source>Passer en vue Perspective</source>
        <translation type="vanished">Swich to perspective view</translation>
    </message>
    <message>
        <source>Passer en vue Orthoscopique</source>
        <translation type="vanished">Swich to orthographic view</translation>
    </message>
    <message>
        <source>Exporter sous...</source>
        <translation type="vanished">Export to...</translation>
    </message>
    <message>
        <source>Erreur</source>
        <translation type="vanished">Error</translation>
    </message>
    <message>
        <source>Enregistrer une capture d&apos;écran</source>
        <translation type="vanished">Save screen capture</translation>
    </message>
    <message>
        <source>Exporter les éléments sélectionnés</source>
        <translation type="vanished">Export selected items</translation>
    </message>
    <message>
        <source>Ajouter les éléments sélectionnés au document...</source>
        <translation type="vanished">Add selected elements to document #...</translation>
    </message>
    <message>
        <source>Configurer les couleurs des points</source>
        <translation type="vanished">Configure colors of items</translation>
    </message>
    <message>
        <source>Augmenter la taille des pixels</source>
        <translation type="vanished">Increase pixel size</translation>
    </message>
    <message>
        <source>Diminuer la taille des pixels</source>
        <translation type="vanished">Decrease pixel size</translation>
    </message>
    <message>
        <source>Mode simplifié (lors de l&apos;arrêt du mouvement)</source>
        <translation type="vanished">Simplified mode (only during movements)</translation>
    </message>
    <message>
        <source>Mode simplifié (toujours)</source>
        <translation type="vanished">Simplified mode (Always)</translation>
    </message>
    <message>
        <source>Mode simplifié (jamais)</source>
        <translation type="vanished">Simplified mode (never)</translation>
    </message>
    <message>
        <source>Ajouter les items sélectionnés au document</source>
        <translation type="vanished">Add selected items to document #...</translation>
    </message>
    <message>
        <source>Changer la taille des pixels</source>
        <translation type="vanished">Change pixels size</translation>
    </message>
    <message>
        <source>Changer le mode de dessin :
- Simplifié lors des déplacements
- Toujours Simplifié- Jamais Simplifié</source>
        <translation type="vanished">Change the drawing mode: 
- Simplified when traveling 
- Always Simple 
- Never Simplified</translation>
    </message>
    <message>
        <source>(Re)construire un octree</source>
        <translation type="vanished">(Re)create octree</translation>
    </message>
    <message>
        <source>Configurer l&apos;affichage</source>
        <translation type="vanished">Configuring the display</translation>
    </message>
</context>
<context>
    <name>GFavoritesMenuDialog</name>
    <message>
        <source>Ajout d&apos;une étape aux favoris</source>
        <translation type="vanished">Add one step to favorites</translation>
    </message>
    <message>
        <source>Arbre des niveaux :</source>
        <translation type="vanished">Levels tree:</translation>
    </message>
    <message>
        <source>Nom</source>
        <translation type="vanished">Name</translation>
    </message>
    <message>
        <source>Nombre d&apos;étapes</source>
        <translation type="vanished">Number of steps</translation>
    </message>
    <message>
        <source>Ajouter un niveau racine</source>
        <translation type="vanished">Add a root level</translation>
    </message>
    <message>
        <source>Ajouter un sous-niveau</source>
        <translation type="vanished">Add a sub-level</translation>
    </message>
    <message>
        <source>Supprimer le niveau</source>
        <translation type="vanished">Delete current level</translation>
    </message>
    <message>
        <source>Niveau</source>
        <translation type="vanished">Level</translation>
    </message>
    <message>
        <source>Vous devez créer un niveau pour ajouter l&apos;étape &quot;%1&quot;</source>
        <translation type="vanished">You have to first create a level to add the &quot;%1&quot; step</translation>
    </message>
    <message>
        <source>Ajout de l&apos;étape &quot;%1&quot; au niveau &quot;%2&quot;</source>
        <translation type="vanished">Add step &quot;%1&quot; to level &quot;%2&quot;</translation>
    </message>
    <message>
        <source>Ajouter un niveau</source>
        <translation type="vanished">Add level</translation>
    </message>
    <message>
        <source>Supprimer</source>
        <translation type="vanished">Delete</translation>
    </message>
    <message>
        <source>Nom du niveau</source>
        <translation type="vanished">Level name</translation>
    </message>
    <message>
        <source>Erreur</source>
        <translation type="vanished">Error</translation>
    </message>
    <message>
        <source>Un niveau ayant ce nom existe déjà</source>
        <translation type="vanished">A level with this name already exist</translation>
    </message>
    <message>
        <source>Nom du sous-niveau</source>
        <translation type="vanished">Sub-level name</translation>
    </message>
    <message>
        <source>Question</source>
        <translation type="vanished">Question</translation>
    </message>
    <message>
        <source>Voulez vous vraiment supprimer ce niveau ?</source>
        <translation type="vanished">Do you really wan&apos;t to delete this level ?</translation>
    </message>
</context>
<context>
    <name>GGraphicsViewImp</name>
    <message>
        <source>Centre de la scene : %1 | %2 | %3</source>
        <translation type="vanished">Scene center : %1 | %2 | %3</translation>
    </message>
    <message>
        <source>Suppression</source>
        <translation type="vanished">Removing</translation>
    </message>
    <message>
        <source>Voulez-vous supprimer les items sélectionnés de la vue ?</source>
        <translation type="vanished">Do you want to remove selected items from the view?</translation>
    </message>
</context>
<context>
    <name>GGraphicsViewOptions</name>
    <message>
        <source>Configuration du graphique</source>
        <translation type="vanished">Graphic configuration</translation>
    </message>
    <message>
        <source>Nombres de points maximum en mode simplifié :</source>
        <translation type="vanished">Maximum number of displayed points in simplified mode:</translation>
    </message>
    <message>
        <source>Informations</source>
        <translation type="vanished">Information</translation>
    </message>
    <message>
        <source> Couleurs et tailles </source>
        <translation type="vanished"> Colors and sizes </translation>
    </message>
    <message>
        <source>Mode de dessin simplifié préféré :</source>
        <translation type="vanished">Prefered simplified mode</translation>
    </message>
    <message>
        <source>Mode simplifié (lors de l&apos;arrêt du mouvement)</source>
        <translation type="vanished">Simplified mode (only during movements)</translation>
    </message>
    <message>
        <source>Mode simplifié (toujours)</source>
        <translation type="vanished">Simplified mode (Always)</translation>
    </message>
    <message>
        <source>Mode simplifié (jamais)</source>
        <translation type="vanished">Simplified mode (never)</translation>
    </message>
    <message>
        <source>Couleur d&apos;arrière plan :</source>
        <translation type="vanished">Background color:</translation>
    </message>
    <message>
        <source>Couleur de sélection :</source>
        <translation type="vanished">Selection color:</translation>
    </message>
    <message>
        <source>Taille des points :</source>
        <translation type="vanished">Points size:</translation>
    </message>
    <message>
        <source> Dessin </source>
        <translation type="vanished"> Drawing </translation>
    </message>
    <message>
        <source>Affichage des axes</source>
        <translation type="vanished">Axis display</translation>
    </message>
    <message>
        <source>Affichage de la grille</source>
        <translation type="vanished">Grid display</translation>
    </message>
    <message>
        <source>Activer la transparence</source>
        <translation type="vanished">Enable transparency</translation>
    </message>
    <message>
        <source>Activer la lumière</source>
        <translation type="vanished">Enable light</translation>
    </message>
    <message>
        <source> Optimisation </source>
        <translation type="vanished"> Optimization </translation>
    </message>
    <message>
        <source>Toujours utiliser l&apos;optimisation</source>
        <translation type="vanished">Always use optimization</translation>
    </message>
    <message>
        <source>Utiliser l&apos;optimisation seulement pendant les déplacements</source>
        <translation type="vanished">Use optimization only when traveling</translation>
    </message>
    <message>
        <source>Ne jamais utiliser l&apos;optimisation</source>
        <translation type="vanished">Never use optimization</translation>
    </message>
    <message>
        <source>Redessiner sans optimisation après :</source>
        <translation type="vanished">Redraw without optimization after:</translation>
    </message>
    <message>
        <source>Minimum d&apos;image par seconde (FPS) à obtenir</source>
        <translation type="vanished">Minimum frame per second (FPS) to obtain</translation>
    </message>
    <message>
        <source> Coordonnées de la caméra </source>
        <translation type="vanished"> Coordinates of the camera </translation>
    </message>
    <message>
        <source> Caméra </source>
        <translation type="vanished">Camera</translation>
    </message>
    <message>
        <source>Centre de la vue</source>
        <translation type="vanished">Center of the view</translation>
    </message>
    <message>
        <source> Affichage </source>
        <translation type="vanished">Display</translation>
    </message>
    <message>
        <source>Coin haut gauche</source>
        <translation type="vanished">Top left corner</translation>
    </message>
    <message>
        <source>Coin haut droit</source>
        <translation type="vanished">Top right corner</translation>
    </message>
    <message>
        <source>Coin bas droit</source>
        <translation type="vanished">Bottom right corner</translation>
    </message>
    <message>
        <source>Coin bas gauche</source>
        <translation type="vanished">Bottom left corner</translation>
    </message>
    <message>
        <source> Vue </source>
        <translation type="vanished"> View </translation>
    </message>
    <message>
        <source>Orthographique</source>
        <translation type="vanished">Orthographic</translation>
    </message>
    <message>
        <source> Octree </source>
        <translation type="vanished">Octree</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="vanished">2</translation>
    </message>
    <message>
        <source>Nombre de cellules</source>
        <translation type="vanished">Number of cells</translation>
    </message>
    <message>
        <source>Afficher</source>
        <translation type="vanished">Activate</translation>
    </message>
    <message>
        <source>Sauvegarder par défaut</source>
        <translation type="vanished">Save by default</translation>
    </message>
    <message>
        <source>Sauvegarde réussi</source>
        <translation type="vanished">Saved with success</translation>
    </message>
    <message>
        <source>La sauvegarde de la configuration a réussi.</source>
        <translation type="vanished">The configuration backup was successful.</translation>
    </message>
    <message>
        <source>Sauvegarde réussie</source>
        <translation type="vanished">Backup successfull</translation>
    </message>
    <message>
        <source>Erreur lors de la sauvegarde</source>
        <translation type="vanished">Error when saving</translation>
    </message>
    <message>
        <source>La sauvegarde de la configuration a échoué.</source>
        <translation type="vanished">The configuration backup has failed.</translation>
    </message>
    <message>
        <source>La sauvegarde de la configuration a Ühoué.</source>
        <translation type="vanished">The configuration backup failed.</translation>
    </message>
</context>
<context>
    <name>GINeedHelpDialog</name>
    <message>
        <source>Besoin d&apos;aide ?</source>
        <translation type="vanished">Need help ?</translation>
    </message>
    <message>
        <source>Pour le &lt;b&gt;sommaire de l&apos;aide Computree&lt;/b&gt; :</source>
        <translation type="vanished">To access the &lt;b&gt;Computree help summary&lt;/b&gt;:</translation>
    </message>
    <message>
        <source>-&gt; &lt;a href=&quot;http://rdinnovation.onf.fr/projects/computree/wiki&quot;&gt;Sommaire de la Wiki Computree&lt;/a&gt;</source>
        <translation type="vanished">-&gt; &lt;a href=&quot;http://rdinnovation.onf.fr/projects/computree/wiki&quot;&gt;Computree Wiki summary&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Pour de l&apos;aide sur &lt;b&gt;l&apos;utilisation de l&apos;interface graphique&lt;/b&gt; :</source>
        <translation type="vanished">For help on &lt;b&gt;graphical interface use&lt;/b&gt;:</translation>
    </message>
    <message>
        <source>-&gt; &lt;a href=&quot;http://rdinnovation.onf.fr/projects/computree/wiki/Fr_computreeGUI&quot;&gt;Wiki sur l&apos;interface utilisateur&lt;/a&gt;</source>
        <translation type="vanished">-&gt; &lt;a href=&quot;http://rdinnovation.onf.fr/projects/computree/wiki/En_computreeGUI&quot;&gt;User interface Wiki&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Pour de l&apos;aide sur &lt;b&gt;une étape insérée&lt;/b&gt; :</source>
        <translation type="vanished">For help on &lt;b&gt; an inserted step&lt;/b&gt;:</translation>
    </message>
    <message>
        <source>-&gt; Faire un clic droit sur l&apos;étape insérée, puis &lt;i&gt;Informations sur l&apos;étape&lt;/i&gt;</source>
        <translation type="vanished">-&gt; Right clic on the inserted step, and next on &lt;i&gt;Informations about step&lt;/i&gt;</translation>
    </message>
    <message>
        <source>Pour de l&apos;aide sur &lt;b&gt;une étape dans un plugin (pas encore insérée)&lt;/b&gt; :</source>
        <translation type="vanished">For help on &lt;b&gt;a step in a plugin (not already inserted)&lt;/b&gt;:</translation>
    </message>
    <message>
        <source>-&gt; Dans le sous-menu d&apos;insertion d&apos;étape du plugin, sur l&apos;étape, faire F1</source>
        <translation type="vanished">-&gt; In the plugin&apos;s sub-menu for step insertion, on the considered step, hit F1</translation>
    </message>
    <message>
        <source>-&gt; Dans la barre de menu, cliquer sur &lt;i&gt;Aide&lt;/i&gt;, puis &lt;i&gt;A propos des Plugins&lt;/i&gt;</source>
        <translation type="vanished">-&gt; In the menu bar, clic on &lt;i&gt;Help&lt;/i&gt;, and next on &lt;i&gt;About Plugins&lt;/i&gt;</translation>
    </message>
    <message>
        <source>-&gt; Déplier &lt;i&gt;Plugins d&apos;étapes&lt;/i&gt;, déplier le plugin d&apos;intérêt, séléctionner l&apos;étape, puis cliquer sur &lt;i&gt;Info Etape&lt;/i&gt;</source>
        <translation type="vanished">-&gt; Unfold &lt;i&gt;Step plugins&lt;/i&gt;, unfold the concerned plugin, select the step and clic on &lt;i&gt;Step info&lt;/i&gt;</translation>
    </message>
    <message>
        <source>Pour de l&apos;aide sur &lt;b&gt;comment réaliser une tâche précise à l&apos;aide des étapes disponibles&lt;/b&gt; :</source>
        <translation type="vanished">For help on &lt;b&gt;How to achieve some specific task with available steps&lt;/b&gt;:</translation>
    </message>
    <message>
        <source>-&gt; Consulter les tutoriels disponibles ici : &lt;a href=&quot;http://rdinnovation.onf.fr/projects/computree/wiki/Fr_tutorials&quot;&gt;Tutoriels thématiques (How To&apos;s)&lt;/a&gt;</source>
        <translation type="vanished">-&gt; Consult How To&apos;s available here: &lt;a href=&quot;http://rdinnovation.onf.fr/projects/computree/wiki/En_tutorials&quot;&gt;How To&apos;s (thematic tutorials)&lt;/a&gt;</translation>
    </message>
    <message>
        <source>-&gt; Faire un clic droit sur l&apos;étape insérée, puis &lt;i&gt;Documentation de l&apos;étape&lt;/i&gt;</source>
        <translation type="vanished">-&gt; Right clic on the inserted step, and next on &lt;i&gt;Step Documentation&lt;/i&gt;</translation>
    </message>
    <message>
        <source>Pour de l&apos;aide sur &lt;b&gt;une étape du menu (pas encore insérée)&lt;/b&gt; :</source>
        <translation type="vanished">For help on &lt;b&gt;a step in the menu (not already inserted)&lt;/b&gt;:</translation>
    </message>
    <message>
        <source>-&gt; Faire un clic droit sur l&apos;étape dans le menu, puis &lt;i&gt;Documentation de l&apos;étape&lt;/i&gt;</source>
        <translation type="vanished">-&gt; Right clic on the step in the menu, and next on &lt;i&gt;Step Documentation&lt;/i&gt;</translation>
    </message>
    <message>
        <source>Pour &lt;b&gt;signaler un bug&lt;/b&gt; ou &lt;b&gt;suggérer une amélioration&lt;/b&gt; :</source>
        <translation type="vanished">To &lt;b&gt;report a bug&lt;/b&gt; or &lt;b&gt;propose an improvement&lt;/b&gt;:</translation>
    </message>
    <message>
        <source>-&gt; Créer une nouvelle demande : &lt;a href=&quot;http://rdinnovation.onf.fr/projects/computree/issues/new&quot;&gt;Nouvelle demande&lt;/a&gt;</source>
        <translation type="vanished">-&gt; Create a new issue: &lt;a href=&quot;http://rdinnovation.onf.fr/projects/computree/issues/new&quot;&gt;New issue&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Pour &lt;b&gt;poster un message sur le forum Computree&lt;/b&gt; :</source>
        <translation type="vanished">To &lt;b&gt;post a message on Computree forum&lt;/b&gt;:</translation>
    </message>
    <message>
        <source>-&gt; &lt;a href=&quot;http://rdinnovation.onf.fr/projects/computree/boards&quot;&gt;Forum Computree&lt;/a&gt;</source>
        <translation type="vanished">-&gt; &lt;a href=&quot;http://rdinnovation.onf.fr/projects/computree/boards&quot;&gt;Computree Forum&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>GItemDrawableConfigurationManagerView</name>
    <message>
        <source>........... Aucun element configurable dans la vue 3D ...........</source>
        <translation type="vanished">........... No configurable element in the 3D view ...........</translation>
    </message>
    <message>
        <source>Aucun élément dans la vue 3D</source>
        <translation type="vanished">No element in 3D view</translation>
    </message>
    <message>
        <source>Nom</source>
        <translation type="vanished">Name</translation>
    </message>
    <message>
        <source>Valeur</source>
        <translation type="vanished">Value</translation>
    </message>
    <message>
        <source>Afficher</source>
        <translation type="vanished">Activate</translation>
    </message>
    <message>
        <source>[GItemDrawableConfigurationManagerView] Modification du type &quot;%1&quot; non implementé.</source>
        <translation type="vanished">[GItemDrawableConfigurationManagerView] Modification of type &quot;%1&quot; not implemented.</translation>
    </message>
    <message>
        <source>Appliquer</source>
        <translation type="vanished">Apply</translation>
    </message>
</context>
<context>
    <name>GItemDrawableManagerOptionsColor</name>
    <message>
        <source>Editeur de couleurs</source>
        <translation type="vanished">Colors editor</translation>
    </message>
    <message>
        <source>Ajouter une couleur</source>
        <translation type="vanished">Add a color</translation>
    </message>
    <message>
        <source>Supprimer</source>
        <translation type="vanished">Delete</translation>
    </message>
    <message>
        <source>Ouvrir</source>
        <translation type="vanished">Open</translation>
    </message>
    <message>
        <source>Sauvegarder sous...</source>
        <translation type="vanished">Save to...</translation>
    </message>
    <message>
        <source>Sauvegarder par défaut</source>
        <translation type="vanished">Save by default</translation>
    </message>
    <message>
        <source>Color File (*%1</source>
        <translation type="vanished">Fichier de couleur (*%1</translation>
    </message>
    <message>
        <source>Ouvrir un fichier de couleur</source>
        <translation type="vanished">Open a color file</translation>
    </message>
    <message>
        <source>Sauvegarder un fichier de couleur</source>
        <translation type="vanished">Save a color file</translation>
    </message>
    <message>
        <source>Sauvegarde russi</source>
        <translation type="vanished">Saved with success</translation>
    </message>
    <message>
        <source>La sauvegarde de la liste des couleurs a russi.</source>
        <translation type="vanished">The list of colors was successfully saved.</translation>
    </message>
    <message>
        <source>Erreur lors de la sauvegarde</source>
        <translation type="vanished">Error when saving</translation>
    </message>
    <message>
        <source>La sauvegarde de la liste des couleurs a chou.</source>
        <translation type="vanished">An error has occurred when saving the list of colors.</translation>
    </message>
    <message>
        <source>La sauvegarde de la liste des couleurs par dfaut a russi.</source>
        <translation type="vanished">The default list of colors was successfully saved.</translation>
    </message>
    <message>
        <source>La sauvegarde de la liste des couleurs par defaut a chou.</source>
        <translation type="vanished">An error has occurred when saving the default list of colors.</translation>
    </message>
</context>
<context>
    <name>GItemDrawableModelManager</name>
    <message>
        <source>Nom</source>
        <translation type="vanished">Name</translation>
    </message>
    <message>
        <source>Couleur uni</source>
        <translation type="vanished">Plain color</translation>
    </message>
    <message>
        <source>%1</source>
        <translation type="vanished">%1</translation>
    </message>
    <message>
        <source>Couleur automatique</source>
        <translation type="vanished">Automatic color</translation>
    </message>
    <message>
        <source>Colorier par...</source>
        <translation type="vanished">Colorize by...</translation>
    </message>
</context>
<context>
    <name>GItemModelViewSyncChooseDialog</name>
    <message>
        <source>Synchroniser avec...</source>
        <translation type="vanished">Sync with...</translation>
    </message>
</context>
<context>
    <name>GLogWidget</name>
    <message>
        <source>Disable</source>
        <translation type="vanished">Désactiver</translation>
    </message>
</context>
<context>
    <name>GMainProgressDialog</name>
    <message>
        <source>Progression</source>
        <translation type="vanished">Progress</translation>
    </message>
    <message>
        <source>Annuler</source>
        <translation type="vanished">Cancel</translation>
    </message>
</context>
<context>
    <name>GMainWindow</name>
    <message>
        <source>Fichier</source>
        <translation type="vanished">File</translation>
    </message>
    <message>
        <source>Edition</source>
        <translation type="vanished">Edit</translation>
    </message>
    <message>
        <source>Aide</source>
        <translation type="vanished">Help</translation>
    </message>
    <message>
        <source>Fenêtre</source>
        <translation type="vanished">Window</translation>
    </message>
    <message>
        <source>Vue</source>
        <translation type="vanished">View</translation>
    </message>
    <message>
        <source>Langue</source>
        <translation type="vanished">Language</translation>
    </message>
    <message>
        <source>toolBar</source>
        <translation type="vanished">ToolBar</translation>
    </message>
    <message>
        <source>StepManager</source>
        <translation type="vanished">Step manager</translation>
    </message>
    <message>
        <source>ModelManager</source>
        <translation type="vanished">Model manager</translation>
    </message>
    <message>
        <source>ItemConfigurator</source>
        <translation type="vanished">Item configurator</translation>
    </message>
    <message>
        <source>Synchronisation des vues</source>
        <translation type="vanished">Synchronization of views</translation>
    </message>
    <message>
        <source>Attention</source>
        <translation type="vanished">Warning</translation>
    </message>
    <message>
        <source>Vous êtes dans le mode manuel, veuillez quitter ce mode avant de fermer l&apos;application.</source>
        <translation type="vanished">You are in the manual mode, please exit this mode before close the application.</translation>
    </message>
    <message>
        <source>Une étape est en cours de traiements, veuillez terminer les traitements avant de fermer l&apos;application.</source>
        <translation type="vanished">A step is under treatment, please complete the treatment before closing the application.</translation>
    </message>
    <message>
        <source>Ouvrir un fichier</source>
        <translation type="vanished">Open a file</translation>
    </message>
    <message>
        <source>Erreur chargement du script</source>
        <translation type="vanished">Error load script</translation>
    </message>
    <message>
        <source>Une erreur est survenu lors de la lecture du script :

%1</source>
        <translation type="vanished">An error occured while reading the script:

%1</translation>
    </message>
    <message>
        <source>Sauvegarder l&apos;arbre des tapes sous...</source>
        <translation type="vanished">Save the tree steps as ...</translation>
    </message>
    <message>
        <source>Ajouter une étape</source>
        <translation type="vanished">Add a step</translation>
    </message>
    <message>
        <source>Ajouter une étape qui n&apos;a pas besoin de résultat en entrÞ</source>
        <translation type="vanished">Add a step that can be added first</translation>
    </message>
    <message>
        <source>Ajouter une étape qui n&apos;a pas besoin de résultat en entrée</source>
        <translation type="vanished">Add a step which doesn&apos;t need input result</translation>
    </message>
    <message>
        <source>Lancer les traitements</source>
        <translation type="vanished">Start processes</translation>
    </message>
    <message>
        <source>Valider le mode manuel et continuer les traitements automatiques</source>
        <translation type="vanished">Enable manual mode and continue the automatic processes</translation>
    </message>
    <message>
        <source>Lancer les traitements en mode debug ou avancer d&apos;un pas</source>
        <translation type="vanished">Start processes in debug mode or go one step forward</translation>
    </message>
    <message>
        <source>Lancer les traitements en mode debug ou avancer de N pas</source>
        <translation type="vanished">Start processes in debug mode or go N step forward</translation>
    </message>
    <message>
        <source>Ajouter un nouveau document</source>
        <translation type="vanished">Add a new 3D document</translation>
    </message>
    <message>
        <source>Ajouter un nouveau document 2D</source>
        <translation type="vanished">Add a new 2D document</translation>
    </message>
    <message>
        <source>Ajouter un nouveau document de type tableur</source>
        <translation type="vanished">Add a new table view document</translation>
    </message>
    <message>
        <source>Configurer</source>
        <translation type="vanished">Configure</translation>
    </message>
    <message>
        <source>Système de coordonnées</source>
        <translation type="vanished">Coordinate system</translation>
    </message>
    <message>
        <source>Nettoyer toutes les vues</source>
        <translation type="vanished">Clean all views</translation>
    </message>
    <message>
        <source>J&apos;ai besoin d&apos;aide !!!</source>
        <translation type="vanished">I need help !!!</translation>
    </message>
    <message>
        <source>Quitter</source>
        <translation type="vanished">Quit</translation>
    </message>
    <message>
        <source>Composants en onglets</source>
        <translation type="vanished">Component in tabs</translation>
    </message>
    <message>
        <source>Composants en colonne</source>
        <translation type="vanished">Component in columns</translation>
    </message>
    <message>
        <source>Composants en colonne (Log en bas)</source>
        <translation type="vanished">Component in columns (Log at bottom)</translation>
    </message>
    <message>
        <source>A propos de Computree...</source>
        <translation type="vanished">About CompuTree...</translation>
    </message>
    <message>
        <source>A propos des plugins...</source>
        <translation type="vanished">About plugins...</translation>
    </message>
    <message>
        <source>Sauvegarder l&apos;arbre des etapes</source>
        <translation type="vanished">Save the tree steps</translation>
    </message>
    <message>
        <source>Attention aucun plugin n&apos;a été trouvé dans :
%1</source>
        <translation type="vanished">Warning no plugin has been found in:
%1</translation>
    </message>
    <message>
        <source>Voulez-vous spécifier dans quel dossier rechercher les plugins ?</source>
        <translation type="vanished">Do you want to specify the folder where to search for plugins ?</translation>
    </message>
    <message>
        <source>Voulez-vous spÜifier dans quel dossier rechercher les plugins ?</source>
        <translation type="vanished">Do you want to specify where you want to search for plugins?</translation>
    </message>
    <message>
        <source>Ouvrir un fichier (CTRL+O)</source>
        <translation type="vanished">Open file (CTRL+O)</translation>
    </message>
    <message>
        <source>Lancer les traitements (CTRL+R)</source>
        <translation type="vanished">Launch processings (CTRL+R)</translation>
    </message>
    <message>
        <source>Lancer les traitements en mode debug ou avancer d&apos;un pas (F5)</source>
        <translation type="vanished">Launch processings in debug mode or run for one step (F5)</translation>
    </message>
    <message>
        <source>Lancer les traitements en mode debug ou avancer de N pas (F10)</source>
        <translation type="vanished">Launch processings in debug mode or run for N steps (F10)</translation>
    </message>
    <message>
        <source>Avancer de N pas automatiquement jusqu&apos;à la fin</source>
        <translation type="vanished">Run for N steps automatically towards end</translation>
    </message>
    <message>
        <source>Ajouter une étape (affiche la fenêtre de choix des étapes)</source>
        <translation type="vanished">Add step (show steps choosing dialog)</translation>
    </message>
    <message>
        <source>Sauvegarder l&apos;arbre des étapes sous...</source>
        <translation type="vanished">Save step tree as...</translation>
    </message>
    <message>
        <source>A propos de la mémoire...</source>
        <translation type="vanished">About memory...</translation>
    </message>
    <message>
        <source>Indiquez le nombre de pas à sauter avant le prochain arrêt de l&apos;étape</source>
        <translation type="vanished">Please give the number of steps to jump before next stop</translation>
    </message>
    <message>
        <source>Indiquez le nombre de pas à sauter avant la prochaine actualisation automatique</source>
        <translation type="vanished">Please give the number of steps to jump before next automatic update</translation>
    </message>
    <message>
        <source>Indiquez le temps en ms entre deux actualisation automatique</source>
        <translation type="vanished">Please give the time in ms between two automatic updates</translation>
    </message>
    <message>
        <source>Parcourir...</source>
        <translation type="vanished">Browse...</translation>
    </message>
    <message>
        <source>Annuler</source>
        <translation type="vanished">Cancel</translation>
    </message>
    <message>
        <source>Dossier contenant les plugins...</source>
        <translation type="vanished">Folder containing the plugins ...</translation>
    </message>
    <message>
        <source>Attention il y a eu des erreurs lors du chargement des plugins du dossier :
%1</source>
        <translation type="vanished">Please note there were errors when loading plugins folder:
%1</translation>
    </message>
    <message>
        <source>Succès</source>
        <translation type="vanished">Success</translation>
    </message>
    <message>
        <source>Le plugin %1 a été chargé avec succès !</source>
        <translation type="vanished">The plugin %1 was loaded with success!</translation>
    </message>
    <message>
        <source>Récupération automatique</source>
        <translation type="vanished">Automatic recovery</translation>
    </message>
    <message>
        <source>&lt;html&gt;L&apos;application a semble-t-il rencontrée un problème lors de la dernière exécution des étapes. Un script a été sauvegardé automatiquement afin de rétablir votre dernière configuration.&lt;br/&gt;&lt;br/&gt;&lt;b&gt;Voulez-vous recharger votre dernière configuration connue ?&lt;/b&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;The application seems to have encounter some error since last execution of steps. A script has been saved automatically to recover last configuration.&lt;br/&gt;&lt;br/&gt;&lt;b&gt;Do you want to load you last known configuration?&lt;/b&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Erreur de chargement du script</source>
        <translation type="vanished">Error during script loading</translation>
    </message>
    <message>
        <source>Une erreur est survenue lors de la lecture du script :&lt;br/&gt;&lt;br/&gt;&lt;i&gt;%1&lt;/i&gt;</source>
        <translation type="vanished">An error has occur during script reading:&lt;br/&gt;&lt;br/&gt;&lt;i&gt;%1&lt;/i&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;%1&lt;br/&gt;&lt;br/&gt;&lt;b&gt;Choisissez le plugin à utiliser ?&lt;/b&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;%1&lt;br/&gt;&lt;br/&gt;&lt;b&gt;Choose plugin to use:&lt;/b&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;%1&lt;br/&gt;&lt;br/&gt;&lt;b&gt;Choisissez une étape de remplacement :&lt;/b&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;%1&lt;br/&gt;&lt;br/&gt;&lt;b&gt;Choose a replacement step:&lt;/b&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;%1&lt;br/&gt;&lt;br/&gt;&lt;b&gt;Voulez vous configurer l&apos;étape manuellement ?&lt;/b&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;%1&lt;br/&gt;&lt;br/&gt;&lt;b&gt;Do you want to configure step manually?&lt;/b&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;%1&lt;br/&gt;&lt;br/&gt;&lt;b&gt;Que voulez vous faire ?&lt;/b&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;%1&lt;br/&gt;&lt;br/&gt;&lt;b&gt;What do you want to do?&lt;/b&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Ne pas charger le script</source>
        <translation type="vanished">Not load the script</translation>
    </message>
    <message>
        <source>Charger le script jusqu&apos;à cette erreur</source>
        <translation type="vanished">Load the script until this error</translation>
    </message>
    <message>
        <source>All Valid Files (</source>
        <translation type="vanished">Tous les fichiers valides (</translation>
    </message>
    <message>
        <source>All files</source>
        <translation type="vanished">Tous les fichiers</translation>
    </message>
    <message>
        <source>Script File (*</source>
        <translation type="vanished">Fichiers scripts (*</translation>
    </message>
    <message>
        <source>Lancer les traitements (F3)</source>
        <translation type="vanished">Launch processings (F3)</translation>
    </message>
    <message>
        <source>Valider le mode manuel et continuer les traitements automatiques (F4)</source>
        <translation type="vanished">Validate manual mode and continue automatical processings (F4)</translation>
    </message>
    <message>
        <source>Lancer les traitements en mode debug ou avancer de N pas (F6)</source>
        <translation type="vanished">Launch processings in debug mode or run for N steps (F10) {6)?}</translation>
    </message>
    <message>
        <source>Ajouter un nouveau document 3D (F7)</source>
        <translation type="vanished">Add a new 3D document (F7)</translation>
    </message>
    <message>
        <source>Ajouter un nouveau document 2D (F8)</source>
        <translation type="vanished">Add a new 2D document (F8)</translation>
    </message>
    <message>
        <source>Ajouter un nouveau document de type tableur (F9)</source>
        <translation type="vanished">Add a new tabular document (F9)</translation>
    </message>
    <message>
        <source>Nettoyer toutes les vues (F10)</source>
        <translation type="vanished">Clean all views (F10)</translation>
    </message>
    <message>
        <source>J&apos;ai besoin d&apos;aide !!! (F1)</source>
        <translation type="vanished">I need help !!! (F1)</translation>
    </message>
    <message>
        <source>Ajouter une étape (affiche la fenêtre de choix des étapes, F2)</source>
        <translation type="vanished">Add a step (show step choosing dialog, F2)</translation>
    </message>
    <message>
        <source>Sauvegarder l&apos;arbre des etapes (CTRL+S)</source>
        <translation type="vanished">Save the steps tree (CTRL+S)</translation>
    </message>
    <message>
        <source>Voud devez redémarrer l&apos;application pour prendre en compte le changement de langue.</source>
        <translation type="vanished">You must restart the application to reflect the language change.</translation>
    </message>
    <message>
        <source>%1 (F1 pour plus d&apos;info)</source>
        <translation type="vanished">%1 (F1 for more info)</translation>
    </message>
    <message>
        <source>Aucune action</source>
        <translation type="vanished">No step</translation>
    </message>
</context>
<context>
    <name>GMinMaxAttributesScalarConfiguration</name>
    <message>
        <source>Ajuster automatiquement</source>
        <translation type="vanished">Automatically adjust</translation>
    </message>
</context>
<context>
    <name>GMultipleItemDrawableModelManager</name>
    <message>
        <source>........... Aucun element configurable ...........</source>
        <translation type="vanished">........... No configurable item ...........</translation>
    </message>
    <message>
        <source>Aucun élément</source>
        <translation type="vanished">No element</translation>
    </message>
</context>
<context>
    <name>GPointOfViewDocumentManager</name>
    <message>
        <source>Ouvrir un fichier contenant des point de vue...</source>
        <translation type="vanished">Open a file containing point of view...</translation>
    </message>
    <message>
        <source>Enregistrer sous...</source>
        <translation type="vanished">Save as...</translation>
    </message>
    <message>
        <source>Nouveau fichier de point de vue</source>
        <translation type="vanished">New point of view file</translation>
    </message>
    <message>
        <source>Ajouter</source>
        <translation type="vanished">Add</translation>
    </message>
    <message>
        <source>Nouveau</source>
        <translation type="vanished">New</translation>
    </message>
    <message>
        <source>Tout supprimer</source>
        <translation type="vanished">Delete all</translation>
    </message>
    <message>
        <source>Ouvrir le fichier...</source>
        <translation type="vanished">Open the file...</translation>
    </message>
    <message>
        <source>Attention</source>
        <translation type="vanished">Warning</translation>
    </message>
    <message>
        <source>Vous allez supprimer tous les points de vue du fichier actuel.</source>
        <translation type="vanished">You will delete all point of view of the current file.</translation>
    </message>
    <message>
        <source>Voulez vous continuer et supprimer les points de vue ou créer un nouveau fichier ?</source>
        <translation type="vanished">Do you want to continue and delete current points of view, or creating a new file ?</translation>
    </message>
    <message>
        <source>Voulez vous continuer et supprimer les points de vue ou crer un nouveau fichier ?</source>
        <translation type="vanished">Do you want to continue and remove point of view or create a new file?</translation>
    </message>
    <message>
        <source>Supprimer</source>
        <translation type="vanished">Delete</translation>
    </message>
    <message>
        <source>Annuler</source>
        <translation type="vanished">Cancel</translation>
    </message>
</context>
<context>
    <name>GPointsAttributesManager</name>
    <message>
        <source>Configurer les couleurs des points</source>
        <translation type="vanished">Configure colors</translation>
    </message>
    <message>
        <source>Ajouter une couleur</source>
        <translation type="vanished">Add a color</translation>
    </message>
    <message>
        <source>Supprimer la couleur</source>
        <translation type="vanished">Delete the color</translation>
    </message>
    <message>
        <source>Enregistrer</source>
        <translation type="vanished">Save</translation>
    </message>
    <message>
        <source>Taille des normales</source>
        <translation type="vanished">Normales size</translation>
    </message>
    <message>
        <source>Couleur des normales</source>
        <translation type="vanished">Normals color</translation>
    </message>
    <message>
        <source>Afficher les normales</source>
        <translation type="vanished">Display normals</translation>
    </message>
    <message>
        <source>Utiliser les couleurs définies par les attributs</source>
        <translation type="vanished">Use colors defined by attributes</translation>
    </message>
    <message>
        <source>Utiliser les normales définies par les attributs</source>
        <translation type="vanished">Use normals defined by attributes</translation>
    </message>
    <message>
        <source>Nom</source>
        <translation type="vanished">Name</translation>
    </message>
    <message>
        <source>Appliquer</source>
        <translation type="vanished">Apply</translation>
    </message>
    <message>
        <source>Utiliser gradient partagé ?</source>
        <translation type="vanished">Use shared gradient?</translation>
    </message>
    <message>
        <source>Configurer</source>
        <translation type="vanished">Configure</translation>
    </message>
    <message>
        <source>Vous n&apos;avez pas enregistré le gradient. Voulez vous le faire maintenant ?</source>
        <translation type="vanished">You have not saved the gradient. Would you do it now?</translation>
    </message>
    <message>
        <source>Couleurs</source>
        <translation type="vanished">Colors</translation>
    </message>
    <message>
        <source>Normales</source>
        <translation type="vanished">Normals</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant le traitement...</source>
        <translation type="vanished">Please wait while processing ...</translation>
    </message>
</context>
<context>
    <name>GRedmineParametersDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Redmine configuration</translation>
    </message>
    <message>
        <source>Choisir le répertoire d&apos;export</source>
        <translation type="vanished">Choose the export directory</translation>
    </message>
    <message>
        <source>Répertoire choisi : </source>
        <translation type="vanished">Choosed directory:</translation>
    </message>
    <message>
        <source>Paramètres de connexion à http://rdinnovation.onf.fr</source>
        <translation type="vanished">Connection settings to http://rdinnovation.onf.fr</translation>
    </message>
    <message>
        <source>Mot de passe :</source>
        <translation type="vanished">Password:</translation>
    </message>
    <message>
        <source>Identifiant :</source>
        <translation type="vanished">User:</translation>
    </message>
    <message>
        <source>Identifiant</source>
        <translation type="vanished">User</translation>
    </message>
    <message>
        <source>Mot de passe</source>
        <translation type="vanished">Password</translation>
    </message>
    <message>
        <source>Utiliser un proxy</source>
        <translation type="vanished">Use a proxy</translation>
    </message>
    <message>
        <source>Type de script</source>
        <translation type="vanished">Script type</translation>
    </message>
    <message>
        <source>Choisir un répertoire d&apos;export</source>
        <translation type="vanished">Choose an export directory</translation>
    </message>
</context>
<context>
    <name>GStepChooserDialog</name>
    <message>
        <source>Etapes</source>
        <translation type="vanished">Steps</translation>
    </message>
    <message>
        <source>Ajouter aux favoris</source>
        <translation type="vanished">Add to favorites</translation>
    </message>
    <message>
        <source>Supprimer des favoris</source>
        <translation type="vanished">Remove from favorites</translation>
    </message>
    <message>
        <source>Monter l&apos;étape dans le menu</source>
        <translation type="vanished">Take up the step in menu</translation>
    </message>
    <message>
        <source>Descendre l&apos;étape dans le menu</source>
        <translation type="vanished">Take down the step in menu</translation>
    </message>
    <message>
        <source>Documentation de l&apos;étape</source>
        <translation type="vanished">Step documentation</translation>
    </message>
    <message>
        <source>Editer</source>
        <translation type="vanished">Edit</translation>
    </message>
    <message>
        <source>Charger</source>
        <translation type="vanished">Load</translation>
    </message>
    <message>
        <source>Exporter</source>
        <translation type="vanished">Export</translation>
    </message>
    <message>
        <source>Ouvrir un fichier favoris</source>
        <translation type="vanished">Open a favorites file</translation>
    </message>
    <message>
        <source>Fichier favoris (*.%1)</source>
        <translation type="vanished">Favorites file (*.%1)</translation>
    </message>
    <message>
        <source>Sauvegarder les favoris</source>
        <translation type="vanished">Save favorites</translation>
    </message>
    <message>
        <source>Erreur</source>
        <translation type="vanished">Error</translation>
    </message>
    <message>
        <source>Impossible d&apos;afficher les informations de cette étape. L&apos;étape n&apos;a pas été trouvée dans le plugin ou le plugin n&apos;est pas présent.</source>
        <translation type="vanished">Impossible to show informations for this step. The step has not been found in the plugin or the plugin is absent. </translation>
    </message>
    <message>
        <source>Supprimer le niveau des favoris</source>
        <translation type="vanished">Clear the level in favorites</translation>
    </message>
</context>
<context>
    <name>GStepManager</name>
    <message>
        <source>Nom</source>
        <translation type="vanished">Name</translation>
    </message>
    <message>
        <source>Progression</source>
        <translation type="vanished">Progress</translation>
    </message>
    <message>
        <source>Temps / Afficher</source>
        <translation type="vanished">Time / Show</translation>
    </message>
    <message>
        <source>Temps</source>
        <translation type="vanished">Elapsed time</translation>
    </message>
    <message>
        <source>Flux d&apos;étapes</source>
        <translation type="vanished">Steps workflow</translation>
    </message>
    <message>
        <source>Impossible d&apos;ajouter l&apos;étape %1</source>
        <translation type="vanished">Impossible to add step %1</translation>
    </message>
    <message>
        <source> après l&apos;étape %2 car elles ne sont pas compatible !</source>
        <translation type="vanished">after step %2 because they are not compatible</translation>
    </message>
    <message>
        <source> à la racine !</source>
        <translation type="vanished">at the root !</translation>
    </message>
    <message>
        <source>Supprimer toutes les étapes</source>
        <translation type="vanished">Remove all steps</translation>
    </message>
    <message>
        <source>Déplier</source>
        <translation type="vanished">Expand</translation>
    </message>
    <message>
        <source>Déplier tous les résultats</source>
        <translation type="vanished">Expand all results</translation>
    </message>
    <message>
        <source>Replier</source>
        <translation type="vanished">Collapse</translation>
    </message>
    <message>
        <source>Replier tous les résultats</source>
        <translation type="vanished">Collapse all results</translation>
    </message>
    <message>
        <source>L&apos;étape ne semble pas être débogable.</source>
        <translation type="vanished">The step seems to not be debuggable</translation>
    </message>
    <message>
        <source>L&apos;tape ne semble pas tre dbogable.</source>
        <translation type="vanished">The step does not seem to be debuggable.</translation>
    </message>
    <message>
        <source>Attention</source>
        <translation type="vanished">Warning</translation>
    </message>
    <message>
        <source>La srialisation semble tre active, si vous executez l&apos;opration  partir de cette tape elle sera dsactive.</source>
        <translation type="vanished">Serialization seems to be active, if you execute the operation from this step it will be deactivated.</translation>
    </message>
    <message>
        <source>Voulez-vous quand même continuer ?</source>
        <translation type="vanished">Do you want to continue anyway?</translation>
    </message>
    <message>
        <source>Voulez-vous quand mme continuer ?</source>
        <translation type="vanished">Do you want to continue anyway?</translation>
    </message>
    <message>
        <source>Une ou plusieurs étapes sont en mode debug or vous allez lancer les traitements en mode normal.</source>
        <translation type="vanished">One or more steps are in debug mode but you want to start in normal mode.</translation>
    </message>
    <message>
        <source>Aucune étape n&apos;est en mode debug or vous allez lancer les traitements dans ce mode.</source>
        <translation type="vanished">No step is in debug mode, but you want to start in this mode.</translation>
    </message>
</context>
<context>
    <name>GStepManager2</name>
    <message>
        <source>Nom</source>
        <translation type="vanished">Name</translation>
    </message>
    <message>
        <source>Temps</source>
        <translation type="vanished">Elapsed time</translation>
    </message>
    <message>
        <source>Flux d&apos;étapes</source>
        <translation type="vanished">Steps workflow</translation>
    </message>
    <message>
        <source>Impossible d&apos;ajouter l&apos;étape %1</source>
        <translation type="vanished">Impossible to add step %1</translation>
    </message>
    <message>
        <source> après l&apos;étape %2 car elles ne sont pas compatible !</source>
        <translation type="vanished">after step %2 because they are not compatible</translation>
    </message>
    <message>
        <source> à la racine !</source>
        <translation type="vanished">at the root !</translation>
    </message>
    <message>
        <source>Supprimer toutes les étapes</source>
        <translation type="vanished">Remove all steps</translation>
    </message>
    <message>
        <source>Déplier</source>
        <translation type="vanished">Expand</translation>
    </message>
    <message>
        <source>Replier</source>
        <translation type="vanished">Collapse</translation>
    </message>
    <message>
        <source>L&apos;étape n&apos;est pas débogable.</source>
        <translation type="vanished">Step is not debbugable</translation>
    </message>
</context>
<context>
    <name>GStepManagerOptions</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Step manager options</translation>
    </message>
    <message>
        <source>Forcer la récursivité de la recherche des résultats pour les nouvelles étapes créées</source>
        <translation type="vanished">Force recusivity for inputs results when new steps are inserted</translation>
    </message>
    <message>
        <source>Option de localisation :</source>
        <translation type="vanished">Locale options:</translation>
    </message>
    <message>
        <source>Sauvegarde automatique</source>
        <translation type="vanished">Automatic backup</translation>
    </message>
    <message>
        <source>Parcourir</source>
        <translation type="vanished">Browse</translation>
    </message>
    <message>
        <source>Effacer les résultats de la mémoire lorsqu&apos;une série d&apos;étapes est terminée</source>
        <translation type="vanished">Clear the results from the memory when a series of steps is completed</translation>
    </message>
    <message>
        <source>Réinitialiser la position de la fenêtre de choix d&apos;étapes</source>
        <translation type="vanished">Reset the position of the step selection window</translation>
    </message>
    <message>
        <source>Dossier de sauvegarde</source>
        <translation type="vanished">Backup directory</translation>
    </message>
</context>
<context>
    <name>GStepViewDefault</name>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Taper du texte pour rechercher une étape. La recherche ne respecte pas la casse.&lt;/p&gt;&lt;p&gt;Vous pouvez si vous le souhaitez utiliser votre propre expression réguilère en tapant tout d&apos;abord &lt;span style=&quot; font-weight:600;&quot;&gt;r:&lt;/span&gt; suivi de votre expression. Si vous ne souhaitez pas utiliser la casse tapez &lt;span style=&quot; font-weight:600;&quot;&gt;i:&lt;/span&gt; après &lt;span style=&quot; font-weight:600;&quot;&gt;r:&lt;/span&gt; puis votre expression régulière. &lt;/p&gt;&lt;p&gt;Exemple simple : &lt;span style=&quot; font-weight:600;&quot;&gt;grilles&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Exemple d&apos;expression régulière : &lt;span style=&quot; font-weight:600;&quot;&gt;r:i:.*grilles.*&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Type text to search for a step. The search don&apos;t respect case&lt;/p&gt;&lt;p&gt;You can use a regular expression if you want, typing first &lt;span style=&quot; font-weight:600;&quot;&gt;r:&lt;/span&gt; followed by your expression. If you don&apos;t want to use case, type &lt;span style=&quot; font-weight:600;&quot;&gt;i:&lt;/span&gt; after &lt;span style=&quot; font-weight:600;&quot;&gt;r:&lt;/span&gt; followed by your regular expression. &lt;/p&gt;&lt;p&gt;Simple example: &lt;span style=&quot; font-weight:600;&quot;&gt;grids&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Example of regular expression : &lt;span style=&quot; font-weight:600;&quot;&gt;r:i:.*grids.*&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Rechercher des étapes...</source>
        <translation type="vanished">Search for steps...</translation>
    </message>
    <message>
        <source>Replacer à la position par défaut</source>
        <translation type="vanished">Replace to default position</translation>
    </message>
    <message>
        <source>Replacer au démarrage à la dernière position connue</source>
        <translation type="vanished">During Computree launch, place at last known position</translation>
    </message>
    <message>
        <source>Nom des étapes</source>
        <translation type="vanished">Steps name</translation>
    </message>
    <message>
        <source>Plugins</source>
        <translation type="obsolete">Plugins</translation>
    </message>
    <message>
        <source>Clé de l&apos;étape au sein du plugin ou d&apos;un script</source>
        <translation type="vanished">Key of the step in plugin or in script</translation>
    </message>
    <message>
        <source>Nom de l&apos;étape</source>
        <translation type="vanished">Step name</translation>
    </message>
    <message>
        <source>Description courte</source>
        <translation type="vanished">Short description</translation>
    </message>
    <message>
        <source>Description détaillée</source>
        <translation type="vanished">Detailled description</translation>
    </message>
    <message>
        <source>Replacer à gauche</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Replacer à droite</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GTreeStepContextMenu</name>
    <message>
        <source>%1 (F1 pour plus d&apos;info)</source>
        <translation type="vanished">%1 (F1 for more info)</translation>
    </message>
    <message>
        <source>Etapes de début de script</source>
        <translation type="vanished">Script starting steps</translation>
    </message>
    <message>
        <source>Config. paramètres</source>
        <translation type="vanished">Config. parameters</translation>
    </message>
    <message>
        <source>Documentation de l&apos;étape</source>
        <translation type="vanished">Step documentation</translation>
    </message>
    <message>
        <source>Localiser dans le menu</source>
        <translation type="vanished">Find this step in the menu</translation>
    </message>
    <message>
        <source>Attention</source>
        <translation type="vanished">Warning</translation>
    </message>
    <message>
        <source>Une ou plusieurs étapes sont en mode debug or vous allez lancer les traitements en mode normal.</source>
        <translation type="vanished">One or more steps are in debug mode but you want to start in normal mode.</translation>
    </message>
    <message>
        <source>Aucune étape n&apos;est en mode debug or vous allez lancer les traitements dans ce mode.</source>
        <translation type="vanished">No step is in debug mode, but you want to start in this mode.</translation>
    </message>
    <message>
        <source>ExÜuter</source>
        <translation type="vanished">Execute</translation>
    </message>
    <message>
        <source>Modifier (mode manuel)</source>
        <translation type="vanished">Modify( manual mode)</translation>
    </message>
    <message>
        <source>Informations sur l&apos;étape</source>
        <translation type="vanished">Informations about step</translation>
    </message>
    <message>
        <source>Configurer les résultats d&apos;entrÞ</source>
        <translation type="vanished">Configure input results</translation>
    </message>
    <message>
        <source>Voir la configuration des résultats d&apos;entrÞ</source>
        <translation type="vanished">Show configuration of input results</translation>
    </message>
    <message>
        <source>Exécuter</source>
        <translation type="vanished">Execute</translation>
    </message>
    <message>
        <source>Configurer les résultats d&apos;entrée</source>
        <translation type="vanished">Configure input results</translation>
    </message>
    <message>
        <source>Voir la configuration des résultats d&apos;entrée</source>
        <translation type="vanished">Show the configuration of input results</translation>
    </message>
    <message>
        <source>Configurer</source>
        <translation type="vanished">Configure</translation>
    </message>
    <message>
        <source>Config. résultats d&apos;entrée</source>
        <translation type="vanished">Config. input results</translation>
    </message>
    <message>
        <source>Supprimer</source>
        <translation type="vanished">Delete</translation>
    </message>
    <message>
        <source>Déplier</source>
        <translation type="vanished">Expand</translation>
    </message>
    <message>
        <source>Déplier toutes les étapes</source>
        <translation type="vanished">Expand all steps</translation>
    </message>
    <message>
        <source>Replier</source>
        <translation type="vanished">Collapse</translation>
    </message>
    <message>
        <source>Replier toutes les étapes</source>
        <translation type="vanished">Collapse all steps</translation>
    </message>
    <message>
        <source>Aucune action</source>
        <translation type="vanished">No action</translation>
    </message>
</context>
<context>
    <name>GTreeView</name>
    <message>
        <source>&lt;html&gt;&lt;p&gt;&lt;font size=&quot;4&quot;&gt;&lt;b&gt;Filtrer une colonne&lt;/b&gt;&lt;/font&gt;&lt;/p&gt;&lt;p&gt;Vous pouvez filtrer une colonne en écrivant son nom puis la valeur à rechercher.&lt;/p&gt;&lt;p&gt;&lt;i&gt;MaColonne : mot&lt;/i&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Vous pouvez aussi filtrer une colonne contenant des nombres en utilisant des fonctions mathématiques. Pour celà il vous faut utiliser la syntaxe (val) qui sera remplacée par la valeur de la cellule de la colonne.&lt;/p&gt;&lt;p&gt;&lt;i&gt;MaColonne : (val) &amp;gt; 0 and (val) &amp;lt; 500&lt;/i&gt;&lt;/p&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;p&gt;&lt;font size=&quot;4&quot;&gt;&lt;b&gt;Filter a column&lt;/b&gt;&lt;/font&gt;&lt;/p&gt;&lt;p&gt;You can filter a column by writing its name and the value to search.&lt;/p&gt;&lt;p&gt;&lt;i&gt;MyColumn : word&lt;/i&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;You can also filter a column containing numbers using mathematical functions. To do this you must use the syntax (val) which will be replaced by the value of the cell in column.&lt;/p&gt;&lt;p&gt;&lt;i&gt;MyColumn : (val) &amp;gt; 0 and (val) &amp;lt; 500&lt;/i&gt;&lt;/p&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Filtrer la colonne...</source>
        <translation type="vanished">Filter the column...</translation>
    </message>
    <message>
        <source>Valider</source>
        <translation type="vanished">Apply</translation>
    </message>
    <message>
        <source>Groupe</source>
        <translation type="vanished">Group</translation>
    </message>
    <message>
        <source>Question</source>
        <translation type="vanished">Question</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant le chargement de la table...</source>
        <translation type="vanished">Please wait during table loading...</translation>
    </message>
    <message>
        <source>Annuler</source>
        <translation type="vanished">Cancel</translation>
    </message>
    <message>
        <source>Sélectionner</source>
        <translation type="vanished">Select</translation>
    </message>
    <message>
        <source>Dé-Sélectionner</source>
        <translation type="vanished">DeSelect</translation>
    </message>
    <message>
        <source>Inverser la sélection</source>
        <translation type="vanished">Inverse the selection</translation>
    </message>
    <message>
        <source>Ajouter au document</source>
        <translation type="vanished">Add to document</translation>
    </message>
    <message>
        <source>Supprimer du document</source>
        <translation type="vanished">Remove from document</translation>
    </message>
    <message>
        <source>%1</source>
        <translation type="vanished">%1</translation>
    </message>
    <message>
        <source>Couleur automatique</source>
        <translation type="vanished">Automatic color</translation>
    </message>
    <message>
        <source>Rafraichir</source>
        <translation type="vanished">Refresh</translation>
    </message>
    <message>
        <source>Couleur unie</source>
        <translation type="vanished">Plain color</translation>
    </message>
    <message>
        <source>Le modèle n&apos;est pas chargé complètement, voulez vous le charger avant de faire la recherche ?</source>
        <translation type="vanished">The model is not completely loaded, do you want to load it before lauching the search ?</translation>
    </message>
    <message>
        <source>Le modèle n&apos;est pas chargé complètement, voulez vous le charger pour utiliser tous les éléments dans le tri ?

Attention cette opération peut être lente.</source>
        <translation type="vanished">The model is not completely loaded, do you want to load it before use all elements in sorting ?\nWarning: this operation could be long.</translation>
    </message>
    <message>
        <source>Ajouter au </source>
        <translation type="vanished">Add to </translation>
    </message>
</context>
<context>
    <name>GraphicsViewDebugMode</name>
    <message>
        <source>Permet d&apos;afficher des informations de débogage</source>
        <translation type="vanished">Allow to display debug information</translation>
    </message>
    <message>
        <source>Debug mode - activé</source>
        <translation type="vanished">Debug mode - on</translation>
    </message>
    <message>
        <source>Colorier les objets de chaque Chunk d&apos;une couleur différente</source>
        <translation type="vanished">Colorize objects of each chunk with de different color</translation>
    </message>
    <message>
        <source>Afficher les systèmes de coordonnées</source>
        <translation type="vanished">Display coordinates systems</translation>
    </message>
    <message>
        <source>Afficher des informations sur les chunks</source>
        <translation type="vanished">Display information about chunks</translation>
    </message>
    <message>
        <source>Afficher le polygon de sélection en 3D</source>
        <translation type="vanished">Display selection polygon in 3D</translation>
    </message>
    <message>
        <source>Colorier les points en vert si ils font partie de la sélection</source>
        <translation type="vanished">Colorize point in green if they are seleted</translation>
    </message>
    <message>
        <source>Affiche le point de pivot de la caméra</source>
        <translation type="vanished">Display pivot point for the camera</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Suppression du resultat %1 des autres vues.</source>
        <translation type="vanished">Remove result %1 from other views.</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant la construction de la table</source>
        <translation type="vanished">Please wait while the construction of the table</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant le rafraichissement de la table</source>
        <translation type="vanished">Please wait while the refreshment of the table</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant l&apos;ajout des éléments à la table</source>
        <translation type="vanished">Please wait while adding items to the table</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant la construction de l&apos;octree</source>
        <translation type="vanished">Please wait while octree creation</translation>
    </message>
    <message>
        <source>Capture d&apos;écran</source>
        <translation type="vanished">Sceen capture</translation>
    </message>
    <message>
        <source>Image JPEG (*.jpg)</source>
        <translation type="vanished">JPEG image (*.jpg)</translation>
    </message>
    <message>
        <source>Nom</source>
        <translation type="vanished">Name</translation>
    </message>
</context>
</TS>
