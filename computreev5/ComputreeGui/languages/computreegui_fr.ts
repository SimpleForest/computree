<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>ColorPickerPopup</name>
    <message>
        <source>Custom</source>
        <translation type="vanished">Personnalisée</translation>
    </message>
</context>
<context>
    <name>DM_AbstractAttributes</name>
    <message>
        <source>Une erreur inconnu est survenu lors du traitement.</source>
        <translation type="vanished">Une erreur inconnue est survenue lors du traitement.</translation>
    </message>
</context>
<context>
    <name>DM_ContextMenuColouristAdder</name>
    <message>
        <source>Couleur uni</source>
        <translation type="vanished">Couleur unie</translation>
    </message>
    <message>
        <source>%1</source>
        <translation type="vanished">%1</translation>
    </message>
</context>
<context>
    <name>DM_GuiManager</name>
    <message>
        <source>Veuillez patienter pendant l&apos;ajout du resultat au document actif.</source>
        <translation type="vanished">Veuillez patienter pendant l&apos;ajout du résultat au document actif.</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant l&apos;ajout des CT_AbstractItemDrawable au document actif.</source>
        <translation type="vanished">Veuillez patienter pendant l&apos;ajout des items au document actif.</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant la suppression de l&apos;etape.</source>
        <translation type="vanished">Veuillez patienter pendant la suppression de l&apos;étape.</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant la suppression des etapes.</source>
        <translation type="vanished">Veuillez patienter pendant la suppression des étapes.</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant le chargement des resultats.</source>
        <translation type="vanished">Veuillez patienter pendant le chargement des résultats.</translation>
    </message>
</context>
<context>
    <name>DM_ItemDrawableTreeViewController</name>
    <message>
        <source>Veuillez patienter pendant la construction de la table</source>
        <translation type="vanished">Veuillez patienter pendant la construction de la table</translation>
    </message>
</context>
<context>
    <name>GAboutDialog</name>
    <message>
        <source>A propos de Computree</source>
        <translation type="vanished">A propos de CompuTree</translation>
    </message>
    <message>
        <source>
&lt;html&gt;
&lt;head/&gt;
&lt;body&gt;

&lt;p&gt;&lt;span style=&quot; font-famliy:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Computree v5.0&lt;/span&gt;&lt;/p&gt;

&lt;p style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Managed by the Computree Group:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt; GIP Ecofor&lt;/li&gt;
&lt;li&gt; ONF&lt;/li&gt;
&lt;li&gt; IGN&lt;/li&gt;
&lt;li&gt; INRA&lt;/li&gt;
&lt;li&gt; University of Sherbrooke&lt;/li&gt;
&lt;/ul&gt;

&lt;p style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;The use of Computree is subject to the acceptance of the &lt;a href=&quot;http://rdinnovation.onf.fr/projects/computree/wiki/En_charte_v5&quot;&gt;Computree Charter&lt;/a&gt;&lt;/p&gt;

&lt;br&gt;
&lt;p style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Computree GUI interface is licenced under GPL license: &lt;a href=&quot;https://opensource.org/licenses/GPL-3.0&quot;&gt;https://opensource.org/licenses/GPL-3.0&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;ComputreeCore, PluginShared and PluginBase are licenced under LGPL license: &lt;a href=&quot;https://opensource.org/licenses/LGPL-3.0&quot;&gt;https://opensource.org/licenses/LGPL-3.0&lt;/a&gt;&lt;/p&gt;

&lt;br&gt;
&lt;p style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Computree uses following libraries:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt; AMKgl: &lt;a href=&quot;http://www.ic-arts.eu/arts/&quot;&gt;http://www.ic-arts.eu/arts&lt;/a&gt;&lt;/li&gt;
&lt;li&gt; MuParser: &lt;a href=&quot;http://muparser.beltoforion.de&quot;&gt;http://muparser.beltoforion.de&lt;/a&gt;&lt;/li&gt;
&lt;li&gt; PCL: &lt;a href=&quot;http://pointclouds.or/&quot;&gt;http://pointclouds.org&lt;/a&gt;&lt;/li&gt;
&lt;li&gt; OpenCV: &lt;a href=&quot;http://opencv.org&quot;&gt;http://opencv.org&lt;/a&gt;&lt;/li&gt;
&lt;li&gt; GDAL: &lt;a href=&quot;http://www.gdal.org&quot;&gt;http://www.gdal.org&lt;/a&gt;&lt;/li&gt;
&lt;li&gt; GEOS: &lt;a href=&quot;https://trac.osgeo.org/geos&quot;&gt;https://trac.osgeo.org/geos&lt;/a&gt;&lt;/li&gt;
&lt;li&gt; GSL: &lt;a href=&quot;https://www.gnu.org/software/gsl&quot;&gt;https://www.gnu.org/software/gsl&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;

&lt;/body&gt;
&lt;/html&gt;</source>
        <translation type="vanished">
&lt;html&gt;
&lt;head/&gt;
&lt;body&gt;

&lt;p&gt;&lt;span style=&quot; font-famliy:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Computree v5.0&lt;/span&gt;&lt;/p&gt;

&lt;p style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Géré par le Groupe Computree :&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt; GIP Ecofor&lt;/li&gt;
&lt;li&gt; ONF&lt;/li&gt;
&lt;li&gt; IGN&lt;/li&gt;
&lt;li&gt; INRA&lt;/li&gt;
&lt;li&gt; University of Sherbrooke&lt;/li&gt;
&lt;/ul&gt;

&lt;p style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;L&apos;utilisation de Computree est soumise à l&apos;accepation de la &lt;a href=&quot;http://rdinnovation.onf.fr/projects/computree/wiki/En_charte_v5&quot;&gt;Charte Computree&lt;/a&gt;&lt;/p&gt;

&lt;br&gt;
&lt;p style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;L&apos;interface Computree GUI est sous licence GPL : &lt;a href=&quot;https://opensource.org/licenses/GPL-3.0&quot;&gt;https://opensource.org/licenses/GPL-3.0&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;ComputreeCore, PluginShared et PluginBase sont sous licence LGPL : &lt;a href=&quot;https://opensource.org/licenses/LGPL-3.0&quot;&gt;https://opensource.org/licenses/LGPL-3.0&lt;/a&gt;&lt;/p&gt;

&lt;br&gt;
&lt;p style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Computree utilise les librairies suivantes :&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt; AMKgl : &lt;a href=&quot;http://www.ic-arts.eu/arts/&quot;&gt;http://www.ic-arts.eu/arts&lt;/a&gt;&lt;/li&gt;
&lt;li&gt; MuParser : &lt;a href=&quot;http://muparser.beltoforion.de&quot;&gt;http://muparser.beltoforion.de&lt;/a&gt;&lt;/li&gt;
&lt;li&gt; PCL : &lt;a href=&quot;http://pointclouds.or/&quot;&gt;http://pointclouds.org&lt;/a&gt;&lt;/li&gt;
&lt;li&gt; OpenCV : &lt;a href=&quot;http://opencv.org&quot;&gt;http://opencv.org&lt;/a&gt;&lt;/li&gt;
&lt;li&gt; GDAL : &lt;a href=&quot;http://www.gdal.org&quot;&gt;http://www.gdal.org&lt;/a&gt;&lt;/li&gt;
&lt;li&gt; GEOS : &lt;a href=&quot;https://trac.osgeo.org/geos&quot;&gt;https://trac.osgeo.org/geos&lt;/a&gt;&lt;/li&gt;
&lt;li&gt; GSL : &lt;a href=&quot;https://www.gnu.org/software/gsl&quot;&gt;https://www.gnu.org/software/gsl&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;

&lt;/body&gt;
&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Computree 5.0&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Developped by:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Krebs Michaël (Arts et Métiers ParisTech site de Cluny, ARTS, Equipe Bois)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Alexandre Piboule (Office National des Forêts, ONF)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Computree GUI interface is licenced under GPL license: &lt;/span&gt;&lt;a href=&quot;https://opensource.org/licenses/GPL-3.0&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://opensource.org/licenses/GPL-3.0&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;ComputreeCore, PluginShared and PluginBase are licenced under LGPL license: &lt;/span&gt;&lt;a href=&quot;https://opensource.org/licenses/LGPL-3.0&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://opensource.org/licenses/LGPL-3.0&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Computree uses following libraries:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;AMKgl : &lt;/span&gt;&lt;a href=&quot;http://www.ic-arts.eu/arts/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.ic-arts.eu/arts/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;MuParser: &lt;/span&gt;&lt;a href=&quot;http://muparser.beltoforion.de&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://muparser.beltoforion.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;PCL: &lt;/span&gt;&lt;a href=&quot;http://pointclouds.or/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://pointclouds.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;OpenCV: &lt;/span&gt;&lt;a href=&quot;http://opencv.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://opencv.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;GDAL: &lt;/span&gt;&lt;a href=&quot;http://www.gdal.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.gdal.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;GEOS : &lt;/span&gt;&lt;a href=&quot;https://trac.osgeo.org/geos&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://trac.osgeo.org/geos&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;GSL: &lt;/span&gt;&lt;a href=&quot;https://www.gnu.org/software/gsl&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://www.gnu.org/software/gsl&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Computree 4.0&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Developped by:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Krebs Michaël (Arts et Métiers ParisTech site de Cluny, ARTS, Equipe Bois)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Alexandre Piboule (Office National des Forêts, ONF)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;L&apos;interface Computree GUI est sous license GPL : &lt;/span&gt;&lt;a href=&quot;https://opensource.org/licenses/GPL-3.0&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://opensource.org/licenses/GPL-3.0&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;ComputreeCore, PluginShared and PluginBase sont sous license LGPL : &lt;/span&gt;&lt;a href=&quot;https://opensource.org/licenses/LGPL-3.0&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://opensource.org/licenses/LGPL-3.0&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Computree utilise les libraries suivantes:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;AMKgl : &lt;/span&gt;&lt;a href=&quot;http://www.ic-arts.eu/arts/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.ic-arts.eu/arts/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;MuParser: &lt;/span&gt;&lt;a href=&quot;http://muparser.beltoforion.de&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://muparser.beltoforion.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;PCL: &lt;/span&gt;&lt;a href=&quot;http://pointclouds.or/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://pointclouds.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;OpenCV: &lt;/span&gt;&lt;a href=&quot;http://opencv.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://opencv.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;GDAL: &lt;/span&gt;&lt;a href=&quot;http://www.gdal.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.gdal.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;GEOS : &lt;/span&gt;&lt;a href=&quot;https://trac.osgeo.org/geos&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://trac.osgeo.org/geos&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;GSL: &lt;/span&gt;&lt;a href=&quot;https://www.gnu.org/software/gsl&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://www.gnu.org/software/gsl&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt; {3C?} {4.0/?} {3.?} {40/?} {1&quot;?} {2&apos;?} {8p?} {400;?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {600;?} {5.0&lt;?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {600;?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {3.0&quot;?} {0000f?} {3.0&lt;?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {3.0&quot;?} {0000f?} {3.0&lt;?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0000f?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0000f?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0000f?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0000f?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0000f?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0000f?} {0p?} {0p?} {0p?} {0p?} {0;?} {0p?} {10p?} {0000f?}</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Computree 4.0&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Developped by:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Krebs Michaël (Arts et Métiers ParisTech site de Cluny, ARTS, Equipe Bois)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Alexandre Piboule (Office National des Forêts, ONF)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Computree GUI interface is licenced under GPL license: &lt;/span&gt;&lt;a href=&quot;https://opensource.org/licenses/GPL-3.0&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://opensource.org/licenses/GPL-3.0&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;ComputreeCore, PluginShared and PluginBase are licenced under LGPL license: &lt;/span&gt;&lt;a href=&quot;https://opensource.org/licenses/LGPL-3.0&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://opensource.org/licenses/LGPL-3.0&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Computree uses following libraries:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;AMKgl : &lt;/span&gt;&lt;a href=&quot;http://www.ic-arts.eu/arts/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.ic-arts.eu/arts/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;MuParser: &lt;/span&gt;&lt;a href=&quot;http://muparser.beltoforion.de&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://muparser.beltoforion.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;PCL: &lt;/span&gt;&lt;a href=&quot;http://pointclouds.or/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://pointclouds.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;OpenCV: &lt;/span&gt;&lt;a href=&quot;http://opencv.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://opencv.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;GDAL: &lt;/span&gt;&lt;a href=&quot;http://www.gdal.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.gdal.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;GEOS : &lt;/span&gt;&lt;a href=&quot;https://trac.osgeo.org/geos&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://trac.osgeo.org/geos&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;GSL: &lt;/span&gt;&lt;a href=&quot;https://www.gnu.org/software/gsl&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://www.gnu.org/software/gsl&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Computree 4.0&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Developped by:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Krebs Michaël (Arts et Métiers ParisTech site de Cluny, ARTS, Equipe Bois)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Alexandre Piboule (Office National des Forêts, ONF)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;L&apos;interface Computree GUI est sous license GPL : &lt;/span&gt;&lt;a href=&quot;https://opensource.org/licenses/GPL-3.0&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://opensource.org/licenses/GPL-3.0&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;ComputreeCore, PluginShared and PluginBase sont sous license LGPL : &lt;/span&gt;&lt;a href=&quot;https://opensource.org/licenses/LGPL-3.0&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://opensource.org/licenses/LGPL-3.0&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Computree utilise les libraries suivantes:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;AMKgl : &lt;/span&gt;&lt;a href=&quot;http://www.ic-arts.eu/arts/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.ic-arts.eu/arts/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;MuParser: &lt;/span&gt;&lt;a href=&quot;http://muparser.beltoforion.de&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://muparser.beltoforion.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;PCL: &lt;/span&gt;&lt;a href=&quot;http://pointclouds.or/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://pointclouds.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;OpenCV: &lt;/span&gt;&lt;a href=&quot;http://opencv.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://opencv.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;GDAL: &lt;/span&gt;&lt;a href=&quot;http://www.gdal.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.gdal.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;GEOS : &lt;/span&gt;&lt;a href=&quot;https://trac.osgeo.org/geos&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://trac.osgeo.org/geos&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;GSL: &lt;/span&gt;&lt;a href=&quot;https://www.gnu.org/software/gsl&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://www.gnu.org/software/gsl&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Computree 2.0&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Développé par :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Krebs Michaël (Arts et Métiers ParisTech site de Cluny, ARTS, Equipe Bois)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Alexandre Piboule (ONF)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Utilise les librairies :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;QGLViewer : &lt;/span&gt;&lt;a href=&quot;http://www.libqglviewer.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.libqglviewer.com/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;MuParser : &lt;/span&gt;&lt;a href=&quot;http://muparser.beltoforion.de/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://muparser.beltoforion.de/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Computree 3.0&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Développé par :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Krebs Michaël (Arts et Métiers ParisTech site de Cluny, ARTS, Equipe Bois)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;- Alexandre Piboule (ONF)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Utilise les librairies :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;QGLViewer : &lt;/span&gt;&lt;a href=&quot;http://www.libqglviewer.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.libqglviewer.com/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;MuParser : &lt;/span&gt;&lt;a href=&quot;http://muparser.beltoforion.de/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://muparser.beltoforion.de/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>GAboutPluginsDialog</name>
    <message>
        <source>Load File Step</source>
        <translation type="vanished">Etape de chargement de fichier</translation>
    </message>
    <message>
        <source>Can be added first Step</source>
        <translation type="vanished">Etape pouvant être ajoutée en premier</translation>
    </message>
    <message>
        <source>Normal Step</source>
        <translation type="vanished">Etape normale</translation>
    </message>
    <message>
        <source>Exporter</source>
        <translation type="vanished">Exportateur</translation>
    </message>
</context>
<context>
    <name>GAboutStepDialog</name>
    <message>
        <source>CT_VirtualAbstractStep informations</source>
        <translation type="vanished">Informations de CT_VirtualAbstractStep</translation>
    </message>
    <message>
        <source>Plugin Name</source>
        <translation type="vanished">Nom du plugin</translation>
    </message>
    <message>
        <source>CT_VirtualAbstractStep Name</source>
        <translation type="vanished">Nom de CT_VirtualAbstractStep</translation>
    </message>
    <message>
        <source>IN Models :</source>
        <translation type="vanished">Modèles d&apos;entrées :</translation>
    </message>
    <message>
        <source>OUT Models :</source>
        <translation type="vanished">Modèles de sorties :</translation>
    </message>
    <message>
        <source>Show references in RIS Format</source>
        <translation type="vanished">Afficher les références au format RIS</translation>
    </message>
</context>
<context>
    <name>GActionsManager</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Gestionnaire d&apos;actions</translation>
    </message>
    <message>
        <source>Manual/Debug mode</source>
        <translation type="vanished">Mode manuel/debug</translation>
    </message>
</context>
<context>
    <name>GCameraGraphicsOptions</name>
    <message>
        <source>Desynchroniser ce document</source>
        <translation type="vanished">Désynchroniser ce document</translation>
    </message>
</context>
<context>
    <name>GCitationDialog</name>
    <message>
        <source>Script citation informations</source>
        <translation type="vanished">Informations de citation pour le script</translation>
    </message>
    <message>
        <source>Script summary</source>
        <translation type="vanished">Résumé du script</translation>
    </message>
    <message>
        <source>Step Name</source>
        <translation type="vanished">Nom de l&apos;étape</translation>
    </message>
    <message>
        <source>Step Description</source>
        <translation type="vanished">Decription de l&apos;étape</translation>
    </message>
    <message>
        <source>Plugin and Step citations</source>
        <translation type="vanished">Citations des plugins et des étapes</translation>
    </message>
    <message>
        <source>Citations in RIS format</source>
        <translation type="vanished">Citations au format RIS</translation>
    </message>
    <message>
        <source>Export Script summary</source>
        <translation type="vanished">Exporter le résumé du script</translation>
    </message>
    <message>
        <source>Export RIS file</source>
        <translation type="vanished">Exporter un fichier RIS de citations</translation>
    </message>
    <message>
        <source>Choose export file</source>
        <translation type="vanished">Choisir le fichier d&apos;export</translation>
    </message>
    <message>
        <source>Text file (*.txt)</source>
        <translation type="vanished">Fichier texte (*.txt)</translation>
    </message>
    <message>
        <source>Step Name	</source>
        <translation type="vanished">Nom de l&apos;étape</translation>
    </message>
    <message>
        <source>Step Description
</source>
        <translation type="vanished">Decription de l&apos;étape</translation>
    </message>
    <message>
        <source>RIS file (*.ris)</source>
        <translation type="vanished">Fichier RIS (*.ris)</translation>
    </message>
</context>
<context>
    <name>GDocumentViewForGraphics</name>
    <message>
        <source>Configurer les couleurs des points</source>
        <translation type="vanished">Configurer les couleurs des élements</translation>
    </message>
    <message>
        <source>Changer le mode de dessin :
- Simplifié lors des déplacements
- Toujours Simplifié- Jamais Simplifié</source>
        <translation type="vanished">Changer le mode de dessin :
- Simplifié lors des déplacements
- Toujours Simplifié
- Jamais Simplifié</translation>
    </message>
</context>
<context>
    <name>GGraphicsViewImp</name>
    <message>
        <source>Save to file error : State file name (%1) references a directory instead of a file.</source>
        <translation type="vanished">Erreur lors de la sauvegarde de fichier : le chemin (%1) référence un répertoire au lieu d&apos;un fichier</translation>
    </message>
    <message>
        <source>Save to file error : Unable to create directory %1</source>
        <translation type="vanished">Erreur lors de la sauvegarde de fichier : Impossible de créer le répertoire %1</translation>
    </message>
    <message>
        <source>Save to file error : Unable to save to file %1</source>
        <translation type="vanished">Erreur lors de la sauvegarde de fichier : Impossible de sauver dans le fichier %1</translation>
    </message>
    <message>
        <source>Problem in state restoration : File %1 is not readable.</source>
        <translation type="vanished">Problème lors de la restauration : Fichier %1 non lisible</translation>
    </message>
    <message>
        <source>Problem in state restoration when set xml content : %1 at line %2 column %3</source>
        <translation type="vanished">Problème de restauration de l&apos;état lors du réglage du contenu xml: %1 à la ligne %2 colonne %3</translation>
    </message>
    <message>
        <source>Open file error : Unable to open file %1</source>
        <translation type="vanished">Erreur de chargement de fichier : Impossible d&apos;ouvrir le fichier %1</translation>
    </message>
</context>
<context>
    <name>GGraphicsViewOptions</name>
    <message>
        <source>Afficher</source>
        <translation type="vanished">Activer</translation>
    </message>
    <message>
        <source>La sauvegarde de la configuration a Ühoué.</source>
        <translation type="vanished">La sauvegarde de la configuration a échouée.</translation>
    </message>
</context>
<context>
    <name>GItemDrawableConfigurationManagerView</name>
    <message>
        <source>Afficher</source>
        <translation type="vanished">Activer</translation>
    </message>
</context>
<context>
    <name>GItemDrawableManagerOptionsColor</name>
    <message>
        <source>Sauvegarde russi</source>
        <translation type="vanished">Sauvegarde réussi</translation>
    </message>
    <message>
        <source>La sauvegarde de la liste des couleurs a russi.</source>
        <translation type="vanished">La sauvegarde de la liste des couleurs a réussi.</translation>
    </message>
    <message>
        <source>La sauvegarde de la liste des couleurs a chou.</source>
        <translation type="vanished">La sauvegarde de la liste des couleurs a échoué.</translation>
    </message>
    <message>
        <source>La sauvegarde de la liste des couleurs par dfaut a russi.</source>
        <translation type="vanished">La sauvegarde de la liste des couleurs par défaut a réussi.</translation>
    </message>
    <message>
        <source>La sauvegarde de la liste des couleurs par defaut a chou.</source>
        <translation type="vanished">La sauvegarde de la liste des couleurs par défaut a échoué.</translation>
    </message>
</context>
<context>
    <name>GLogWidget</name>
    <message>
        <source>Clear</source>
        <translation type="vanished">Effacer</translation>
    </message>
    <message>
        <source>If checked: no message is sent to this log anymore.
Checked automatically when number of messages increases above 200.
However, checked or not, all messages are always sent to log file: Computree/log_computree.log</source>
        <translation type="vanished">Si coché: aucun message ne sera pls envoyé au log. 
Coché automatiquement quand le nombre de messages dépasse 200.
Cependant, coché ou non, tous les messages sont toujours envoyés au fichier log: Computree/log_computree.log</translation>
    </message>
</context>
<context>
    <name>GMainWindow</name>
    <message>
        <source>toolBar</source>
        <translation type="vanished">Boite à outils</translation>
    </message>
    <message>
        <source>StepManager</source>
        <translation type="vanished">Gestionnaire d&apos;étape</translation>
    </message>
    <message>
        <source>ModelManager</source>
        <translation type="vanished">Gestionnaire de modèle</translation>
    </message>
    <message>
        <source>ItemConfigurator</source>
        <translation type="vanished">Configurateur d&apos;item</translation>
    </message>
    <message>
        <source>Tile</source>
        <translation type="vanished">Tuile</translation>
    </message>
    <message>
        <source>Une étape est en cours de traiements, veuillez terminer les traitements avant de fermer l&apos;application.</source>
        <translation type="vanished">Une étape est en cours de traitements, veuillez terminer les traitements avant de fermer l&apos;application.</translation>
    </message>
    <message>
        <source>Script File (</source>
        <translation type="vanished">Fichier script (</translation>
    </message>
    <message>
        <source>Informations de citation</source>
        <translation type="vanished">Citation information</translation>
    </message>
    <message>
        <source>Sauvegarder l&apos;arbre des tapes sous...</source>
        <translation type="vanished">Sauvegarder l&apos;arbre des étapes sous...</translation>
    </message>
    <message>
        <source>Ajouter une étape qui n&apos;a pas besoin de résultat en entrÞ</source>
        <translation type="vanished">Ajouter une étape qui peut être ajoutée en premier</translation>
    </message>
    <message>
        <source>Ajouter un nouveau document</source>
        <translation type="vanished">Ajouter un nouveau document 3D</translation>
    </message>
    <message>
        <source>Composants en colonne</source>
        <translation type="vanished">Composants en colonnes</translation>
    </message>
    <message>
        <source>Voulez-vous spÜifier dans quel dossier rechercher les plugins ?</source>
        <translation type="vanished">Voulez-vous spécifier dans quel dossier rechercher les plugins ?</translation>
    </message>
    <message>
        <source>Voud devez redémarrer l&apos;application pour prendre en compte le changement de langue.</source>
        <translation type="vanished">Vous devez redémarrer l&apos;application pour prendre en compte le changement de langue.</translation>
    </message>
    <message>
        <source>Aucune action</source>
        <translation type="vanished">Aucune étape</translation>
    </message>
</context>
<context>
    <name>GMultipleItemDrawableModelManager</name>
    <message>
        <source>........... Aucun element configurable ...........</source>
        <translation type="vanished">........... Aucun élément configurable ...........</translation>
    </message>
</context>
<context>
    <name>GOsgGraphicsView</name>
    <message>
        <source>Save to file error : State file name (%1) references a directory instead of a file.</source>
        <translation type="vanished">Erreur lors de la sauvegarde de fichier : le chemin (%1) référence un répertoire au lieu d&apos;un fichier</translation>
    </message>
    <message>
        <source>Save to file error : Unable to create directory %1</source>
        <translation type="vanished">Erreur lors de la sauvegarde de fichier : Impossible de créer le répertoire %1</translation>
    </message>
    <message>
        <source>Save to file error : Unable to save to file %1</source>
        <translation type="vanished">Erreur lors de la sauvegarde de fichier : Impossible de sauver dans le fichier %1</translation>
    </message>
    <message>
        <source>Problem in state restoration : File %1 is not readable.</source>
        <translation type="vanished">Problème lors de la restauration : Fichier %1 non lisible</translation>
    </message>
    <message>
        <source>Open file error : Unable to open file %1</source>
        <translation type="vanished">Erreur de chargement de fichier : Impossible d&apos;ouvrir le fichier %1</translation>
    </message>
</context>
<context>
    <name>GPointOfViewDocumentManager</name>
    <message>
        <source>Point of view Files (</source>
        <translation type="vanished">Fichiers de point de vue (</translation>
    </message>
    <message>
        <source>DEFAULT</source>
        <translation type="vanished">Défaut</translation>
    </message>
    <message>
        <source>Voulez vous continuer et supprimer les points de vue ou crer un nouveau fichier ?</source>
        <translation type="vanished">Voulez vous continuer et supprimer les points de vue ou créer un nouveau fichier ?</translation>
    </message>
</context>
<context>
    <name>GPointsAttributesManager</name>
    <message>
        <source>Configurer les couleurs des points</source>
        <translation type="vanished">Configurer les couleurs</translation>
    </message>
    <message>
        <source>Edge</source>
        <translation type="vanished">Arrête</translation>
    </message>
</context>
<context>
    <name>GRedmineParametersDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Configuration de Redmine</translation>
    </message>
</context>
<context>
    <name>GStepChooserDialog</name>
    <message>
        <source>Exporter</source>
        <translation type="vanished">Exportateur</translation>
    </message>
</context>
<context>
    <name>GStepManager</name>
    <message>
        <source>L&apos;tape ne semble pas tre dbogable.</source>
        <translation type="vanished">L&apos;étape ne semble pas être débogable.</translation>
    </message>
    <message>
        <source>La srialisation semble tre active, si vous executez l&apos;opration  partir de cette tape elle sera dsactive.</source>
        <translation type="vanished">La sérialisation semble être active, si vous exécuter l&apos;opération à partir de cette étape elle sera désactivée.</translation>
    </message>
    <message>
        <source>Voulez-vous quand mme continuer ?</source>
        <translation type="vanished">Voulez-vous quand même continuer ?</translation>
    </message>
    <message>
        <source>Une ou plusieurs étapes sont en mode debug or vous allez lancer les traitements en mode normal.</source>
        <translation type="vanished">Une ou plusieurs étapes sont en mode debug mais vous voulez lancer les traitements en mode normal.</translation>
    </message>
    <message>
        <source>Aucune étape n&apos;est en mode debug or vous allez lancer les traitements dans ce mode.</source>
        <translation type="vanished">Aucune étape n&apos;est en mode debug mais vous voulez lancer les traitements dans ce mode.</translation>
    </message>
</context>
<context>
    <name>GStepManagerOptions</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Options du gestionnaire d&apos;étapes</translation>
    </message>
</context>
<context>
    <name>GStepViewDefault</name>
    <message>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation></translation>
    </message>
    <message>
        <source>Nom des étapes</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Taper du texte pour rechercher une étape. La recherche ne respecte pas la casse.&lt;/p&gt;&lt;p&gt;Vous pouvez si vous le souhaitez utiliser votre propre expression réguilère en tapant tout d&apos;abord &lt;span style=&quot; font-weight:600;&quot;&gt;r:&lt;/span&gt; suivi de votre expression. Si vous ne souhaitez pas utiliser la casse tapez &lt;span style=&quot; font-weight:600;&quot;&gt;i:&lt;/span&gt; après &lt;span style=&quot; font-weight:600;&quot;&gt;r:&lt;/span&gt; puis votre expression régulière. &lt;/p&gt;&lt;p&gt;Exemple simple : &lt;span style=&quot; font-weight:600;&quot;&gt;grilles&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Exemple d&apos;expression régulière : &lt;span style=&quot; font-weight:600;&quot;&gt;r:i:.*grilles.*&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>Rechercher des étapes...</source>
        <translation></translation>
    </message>
    <message>
        <source>Replacer au démarrage à la dernière position connue</source>
        <translation></translation>
    </message>
    <message>
        <source>Clé de l&apos;étape au sein du plugin ou d&apos;un script</source>
        <translation></translation>
    </message>
    <message>
        <source>Nom de l&apos;étape</source>
        <translation></translation>
    </message>
    <message>
        <source>Description courte</source>
        <translation></translation>
    </message>
    <message>
        <source>Description détaillée</source>
        <translation></translation>
    </message>
    <message>
        <source>Replacer à gauche</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Replacer à droite</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GTreeStepContextMenu</name>
    <message>
        <source>ExÜuter</source>
        <translation type="vanished">Exécuter</translation>
    </message>
    <message>
        <source>Une ou plusieurs étapes sont en mode debug or vous allez lancer les traitements en mode normal.</source>
        <translation type="vanished">Une ou plusieurs étapes sont en mode debug mais vous voulez lancer les traitements en mode normal.</translation>
    </message>
    <message>
        <source>Aucune étape n&apos;est en mode debug or vous allez lancer les traitements dans ce mode.</source>
        <translation type="vanished">Aucune étape n&apos;est en mode debug mais vous voulez lancer les traitements dans ce mode.</translation>
    </message>
</context>
<context>
    <name>GTreeView</name>
    <message>
        <source>Sync with..</source>
        <translation type="vanished">Synchroniser avec...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Suppression du resultat %1 des autres vues.</source>
        <translation type="vanished">Suppression du résultat %1 des autres vues.</translation>
    </message>
    <message>
        <source>Veuillez patienter pendant la construction de la table</source>
        <translation type="vanished">Veuillez patienter pendant la construction de la table</translation>
    </message>
    <message>
        <source>G3DPainter (points) =&gt; Vertex shader compilation error : %1</source>
        <translation type="vanished">G3DPainter (points) =&gt; Erreur de compilation des Vertex Shader: %1</translation>
    </message>
    <message>
        <source>G3DPainter (points) =&gt; Link error : %1</source>
        <translation type="vanished">G3DPainter (points) =&gt; Erreur de linkage (Link error) : %1</translation>
    </message>
    <message>
        <source>G3DPainter (points) =&gt; Bind error : %1</source>
        <translation type="vanished">G3DPainter (points) =&gt; Erreur de liaison (Bind error) : %1</translation>
    </message>
</context>
<context>
    <name>QtColorPicker</name>
    <message>
        <source>Black</source>
        <translation type="vanished">Noir</translation>
    </message>
    <message>
        <source>White</source>
        <translation type="vanished">Blanc</translation>
    </message>
    <message>
        <source>Red</source>
        <translation type="vanished">Rouge</translation>
    </message>
    <message>
        <source>Dark red</source>
        <translation type="vanished">Rouge foncé</translation>
    </message>
    <message>
        <source>Green</source>
        <translation type="vanished">Vert</translation>
    </message>
    <message>
        <source>Dark green</source>
        <translation type="vanished">Vert foncé</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation type="vanished">Bleu</translation>
    </message>
    <message>
        <source>Dark blue</source>
        <translation type="vanished">Bleu foncé</translation>
    </message>
    <message>
        <source>Dark cyan</source>
        <translation type="vanished">Cyan foncé</translation>
    </message>
    <message>
        <source>Magenta</source>
        <translation type="vanished">Magenta</translation>
    </message>
    <message>
        <source>Dark magenta</source>
        <translation type="vanished">Magenta foncé</translation>
    </message>
    <message>
        <source>Yellow</source>
        <translation type="vanished">Jaune</translation>
    </message>
    <message>
        <source>Dark yellow</source>
        <translation type="vanished">Jaune foncé</translation>
    </message>
    <message>
        <source>Gray</source>
        <translation type="vanished">Gris</translation>
    </message>
    <message>
        <source>Dark gray</source>
        <translation type="vanished">Gris foncé</translation>
    </message>
    <message>
        <source>Light gray</source>
        <translation type="vanished">Gris clair</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="vanished">Personnalisé</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Load script file &lt;path&gt;</source>
        <translation type="vanished">Charger fichier script &lt;path&gt;</translation>
    </message>
    <message>
        <source>Run script on load</source>
        <translation type="vanished">Lancer le script au chargement</translation>
    </message>
</context>
</TS>
