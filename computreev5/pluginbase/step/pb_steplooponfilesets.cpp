#include "pb_steplooponfilesets.h"
#include "ct_result/ct_resultgroup.h"
#include "ct_result/model/outModel/ct_outresultmodelgroup.h"
#include "ct_view/ct_genericconfigurablewidget.h"
#include "ct_abstractstepplugin.h"
#include "ct_reader/ct_standardreaderseparator.h"

#include "ct_view/ct_combobox.h"
#include "ct_global/ct_context.h"
#include "ct_model/tools/ct_modelsearchhelper.h"
#include "ct_itemdrawable/ct_readeritem.h"
#include "ct_itemdrawable/ct_itemattributelist.h"
#include "ct_view/tools/ct_configurablewidgettodialog.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>

// Alias for indexing models
#define DEFout_res "res"
#define DEFout_grp "grp"
#define DEFout_plotname "plotname"
#define DEFout_plotnameAtt "plotnameAtt"
#define DEFout_grpHeader "grpHeader"
#define DEFout_header "header"
#define DEFout_reader "reader"

// Constructor : initialization of parameters
PB_StepLoopOnFileSets::PB_StepLoopOnFileSets(CT_StepInitializeData &dataInit) : CT_StepBeginLoop(dataInit)
{
    _readersListValue = "";

    initListOfAvailableReaders();
}

PB_StepLoopOnFileSets::~PB_StepLoopOnFileSets()
{
    clear();
}

// Step description (tooltip of contextual menu)
QString PB_StepLoopOnFileSets::getStepDescription() const
{
    return tr("4- Loops on files sets defined in a text file");
}

// Step detailled description
QString PB_StepLoopOnFileSets::getStepDetailledDescription() const
{
    return tr("");
}

// Step URL
QString PB_StepLoopOnFileSets::getStepURL() const
{
    //return tr("STEP URL HERE");
    return CT_AbstractStepCanBeAddedFirst::getStepURL(); //by default URL of the plugin
}

// Step copy method
CT_VirtualAbstractStep* PB_StepLoopOnFileSets::createNewInstance(CT_StepInitializeData &dataInit)
{
    return new PB_StepLoopOnFileSets(dataInit);
}


//////////////////// PROTECTED METHODS //////////////////

void PB_StepLoopOnFileSets::createPreConfigurationDialog()
{
    CT_StepConfigurableDialog *configDialog = newStandardPreConfigurationDialog();

    QStringList list_readersList;

    QListIterator<CT_AbstractReader*> it(_readersInstancesList);

    while (it.hasNext())
        list_readersList.append(it.next()->GetReaderClassName());

    if (list_readersList.isEmpty())
        list_readersList.append(tr("ERREUR : aucun reader disponible"));

    configDialog->addStringChoice(tr("Choose file type"), "", list_readersList, _readersListValue);
}

bool PB_StepLoopOnFileSets::postConfigure()
{
    QString fileFilter = getFormat(_readersListValue);

    QStringList fileList;

    CT_GenericConfigurableWidget configDialog;

    configDialog.addFileChoice(tr("File with sets"), CT_FileChoiceButton::OneExistingFile, tr("Fichier texte (*.txt) ; Fichier texte (*.*)"), _correspfile, tr("First column must contain set name, Second column must contain file path. No header."));

    configDialog.addEmpty();

    configDialog.addFileChoice(tr("Choisir un fichier exemple"), CT_FileChoiceButton::OneOrMoreExistingFiles, fileFilter, fileList);
    configDialog.addTitle(tr("Le fichier choisi doit :"));
    configDialog.addText("", tr("- Etre dans le répertoire des fichiers à charger"), "");
    configDialog.addText("", tr("- Avoir le même format que les fichiers à charger"), "");
    configDialog.addText("", tr("- Avoir la même structure / version que les fichiers à charger"), "");


    if(CT_ConfigurableWidgetToDialog::exec(&configDialog) == QDialog::Accepted) {

        if(fileList.isEmpty())
            return false;

        CT_AbstractReader *reader = getReader(_readersListValue);

        if((reader != NULL) && reader->setFilePath(fileList.first())) {
            reader->setFilePathCanBeModified(false);
            bool ok = reader->configure();
            reader->setFilePathCanBeModified(true);

            if(ok) {
                _basePath.clear();
                _basePath.append(QFileInfo(fileList.first()).path());
                setSettingsModified(true);
            }

            return ok;
        }
    }

    return false;
}

// Creation and affiliation of OUT models
void PB_StepLoopOnFileSets::createOutResultModelListProtected(CT_OutResultModelGroup *firstResultModel)
{
    Q_UNUSED(firstResultModel);

    // create a new result
    CT_OutResultModelGroup *outRes = createNewOutResultModel(DEFout_res, tr("Liste de readers"));

    // add a root group
    outRes->setRootGroup(DEFout_grp, new CT_StandardItemGroup(), tr("Groupe"));
    outRes->addItemModel(DEFout_grp, DEFout_plotname, new CT_ItemAttributeList(), tr("GroupName"));
    outRes->addItemAttributeModel(DEFout_plotname, DEFout_plotnameAtt, new CT_StdItemAttributeT<QString>(CT_AbstractCategory::DATA_VALUE), tr("Name"));


    // get the reader selected
    CT_AbstractReader *reader = getReader(_readersListValue);

    // if one reader was selected and at least one file is defined
    if (reader != NULL && _correspfile.size() > 0)
    {
        // get the header
        CT_FileHeader *rHeader = reader->createHeaderPrototype();

        if(rHeader != NULL) {
            // copy the reader (copyFull = with configuration and models)
            CT_AbstractReader* readerCpy = reader->copyFull();

            outRes->addGroupModel(DEFout_grp, DEFout_grpHeader, new CT_StandardItemGroup(), tr("File"));
            outRes->addItemModel(DEFout_grpHeader, DEFout_reader, new CT_ReaderItem(NULL, NULL, readerCpy), tr("Reader"));
            outRes->addItemModel(DEFout_grpHeader, DEFout_header, rHeader, tr("Header"));
        }
    }
}

void PB_StepLoopOnFileSets::compute(CT_ResultGroup *outRes, CT_StandardItemGroup* group)
{
    Q_UNUSED(outRes);
    Q_UNUSED(group);

    QList<CT_ResultGroup*> outResultList = getOutResultList();

    // get the out result
    CT_ResultGroup* resultOut = outResultList.at(1);

    int currentTurn = (int) _counter->getCurrentTurn();

    if (currentTurn == 1)
    {
        if(_correspfile.isEmpty()) {_counter->setNTurns(1); return;}


        CT_AbstractReader *reader = getReader(_readersListValue);
        if(reader == NULL) {return;}

        int nturns = 0;

        if (_correspfile.size() > 0)
        {
            _setFileInfo = QFileInfo(_correspfile.first());
            if (!_setFileInfo.exists()) {return;}

            QFile setFile(_correspfile.first());
            if (setFile.exists() && setFile.open(QFile::Text | QFile::ReadOnly))
            {
                QTextStream stream(&setFile);

                while (!stream.atEnd())
                {
                    QString line = stream.readLine();

                    QStringList vals = line.split(QRegExp("[\t;,]"));
                    if (vals.size() > 1)
                    {
                        _sets.insert(vals.at(0), vals.at(1));
                        if (!_setKeys.contains(vals.at(0))) {_setKeys.append(vals.at(0)); nturns++;}
                    }
                }
                setFile.close();
            }
        }

        _counter->setNTurns(nturns);
    }

    // search the model for headers
    CT_OutAbstractItemModel* headerModel = (CT_OutAbstractItemModel*)PS_MODELS->searchModelForCreation(DEFout_header, resultOut);

    // create the root group and add it to result
    CT_StandardItemGroup* grp = new CT_StandardItemGroup(DEFout_grp, resultOut);
    resultOut->addGroup(grp);


    CT_AbstractReader *reader = getReader(_readersListValue);

    if(reader != NULL)
    {
        // for each current file in the list
        // copy the reader (copyFull = with configuration and models)

        if (_setKeys.size() >= _counter->getCurrentTurn())
        {
            QString grpKey = _setKeys.at((int)_counter->getCurrentTurn() - 1);
            QStringList filesToRead = _sets.values(grpKey);

            _counter->setTurnName(grpKey);

            CT_ItemAttributeList* attList = new CT_ItemAttributeList(DEFout_plotname, resultOut);
            attList->addItemAttribute(new CT_StdItemAttributeT<QString>(DEFout_plotnameAtt,
                                                                        CT_AbstractCategory::DATA_VALUE,
                                                                        resultOut,
                                                                        grpKey));
            grp->addItemDrawable(attList);


            for (int f = 0 ; f < filesToRead.size() ; f++)
            {
                CT_AbstractReader* readerCpy = reader->copyFull();
                const QList<FileFormat> &formats = readerCpy->readableFormats();

                QString filepath = filesToRead.at(f);

                if (_basePath.size() > 0)
                {
                    QString basePath = _basePath.first();
                    if (basePath.size() > 0)
                    {
                        filepath.push_front(QString("%1%2").arg(_basePath.first()).arg("/"));
                    }
                }

                if (formats.size() > 0)
                {
                    QFileInfo filepathInfo(filepath);
                    filepath = QString("%1/%2.%3").arg(filepathInfo.path()).arg(filepathInfo.baseName()).arg(formats.first().suffixes().first());
                }

                PS_LOG->addMessage(LogInterface::info, LogInterface::step, tr("Chargement du fichier %1").arg(filepath));

                // set the new filepath and check if it is valid
                if (readerCpy->setFilePath(filepath))
                {
                    // create models of this reader
                    if(readerCpy->outItemDrawableModels().isEmpty() && reader->outGroupsModel().isEmpty())
                    {
                        readerCpy->createOutItemDrawableModelList();
                    }

                    // create the group that will contains header and reader (represent a File)
                    CT_StandardItemGroup* grpHeader = new CT_StandardItemGroup(DEFout_grpHeader, resultOut);

                    CT_FileHeader *header = readerCpy->readHeader();
                    header->changeResult(resultOut);
                    header->setModel(headerModel);

                    // add the header
                    grpHeader->addItemDrawable(header);

                    // add the reader
                    grpHeader->addItemDrawable(new CT_ReaderItem(DEFout_reader, resultOut, readerCpy));

                    // add the group to the root
                    grp->addGroup(grpHeader);

                }
                else
                {
                    PS_LOG->addMessage(LogInterface::warning, LogInterface::step, tr("Fichier %1 inexistant ou non valide").arg(filepath));
                    delete readerCpy;
                }
            }
        }
    }

}

SettingsNodeGroup *PB_StepLoopOnFileSets::getAllSettings() const
{
    SettingsNodeGroup *root = CT_AbstractStepCanBeAddedFirst::getAllSettings();
    SettingsNodeGroup *group = new SettingsNodeGroup("PB_StepLoopOnFileSets");
    group->addValue(new SettingsNodeValue("Version", "1"));

    SettingsNodeGroup *settings = new SettingsNodeGroup("Settings");
    settings->addValue(new SettingsNodeValue("ReaderSelected", _readersListValue));

    QString fileFolder = "";
    if (_correspfile.size() > 0)
    {
        fileFolder = _correspfile.first();
    }
    settings->addValue(new SettingsNodeValue("path", fileFolder));


    QString basePathfileFolder = "";
    if (_basePath.size() > 0)
    {
        basePathfileFolder = _basePath.first();
    }
    settings->addValue(new SettingsNodeValue("basefilepath", basePathfileFolder));


    SettingsNodeGroup *readerSettings = new SettingsNodeGroup("ReaderSettings");
    settings->addGroup(readerSettings);

    CT_AbstractReader *reader = getReader(_readersListValue);

    if(reader != NULL)
        readerSettings->addGroup(reader->getAllSettings());

    group->addGroup(settings);
    root->addGroup(group);

    return root;
}

bool PB_StepLoopOnFileSets::setAllSettings(const SettingsNodeGroup *settings)
{
    bool ok = CT_AbstractStepCanBeAddedFirst::setAllSettings(settings);

    if(ok)
    {
        QList<SettingsNodeGroup*> groups = settings->groupsByTagName("PB_StepLoopOnFileSets");
        if(groups.isEmpty()) {return false;}

        groups = groups.first()->groupsByTagName("Settings");
        if(groups.isEmpty()) {return false;}

        SettingsNodeGroup *settings2 = groups.first();

        QList<SettingsNodeValue*> values = settings2->valuesByTagName("ReaderSelected");
        if(values.isEmpty()) {return false;}

        _readersListValue = values.first()->value().toString();

        _correspfile.clear();

        values = settings2->valuesByTagName("path");
        if(values.isEmpty()) {return false;}

        _correspfile.append(values.first()->value().toString());


        _basePath.clear();

        values = settings2->valuesByTagName("basefilepath");
        if(values.isEmpty()) {return false;}

        _basePath.append(values.first()->value().toString());



        groups = settings2->groupsByTagName("ReaderSettings");
        if(groups.isEmpty()) {return false;}

        groups = groups.first()->groups();

        CT_AbstractReader *reader = getReader(_readersListValue);

        if(reader != NULL) {
            if(groups.isEmpty()) {return false;}

            ok = reader->setAllSettings(groups.first());
        }

        //        if(!groups.isEmpty())
        //            return false;

        return true;
    }

    return ok;
}

void PB_StepLoopOnFileSets::initListOfAvailableReaders()
{
    clear();

    // get the plugin manager
    PluginManagerInterface *pm = PS_CONTEXT->pluginManager();
    int s = pm->countPluginLoaded();

    // for each plugin
    for(int i = 0; i < s; ++i)
    {
        CT_AbstractStepPlugin *p = pm->getPlugin(i);

        // get readers
        QList<CT_StandardReaderSeparator*> rsl = p->getReadersAvailable();
        QListIterator<CT_StandardReaderSeparator*> itR(rsl);

        while(itR.hasNext())
        {
            CT_StandardReaderSeparator *rs = itR.next();
            QListIterator<CT_AbstractReader*> itE(rs->readers());

            while(itE.hasNext())
            {
                CT_AbstractReader *reader = itE.next();

                // copy the reader
                CT_AbstractReader *readerCpy = reader->copy();
                readerCpy->init(false);

                // and add it to the list
                _readersInstancesList.append(readerCpy);
            }
        }
    }
}

void PB_StepLoopOnFileSets::clear()
{
    qDeleteAll(_readersInstancesList.begin(), _readersInstancesList.end());
    _readersInstancesList.clear();
}

QString PB_StepLoopOnFileSets::getFormat(QString readerClassName) const
{
    CT_AbstractReader *reader = getReader(readerClassName);

    QStringList formats;
    if(reader != NULL) {
        FileFormat fileFormat = reader->readableFormats().first();

        const QList<QString> &suffixes = fileFormat.suffixes();
        for (int i = 0 ; i < suffixes.size() ; i++)
        {
            QString formatText = "*.";
            formatText.append(suffixes.at(i));
            formats.append(formatText);
        }
    }
    return formats.first();
}

CT_AbstractReader *PB_StepLoopOnFileSets::getReader(QString readerClassName) const
{
    QListIterator<CT_AbstractReader*> it(_readersInstancesList);

    while(it.hasNext()) {
        CT_AbstractReader *reader = it.next();

        if(reader->GetReaderClassName() == readerClassName) {
            return reader;
        }
    }

    return NULL;
}

