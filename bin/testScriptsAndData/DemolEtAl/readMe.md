## Evalutation of SimpleForest QSMs

* QSMs up to twigs:

![plot](./QSMComplete.png)

* QSMs cropped at 7cm diameter:

![plot](./QSMCoarse.png)

* QSMs up to twigs. SimpleForest and TreeQSM have to be in 5% range to be not filtered out:

![plot](./QSMBest.png)

## Data repository homepage:
    https://zenodo.org/record/4557401
    
## Related publication:
    https://link.springer.com/article/10.1007/s00468-020-02067-7
    
## Publication date:
    February 23, 2021
    
## DOI:
    10.5281/zenodo.4557401
    
## Subject(s):
    Fraxinus excelsior
    Fagus sylvatica
    Pinus sylvestris
    Larix decidua
    
## Grants:
    European Commission:
        RINGO - Readiness of ICOS for Necessities of integrated Global Observations (730944)

## Related identifiers:
    Documented by
        10.1007/s00468-020-02067-7 (Journal article)

## License (for files):
    Creative Commons Attribution 4.0 International
