#include "copymodelcreator.h"
#include "model/step/models/inresultmodel.h"
#include "model/step/models/copyresultmodel.h"
#include "model/step/models/copygroupmodel.h"
#include "model/step/models/copyitemmodel.h"

#include "model/step/tools.h"

CopyModelCreator::CopyModelCreator(QStandardItemModel *inModel, QObject *parent) :
    QObject(parent)
{
    _model = new QStandardItemModel();
    _inModel = inModel;
    init();
}

void CopyModelCreator::init()
{
    QStandardItem* inRoot = _inModel->invisibleRootItem();
    _model->clear();

    QStandardItem* copyRoot = _model->invisibleRootItem();
    int count = inRoot->rowCount();
    for (int i = 0 ; i < count ; i++)
    {
        INResultModel* inResult = (INResultModel*) inRoot->child(i);

        if (inResult->isCopyResult())
        {
            COPYResultModel* copyResult = new COPYResultModel();
            copyResult->init(inResult);
            copyRoot->appendRow(copyResult);
            recursiveAddChildren(copyResult, inResult);
        }
    }
}


// static
void CopyModelCreator::recursiveAddChildren(AbstractCopyModel* copyModel, AbstractInModel* inModel)
{
    int count = inModel->rowCount();
    for (int i = 0 ; i < count ; i++)
    {
        AbstractInModel* inItem = (AbstractInModel*) inModel->child(i);

        if (inItem->getModelType() == AbstractInModel::M_Group_IN)
        {
            COPYGroupModel* copyItem = new COPYGroupModel();
            copyItem->init((INGroupModel*) inItem);
            copyModel->appendRow(copyItem);
            recursiveAddChildren(copyItem, inItem);

        } else if (inItem->getModelType() == AbstractInModel::M_Item_IN)
        {
            COPYItemModel* copyItem = new COPYItemModel();
            copyItem->init((INItemModel*) inItem);
            copyModel->appendRow(copyItem);
            recursiveAddChildren(copyItem, inItem);
        }
    }
}

QString CopyModelCreator::getAutoRenamesDeclarations()
{
    QString result = "";
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();
    for (int i = 0 ; i < count ; i++)
    {
        AbstractCopyModel* item = (AbstractCopyModel*) root->child(i);
        result += item->getAutoRenamesDeclarations();
    }
    return result;
}

QString CopyModelCreator::getCopyDefines()
{
    QString result = "";
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();
    for (int i = 0 ; i < count ; i++)
    {
        AbstractCopyModel* item = (AbstractCopyModel*) root->child(i);
        result += item->getCopyModelsDefines();
        result += "\n";
    }
    return result;
}


void CopyModelCreator::getIncludes(QSet<QString> &list)
{
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();
    for (int i = 0 ; i < count ; i++)
    {
        AbstractCopyModel* item = (AbstractCopyModel*) root->child(i);
        item->getIncludes(list);
    }
}

QString CopyModelCreator::getCreateOutResultModelListProtectedContent()
{
    QString result = "";
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();
    for (int i = 0 ; i < count ; i++)
    {
        AbstractCopyModel* item = (AbstractCopyModel*) root->child(i);
        result += item->getCreateOutResultModelListProtectedContent();
    }
    return result;
}

QString CopyModelCreator::getResultRecovery(int &nextRank)
{
    QString result = "";
    nextRank = 0;
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();

    if (count > 0)
    {
        result += Tools::getIndentation(1) + "QList<CT_ResultGroup*> outResultList = getOutResultList();\n";
    }

    for (int i = 0 ; i < count ; i++)
    {
        COPYResultModel* item = (COPYResultModel*) root->child(i);
        result += Tools::getIndentation(1) + "CT_ResultGroup* " + item->getName() + " = outResultList.at(" + QString("%1").arg(nextRank++) + ");\n";
    }
    return result;
}

QString CopyModelCreator::getComputeContent()
{
    QString result = "";
    QStandardItem* root = _model->invisibleRootItem();
    int count = root->rowCount();

    for (int i = 0 ; i < count ; i++)
    {
        COPYResultModel* item = (COPYResultModel*) root->child(i);
        result += item->getComputeContent();
    }
    return result;
}
