#include "initemmodel.h"
#include "view/step/models/initemwidget.h"
#include "model/step/tools.h"

INItemModel::INItemModel() : AbstractInModel()
{
    _widget = new INItemWidget(this);
    setText(getPrefixedAlias());
}

QString INItemModel::getName()
{
    return "itemIn_" + getAlias();
}

QString INItemModel::getItemType()
{
    return ((INItemWidget*) _widget)->getItemType();
}

QString INItemModel::getItemTemplate()
{
    return ((INItemWidget*) _widget)->getTemplate();
}

QString INItemModel::getItemTypeWithTemplate()
{
    return ((INItemWidget*) _widget)->getItemTypeWithTemplate();
}

QString INItemModel::getItemInCode(int indent, QString name)
{
    AbstractItemType* itemType = Tools::ITEMTYPE.value(getItemType(), NULL);
    if (itemType == NULL) {return "";}
    QString result = itemType->getInCode(indent, name);
    return result;
}

QString INItemModel::getChoiceMode()
{
    INItemWidget::ChoiceMode choiceMode = ((INItemWidget*)_widget)->getChoiceMode();

    if (choiceMode == INItemWidget::C_MultipleIfMultiple)
    {
        return "CT_InAbstractModel::C_ChooseMultipleIfMultiple";;
    }
    return "CT_InAbstractModel::C_ChooseOneIfMultiple";
}


QString INItemModel::getFinderMode()
{
    INItemWidget::FinderMode finderMode = ((INItemWidget*)_widget)->getFinderMode();

    if (finderMode == INItemWidget::F_Optional)
    {
        return "CT_InAbstractModel::F_IsOptional";
    }
    return "CT_InAbstractModel::F_IsObligatory";
}

void INItemModel::getIncludes(QSet<QString> &list)
{
    AbstractItemType* itemType = Tools::ITEMTYPE.value(getItemType(), NULL);
    if (itemType != NULL)
    {
        list.insert(itemType->getInclude());
    }
}

QString INItemModel::getCreateInResultModelListProtectedContent(QString parentDEF, QString resultModelName, bool rootGroup)
{
    Q_UNUSED(rootGroup);

    QString result = "";

    result += Tools::getIndentation(1) + resultModelName;
    result += "->addItemModel(" + parentDEF + ", " + getDef() + ", " + getItemTypeWithTemplate() + "::staticGetType()";

    if (getDisplayableName() != "" || getDescription() != "" || getChoiceMode() != "CT_InAbstractModel::C_ChooseOneIfMultiple" || getFinderMode() != "CT_InAbstractModel::F_IsObligatory")
    {
        result += ", " + Tools::trs(getDisplayableName());
    }

    if (getDescription() != "" || getChoiceMode() != "CT_InAbstractModel::C_ChooseOneIfMultiple" || getFinderMode() != "CT_InAbstractModel::F_IsObligatory")
    {
        result += ", " + Tools::trs(getDescription());
    }

    if (getChoiceMode() != "CT_InAbstractModel::C_ChooseOneIfMultiple" || getFinderMode() != "CT_InAbstractModel::F_IsObligatory")
    {
        result += ", " + getChoiceMode();
    }

    if (getFinderMode() != "CT_InAbstractModel::F_IsObligatory")
    {
        result += ", " + getFinderMode();
    }
    result += ");\n";

    return result;
}

QString INItemModel::getComputeContent(QString parentName, int indent, bool rootGroup)
{
    Q_UNUSED(rootGroup);

    QString result = "";
    QString itemCode = getItemInCode(indent+1, getName());

    result += Tools::getIndentation(indent) + "const " + getItemTypeWithTemplate() + "* " + getName();
    result += " = (" + getItemTypeWithTemplate() + "*)" + parentName + "->firstItemByINModelName(this, " + getDef() + ");\n";
    result += Tools::getIndentation(indent) + "if (" + getName() + " != NULL)\n";
    result += Tools::getIndentation(indent) + "{\n";
    if (itemCode.size() > 0)
    {
        result += itemCode;
        result += "\n";
    }
    result += Tools::getIndentation(indent) + "}\n";
    result += "\n";



    return result;
}

