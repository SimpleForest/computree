#ifndef COPYITEMMODEL_H
#define COPYITEMMODEL_H

#include "model/step/models/initemmodel.h"
#include "model/step/models/abstractcopymodel.h"

class COPYItemModel : public AbstractCopyModel
{
public:
    COPYItemModel();
    void init(INItemModel *inModel);
    void init(QString itemType, QString temp, QString alias, QString name, QString desc);

    QString getName();
    virtual AbstractCopyModel::ModelType getModelType() {return AbstractCopyModel::M_Item_COPY;}
    QString getItemType();
    QString getItemTemplate();
    QString getItemTypeWithTemplate();
    QString getItemInCode(int indent, QString name);
    QString getItemOutCode(int indent, QString name, QString modelName, QString resultName);

    virtual void getIncludes(QSet<QString> &list);
    virtual QString getCreateOutResultModelListProtectedContent(QString parentDEF = "", QString resultModelName = "");
    virtual QString getComputeContent(QString resultName, QString parentName = "", int indent = 1, bool rootGroup = false);

};

#endif // OUITEMMODEL_H
