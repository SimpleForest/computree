#ifndef ABTRACTINMODEL_H
#define ABTRACTINMODEL_H

#include "qstandarditemmodel.h"
#include "view/step/models/abstractinwidget.h"
#include "qset.h"

class AbstractInModel : public QStandardItem
{
public:

    enum ModelType
    {
        M_Result_IN,
        M_Group_IN,
        M_Item_IN
    };

    AbstractInModel();
    ~AbstractInModel();

    virtual AbstractInModel::ModelType getModelType() = 0;
    virtual AbstractInWidget* getWidget();

    virtual QString getDef();
    virtual QString getPrefixedAlias();
    virtual QString getAlias();
    virtual QString getName() = 0;
    virtual QString getDisplayableName();
    virtual QString getDescription();
    virtual bool isValid();

    virtual QString getInModelsDefines();

    virtual void getIncludes(QSet<QString> &list) = 0;
    virtual QString getCreateInResultModelListProtectedContent(QString parentDEF = "", QString resultModelName = "", bool rootGroup = false) = 0;
    virtual QString getComputeContent(QString parentName = "", int indent = 1, bool rootGroup = false) = 0;

    virtual void getChildrenIncludes(QSet<QString> &list);
    virtual void getChildrenCreateInResultModelListProtectedContent(QString &result, QString parentDEF = "", QString resultModelName = "");
    virtual void getChildrenComputeContent(QString &result, int indent);


    void onAliasChange();

protected:
    AbstractInWidget*  _widget;

};

#endif // ABTRACTINMODEL_H
