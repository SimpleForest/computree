#ifndef OUTITEMMODEL_H
#define OUTITEMMODEL_H

#include "model/step/models/abstractoutmodel.h"

class OUTItemModel : public AbstractOutModel
{
public:
    OUTItemModel();

    virtual AbstractOutModel::ModelType getModelType() {return AbstractOutModel::M_Item_OUT;}
    QString getItemType();
    QString getItemTemplate();
    QString getItemTypeWithTemplate();
    QString getName();
    QString getItemOutCode(int indent, QString name, QString modelName, QString resultName);

    virtual void getIncludes(QSet<QString> &list);
    virtual QString getCreateOutResultModelListProtectedContent(QString parentDEF = "", QString resultModelName = "", bool rootGroup = false);
    virtual QString getComputeContent(QString resultName, QString parentName);
};

#endif // OUITEMMODEL_H
