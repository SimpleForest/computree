#ifndef ABTRACTCOPYMODEL_H
#define ABTRACTCOPYMODEL_H

#include "qstandarditemmodel.h"
#include "view/step/models/abstractcopywidget.h"
#include "qset.h"

class AbstractCopyModel : public QStandardItem
{
public:

    enum ModelType
    {
        M_Result_COPY,
        M_Group_COPY,
        M_Item_COPY
    };

    enum Status
    {
        S_Copy,
        S_DeletedCopy,
        S_Added
    };

    AbstractCopyModel();
    ~AbstractCopyModel();

    virtual AbstractCopyModel::ModelType getModelType() = 0;
    AbstractCopyWidget* getWidget();

    virtual QString getDef();
    virtual QString getPrefixedAlias();
    virtual QString getAlias();
    virtual QString getName() = 0;
    virtual QString getAutoRenameName();
    virtual QString getDisplayableName();
    virtual QString getDescription();
    virtual bool isValid();

    virtual QString getCopyModelsDefines();

    inline AbstractCopyModel::Status getStatus() {return _status;}

    QString getAutoRenamesDeclarations();

    virtual void getIncludes(QSet<QString> &list) = 0;
    virtual QString getCreateOutResultModelListProtectedContent(QString parentDEF = "", QString resultModelName = "") = 0;
    virtual QString getComputeContent(QString resultName, QString parentName = "", int indent = 1, bool rootGroup = false) = 0;

    virtual void getChildrenIncludes(QSet<QString> &list);
    virtual void getChildrenCreateOutResultModelListProtectedContent(QString &result, QString parentDEF = "", QString resultModelName = "");
    virtual void getChildrenComputeContent(QString &result, QString resultName, int indent);

    void setDeleted();
    void setNotDeleted();

    void onAliasChange();

protected:
    AbstractCopyWidget*  _widget;
    AbstractCopyModel::Status   _status;

};

#endif // ABTRACTCOPYMODEL_H
