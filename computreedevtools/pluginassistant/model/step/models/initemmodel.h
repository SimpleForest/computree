#ifndef INITEMMODEL_H
#define INITEMMODEL_H

#include "model/step/models/abstractinmodel.h"

class INItemModel : public AbstractInModel
{
public:
    INItemModel();

    QString getName();
    virtual AbstractInModel::ModelType getModelType() {return AbstractInModel::M_Item_IN;}
    QString getItemType();
    QString getItemTemplate();
    QString getItemTypeWithTemplate();
    QString getItemInCode(int indent, QString name);
    QString getItemOutCode(int indent, QString name, QString modelName, QString resultName);

    QString getChoiceMode();
    QString getFinderMode();

    virtual void getIncludes(QSet<QString> &list);
    virtual QString getCreateInResultModelListProtectedContent(QString parentDEF = "", QString resultModelName = "", bool rootGroup = false);
    virtual QString getComputeContent(QString parentName = "", int indent = 1, bool rootGroup = false);

};

#endif // INITEMMODEL_H
