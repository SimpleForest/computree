#include "model/step/models/abstractcopymodel.h"
#include "model/step/tools.h"

AbstractCopyModel::AbstractCopyModel() : QStandardItem()
{
    _status = AbstractCopyModel::S_Copy;
}

AbstractCopyModel::~AbstractCopyModel()
{
    delete _widget;
}
AbstractCopyWidget* AbstractCopyModel::getWidget()
{
    return _widget;
}

QString AbstractCopyModel::getDef()
{
    return _widget->getDEF();
}

QString AbstractCopyModel::getPrefixedAlias()
{
    return _widget->getPrefixedAliad();
}

QString AbstractCopyModel::getAlias()
{
    return _widget->getAlias();
}

QString AbstractCopyModel::getAutoRenameName()
{
    return QString("_%1_ModelName").arg(getAlias());
}

QString AbstractCopyModel::getDisplayableName()
{
    return _widget->getDisplayableName();
}

QString AbstractCopyModel::getDescription()
{
    return _widget->getDescription();
}

bool AbstractCopyModel::isValid()
{
    for (int i = 0 ; i < rowCount() ; i++)
    {
        AbstractCopyModel* item = (AbstractCopyModel*) child(i);
        if (!item->isValid()) {return false;}
    }
    return getWidget()->isvalid();
}

QString AbstractCopyModel::getCopyModelsDefines()
{
    QString result = "";

//    if (getStatus() == AbstractCopyModel::S_Added)
//    {
//        result += "#define ";
//        result.append(getDef());
//        result.append(QString(" \"%1\"\n").arg(getAlias()));
//    }

//    int size = rowCount();
//    for (int i = 0 ; i < size ; i++)
//    {
//        AbstractCopyModel* item = (AbstractCopyModel*) child(i);
//        result.append(item->getCopyModelsDefines());
//    }
    return result;
}

QString AbstractCopyModel::getAutoRenamesDeclarations()
{
    QString result = "";

    if (getStatus()==AbstractCopyModel::S_Added)
    {
        result += Tools::getIndentation(1) + "CT_AutoRenameModels" + Tools::getIndentation(1) + getAutoRenameName() + ";\n";
    }

    int count = rowCount();
    for (int i = 0 ; i < count ; i++)
    {
        AbstractCopyModel* item = (AbstractCopyModel*) child(i);
        result += item->getAutoRenamesDeclarations();
    }
    return result;
}

void AbstractCopyModel::getChildrenIncludes(QSet<QString> &list)
{
    int size = rowCount();
    for (int i = 0 ; i < size ; i++)
    {
        AbstractCopyModel* item = (AbstractCopyModel*) child(i);
        item->getIncludes(list);
    }
}

void AbstractCopyModel::getChildrenCreateOutResultModelListProtectedContent(QString &result, QString parentDEF, QString resultModelName)
{
    int size = rowCount();
    for (int i = 0 ; i < size ; i++)
    {
        AbstractCopyModel* item = (AbstractCopyModel*) child(i);

        QString childContent = item->getCreateOutResultModelListProtectedContent(parentDEF, resultModelName);
        if (childContent.size() > 0)
        {
            result.append(childContent);
        }
    }
}

void AbstractCopyModel::getChildrenComputeContent(QString &result, QString resultName, int indent)
{
    int size = rowCount();
    for (int i = 0 ; i < size ; i++)
    {
        AbstractCopyModel* item = (AbstractCopyModel*) child(i);

        QString childContent = item->getComputeContent(resultName, getName(), indent);
        if (childContent.size() > 0)
        {
            result.append(childContent);
        }
    }
}

void AbstractCopyModel::setDeleted()
{
    _status = AbstractCopyModel::S_DeletedCopy;
    setData(QVariant(QColor(Qt::red)),Qt::ForegroundRole);
}

void AbstractCopyModel::setNotDeleted()
{
    _status = AbstractCopyModel::S_Copy;
    setData(QVariant(QColor(Qt::black)),Qt::ForegroundRole);
}


void AbstractCopyModel::onAliasChange()
{
    setText(getPrefixedAlias());
}
