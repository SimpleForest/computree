#ifndef OUTGROUPMODEL_H
#define OUTGROUPMODEL_H

#include "model/step/models/abstractoutmodel.h"

class OUTGroupModel : public AbstractOutModel
{
public:
    OUTGroupModel();

    virtual AbstractOutModel::ModelType getModelType() {return AbstractOutModel::M_Group_OUT;}
    QString getName();

    virtual void getIncludes(QSet<QString> &list);
    virtual QString getCreateOutResultModelListProtectedContent(QString parentDEF = "", QString resultModelName = "", bool rootGroup = false);
    virtual QString getComputeContent(QString resultName, QString parentName);

};

#endif // OUTGROUPMODEL_H
