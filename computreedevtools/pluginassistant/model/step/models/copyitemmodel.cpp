#include "copyitemmodel.h"
#include "view/step/models/copyitemwidget.h"
#include "model/step/tools.h"
#include "assert.h"

COPYItemModel::COPYItemModel() : AbstractCopyModel()
{
    _widget = new COPYItemWidget(this);
    _status = AbstractCopyModel::S_Added;
    setData(QVariant(QColor(Qt::blue)),Qt::ForegroundRole);
    setText(getPrefixedAlias());
}


void COPYItemModel::init(QString itemType, QString temp, QString alias, QString name, QString desc)
{
    ((COPYItemWidget*)_widget)->init(itemType, temp, alias, name, desc);
    _status = AbstractCopyModel::S_Copy;
    setData(QVariant(QColor(Qt::black)),Qt::ForegroundRole);
    setText(getPrefixedAlias() + " (copie)");
}

void COPYItemModel::init(INItemModel *inModel)
{
    init(inModel->getItemType(), inModel->getItemTemplate(), inModel->getAlias(), inModel->getDisplayableName(), inModel->getDescription());
}

QString COPYItemModel::getName()
{
    return "itemCpy_" + getAlias();
}

QString COPYItemModel::getItemType()
{
    return ((COPYItemWidget*) _widget)->getItemType();
}


QString COPYItemModel::getItemTemplate()
{
    return ((COPYItemWidget*) _widget)->getTemplate();
}

QString COPYItemModel::getItemTypeWithTemplate()
{
    return ((COPYItemWidget*) _widget)->getItemTypeWithTemplate();
}

QString COPYItemModel::getItemInCode(int indent, QString name)
{
    AbstractItemType* itemType = Tools::ITEMTYPE.value(getItemType(), NULL);
    if (itemType == NULL) {return "";}
    QString result = itemType->getInCode(indent, name);
    return result;
}

QString COPYItemModel::getItemOutCode(int indent, QString name, QString modelName, QString resultName)
{
    AbstractItemType* itemType = Tools::ITEMTYPE.value(getItemType(), NULL);
    if (itemType == NULL) {return "";}
    QString result = itemType->getOutCode(indent, name, modelName, resultName);
    return result;
}

void COPYItemModel::getIncludes(QSet<QString> &list)
{
    AbstractItemType* itemType = Tools::ITEMTYPE.value(getItemType(), NULL);
    if (itemType != NULL)
    {
        list.insert(itemType->getInclude());
    }
}

QString COPYItemModel::getCreateOutResultModelListProtectedContent(QString parentDEF, QString resultModelName)
{
    QString result = "";

    if (getStatus() == S_Added) {

        result += Tools::getIndentation(2) + resultModelName;

        if (parentDEF == "") {
            result += "->addItemModel(\"\", ";
        } else {
            result += "->addItemModel(" + parentDEF +", ";
        }

        result += getAutoRenameName() + ", new " + getItemTypeWithTemplate() + "()";

        if (getDisplayableName() != "" || getDescription() != "")
        {
            result += ", " + Tools::trs(getDisplayableName());
        }

        if (getDescription() != "")
        {
            result += ", " + Tools::trs(getDescription());
        }

        result += ");\n";
    } else if (getStatus() == S_DeletedCopy){

        result += Tools::getIndentation(2) + resultModelName;
        result += "->removeItemModel(" + getDef() +");\n";
    }

    return result;
}

QString COPYItemModel::getComputeContent(QString resultName, QString parentName, int indent, bool rootGroup)
{
    Q_UNUSED(rootGroup);

    QString result = "";
    QString itemCode = getItemInCode(indent+1, getName());

    if (getStatus() == S_Copy)
    {
        result += Tools::getIndentation(indent) + "const " + getItemTypeWithTemplate() + "* " + getName();
        result += " = (" + getItemTypeWithTemplate() + "*)" + parentName + "->firstItemByINModelName(this, " + getDef() + ");\n";
        result += Tools::getIndentation(indent) + "if (" + getName() + " != NULL)\n";
        result += Tools::getIndentation(indent) + "{\n";
        if (itemCode.size() > 0)
        {
            result += "\n";
            result += itemCode;
            result += "\n";
        }
        result += Tools::getIndentation(indent) + "}\n";

    } else if (getStatus() == S_Added)
    {
        QString itemOutCode = getItemOutCode(indent, getName(), getAutoRenameName() + ".completeName()", resultName);
        if (itemOutCode.size() > 0)
        {
            result += itemOutCode;
            result += Tools::getIndentation(indent) + parentName + "->addItemDrawable(" + getName() + ");\n";
            result += "\n";
        }
    }

    return result;
}
