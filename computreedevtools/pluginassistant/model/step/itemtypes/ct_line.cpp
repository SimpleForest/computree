#include "ct_line.h"
#include "model/step/tools.h"

CT_Line::CT_Line() : AbstractItemType()
{
}

QString CT_Line::getTypeName()
{
    return "CT_Line";
}

QString CT_Line::getInCode(int indent, QString name)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    return "";
}

QString CT_Line::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    QString result = "";
    result += getCreationCode(indent, name, modelName, resultName);
    return result;
}
