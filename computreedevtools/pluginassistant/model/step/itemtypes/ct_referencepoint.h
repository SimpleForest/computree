#ifndef CT_REFERENCEPOINT_H
#define CT_REFERENCEPOINT_H

#include "model/step/itemtypes/abstractitemtype.h"

class CT_ReferencePoint : public AbstractItemType
{
public:
    CT_ReferencePoint();

    virtual QString getTypeName();

    virtual QString getInCode(int indent, QString name);
    virtual QString getOutCode(int indent, QString name, QString modelName, QString resultName);

};

#endif // CT_REFERENCEPOINT_H
