#include "ct_scene.h"
#include "model/step/tools.h"

CT_Scene::CT_Scene() : AbstractItemType()
{
}

QString CT_Scene::getTypeName()
{
    return "CT_Scene";
}

QString CT_Scene::getInclude()
{
    QString result = "";
    result += AbstractItemType::getInclude();
    result += "#include \"ct_pointcloudindex/ct_pointcloudindexvector.h\"\n";
    return result;
}

QString CT_Scene::getInCode(int indent, QString name)
{

    QString result = "";

    result += Tools::getIndentation(indent) + "const CT_AbstractPointCloudIndex *PCI_" + name + " = " + name +"->getPointCloudIndex();\n";
    result += Tools::getIndentation(indent) + "size_t nPts_" + name + " = PCI_" + name + "->size();\n";
    result += "\n";

    result += Tools::getIndentation(indent) + "for (int i = 0 ; i < nPts_" + name + " && !isStopped(); i++)\n";
    result += Tools::getIndentation(indent) + "{\n";
    result += Tools::getIndentation(indent+1) + "size_t index; // global index in the point repository\n";
    result += Tools::getIndentation(indent+1) + "const CT_Point &point = PCI_" + name + "->constTAt(i, index);\n";
    result += Tools::getIndentation(indent) + "}\n";

    return result;
}

QString CT_Scene::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    QString result = "";

    result += Tools::getIndentation(indent) + "// Limits of the CT_Scene bounding box (should be updated)\n";
    result += Tools::getIndentation(indent) + "float xmin = std::numeric_limits<float>::max();\n";
    result += Tools::getIndentation(indent) + "float ymin = std::numeric_limits<float>::max();\n";
    result += Tools::getIndentation(indent) + "float zmin = std::numeric_limits<float>::max();\n";
    result += Tools::getIndentation(indent) + "float xmax = -std::numeric_limits<float>::max(); // -max and not min, because for floats, min give the smallest positive number\n";
    result += Tools::getIndentation(indent) + "float ymax = -std::numeric_limits<float>::max(); // -max and not min, because for floats, min give the smallest positive number\n";
    result += Tools::getIndentation(indent) + "float zmax = -std::numeric_limits<float>::max(); // -max and not min, because for floats, min give the smallest positive number\n";
    result += "\n";
    result += getCreationCode(indent, name, modelName, resultName);
    result += Tools::getIndentation(indent) + name + "->setBoundingBox(xmin,ymin,zmin, xmax,ymax,zmax);\n";
    result += "\n";
    result += Tools::getIndentation(indent) + "CT_PointCloudIndexVector* " + name + "_PCI = new CT_PointCloudIndexVector();\n";
    result += Tools::getIndentation(indent) + "size_t index; // Global index (in point repository), to be added to the CT_Scene Point Cloud Index\n";
    result += Tools::getIndentation(indent) + name + "_PCI->addIndex(index);\n";
    result += "\n";
    result += Tools::getIndentation(indent) + "if (" + name + "_PCI->size() > 0)\n";
    result += Tools::getIndentation(indent) + "{\n";

    result += Tools::getIndentation(indent+1) + "// The PCI must be registered to the point repository :\n";
    result += Tools::getIndentation(indent+1) + name + "->setPointCloudIndexRegistered(PS_REPOSITORY->registerPointCloudIndex(" + name + "_PCI));\n";
    result += Tools::getIndentation(indent) + "} else {delete " + name + "_PCI;}\n";
    result += "\n";

    return result;
}
