#ifndef CT_CIRCLE_H
#define CT_CIRCLE_H

#include "model/step/itemtypes/abstractitemtype.h"

class CT_Circle : public AbstractItemType
{
public:
    CT_Circle();

    virtual QString getTypeName();

    virtual QString getInCode(int indent, QString name);
    virtual QString getOutCode(int indent, QString name, QString modelName, QString resultName);

};

#endif // CT_CIRCLE_H
