#include "ct_cylinder.h"
#include "model/step/tools.h"

CT_Cylinder::CT_Cylinder() : AbstractItemType()
{
}

QString CT_Cylinder::getTypeName()
{
    return "CT_Cylinder";
}

QString CT_Cylinder::getInCode(int indent, QString name)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    return "";
}

QString CT_Cylinder::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    QString result = "";
    result += getCreationCode(indent, name, modelName, resultName);
    return result;
}
