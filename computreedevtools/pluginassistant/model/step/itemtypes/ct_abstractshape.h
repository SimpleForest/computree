#ifndef CT_ABSTRACTSHAPE_H
#define CT_ABSTRACTSHAPE_H

#include "model/step/itemtypes/abstractitemtype.h"

class CT_AbstractShape : public AbstractItemType
{
public:
    CT_AbstractShape();

    virtual bool isInstanciable() {return false;}
    virtual QString getTypeName();
    virtual QString getInclude() {return "#include \"ct_itemdrawable/abstract/" + getTypeName().toLower() + ".h\"\n";}

    virtual QString getInCode(int indent, QString name);
    virtual QString getOutCode(int indent, QString name, QString modelName, QString resultName);

};

#endif // CT_ABSTRACTSHAPE_H
