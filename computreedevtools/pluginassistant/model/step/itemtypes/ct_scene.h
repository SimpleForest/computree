#ifndef CT_SCENE_H
#define CT_SCENE_H

#include "model/step/itemtypes/abstractitemtype.h"

class CT_Scene : public AbstractItemType
{
public:
    CT_Scene();

    virtual QString getTypeName();
    virtual QString getInclude();

    virtual QString getInCode(int indent, QString name);
    virtual QString getOutCode(int indent, QString name, QString modelName, QString resultName);

};

#endif // CT_SCENE_H
