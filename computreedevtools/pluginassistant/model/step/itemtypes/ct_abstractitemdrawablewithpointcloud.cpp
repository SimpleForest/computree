#include "ct_abstractitemdrawablewithpointcloud.h"
#include "model/step/tools.h"

CT_AbstractItemDrawableWithPointCloud::CT_AbstractItemDrawableWithPointCloud() : AbstractItemType()
{
}

QString CT_AbstractItemDrawableWithPointCloud::getTypeName()
{
    return "CT_AbstractItemDrawableWithPointCloud";
}

QString CT_AbstractItemDrawableWithPointCloud::getInCode(int indent, QString name)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    return "";
}

QString CT_AbstractItemDrawableWithPointCloud::getOutCode(int indent, QString name, QString modelName, QString resultName)
{
    Q_UNUSED(indent);
    Q_UNUSED(name);
    Q_UNUSED(modelName);
    Q_UNUSED(resultName);
    return "";
}
