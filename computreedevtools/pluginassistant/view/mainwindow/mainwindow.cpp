#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "view/plugin/createplugindialog.h"
#include "view/step/createstepdialog.h"
#include "view/action/createactiondialog.h"
#include "view/metric/createmetricdialog.h"
#include "view/filter/createfilterdialog.h"
#include "view/exporter/createexporterdialog.h"
#include "view/reader/createreaderdialog.h"
#include "view/item/createitemdialog.h"

#include "model/plugincreator.h"
#include "model/stepcreator.h"
#include "model/actioncreator.h"
#include "model/metriccreator.h"
#include "model/filtercreator.h"
#include "model/exportercreator.h"
#include "model/readercreator.h"
#include "model/itemcreator.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setValidPlugin(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::writeToLog(QString text)
{
    ui->log->appendPlainText(text);
}

void MainWindow::setValidPlugin(bool valid)
{
    if (!valid)
    {
        ui->directory->setText("");
        ui->pluginCode->setText("");
        ui->pluginName->setText("");
    }
    ui->pb_step->setEnabled(valid);
    ui->pb_action->setEnabled(valid);
    ui->pb_exporter->setEnabled(valid);
    ui->pb_reader->setEnabled(valid);
    ui->pb_item->setEnabled(valid);
}

void MainWindow::on_pb_new_clicked()
{
    QString errors = "";
    CreatePluginDialog dialog(ui->directory->text(), this);

    if (dialog.exec() == QDialog::Accepted)
    {
        if (PluginCreator::isValidPluginParameters(dialog.getDirectory(), dialog.getName(), dialog.getCode()))
        {
            PluginCreator creator(dialog.getDirectory(), dialog.getName(), dialog.getCode());
            errors = creator.createPluginFiles();
            if (errors == "")
            {
                ui->directory->setText(dialog.getDirectory());
                ui->pluginCode->setText(dialog.getCode());
                ui->pluginName->setText(dialog.getName());
                setValidPlugin(true);
                writeToLog("Les fichiers du plugins ont été créés avec succès");
                return;
            }
        } else {errors.append(tr("Paramètres invalides"));}
    } else {errors.append(tr("Choix du plugin annulé"));}

    setValidPlugin(false);
    writeToLog(errors);
}

void MainWindow::on_pb_open_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Choisir un fichier de type XX_PluginEntry.cpp"),"", "PluginEntry (*pluginentry.cpp)");
    QFileInfo fileInfo(fileName);

    QString name = "";
    QString code = "";
    QString errors = PluginCreator::getCodeAndNameFromEntryFile(fileName, code, name);

    if (errors.isEmpty())
    {
        ui->directory->setText(fileInfo.path());
        ui->pluginName->setText(name);
        ui->pluginCode->setText(code);
        setValidPlugin(true);

    } else {
        writeToLog(errors);
        setValidPlugin(false);
    }
}

void MainWindow::on_pb_step_clicked()
{
    if (ui->directory->text() != "" && ui->pluginCode->text() != "")
    {
        CreateStepDialog dialog(ui->directory->text(), ui->pluginCode->text(), this);
        dialog.exec();

        QString errors = dialog.getErrors();

        if (errors == "")
        {
            writeToLog(tr("Fichiers de l'étape créés avec succès"));
            writeToLog(QString(tr("Dans le fichier %1_PluginMananger.cpp, ajoutez les lignes suivantes :")).arg(ui->pluginCode->text()));
            writeToLog("----------------------------------------------------------");
            writeToLog("// au début du fichier :");
            writeToLog(QString("#include \"step/%1.h\"").arg(dialog.getSepName().toLower()));
            writeToLog("// ... puis dans la méthode loadGenericsStep() et/ou loadCanBeAddedFirstStep()");
            writeToLog(QString("    sep->addStep(new %1(*createNewStepInitializeData(NULL)));").arg(dialog.getSepName()));
            writeToLog("----------------------------------------------------------");
        } else {
            writeToLog(errors);
        }
    }
}

void MainWindow::on_pb_action_clicked()
{
    CreateActionDialog dialog(ui->pluginCode->text(), this);
    if (ui->directory->text() != "" && dialog.exec())
    {
        ActionCreator actionCreator(ui->directory->text(), dialog.getName());
        QString errors = actionCreator.createActionFiles();

        if (errors == "")
        {
            writeToLog(tr("Fichiers de l'action créés avec succès"));
            writeToLog(QString(tr("Dans le fichier %1_PluginMananger.cpp, ajoutez les lignes suivantes :")).arg(ui->pluginCode->text()));
            writeToLog("----------------------------------------------------------");
            writeToLog("// au début du fichier :");
            writeToLog(QString("#include \"actions/%1.h\"").arg(dialog.getName().toLower()));
            writeToLog("// ... puis dans la méthode loadActions()");
            writeToLog(QString("    sep->addAction(new %1());").arg(dialog.getName()));
            writeToLog("----------------------------------------------------------");
        } else {
            writeToLog(errors);
        }
    } else {
        writeToLog(tr("Impossible de créer les fichiers de l'action"));
    }
}

void MainWindow::on_pb_exporter_clicked()
{
    CreateExporterDialog dialog(ui->pluginCode->text(), this);
    if (ui->directory->text() != "" && dialog.exec())
    {
        ExporterCreator exporterCreator(ui->directory->text(), dialog.getName());
        QString errors = exporterCreator.createExporterFiles();

        if (errors == "")
        {
            writeToLog(tr("Fichiers de l'exporter créés avec succès"));
            writeToLog(QString(tr("Dans le fichier %1_PluginMananger.cpp, ajoutez les lignes suivantes :")).arg(ui->pluginCode->text()));
            writeToLog("----------------------------------------------------------");
            writeToLog("// au début du fichier :");
            writeToLog(QString("#include \"exporter/%1.h\"").arg(dialog.getName().toLower()));
            writeToLog("// ... puis dans la méthode loadExporters()");
            writeToLog(QString("    sep->addExporter(new %1());").arg(dialog.getName()));
            writeToLog("----------------------------------------------------------");
        } else {
            writeToLog(errors);
        }
    } else {
        writeToLog(tr("Impossible de créer les fichiers de l'exporter"));
    }
}

void MainWindow::on_pb_reader_clicked()
{
    CreateReaderDialog dialog(ui->pluginCode->text(), this);
    if (ui->directory->text() != "" && dialog.exec())
    {
        ReaderCreator readerCreator(ui->directory->text(), dialog.getName());
        QString errors = readerCreator.createReaderFiles();

        if (errors == "")
        {
            writeToLog(tr("Fichiers du reader créés avec succès"));
            writeToLog(QString(tr("Dans le fichier %1_PluginMananger.cpp, ajoutez les lignes suivantes :")).arg(ui->pluginCode->text()));
            writeToLog("----------------------------------------------------------");
            writeToLog("// au début du fichier :");
            writeToLog(QString("#include \"reader/%1.h\"").arg(dialog.getName().toLower()));
            writeToLog("// ... puis dans la méthode loadReaders()");
            writeToLog(QString("    sep->addReader(new %1());").arg(dialog.getName()));
            writeToLog("----------------------------------------------------------");
        } else {
            writeToLog(errors);
        }
    } else {
        writeToLog(tr("Impossible de créer les fichiers du reader"));
    }
}

void MainWindow::on_pb_item_clicked()
{
    CreateItemDialog dialog(this);
    dialog.exec();
}

void MainWindow::on_pb_metric_clicked()
{
    CreateMetricDialog dialog(ui->pluginCode->text(), this);
    if (ui->directory->text() != "" && dialog.exec())
    {
        MetricCreator metricCreator(ui->directory->text(), dialog.getName());
        QString errors = metricCreator.createMetricFiles();

        if (errors == "")
        {
            writeToLog(tr("Fichiers de la métrique créés avec succès"));
            writeToLog(QString(tr("Dans le fichier %1_PluginMananger.cpp, ajoutez les lignes suivantes :")).arg(ui->pluginCode->text()));
            writeToLog("----------------------------------------------------------");
            writeToLog("// au début du fichier :");
            writeToLog(QString("#include \"metric/%1.h\"").arg(dialog.getName().toLower()));
            writeToLog("// ... puis dans la méthode loadMetrics()");
            writeToLog(QString("    addNewMetric(new %1());").arg(dialog.getName()));
            writeToLog("----------------------------------------------------------");
        } else {
            writeToLog(errors);
        }
    } else {
        writeToLog(tr("Impossible de créer les fichiers de la métrique"));
    }
}

void MainWindow::on_pb_filter_clicked()
{
    CreateFilterDialog dialog(ui->pluginCode->text(), this);
    if (ui->directory->text() != "" && dialog.exec())
    {
        FilterCreator filterCreator(ui->directory->text(), dialog.getName());
        QString errors = filterCreator.createFilterFiles();

        if (errors == "")
        {
            writeToLog(tr("Fichiers du filtre créés avec succès"));
            writeToLog(QString(tr("Dans le fichier %1_PluginMananger.cpp, ajoutez les lignes suivantes :")).arg(ui->pluginCode->text()));
            writeToLog("----------------------------------------------------------");
            writeToLog("// au début du fichier :");
            writeToLog(QString("#include \"filter/%1.h\"").arg(dialog.getName().toLower()));
            writeToLog("// ... puis dans la méthode loadFilters()");
            writeToLog(QString("    addNewFilter(new %1());").arg(dialog.getName()));
            writeToLog("----------------------------------------------------------");
        } else {
            writeToLog(errors);
        }
    } else {
        writeToLog(tr("Impossible de créer les fichiers du filtre"));
    }
}
