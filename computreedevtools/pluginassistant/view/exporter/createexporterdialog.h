#ifndef CREATEEXPORTERDIALOG_H
#define CREATEEXPORTERDIALOG_H

#include <QDialog>

namespace Ui {
class CreateExporterDialog;
}

class CreateExporterDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateExporterDialog(QString code, QWidget *parent = 0);
    ~CreateExporterDialog();

    QString getName();

private:
    Ui::CreateExporterDialog *ui;
};

#endif // CREATEEXPORTERDIALOG_H
