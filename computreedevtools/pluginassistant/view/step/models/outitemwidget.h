#ifndef INITEMWIDGET_H
#define INITEMWIDGET_H


#include "view/step/models/abstractoutwidget.h"

namespace Ui {
    class OUTItemWidget;
}

class OUTItemWidget : public AbstractOutWidget
{
    Q_OBJECT

public:

    explicit OUTItemWidget(AbstractOutModel* model, QWidget *parent = 0);
    ~OUTItemWidget();

    bool isvalid();
    QString getItemType();
    QString getItemTypeWithTemplate();
    QString getTemplate();
    QString getPrefixedAliad();
    QString getAlias();
    QString getDEF();
    QString getDisplayableName();
    QString getDescription();

private slots:
    void on_alias_textChanged(const QString &arg1);

    void on_cb_itemType_currentIndexChanged(const QString &arg1);

private:
    Ui::OUTItemWidget *ui;
};

#endif // INITEMWIDGET_H
