#ifndef INGROUPWIDGET_H
#define INGROUPWIDGET_H


#include "view/step/models/abstractoutwidget.h"

namespace Ui {
    class OUTGroupWidget;
}

class OUTGroupWidget : public AbstractOutWidget
{
    Q_OBJECT

public:

    explicit OUTGroupWidget(AbstractOutModel* model, QWidget *parent = 0);
    ~OUTGroupWidget();

    bool isvalid();
    QString getPrefixedAliad();
    QString getAlias();
    QString getDEF();
    QString getDisplayableName();
    QString getDescription();


private slots:
    void on_alias_textChanged(const QString &arg1);

private:
    Ui::OUTGroupWidget *ui;
};

#endif // INRESULTWIDGET_H
