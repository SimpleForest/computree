#include "view/step/models/abstractoutwidget.h"

AbstractOutWidget::AbstractOutWidget(AbstractOutModel *model, QWidget *parent) : QWidget(parent)
{
    _model = model;
    setFocusPolicy(Qt::StrongFocus);
}
