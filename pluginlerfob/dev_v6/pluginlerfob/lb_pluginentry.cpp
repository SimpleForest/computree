#include "lb_pluginentry.h"
#include "lb_pluginmanager.h"

LB_PluginEntry::LB_PluginEntry()
{
    _pluginManager = new LB_PluginManager();
}

LB_PluginEntry::~LB_PluginEntry()
{
    delete _pluginManager;
}

QString LB_PluginEntry::getVersion() const
{
    return "1.0";
}

CT_AbstractStepPlugin* LB_PluginEntry::getPlugin() const
{
    return _pluginManager;
}

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    Q_EXPORT_PLUGIN2(plug_lerfob, LB_PluginEntry)
#endif
