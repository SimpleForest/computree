/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "itemsimpletreedrawmanager.h"
#include "itemsimpletree.h"

const QString itemSimpleTreeDrawManager::INDEX_CONFIG_CYLINDERS_VISIBLE = itemSimpleTreeDrawManager::staticInitConfigCylindersVisible();

itemSimpleTreeDrawManager::itemSimpleTreeDrawManager(QString drawConfigurationName) : CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager(drawConfigurationName.isEmpty() ? "Simple Tree" : drawConfigurationName)
{

}

itemSimpleTreeDrawManager::~itemSimpleTreeDrawManager()
{
}

void itemSimpleTreeDrawManager::draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const
{
    CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::draw(view, painter, itemDrawable);

    const itemSimpleTree &item = dynamic_cast<const itemSimpleTree&>(itemDrawable);

    //const itemSimpleTree &item = (const itemSimpleTree&)itemDrawable;

    if(getDrawConfiguration()->getVariableValue(INDEX_CONFIG_CYLINDERS_VISIBLE).toBool())
        drawCylinders(view, painter, item);
}

CT_ItemDrawableConfiguration itemSimpleTreeDrawManager::createDrawConfiguration(QString drawConfigurationName) const
{
    CT_ItemDrawableConfiguration item = CT_ItemDrawableConfiguration(drawConfigurationName);

    item.addAllConfigurationOf(CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager::createDrawConfiguration(drawConfigurationName));
    item.addNewConfiguration(itemSimpleTreeDrawManager::staticInitConfigCylindersVisible(), "Cylinders", CT_ItemDrawableConfiguration::Bool, true);

    return item;
}

// PROTECTED //

QString itemSimpleTreeDrawManager::staticInitConfigCylindersVisible()
{
    return "DEM_ST";
}

void itemSimpleTreeDrawManager::drawCylinders(GraphicsViewInterface &view, PainterInterface &painter, const itemSimpleTree &item) const
{   
    treeTopology mo = item.getModel();
    std::vector<cylinder> lb = mo.getListCylinders();

    for(int j=0;j<lb.size();j++)
    {
        cylinder c =  lb.at(j);

        Eigen::Vector3d direction;
        direction[0] = c.x2 - c.x1;
        direction[1] = c.y2 - c.y1;
        direction[2] = c.z2 - c.z1;

        Eigen::Vector3d center;
        center[0] = 0.5*(c.x1+c.x2);
        center[1] = 0.5*(c.y1+c.y2);
        center[2] = 0.5*(c.z1+c.z2);

        double h = direction.norm();

        if(c.isFirst){
            painter.setColor(200, 50, 50);
        }else{
            painter.setColor(0, 255, 255);
        }

        painter.drawCylinder3D(center,direction,c.radius,h);

    }

}

