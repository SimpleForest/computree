/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "localCell.h"

#include <iostream>

#include <pcl/common/eigen.h>
#include <pcl/common/transforms.h>

#include <gsl/gsl_multifit.h>

#include "../leastsquareregsolver.h"

localCell::localCell(const pcl::PointCloud<pcl::PointXYZ> &pPtsGlobalRef, float radius, bool useWeight) : ptsGlobalRef(pPtsGlobalRef)
{
    computeCentroid();
    getNormal();
    getBasis();
    applyTransform();
}

localCell::localCell(pcl::PointXYZ center,const pcl::PointCloud<pcl::PointXYZ> &pPtsGlobalRef, float radius, bool useWeight) : ptsGlobalRef(pPtsGlobalRef)
{
    centroid[0] = center.x;
    centroid[1] = center.y;
    centroid[2] = center.z;

    getNormal();
    getBasis();
    applyTransform();
}

localCell::localCell(pcl::PointXYZ center, pcl::PointXYZ normal, const pcl::PointCloud<pcl::PointXYZ> &pPtsGlobalRef, float radius, bool useWeight) : ptsGlobalRef(pPtsGlobalRef)
{
    centroid[0] = center.x;
    centroid[1] = center.y;
    centroid[2] = center.z;

    w[0]=normal.x;
    w[1]=normal.y;
    w[2]=normal.z;
    w=w/w.norm();

    getBasis();
    applyTransform();
}

localCell::localCell(pcl::PointXYZ center, pcl::PointXYZ normal)
{
    centroid[0] = center.x;
    centroid[1] = center.y;
    centroid[2] = center.z;

    w[0]=normal.x;
    w[1]=normal.y;
    w[2]=normal.z;
    w=w/w.norm();

    getBasis();
}

void localCell::getNormal()
{
    u.setZero(3);
    v.setZero(3);
    w.setZero(3);

    std::vector<double> coeffPlane;
    double errorPlane;
    pcl::PointXYZ cen;
    cen.x = centroid[0];
    cen.y = centroid[1];
    cen.z = centroid[2];
    leastSquareRegSolver::solvePlane(cen,ptsGlobalRef,&coeffPlane,&errorPlane);
    w[0]=-coeffPlane.at(0);
    w[1]=-coeffPlane.at(1);
    w[2]=1.;

    w=w/w.norm();
}

void localCell::getBasis()
{
    Eigen::Matrix3f R;
    R = Eigen::Quaternionf().setFromTwoVectors(Eigen::Vector3f::UnitZ(),w);

    u = R* Eigen::Vector3f::UnitX();
    v = R* Eigen::Vector3f::UnitY();

    pcl::getTransformationFromTwoUnitVectorsAndOrigin(v,w,centroid,transfo);
}

void localCell::computeCentroid()
{
    centroid.setZero(3);

    for(unsigned int i=0; i<ptsGlobalRef.size(); i++){
        centroid[0] += ptsGlobalRef.at(i).x/(float)ptsGlobalRef.size();
        centroid[1] += ptsGlobalRef.at(i).y/(float)ptsGlobalRef.size();
        centroid[2] += ptsGlobalRef.at(i).z/(float)ptsGlobalRef.size();
    }
}

void localCell::applyTransform()
{
    pcl::transformPointCloud(ptsGlobalRef,ptsLocalRef,transfo);
}

float localCell::getValueOfImplicitFunction(float x, float y, float z) const
{
    Eigen::Vector4f X(x,y,z,1);
    return (double)((X.transpose()*m1.matrix())*X) + (float)(m2.transpose() * X);
}
