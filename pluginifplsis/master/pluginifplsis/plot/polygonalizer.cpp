/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "polygonalizer.h"
#include "../core/wendlandRbf.h"
#include <float.h>
#include <pcl/common/common.h>
#include <fstream>

polygonalizer::polygonalizer(surfaceFrbf pSurf,int pNbDivX, int pNbDivY, int pNbDivZ) : surf(pSurf)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pts_ptr (new pcl::PointCloud<pcl::PointXYZ>(surf.getTree().getPts()));

    pcl::getMinMax3D (*cloud_pts_ptr, min, max);

    nbDivX = pNbDivX;
    nbDivY = pNbDivY;
    nbDivZ = pNbDivZ;

    min.z = min.z - 2.0;
    max.z = max.z + 2.0;

    sizeCellX = ((max.x-min.x))/(float)nbDivX;
    sizeCellY = ((max.y-min.y))/(float)nbDivY;
    sizeCellZ = ((max.z-min.z))/(float)nbDivZ;

    scalarField field(min,max,(nbDivX+1),(nbDivY+1),(nbDivZ+1));
    fillScalarField(field);
    std::cout<<"       Scalar field...........OK"<<std::endl;

    //iso = new isoSurface(&field, 0,nbDivX,nbDivY,nbDivZ,sizeCellX,sizeCellY,sizeCellZ,min);
    //iso->generateSurface();
    std::cout<<"       Triangulation..........OK"<<std::endl;

    //computeNormals();
    //std::cout<<"       Normals................OK"<<std::endl;
}

void polygonalizer::fillScalarField(scalarField &field)
{
    for(std::size_t m=0;m<surf.listSphere.size();m++)
    {
        int coordCenterX, coordCenterY, coordCenterZ;
        coordCenterX = (int)((surf.listSphere.at(m).getCenter().x-min.x)/sizeCellX);
        coordCenterY = (int)((surf.listSphere.at(m).getCenter().y-min.y)/sizeCellY);
        coordCenterZ = (int)((surf.listSphere.at(m).getCenter().z-min.z)/sizeCellZ);
        int stepX = (int)(surf.listSphere.at(m).getRadius()/sizeCellX)+1;
        int stepY = (int)(surf.listSphere.at(m).getRadius()/sizeCellY)+1;
        int stepZ = (int)(surf.listSphere.at(m).getRadius()/sizeCellZ)+1;

        for(int k=coordCenterZ-stepZ;k<coordCenterZ+stepZ;k++)
        {
            for(int j=coordCenterY-stepY;j<coordCenterY+stepY;j++)
            {
                for(int i=coordCenterX-stepX;i<coordCenterX+stepX;i++)
                {
                    if(i>=0 && i<nbDivX+1 && j>=0 && j<nbDivY+1 && k>=0 && k<nbDivZ+1){

                        float x = (float)i*sizeCellX+min.x;
                        float y = (float)j*sizeCellY+min.y;
                        float z = (float)k*sizeCellZ+min.z;

                        float sum = 0.;
                        if(field.getValue(i,j,k) != scalarField::NODATA)
                        {
                            sum = field.getValue(i,j,k);
                        }

                        bool hasBeenUpdated = false;

                        float tmpx = (surf.listSphere.at(m).getCenter().x-x);
                        float tmpy = (surf.listSphere.at(m).getCenter().y-y);
                        float tmpz = (surf.listSphere.at(m).getCenter().z-z);
                        float distR = sqrt(tmpx*tmpx+tmpy*tmpy+tmpz*tmpz);

                        float r = distR/surf.listSphere.at(m).getRadius();

                        if(r<1.)
                        {
                            float res = surf.listSphere.at(m).getLocalRef().getValueOfImplicitFunction(x,y,z);
                            sum += (res)*wendlandRbf::getValueRbfWendland(r);

                            hasBeenUpdated = true;
                        }

                        if(hasBeenUpdated)field.setValue(i,j,k,sum);
                    }
                }
            }
        }
    }
}
