/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef DEM_RBFSURFACEDRAWMANAGER_H
#define DEM_RBFSURFACEDRAWMANAGER_H

#include "ct_itemdrawable/tools/drawmanager/ct_standardabstractitemdrawablewithoutpointclouddrawmanager.h"

class itemFrbfsurface;

class itemFrbfsurfaceDrawManager : public CT_StandardAbstractItemDrawableWithoutPointCloudDrawManager
{
public:
    itemFrbfsurfaceDrawManager(QString drawConfigurationName = "");
    virtual ~itemFrbfsurfaceDrawManager();

    virtual void draw(GraphicsViewInterface &view, PainterInterface &painter, const CT_AbstractItemDrawable &itemDrawable) const;

protected:

    const static QString INDEX_CONFIG_TRIANGLES_VISIBLE;
    const static QString INDEX_CONFIG_QUADTREE_VISIBLE;
    const static QString INDEX_CONFIG_SPHERES_VISIBLE;
    const static QString INDEX_CONFIG_CENTERS_VISIBLE;
    const static QString INDEX_CONFIG_QUADPTS_VISIBLE;
    const static QString INDEX_CONFIG_SPHERESURFACES_VISIBLE;

    static QString staticInitConfigTrianglesVisible();
    static QString staticInitConfigQuadTreeVisible();
    static QString staticInitConfigSphereVisible();
    static QString staticInitConfigCenterVisible();
    static QString staticInitConfigQuadPtsVisible();
    static QString staticInitConfigSphereSurfaceVisible();

    virtual CT_ItemDrawableConfiguration createDrawConfiguration(QString drawConfigurationName) const;

    void drawTriangles(GraphicsViewInterface &view, PainterInterface &painter, const itemFrbfsurface &item) const;
    void drawQuadTree(GraphicsViewInterface &view, PainterInterface &painter, const itemFrbfsurface &item) const;
    void drawSpheres(GraphicsViewInterface &view, PainterInterface &painter, const itemFrbfsurface &item) const;
    void drawCenters(GraphicsViewInterface &view, PainterInterface &painter, const itemFrbfsurface &item) const;
    void drawQuadPts(GraphicsViewInterface &view, PainterInterface &painter, const itemFrbfsurface &item) const;
    void drawSphereSurfaces(GraphicsViewInterface &view, PainterInterface &painter, const itemFrbfsurface &item) const;
};

#endif // DEM_RBFSURFACEDRAWMANAGER_H
