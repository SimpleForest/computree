/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "quadtree.h"

#include <algorithm>
#include <iostream>
#include <exception>

#include <pcl/point_types.h>
#include <pcl/common/common.h>

quadTree::quadTree(const pcl::PointCloud<pcl::PointXYZ>  &pPts, float pThreshold, int pNbrPointsMin, float pMinSizeLeaf, float pHistoRange, int pHistoWindowSizeSmooth, float pThresholdErrorBetweenNeighbors) : pts(pPts), errorThreshold(pThreshold),nbrPointsMin(pNbrPointsMin),minSizeLeaf(pMinSizeLeaf),histoRange(pHistoRange),histoWindowSizeSmooth(pHistoWindowSizeSmooth),thresholdErrorBetweenNeighbors(pThresholdErrorBetweenNeighbors)
{
    build();
}

quadTree::~quadTree()
{
    /*for(auto leaf : listLeaf)
    {
        if(leaf!=NULL){delete leaf;leaf=NULL;}
    }*/
}

void quadTree::build()
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pts_ptr (new pcl::PointCloud<pcl::PointXYZ>(pts));
    pcl::getMinMax3D (*cloud_pts_ptr, min, max);
    rectangle rectangleRoot = rectangle(min.x,max.x,min.y,max.y);
    std::bitset<QUAD_MAX_LEVEL> bs((long)0);
    quadLeaf* root = new quadLeaf(NULL,rectangleRoot,0,bs,pts,nbrPointsMin,minSizeLeaf,histoWindowSizeSmooth);
    root->isRoot = true;
    listLeaf.push_front(root);

    while(findFirstLeafAboveThreshold())
    {

        quadLeaf* firstLeaf = findFirstLeafAboveThreshold();

        try{

            split(firstLeaf);
            firstLeaf->notToDivide = true;

            if(!firstLeaf->isRoot)incEqualSizeNeighbors(firstLeaf,false);

            listLeaf.erase(listLeaf.begin()+getPositionInList(*firstLeaf));

        }catch(std::domain_error& e)
        {
            firstLeaf->notToDivide = true;
        }
    }
}

void quadTree::split(quadLeaf* pLeaf)
{
    float subDeltaX = pLeaf->getQuadrant().getdeltaX() / 2.;
    float subDeltaY = pLeaf->getQuadrant().getdeltaY() / 2.;

    rectangle rectangleSW(pLeaf->getQuadrant().getXMin(),pLeaf->getQuadrant().getXMin()+subDeltaX,pLeaf->getQuadrant().getYMin(),pLeaf->getQuadrant().getYMin()+subDeltaY);
    quadLeaf* lSW = new quadLeaf(pLeaf,rectangleSW,pLeaf->getLevel()+1,calcPosition(pLeaf->getLocationCode(),SOUTH_WEST),pts,nbrPointsMin,minSizeLeaf,histoWindowSizeSmooth);

    rectangle rectangleSE(pLeaf->getQuadrant().getXMin()+subDeltaX,pLeaf->getQuadrant().getXMax(),pLeaf->getQuadrant().getYMin(),pLeaf->getQuadrant().getYMin()+subDeltaY);
    quadLeaf* lSE = new quadLeaf(pLeaf,rectangleSE,pLeaf->getLevel()+1,calcPosition(pLeaf->getLocationCode(),SOUTH_EAST),pts,nbrPointsMin,minSizeLeaf,histoWindowSizeSmooth);

    rectangle rectangleNW(pLeaf->getQuadrant().getXMin(),pLeaf->getQuadrant().getXMin()+subDeltaX,pLeaf->getQuadrant().getYMin()+subDeltaY,pLeaf->getQuadrant().getYMax());
    quadLeaf* lNW = new quadLeaf(pLeaf,rectangleNW,pLeaf->getLevel()+1,calcPosition(pLeaf->getLocationCode(),NORTH_WEST),pts,nbrPointsMin,minSizeLeaf,histoWindowSizeSmooth);

    rectangle rectangleNE(pLeaf->getQuadrant().getXMin()+subDeltaX,pLeaf->getQuadrant().getXMax(),pLeaf->getQuadrant().getYMin()+subDeltaY,pLeaf->getQuadrant().getYMax());
    quadLeaf* lNE = new quadLeaf(pLeaf,rectangleNE,pLeaf->getLevel()+1,calcPosition(pLeaf->getLocationCode(),NORTH_EAST),pts,nbrPointsMin,minSizeLeaf,histoWindowSizeSmooth);

    if(pLeaf->getLevel()+1 > quadLeaf::levelMax)
    {
        quadLeaf::levelMax=pLeaf->getLevel()+1;
    }

    listLeaf.push_back(lSW);
    listLeaf.push_back(lSE);
    listLeaf.push_back(lNW);
    listLeaf.push_back(lNE);

    if(!pLeaf->isRoot){
        incEqualSizeNeighbors(lSW,true);
        incEqualSizeNeighbors(lSE,true);
        incEqualSizeNeighbors(lNW,true);
        incEqualSizeNeighbors(lNE,true);
    }

}

void quadTree::update()
{
    std::cout<<"       "<<this->getListLeaf().size()<<" leafs in the tree"<<std::endl;

    int histoFiltered = filterLeafsWithHisto();
    int firstFilterRes = rebuildPointCloud();
    std::cout<<"       "<<histoFiltered<<" leafs have been filtered with histogram ("<<firstFilterRes<<" points deleted)"<<std::endl;

    int cleaned = filterLeafsWithNeighbors();
    int secondFilterRes = rebuildPointCloud();
    std::cout<<"       "<<cleaned<<" leafs have been cleaned and reconstructed with the neighbors data ("<<secondFilterRes<<" points deleted)"<<std::endl;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pts_ptr (new pcl::PointCloud<pcl::PointXYZ>(pts));
    pcl::getMinMax3D (*cloud_pts_ptr, min, max);
}

int quadTree::filterLeafsWithHisto()
{
    int counterLeafCleaned = 0;
    for(std::deque<quadLeaf*>::iterator it = listLeaf.begin();it!=listLeaf.end();it++)
    {
        try{
            bool isDone = (*it)->filterWithHisto(errorThreshold,histoRange);
            if(isDone) counterLeafCleaned++;
        }catch(std::exception & e)
        {
            std::cout<<"       An error occured during the histogram filtering process on "<<(*it)->getLocationCode()<<" lvl "<<(*it)->getLevel()<<" "<< e.what()<<std::endl;
        }
    }
    return counterLeafCleaned;
}

void quadTree::buildLeafsSurface()
{
    for(std::deque<quadLeaf*>::iterator it = listLeaf.begin();it!=listLeaf.end();it++)
    {
        (*it)->polygonizeLevelSet(10,10,10,0.);
    }
}

int quadTree::rebuildPointCloud()
{
    int nbBefore = pts.size();

    pts.clear();
    for(std::deque<quadLeaf*>::iterator it = listLeaf.begin();it!=listLeaf.end();it++)
    {
        if(!(*it)->isRebuild()){
            pts.insert( pts.end(), (*it)->getLocalCell().getGlobalPts().begin(), (*it)->getLocalCell().getGlobalPts().end() );
        }
    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pts_ptr (new pcl::PointCloud<pcl::PointXYZ>(pts));
    pcl::getMinMax3D (*cloud_pts_ptr, min, max);

    return nbBefore - pts.size();
}

int quadTree::filterLeafsWithNeighbors()
{
    int counterLeafCleaned = 0;
    int counterLeafToRebuild = 0;

    for(std::deque<quadLeaf*>::iterator it = listLeaf.begin();it!=listLeaf.end();it++)
    {
        try{

            std::vector<quadLeaf*> neighbors = (*it)->getNeighbors();
            float averageError=0;
            for(std::vector<quadLeaf*>::iterator itNeighbor = neighbors.begin();itNeighbor!=neighbors.end();itNeighbor++)
            {
                pcl::PointXYZ center;
                center.x = (*itNeighbor)->getLocalCell().getCentroid()[0];
                center.y = (*itNeighbor)->getLocalCell().getCentroid()[1];
                center.z = (*itNeighbor)->getLocalCell().getCentroid()[2];

                averageError += (*it)->distancePointToSurface(center)*(*it)->distancePointToSurface(center);
            }
            averageError = averageError/neighbors.size();
            averageError = sqrt(averageError);

            //if((*it)->getLocalCell().getError()>thresholdRMSLeaf || averageError>thresholdErrorBetweenNeighbors || (*it)->toFilterWithNeighbors)
            if(averageError>thresholdErrorBetweenNeighbors || (*it)->toFilterWithNeighbors)
            {
                (*it)->toRebuildWithNeighbors = true;
                counterLeafToRebuild++;
            }

        }catch(std::exception & e)
        {
            std::cout<<"       An error occured during the cleaning process on "<<(*it)->getLocationCode()<<" lvl "<<(*it)->getLevel()<<std::endl;
        }
    }


    while(counterLeafToRebuild>0)
    {
        for(std::deque<quadLeaf*>::iterator it = listLeaf.begin();it!=listLeaf.end();it++)
        {
            try{
                if((*it)->toRebuildWithNeighbors && !(*it)->hadBeenRebuild){
                    bool isDone = (*it)->filterWithNeighbors();
                    if(isDone) {
                        counterLeafCleaned++;
                        counterLeafToRebuild--;
                    }
                }
            }catch(std::exception & e)
            {
                std::cout<<"       An error occured during the cleaning process on "<<(*it)->getLocationCode()<<" lvl "<<(*it)->getLevel()<<std::endl;
            }
        }
    }

    return counterLeafCleaned;
}

quadLeaf* quadTree::findFirstLeafAboveThreshold()
{
    std::deque<quadLeaf*>::iterator it = listLeaf.begin();
    while (it != listLeaf.end())
    {
        if((*it)->getLocalCell().getError() > errorThreshold  && !(*it)->notToDivide)
        {
            return *it;
        }
        it++;
    }
    return NULL;
}

quadLeaf* quadTree::findByBitset(std::bitset<QUAD_MAX_LEVEL> pCode, int pLevel)
{
    std::deque<quadLeaf*>::iterator it = listLeaf.begin();
    while (it != listLeaf.end())
    {
        if((*it)->getLocationCode() == pCode && (*it)->getLevel() == pLevel)
        {
            return *it;
        }
        it++;
    }
    return NULL;
}

int quadTree::getPositionInList(quadLeaf leaf)
{
    std::deque<quadLeaf*>::iterator it = std::find_if(listLeaf.begin(), listLeaf.end(),findByCodeWithChildren(leaf.getLocationCode(),leaf.getLevel()));
    return std::distance( listLeaf.begin(), it );
}

void quadTree::incEqualSizeNeighbors(quadLeaf* pLeaf, bool checkIfBrother)
{
    if(pLeaf->deltaLevelEast!=INT_MAX){
    std::bitset<QUAD_MAX_LEVEL> lcEast = calcNeighborsEqualSize(pLeaf->getLocationCode(),EAST_NEIGHBOR);
    if(findByBitset(lcEast,pLeaf->getLevel()))
    {
        if(checkIfBrother)
        {
            if(!isBrother(pLeaf->getLocationCode(),lcEast))
            {
                if(findByBitset(lcEast,pLeaf->getLevel())->deltaLevelWest != INT_MAX)
                {
                    findByBitset(lcEast,pLeaf->getLevel())->deltaLevelWest++;
                }
            }

        }else{
            if(findByBitset(lcEast,pLeaf->getLevel())->deltaLevelWest != INT_MAX)
            {
                findByBitset(lcEast,pLeaf->getLevel())->deltaLevelWest++;
            }
        }
    }
    }

    if(pLeaf->deltaLevelNorth!=INT_MAX){
    std::bitset<QUAD_MAX_LEVEL> lcNorth = calcNeighborsEqualSize(pLeaf->getLocationCode(),NORTH_NEIGHBOR);
    if(findByBitset(lcNorth,pLeaf->getLevel()))
    {
        if(checkIfBrother)
        {
            if(!isBrother(pLeaf->getLocationCode(),lcNorth))
            {
                if(findByBitset(lcNorth,pLeaf->getLevel())->deltaLevelSouth != INT_MAX)
                {
                    findByBitset(lcNorth,pLeaf->getLevel())->deltaLevelSouth++;
                }
            }
        }else{
            if(findByBitset(lcNorth,pLeaf->getLevel())->deltaLevelSouth != INT_MAX)
            {
                findByBitset(lcNorth,pLeaf->getLevel())->deltaLevelSouth++;
            }
        }
    }
    }

    if(pLeaf->deltaLevelWest!=INT_MAX){
    std::bitset<QUAD_MAX_LEVEL> lcWest = calcNeighborsEqualSize(pLeaf->getLocationCode(),WEST_NEIGHBOR);
    if(findByBitset(lcWest,pLeaf->getLevel()))
    {
        if(checkIfBrother)
        {
            if(!isBrother(pLeaf->getLocationCode(),lcWest))
            {
                if(findByBitset(lcWest,pLeaf->getLevel())->deltaLevelEast != INT_MAX)
                {
                    findByBitset(lcWest,pLeaf->getLevel())->deltaLevelEast++;
                }
            }
        }else{
            if(findByBitset(lcWest,pLeaf->getLevel())->deltaLevelEast != INT_MAX)
            {
                findByBitset(lcWest,pLeaf->getLevel())->deltaLevelEast++;
            }
        }
    }
    }

    if(pLeaf->deltaLevelSouth!=INT_MAX){
    std::bitset<QUAD_MAX_LEVEL> lcSouth = calcNeighborsEqualSize(pLeaf->getLocationCode(),SOUTH_NEIGHBOR);
    if(findByBitset(lcSouth,pLeaf->getLevel()))
    {
        if(checkIfBrother)
        {
            if(!isBrother(pLeaf->getLocationCode(),lcSouth))
            {
                if(findByBitset(lcSouth,pLeaf->getLevel())->deltaLevelNorth != INT_MAX)
                {
                    findByBitset(lcSouth,pLeaf->getLevel())->deltaLevelNorth++;
                }
            }
        }else{
            if(findByBitset(lcSouth,pLeaf->getLevel())->deltaLevelNorth != INT_MAX)
            {
                findByBitset(lcSouth,pLeaf->getLevel())->deltaLevelNorth++;
            }
        }
    }
    }
}

std::bitset<QUAD_MAX_LEVEL> quadTree::calcPosition(std::bitset<QUAD_MAX_LEVEL> pLocation, int pPosition )
{
    std::bitset<QUAD_MAX_LEVEL> bsPosition( (long) pPosition );
    return bsPosition|(pLocation<<2);
}

std::bitset<QUAD_MAX_LEVEL> quadTree::calcNeighborsEqualSize(std::bitset<QUAD_MAX_LEVEL> pLocation, std::bitset<QUAD_MAX_LEVEL> pDirection )
{
    return quadLocationAdd(pLocation,pDirection);
}

std::bitset<QUAD_MAX_LEVEL> quadTree::quadLocationAdd( std::bitset<QUAD_MAX_LEVEL> a, std::bitset<QUAD_MAX_LEVEL> b ) {

    std::bitset<QUAD_MAX_LEVEL> bstx = getTx(quadLeaf::levelMax);
    std::bitset<QUAD_MAX_LEVEL> bsty = getTy(quadLeaf::levelMax);
    return (add(a|bsty,b&bstx)&bstx)|(add(a|bstx,b&bsty)&bsty);
}

std::bitset<QUAD_MAX_LEVEL> quadTree::add(std::bitset<QUAD_MAX_LEVEL> a, std::bitset<QUAD_MAX_LEVEL> b)
{
    std::bitset<QUAD_MAX_LEVEL> const m((long)1);
    std::bitset<QUAD_MAX_LEVEL> result;
    for (std::size_t i = 0; i < result.size(); ++i) {
        std::bitset<QUAD_MAX_LEVEL> const diff(((a >> i)&m).to_ulong() + ((b >> i)&m).to_ulong() + (result >> i).to_ulong());
        result ^= (diff ^ (result >> i)) << i;
    }
    return result;
}

std::bitset<QUAD_MAX_LEVEL> quadTree::getTx(int pLevelMax)
{
    std::bitset<QUAD_MAX_LEVEL> ret;
    std::bitset<QUAD_MAX_LEVEL> unit( (long) 1);
    for(int i=0;i<pLevelMax;i++)
    {
        ret = (ret << 2) | unit;
    }
    return ret;
}

std::bitset<QUAD_MAX_LEVEL> quadTree::getTy(int pLevelMax)
{
    std::bitset<QUAD_MAX_LEVEL> ret;
    std::bitset<QUAD_MAX_LEVEL> unit( (long) 2 );
    for(int i=0;i<pLevelMax;i++)
    {
        ret = (ret << 2) | unit;
    }
    return ret;
}

bool quadTree::isBrother(std::bitset<QUAD_MAX_LEVEL> pLeafA, std::bitset<QUAD_MAX_LEVEL> pLeafB)
{
    bool test=true;
    for(std::size_t i=2;i<pLeafA.size();i++)
    {
        if(pLeafA.test(i) != pLeafB.test(i))test=false;
    }
    return test;
}

void quadTree::printList()
{
    std::cout<<"***************************************"<<std::endl;
    for(std::size_t i=0;i<listLeaf.size();i++)
    {
        if(listLeaf.at(i)->getParent()!=NULL)
        {
            std::cout<<i<<" "<<listLeaf.at(i)->getLocationCode()<<" lvl "<<listLeaf.at(i)->getLevel()<<" son of "<<listLeaf.at(i)->getParent()->getLocationCode()<<" lvl "<<listLeaf.at(i)->getParent()->getLevel()<<std::endl;
        }else{
            std::cout<<i<<" "<<listLeaf.at(i)->getLocationCode()<<" son of NULL"<<std::endl;
        }
    }
}
