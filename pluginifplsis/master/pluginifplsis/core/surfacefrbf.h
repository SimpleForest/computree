/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef SURFACE_H
#define SURFACE_H

#include "quadtree.h"
#include "graph.h"
#include "sphereSupport.h"

#include "../plot/implicitfunction.h"

class surfaceFrbf : public implicitFunction
{
public:

    surfaceFrbf(){}
    surfaceFrbf(const pcl::PointCloud<pcl::PointXYZ> &pCloud, float pThreshold, int nbrPointsMin, float minSizeLeaf, float histoRange, int histoWindowSizeSmooth, float thresholdErrorBetweenNeighbors);

    std::vector<sphereSupport> listSphere;

    const quadTree& getTree() const {return *tree;}

    double computeImplicitFctValue(float x, float y, float z);

    virtual void fillScalarField(scalarField &field);

private:

    quadTree * tree;
    graph * graphOfLeafNeighborhood;

    float threshold;

    void buildSpheres();

    std::vector<float> valuesImplicitFunction;
};

#endif // SURFACE_H
