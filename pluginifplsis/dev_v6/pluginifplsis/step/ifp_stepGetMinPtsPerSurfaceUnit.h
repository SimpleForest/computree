/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#ifndef IFP_StepGetMinPtsPerSurface_H
#define IFP_StepGetMinPtsPerSurface_H

#include "ct_step/abstract/ct_abstractstep.h"
#include "ct_tools/model/ct_autorenamemodels.h"

/*!
 * \class ifp_StepGetMinPtsPerSurface
 * \ingroup Steps_AMAP
 * \brief <b>Filter point cloud to keep only the minimum points.</b>
 *
 * Detailed decription of step purpose.
 * Please also give a general view of the algorithm.
 *
 * \param _res 
 *
 *
 * <b>Input Models:</b>
 *
 * - CT_ResultGroup (in)\n
 *     - CT_StandardItemGroup (in)...\n
 *         - CT_Scene (in)\n
 *
 * <b>Output Models:</b>
 *
 * - CT_ResultGroup (out)\n
 *     - CT_StandardItemGroup (out)...\n
 *         - CT_Scene (out)\n
 *
 */

class IFP_stepGetMinPtsPerSurface: public CT_AbstractStep
{
    Q_OBJECT

public:

    /*! \brief Step constructor
     * 
     * Create a new instance of the step
     * 
     * \param dataInit Step parameters object
     */
    IFP_stepGetMinPtsPerSurface(CT_StepInitializeData &dataInit);

    /*! \brief Step description
     * 
     * Return a description of the step function
     */
    QString getStepDescription() const;
    QString getStepDetailledDescription() const;


    /*! \brief Step copy
     * 
     * Step copy, used when a step is added by step contextual menu
     */
    CT_VirtualAbstractStep* createNewInstance(CT_StepInitializeData &dataInit);

protected:

    /*! \brief Input results specification
     * 
     * Specification of input results models needed by the step (IN)
     */
    void createInResultModelListProtected();

    /*! \brief Parameters DialogBox
     * 
     * DialogBox asking for step parameters
     */
    void createPostConfigurationDialog();

    /*! \brief Output results specification
     * 
     * Specification of output results models created by the step (OUT)
     */
    void createOutResultModelListProtected();

    /*! \brief Algorithm of the step
     * 
     * Step computation, using input results, and creating output results
     */
    void compute();

private:

    CT_AutoRenameModels     _outsceneModelName;

    // Step parameters
    double    _res;    /*!<  */
    float _minx;
    float _miny;
    float _minz;
    size_t _dimx;
    size_t _dimy;
    size_t _dimz;

    size_t gridIndex(const float &x, const float &y, size_t &colx, size_t &liny) const;

};

#endif // IFP_StepGetMinPtsPerSurface_H
