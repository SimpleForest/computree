/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/
#include "graph.h"

graph::graph(quadTree &tree)
{
    buildGraphOfNeighborhood(tree);
    setRadius(tree);
    setNeighborhood(tree);
}

void graph::updateRadius(quadTree &tree)
{
    setRadius(tree);
}

void graph::buildGraphOfNeighborhood(quadTree &tree)
{
    for(std::size_t i=0;i<tree.getListLeaf().size();i++)
    {
        if(tree.getListLeaf().at(i)->deltaLevelEast<=0){
            addEdge(tree,tree.getListLeaf().at(i),tree.getListLeaf().at(i)->deltaLevelEast,EAST_NEIGHBOR);
        }

        if(tree.getListLeaf().at(i)->deltaLevelNorth<=0){
            addEdge(tree,tree.getListLeaf().at(i),tree.getListLeaf().at(i)->deltaLevelNorth,NORTH_NEIGHBOR);
        }

        if(tree.getListLeaf().at(i)->deltaLevelWest<=0){
            addEdge(tree,tree.getListLeaf().at(i),tree.getListLeaf().at(i)->deltaLevelWest,WEST_NEIGHBOR);
        }

        if(tree.getListLeaf().at(i)->deltaLevelSouth<=0){
            addEdge(tree,tree.getListLeaf().at(i),tree.getListLeaf().at(i)->deltaLevelSouth,SOUTH_NEIGHBOR);
        }
    }
}

std::vector<quadLeaf*> graph::getNeighbors(quadLeaf *pLeaf)
{
    std::vector<quadLeaf*> listOfNeighbors;
    for(std::size_t k = 0;k<edgeVec.size();k++)
    {
        if(edgeVec.at(k).first == pLeaf)
        {
            listOfNeighbors.push_back(edgeVec.at(k).second);
        }
    }
    return listOfNeighbors;
}

void graph::setRadius(quadTree &tree)
{
    for(std::size_t i=0;i<tree.getListLeaf().size();i++)
    {
        quadLeaf* leaf = tree.getListLeaf().at(i);
        std::vector<quadLeaf*> neighbors = getNeighbors(leaf);

        if(neighbors.empty())
        {
            std::cout<<"!!! BUG !!! One leaf with no neighbors"<<std::endl;

        }else{

            std::vector<float> distances;

            float x1 = leaf->getLocalCell().getCentroid()[0];
            float y1 = leaf->getLocalCell().getCentroid()[1];
            float z1 = leaf->getLocalCell().getCentroid()[2];

            for(std::size_t i=0;i<neighbors.size();i++)
            {
                float x2 = neighbors.at(i)->getLocalCell().getCentroid()[0];
                float y2 = neighbors.at(i)->getLocalCell().getCentroid()[1];
                float z2 = neighbors.at(i)->getLocalCell().getCentroid()[2];
                distances.push_back(sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2)));
            }

            float maxDist = *std::max_element(distances.begin(),distances.end());
            leaf->setRadius(maxDist);
        }
    }
}

void graph::setNeighborhood(quadTree &tree)
{
    for(std::size_t i=0;i<tree.getListLeaf().size();i++)
    {
        std::vector<quadLeaf*> neighbors = getNeighbors(tree.getListLeaf().at(i));
        tree.getListLeaf().at(i)->setNeighbors(neighbors);
    }
}


void graph::addEdge(quadTree &tree,quadLeaf* pLeaf, int deltaLevel, std::bitset<QUAD_MAX_LEVEL> pDirection)
{
    int drift = 2*(abs(deltaLevel));

    std::bitset<QUAD_MAX_LEVEL> neighborCode;
    std::bitset<QUAD_MAX_LEVEL> tLeft;
    std::bitset<QUAD_MAX_LEVEL> tRight;
    if(deltaLevel<0)
    {
        tLeft = (pLeaf->getLocationCode()>>drift)<<drift;
        tRight = pDirection<<drift;
        neighborCode = tree.quadLocationAdd(tLeft,tRight);
        neighborCode = neighborCode>>(2*(abs(deltaLevel)));
    }else
    {
        neighborCode = tree.quadLocationAdd(pLeaf->getLocationCode(),pDirection);
    }

    if(tree.findByBitset(neighborCode,pLeaf->getLevel()-abs(deltaLevel))){

        quadLeaf* neighbor = tree.findByBitset(neighborCode,pLeaf->getLevel()-abs(deltaLevel));
        edgeVec.push_back(Edge(pLeaf,neighbor));
        if(deltaLevel!=0){
            edgeVec.push_back(Edge(neighbor,pLeaf));
        }
    }
}

void graph::print(quadTree &tree)
{
    for(std::size_t i=0;i<tree.getListLeaf().size();i++)
    {
        std::cout<<"neighbors of leaf "<<tree.getListLeaf().at(i)->getLocationCode()<<" lvl "<<tree.getListLeaf().at(i)->getLevel()<<std::endl;
        std::vector<quadLeaf*> neighbors = getNeighbors(tree.getListLeaf().at(i));

        for(std::size_t j=0;j<neighbors.size();j++)
        {
            std::cout<<"    "<<neighbors.at(j)->getLocationCode()<<" lvl "<<neighbors.at(j)->getLevel()<<std::endl;
        }
    }
}
