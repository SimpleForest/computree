/****************************************************************************
 Copyright (C) 2017 Jules Morel

 Contact : jules.morel@ifpindia.org

 Developers : Jules MOREL (IFP LSIS)

 This file is part of PluginIFPLSIS library.

 PluginIFPLSIS is free library: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 PluginIFPLSIS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl.html>.
*****************************************************************************/

#include "ifp_pluginentry.h"
#include "ifp_steppluginmanager.h"

IFP_PluginEntry::IFP_PluginEntry()
{
    _stepPluginManager = new IFP_StepPluginManager();
}

IFP_PluginEntry::~IFP_PluginEntry()
{
    delete _stepPluginManager;
}

QString IFP_PluginEntry::getVersion() const
{
    return "1.0";
}

CT_AbstractStepPlugin* IFP_PluginEntry::getPlugin() const
{
    return _stepPluginManager;
}

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    Q_EXPORT_PLUGIN2(plug_ifplsis, IFP_PluginEntry)
#endif
